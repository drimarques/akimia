<?php
class Rastreamento{
	private $id;
	private $numeroPedido;
	private $cliente;
	private $email = "contato@akimia.com.br";
	private $formaVenda;
	private $codRastreio;
	private $entregue = 0;
	private $historicoRastreamentos = array();
	private $dataCadastro;
	
	
	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
		return $this;
	}
	public function getNumeroPedido() {
		return $this->numeroPedido;
	}
	public function setNumeroPedido($numeroPedido) {
		$this->numeroPedido = $numeroPedido;
		return $this;
	}
	public function getCliente() {
		return $this->cliente;
	}
	public function setCliente($cliente) {
		$this->cliente = $cliente;
		return $this;
	}
	public function getEmail() {
		return $this->email;
	}
	public function setEmail($email) {
		$this->email = $email;
		return $this;
	}
	public function getCodRastreio() {
		return $this->codRastreio;
	}
	public function setCodRastreio($codRastreio) {
		$this->codRastreio = $codRastreio;
		return $this;
	}
	public function getFormaVenda() {
		return $this->formaVenda;
	}
	public function setFormaVenda(FormaVenda $formaVenda) {
		$this->formaVenda = $formaVenda;
		return $this;
	}
	public function getEntregue() {
		return $this->entregue;
	}
	public function setEntregue($entregue) {
		$this->entregue = $entregue;
		return $this;
	}
	public function getHistoricoRastreamentos() {
		return $this->historicoRastreamentos;
	}
	public function adicionaHistoricoRastreamentos(HistoricoRastreio $historicoRastreamentos) {
		array_push($this->historicoRastreamentos, $historicoRastreamentos);
		return $this;
	}
	public function getDataCadastro() {
		return $this->dataCadastro;
	}
	public function setDataCadastro($dataCadastro) {
		$this->dataCadastro = $dataCadastro;
		return $this;
	}
	
	
}