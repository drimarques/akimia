<?php
class ParametrosDeCalculo {		
	private $id;
	private $id_fechamento;
	private $cotacao_dolar;
	private $imposto_usa;
	private $iof;
	private $despesa_viagem;
	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
		return $this;
	}
	public function getIdFechamento() {
		return $this->id_fechamento;
	}
	public function setIdFechamento(Fechamento $fechamento) {
		$this->id_fechamento = $fechamento->getId();
		return $this;
	}
	public function getCotacaoDolar() {
		return $this->cotacao_dolar;
	}
	public function setCotacaoDolar($cotacao_dolar) {
		$this->cotacao_dolar = $cotacao_dolar;
		return $this;
	}
	public function getImpostoUsa() {
		return $this->imposto_usa;
	}
	public function setImpostoUsa($imposto_usa) {
		$this->imposto_usa = $imposto_usa;
		return $this;
	}
	public function getIof() {
		return $this->iof;
	}
	public function setIof($iof) {
		$this->iof = $iof;
		return $this;
	}
	public function getDespesaViagem() {
		return $this->despesa_viagem;
	}
	public function setDespesaViagem($despesa_viagem) {
		$this->despesa_viagem = $despesa_viagem;
		return $this;
	}
	
	
}