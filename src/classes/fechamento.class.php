<?php
class Fechamento {		
	private $id;
	private $nomeFechamento;
	private $dt_inicio;
	private $dt_termino;
	private $encerrado;
	
	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
		return $this;
	}
	public function getNomeFechamento() {
		return $this->nomeFechamento;
	}
	public function setNomeFechamento($nomeFechamento) {
		$this->nomeFechamento = $nomeFechamento;
		return $this;
	}
	public function getDtInicio() {
		return $this->dt_inicio;
	}
	public function setDtInicio($dt_inicio) {
		$this->dt_inicio = $dt_inicio;
		return $this;
	}
	public function getDtTermino() {
		return $this->dt_termino;
	}
	public function setDtTermino($dt_termino) {
		$this->dt_termino = $dt_termino;
		return $this;
	}
	public function getEncerrado() {
		return $this->encerrado;
	}
	public function setEncerrado($encerrado) {
		$this->encerrado = $encerrado;
		return $this;
	}
	
	
}