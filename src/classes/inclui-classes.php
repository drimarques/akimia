<?php
require_once (dirname(__FILE__)."/conexao.class.php");
require_once (dirname(__FILE__)."/usuario.class.php");
require_once (dirname(__FILE__)."/fechamento.class.php");
require_once (dirname(__FILE__)."/lista-de-compras.class.php");
require_once (dirname(__FILE__)."/parametros-de-calculo.class.php");
require_once (dirname(__FILE__)."/forma-venda.class.php");
require_once (dirname(__FILE__)."/rastreamento.class.php");
require_once (dirname(__FILE__)."/historico-rastreio.class.php");
require_once (dirname(__FILE__)."/envio-email.class.php");
require_once (dirname(__FILE__)."/dolar.class.php");