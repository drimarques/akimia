<?php
class HistoricoRastreio{
	private $id;
	private $rastreamento;
	private $data;
	private $localizacao;
	private $acao;
	private $detalhes;
	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
		return $this;
	}
	public function getData() {
		return $this->data;
	}
	public function setData($data) {
		$this->data = $data;
		return $this;
	}
	public function getLocalizacao() {
		return $this->localizacao;
	}
	public function setLocalizacao($localizacao) {
		$this->localizacao = $localizacao;
		return $this;
	}
	public function getAcao() {
		return $this->acao;
	}
	public function setAcao($acao) {
		$this->acao = $acao;
		return $this;
	}
	public function getDetalhes() {
		return $this->detalhes;
	}
	public function setDetalhes($detalhes) {
		$this->detalhes = $detalhes;
		return $this;
	}
	public function getRastreamento() {
		return $this->rastreamento;
	}
	public function setRastreamento(Rastreamento $rastreamento) {
		$this->rastreamento = $rastreamento;
		return $this;
	}
	
	
	
	
}