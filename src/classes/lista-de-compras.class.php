<?php
class ListaDeCompras {
		
	private $id;
	private $id_fechamento;
	private $parametrosDeCalculo;
	private $marca;
	private $sexo;
	private $tipo;
	private $produto;	
	private $modelo;
	private $cor;
	private $tamanho;
	private $quantidade_total;
	private $quantidade_comprada = 0;
	private $comprada = 0;
	private $iof = 1;
	private $custoDolar;
	private $custoBR;
	private $custoTotal;
	private $markup;
	private $venda;
	private $vendaTotal;
	private $lucro;
	private $lucro_total;
	private $cliente;
	private $telefone_cliente;
	private $email_cliente;
	
	public function setId($id) {
		$this->id = $id;
		return $this;
	}
	public function getIdFechamento() {
		return $this->id_fechamento;
	}
	public function setIdFechamento(Fechamento $fechamento) {
		$this->id_fechamento = $fechamento->getId();
		return $this;
	}
	public function getMarca() {
		return $this->marca;
	}
	public function setMarca($marca) {
		$this->marca = $marca;
		return $this;
	}
	public function getSexo() {
		return $this->sexo;
	}
	public function setSexo($sexo) {
		$this->sexo = $sexo;
		return $this;
	}
	public function getTipo() {
		return $this->tipo;
	}
	public function setTipo($tipo) {
		$this->tipo = $tipo;
		return $this;
	}
	public function getProduto() {
		return $this->produto;
	}
	public function setProduto($produto) {
		$this->produto = $produto;
		return $this;
	}
	public function getModelo() {
		return $this->modelo;
	}
	public function setModelo($modelo) {
		$this->modelo = $modelo;
		return $this;
	}
	public function getCor() {
		return $this->cor;
	}
	public function setCor($cor) {
		$this->cor = $cor;
		return $this;
	}
	public function getTamanho() {
		return $this->tamanho;
	}
	public function setTamanho($tamanho) {
		$this->tamanho = $tamanho;
		return $this;
	}
	public function getQuantidadeTotal() {
		return $this->quantidade_total;
	}
	public function setQuantidadeTotal($quantidade_total) {
		$this->quantidade_total = $quantidade_total;
		return $this;
	}
	public function getQuantidadeComprada() {
		return $this->quantidade_comprada;
	}
	public function setQuantidadeComprada($quantidade_comprada) {
		$this->quantidade_comprada = $quantidade_comprada;
		return $this;
	}
	public function getComprada() {
		return $this->comprada;
	}
	public function setComprada($comprada) {
		$this->comprada = $comprada;
		return $this;
	}
	public function getIof() {
		return $this->iof;
	}
	public function setIof($iof) {
		$this->iof = $iof;
		return $this;
	}
	public function getCustoDolar() {
		return $this->custoDolar;
	}
	public function setCustoDolar($custoDolar) {
		$this->custoDolar = $custoDolar;
		return $this;
	}
	public function getCustoBR() {
		return $this->custoBR;
	}
	public function setCustoBR($custoBR) {
		$this->custoBR = $custoBR;
		return $this;
	}
	public function getCustoTotal() {
		return $this->custoTotal;
	}
	public function setCustoTotal($custoTotal) {
		$this->custoTotal = $custoTotal;
		return $this;
	}
	public function getMarkup() {
		return $this->markup;
	}
	public function setMarkup($markup) {
		$this->markup = $markup;
		return $this;
	}
	public function getVenda() {
		return $this->venda;
	}
	public function setVenda($venda) {
		$this->venda = $venda;
		return $this;
	}
	public function getVendaTotal() {
		return $this->vendaTotal;
	}
	public function setVendaTotal($vendaTotal) {
		$this->vendaTotal = $vendaTotal;
		return $this;
	}
	public function getLucro() {
		return $this->lucro;
	}
	public function setLucro($lucro) {
		$this->lucro = $lucro;
		return $this;
	}
	public function getLucroTotal() {
		return $this->lucro_total;
	}
	public function setLucroTotal($lucro_total) {
		$this->lucro_total = $lucro_total;
		return $this;
	}
	public function getCliente() {
		return $this->cliente;
	}
	public function setCliente($cliente) {
		$this->cliente = $cliente;
		return $this;
	}
	public function getTelefoneCliente() {
		return $this->telefone_cliente;
	}
	public function setTelefoneCliente($telefone_cliente) {
		$this->telefone_cliente = $telefone_cliente;
		return $this;
	}
	public function getEmailCliente() {
		return $this->email_cliente;
	}
	public function setEmailCliente($email_cliente) {
		$this->email_cliente = $email_cliente;
		return $this;
	}
	public function getId() {
		return $this->id;
	}
	public function getParametrosDeCalculo() {
		return $this->parametrosDeCalculo;
	}
	public function setParametrosDeCalculo(ParametrosDeCalculo $parametros) {
		$this->parametrosDeCalculo = $parametros;
		return $this;
	}
		
	public function realizaCalculos(){
		if ($this->getIof() == "1") {
			$this->setCustoBR($this->getCustoDolar() * $this->getParametrosDeCalculo()->getCotacaoDolar() * (1 + ($this->getParametrosDeCalculo()->getImpostoUsa() / 100) + ($this->getParametrosDeCalculo()->getIof() / 100)));
			$this->setMarkup(($this->getVenda() / $this->getCustoBR()) * 100 - 100);
			$this->setLucro($this->getVenda() - $this->getCustoBR());
		} 
		else {
			$this->setCustoBR($this->getCustoDolar() * $this->getParametrosDeCalculo()->getCotacaoDolar() * (1 + ($this->getParametrosDeCalculo()->getImpostoUsa() / 100)));
			$this->setMarkup(($this->getVenda() / $this->getCustoBR()) * 100 - 100);
			$this->setLucro($this->getVenda() - $this->getCustoBR());
		}
		
		$this->setCustoTotal($this->getCustoBR() * $this->getQuantidadeTotal());
		$this->setVendaTotal($this->getVenda() * $this->getQuantidadeTotal());
		$this->setLucroTotal($this->getLucro() * $this->getQuantidadeTotal());
		
		return $this;
	}
	
}