<?php

require_once (dirname ( __FILE__ ) . "/../../resources/php-mailer/PHPMailerAutoload.php");

class EnvioEmail {
	
	//Pra essa função classe no hostgator	
	//se a aplicação que faz o envio esta no ambiente de hospedagem deve colocar "localhost" e usar a porta 26.
	//se a aplicação esta fora do ambiente de hospedagem deve usar mail.dominio e porta 587
	
	const charSet = "UTF-8";
	const host = "smtp.gmail.com";
	const porta = 587;
	const seguranca = "tls";
	
	const autenticacao = true;
	const usuario = "adrianomfpassos@gmail.com";
	const senha = "passos!dri09";
	
	private $destinatario = array();
	private $assunto;
	private $mensagem;
	private $from;
	private $copiaOculta;
	
	public function getDestinatario() {
		return $this->destinatario;
	}
	public function adicionaDestinatario($destinatario) {
		array_push($this->destinatario, $destinatario);
		return $this;
	}
	public function getAssunto() {
		return $this->assunto;
	}
	public function setAssunto($assunto) {
		$this->assunto = $assunto;
		return $this;
	}
	public function getMensagem() {
		return $this->mensagem;
	}
	public function setMensagem($mensagem) {
		$this->mensagem = utf8_decode($mensagem);
		return $this;
	}
	
	public function getFrom() {
		return $this->from;
	}
	public function setFrom($from) {
		$this->from = $from;
		return $this;
	}
	
	public function setCopiaOculta($copiaOculta) {
		$this->copiaOculta = $copiaOculta;
		return $this;
	}
		
	function enviaEmail() {
		$mail = new PHPMailer();
		$mail->charSet = self::charSet;
		$mail->isSMTP();
		$mail->isHTML(true);
		$mail->Host = self::host;
		$mail->Port = self::porta;
		
		$mail->SMTPSecure = self::seguranca;
		$mail->SMTPAuth = self::autenticacao;
		
		$mail->Username = self::usuario;
		$mail->Password = self::senha;
		
		$mail->setFrom(self::usuario, $this->from);		
		$mail->Subject = utf8_decode($this->assunto);				
		$mail->msgHTML($this->mensagem);
		
		$listaDestinatarios = null;
		for($i = 0; $i < count($this->getDestinatario()); $i++){
			$destinatarios = $this->getDestinatario();
			$mail->addAddress($destinatarios[$i]);
			$listaDestinatarios .= $destinatarios[$i];
		}
		
		if($this->copiaOculta != null){
			$mail->addBCC($this->copiaOculta);
		}
		
		// email em texto puro
		//$mail->AltBody = $this->mensagem;
		
		// anexar arquivos
		// $mail->addAttachment($path)
		
		if ($mail->send()) {
			echo "E-mail para {$listaDestinatarios} enviado com sucesso! <br>";
		} else {
			echo $mail->ErrorInfo;			
		}
	}
	
}