<?php
class CotacaoDolar {
	private $valorDolar; 
	
	function __construct() {

	}
	
	public function getValorDolar() {
		$conexao = new Conexao();
		$query = "SELECT * FROM dolar_calculadora WHERE id = 1";
		$retorno = mysqli_query($conexao->getConexao(), $query);
		$linha = mysqli_fetch_array($retorno);
		
		$valorReal = str_replace(".", ",", $linha["valorDolar"]);
		return $valorReal;
	}
	
	public function setValorDolar($valorDolar) {
		$this->valorDolar = $valorDolar;
		
		$conexao = new Conexao();
		$query = "UPDATE dolar_calculadora SET valorDolar = '{$this->valorDolar}' WHERE id = 1";
		mysqli_query($conexao->getConexao(), $query);
	}
	
	public function atualizaDolar() {
		try {
			ini_set("soap.wsdl_cache_enabled", "0");
		
			$WsSOAP = new SoapClient("https://www3.bcb.gov.br/sgspub/JSP/sgsgeral/FachadaWSSGS.wsdl");
			$dadosPesquisa = array("in0" => "1");
		
			$ResultadoPesquisaWS = $WsSOAP->getUltimoValorXML($dadosPesquisa);
		
			if (isset($ResultadoPesquisaWS)) {
				$CotacaoMoedaWS = simplexml_load_string($ResultadoPesquisaWS);
		
				$valorDolar = $CotacaoMoedaWS->SERIE->VALOR;
				$dia = $CotacaoMoedaWS->SERIE->DATA->DIA;
				$mes = $CotacaoMoedaWS->SERIE->DATA->MES;
				$ano = $CotacaoMoedaWS->SERIE->DATA->ANO;
				
				$valorDolar = str_replace(",", ".", $valorDolar);
				$valorCalculado = $this->aplicaPercentual($valorDolar, 1.06);
		
				$this->setValorDolar($valorCalculado);
			}  else {
				return null;
			}
		} catch (Exception $e) {
			return null;
		}
	}
	
	function aplicaPercentual($dolar, $percentual) {
		return $dolar * $percentual;
	}
}