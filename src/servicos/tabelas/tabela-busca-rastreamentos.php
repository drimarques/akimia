<?php
require_once '../../classes/inclui-classes.php';
require_once '../../daos/inclui-daos.php';

$busca = $_POST["busca"];
$entregue = $_POST["entregue"];

if($busca == ""){
	$query = "SELECT * FROM rastreamentos ORDER BY id desc"; 
}
else{
	$query = "SELECT r.*
				FROM rastreamentos r LEFT JOIN forma_venda f
				ON r.id_forma_venda = f.id
				WHERE (
					r.numero_pedido like '%{$busca}%'
					OR f.nome like '%{$busca}%'
					OR r.cliente like '%{$busca}%'
					OR r.email like '%{$busca}%'
					OR r.cod_rastreio like '%{$busca}%'
				)
				AND r.entregue = {$entregue} 
				ORDER BY id DESC";
}

$r = new RastreamentoDAO();
$r->getTabelaBuscaRastreamentos($query);