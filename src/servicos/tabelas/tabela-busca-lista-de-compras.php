<?php
require_once '../../classes/inclui-classes.php';
require_once '../../daos/inclui-daos.php';

$query = $_POST["query"];
$idFechamento = $_POST["idFechamento"];

$fechamento = new Fechamento();
$fechamento->setId($idFechamento);

$comprasDAO = new ListaDeComprasDAO();
echo $comprasDAO->getRelatorioListaCompras($fechamento, $query);