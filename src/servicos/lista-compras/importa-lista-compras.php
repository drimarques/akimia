<?php
require_once '../../classes/inclui-classes.php';
require_once '../../daos/inclui-daos.php';
require_once '../../../resources/excel-reader/reader.php';

date_default_timezone_set('Brazil/East');
date_default_timezone_set('America/Sao_Paulo');

//Se o arquivo não for vazio e for um arquivo excel
if(!empty($_FILES['arquivo-lista-compras']) && ($_FILES['arquivo-lista-compras']['type'] == "application/vnd.ms-excel" || $_FILES['arquivo-lista-compras']['type'] == "application/octet-stream")) {

	$data = new Spreadsheet_Excel_Reader();
	$data->setOutputEncoding('UTF-8');	
	$data->read($_FILES['arquivo-lista-compras']['tmp_name']);
	
	/*Varrendo produtos*/
	$compraDAO = new ListaDeComprasDAO();
	$qtdLinhas = $data->sheets[0]['numRows'];			
	for($i = 3; $i <= $qtdLinhas; $i ++) {
		$compra = new ListaDeCompras();
				
		$compra->setMarca(utf8_encode($data->sheets[0]['cells'][$i][5])); //Marca
		$compra->setSexo(utf8_encode($data->sheets[0]['cells'][$i][6])); //Sexo
		$compra->setTipo(utf8_encode($data->sheets[0]['cells'][$i][7])); //Tipo
		$compra->setProduto(utf8_encode($data->sheets[0]['cells'][$i][8])); //Produto
		$compra->setModelo(utf8_encode($data->sheets[0]['cells'][$i][9])); //Modelo
		$compra->setCor(utf8_encode($data->sheets[0]['cells'][$i][10])); //Cor
		$compra->setTamanho(utf8_encode($data->sheets[0]['cells'][$i][11])); //Tamanho
		$compra->setQuantidadeTotal((int)$data->sheets[0]['cells'][$i][12]); //Quantidade		
		$compra->setCliente(utf8_encode($data->sheets[0]['cells'][$i][15])); //Cliente
		$compra->setTelefoneCliente(utf8_encode($data->sheets[0]['cells'][$i][16])); //Telefone
		$compra->setEmailCliente(utf8_encode($data->sheets[0]['cells'][$i][17])); //E-mail

		$marca = $compra->getMarca();
						
		if(!empty($marca)){			
			$compraDAO->cadastraItemListaCompras($compra);			
		}		
	}	
}
else{
	echo "erro_arquivo_excel";
}