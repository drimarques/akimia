<?php

require_once '../../classes/inclui-classes.php';
require_once '../../daos/inclui-daos.php';

date_default_timezone_set('Brazil/East');
date_default_timezone_set('America/Sao_Paulo');
$data_inicio = date("Y-m-d H:i:s");

$nomeFechamento = $_POST["nomeFechamento"];

$fechamento = new Fechamento();
$fechamento->setNomeFechamento($nomeFechamento);
$fechamento->setDtInicio($data_inicio);

$fechamentoDAO = new FechamentoDAO();
$fechamento = $fechamentoDAO->gravaFechamento($fechamento);

$parametrosDeCalculo = new ParametrosDeCalculo();
$parametrosDeCalculo->setIdFechamento($fechamento);
$parametrosDeCalculoDAO = new ParametrosDeCalculoDAO();
$parametrosDeCalculo = $parametrosDeCalculoDAO->gravaParametrosDeCalculo($parametrosDeCalculo);

$listaDao = new ListaDeComprasDAO();
$listaDao->geraFechamentoPorListaCompras($fechamento);

echo $fechamento->getId();