<?php
require_once '../classes/inclui-classes.php';
require_once '../daos/inclui-daos.php';

$idParametroCalculo = $_POST["idParametroCalculo"];
$nomeCampo = $_POST["nomeParametro"];
$valor = $_POST["valor"];
$idFechamento = $_POST["idFechamento"];

switch ($nomeCampo) {
	case "cotacao-dolar":
		$campo = "cotacao_dolar";
	break;
	case "imposto-usa":
		$campo = "imposto_usa";
	break;
	case "iof-parametros-calculo":
		$campo = "iof";
	break;
	case "despesa-viagem":
		$campo = "despesa_viagem";
	break;
	default:
		;
	break;
}

$parametros = new ParametrosDeCalculo();
$parametros->setId($idParametroCalculo);

$parametrosDAO = new ParametrosDeCalculoDAO();
$parametrosDAO->updParametroCalculo($parametros, $campo, $valor);

$fechamento = new Fechamento();
$fechamento->setId($idFechamento);

$query = "SELECT * FROM lista_de_compras WHERE id_fechamento = '{$fechamento->getId()}' ORDER BY marca ASC";

$comprasDAO = new ListaDeComprasDAO();
$comprasDAO->updValoresListaCompras($fechamento, $query);

echo $comprasDAO->getRelatorioListaCompras($fechamento, $query);