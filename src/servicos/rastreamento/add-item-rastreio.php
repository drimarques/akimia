<?php
require_once '../../classes/inclui-classes.php';
require_once '../../daos/inclui-daos.php';

date_default_timezone_set('Brazil/East');
date_default_timezone_set('America/Sao_Paulo');
$dataCadastro = date("Y-m-d H:i:s");

$numeroPedido = $_POST["numeroPedido"];
$cliente = $_POST["cliente"];
$email = $_POST["email"];
$idFormaVenda = $_POST["formaVenda"];
$codRastreio = $_POST["codRastreio"];

$rastreio = new Rastreamento();
$rastreio->setNumeroPedido($numeroPedido);
$rastreio->setCliente($cliente);
$rastreio->setEmail($email);
$rastreio->setCodRastreio($codRastreio);
$rastreio->setDataCadastro($dataCadastro);

$idFormaVenda = ($idFormaVenda == "null") ? "NULL" : $idFormaVenda;
$formaVenda = new FormaVenda();
$formaVenda->setId($idFormaVenda);
$rastreio->setFormaVenda($formaVenda);

$rDAO = new RastreamentoDAO();
if($rDAO->gravaRastreio($rastreio)){
	echo "sucesso";
}
else{
	echo "erro";
}






