<?php

require_once '../../classes/inclui-classes.php';
require_once '../../daos/inclui-daos.php';

$idsURL = $_GET["idsRastreio"];
$array = explode(",", $idsURL);
$dao = new RastreamentoDAO();

for ($i = 0; $i < count($array); $i++) {
	$idRastreio = $array[$i];
	$dao->notificaRastreio($idRastreio);
}

?>
<script>
	setTimeout(function(){
		window.close();	
	}, 5000)
</script>