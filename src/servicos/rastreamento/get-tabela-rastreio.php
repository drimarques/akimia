<?php
require_once '../../classes/inclui-classes.php';
require_once '../../daos/inclui-daos.php';

$idRastreamento = $_POST["idRastreio"];
$dao = new RastreamentoDAO();
$array = $dao->buscaRastreamentos("id", $idRastreamento);
$rastreamento = $array[0];
$tabela = $dao->montaTabelaHistoricoRastreio($rastreamento);

echo $tabela;
