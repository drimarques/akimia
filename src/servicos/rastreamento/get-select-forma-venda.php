<?php
require_once '../../classes/inclui-classes.php';
require_once '../../daos/inclui-daos.php';

$r = new Rastreamento();
$f = new FormaVendaDAO();
$fVenda = new FormaVenda();
$r->setFormaVenda($fVenda);

$formasVenda = $f->getFormasVenda("ativas");
$options = $f->montaFormaVenda($r, $formasVenda);
$options .= "<option value='outro' class='abre-modal-forma-venda'>Outra (Cadastro)</option>";

echo $options;