<?php
require_once '../../classes/inclui-classes.php';
require_once '../../daos/inclui-daos.php';

$nome = $_POST["nome"];
$sigla = $_POST["sigla"];

$formaVenda = new FormaVenda();
$formaVenda->setNome($nome);
$formaVenda->setSigla($sigla);

$formaVendaDAO = new FormaVendaDAO();
$formaVenda = $formaVendaDAO->cadastraFormaVenda($formaVenda);

echo $formaVenda->getId();