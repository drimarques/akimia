<?php
require_once '../../classes/inclui-classes.php';
require_once '../../daos/inclui-daos.php';

$campo = $_POST["campo"];
$valor = $_POST["valor"];
$idRastreio = $_POST["idRastreio"];

$rastreio = new Rastreamento();
$rastreio->setId($idRastreio);

$rastreioDAO = new RastreamentoDAO();
$rastreioDAO->updateRastreio($campo, $valor, $rastreio);