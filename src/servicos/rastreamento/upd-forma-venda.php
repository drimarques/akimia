<?php
require_once '../../classes/inclui-classes.php';
require_once '../../daos/inclui-daos.php';

$idFormaVenda = $_POST["idFormaVenda"];
$f = new FormaVenda();
$f->setId($idFormaVenda);

$campo = $_POST["campo"];
$valor = $_POST["valor"];
$dao = new FormaVendaDAO();
$dao->updateFormaVenda($campo, $valor, $f);