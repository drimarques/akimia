<?php
require_once "../../classes/inclui-classes.php";
require_once "../../daos/inclui-daos.php";
require_once '../../../resources/excel-reader/reader.php';

date_default_timezone_set('Brazil/East');
date_default_timezone_set('America/Sao_Paulo');
$data_cadastro = date("Y-m-d H:i:s");

//Se o arquivo não for vazio e for um arquivo excel
if(!empty($_FILES['arquivo-rastreio']) &&($_FILES['arquivo-rastreio']['type'] == "application/vnd.ms-excel" || $_FILES['arquivo-rastreio']['type'] == "application/octet-stream")) {
	$data = new Spreadsheet_Excel_Reader();
	$data->setOutputEncoding('UTF-8');	
	$data->read($_FILES['arquivo-rastreio']['tmp_name']);
	
	$formaVendaDAO = new FormaVendaDAO();
	$arrayFormaVenda = $formaVendaDAO->getFormasVenda("todas");
	
	/*Varrendo rastreios*/
	$rastreamentoDAO = new RastreamentoDAO();
	$qtdLinhas = $data->sheets[0]['numRows'];			
	for($i = 3; $i <= $qtdLinhas; $i ++) {
		$rastreio = new Rastreamento();
		
		$rastreio->setCliente(utf8_encode($data->sheets[0]['cells'][$i][3]));
		$email = utf8_encode($data->sheets[0]['cells'][$i][4]);
		if($email != ""){
			$rastreio->setEmail($email);
		}
		$rastreio->setCodRastreio(utf8_encode($data->sheets[0]['cells'][$i][6]));
		$rastreio->setDataCadastro($data_cadastro);
		
		$formaVendaPLanilha = utf8_encode($data->sheets[0]['cells'][$i][5]);
		
		$flEncontrou = false;
		$formaVendaEncontrada = new FormaVenda();
		if($formaVendaPLanilha != ""){
			for ($j = 0; $j < count($arrayFormaVenda); $j++) {
				$forma = $arrayFormaVenda[$j];
				if($forma->getNome() == $formaVendaPLanilha){
					$formaVendaEncontrada = $forma;
					$flEncontrou = true;
				}
			}
		}
		else{
			$flEncontrou = true;
		}
		
// 		$erroFormaVenda = false;
// 		if($flEncontrou == false){
// 			$erroFormaVenda = true;
// 		}
		
// 		if($erroFormaVenda == true){
// 			//echo "erro_forma_venda,".$formaVendaPLanilha.",".$i."__";
// 		}
// 		else{
// 			if ($formaVendaPLanilha == "WEB" || $formaVendaPLanilha == "Mercado Livre"){
// 				$rastreio->setNumeroPedido(utf8_encode($data->sheets[0]['cells'][$i][2]));
// 			}
// 			else{
// 				$rastreio->setNumeroPedido($rastreamentoDAO->getUltimoIdRastreio());
// 			}
// 			$rastreio->setFormaVenda($formaVendaEncontrada);
// 			$rastreamentoDAO->gravaRastreio($rastreio);
// 		}
		
		if ($formaVendaPLanilha == "WEB" || $formaVendaPLanilha == "Mercado Livre"){
			$rastreio->setNumeroPedido(utf8_encode($data->sheets[0]['cells'][$i][2]));
		}
		else{
			$rastreio->setNumeroPedido($rastreamentoDAO->getUltimoIdRastreio());
		}
		
		$rastreio->setFormaVenda($formaVendaEncontrada);
		$rastreamentoDAO->gravaRastreio($rastreio);
	}
}
else{	
	echo "erro_arquivo_excel";
}