<?php
require_once '../classes/inclui-classes.php';
require_once '../daos/inclui-daos.php';
require_once '../../resources/excel-reader/reader.php';

date_default_timezone_set('Brazil/East');
date_default_timezone_set('America/Sao_Paulo');
$data_inicio = date("Y-m-d H:i:s");

$nome_fechamento = $_GET["nomeFechamento"];

$fechamento = new Fechamento();
$fechamento->setNomeFechamento($nome_fechamento);
$fechamento->setDtInicio($data_inicio);

$fechamentoDAO = new FechamentoDAO();
$fechamento = $fechamentoDAO->gravaFechamento($fechamento);

//Se o arquivo não for vazio e for um arquivo excel
if(!empty($_FILES['arquivo-excel']) &&($_FILES['arquivo-excel']['type'] == "application/vnd.ms-excel" || $_FILES['arquivo-excel']['type'] == "application/octet-stream")) {

	$data = new Spreadsheet_Excel_Reader();
	$data->setOutputEncoding('UTF-8');	
	$data->read($_FILES['arquivo-excel']['tmp_name']);
	
	$parametrosDeCalculo = new ParametrosDeCalculo();
	$parametrosDeCalculo->setIdFechamento($fechamento);
	$parametrosDeCalculo->setCotacaoDolar((double)$data->sheets[0]['cells'][3][3]);
	$parametrosDeCalculo->setImpostoUsa((double)$data->sheets[0]['cells'][4][3]);
	$parametrosDeCalculo->setIof((double)$data->sheets[0]['cells'][5][3]);
	$parametrosDeCalculo->setDespesaViagem((double)$data->sheets[0]['cells'][6][3]);
	
	$parametrosDeCalculoDAO = new ParametrosDeCalculoDAO();
	$parametrosDeCalculo = $parametrosDeCalculoDAO->gravaParametrosDeCalculo($parametrosDeCalculo);
	
 	$calcula = true;
 	$cotacaoDolar = $parametrosDeCalculo->getCotacaoDolar();
 	$impostoUsa = $parametrosDeCalculo->getImpostoUsa();
 	$iofParametros = $parametrosDeCalculo->getIof();
 	$despesaViagem = $parametrosDeCalculo->getDespesaViagem();
 	
	if(empty($cotacaoDolar) || empty($impostoUsa) || empty($iofParametros) || empty($despesaViagem)){
		//não terá calculos
		$calcula = false;
	}
	
	/*Varrendo produtos*/
	$compraDAO = new ListaDeComprasDAO();
	$qtdLinhas = $data->sheets[0]['numRows'];
	for($i = 3; $i <= $qtdLinhas; $i ++) {
		$compra = new ListaDeCompras();
		
		$compra->setIdFechamento($fechamento);
		$compra->setParametrosDeCalculo($parametrosDeCalculo);
		$compra->setMarca(utf8_encode($data->sheets[0]['cells'][$i][5])); //Marca
		$compra->setSexo(utf8_encode($data->sheets[0]['cells'][$i][6])); //Sexo
		$compra->setTipo(utf8_encode($data->sheets[0]['cells'][$i][7])); //Tipo
		$compra->setProduto(utf8_encode($data->sheets[0]['cells'][$i][8])); //Produto
		$compra->setModelo(utf8_encode($data->sheets[0]['cells'][$i][9])); //Modelo
		$compra->setCor(utf8_encode($data->sheets[0]['cells'][$i][10])); //Cor
		$compra->setTamanho(utf8_encode($data->sheets[0]['cells'][$i][11])); //Tamanho
		$compra->setQuantidadeTotal((int)$data->sheets[0]['cells'][$i][12]); //Quantidade
		$compra->setCustoDolar((double)$data->sheets[0]['cells'][$i][13]); //Custo US
		$compra->setVenda((double)$data->sheets[0]['cells'][$i][14]); //Venda RS
		$compra->setCliente(utf8_encode($data->sheets[0]['cells'][$i][15])); //Cliente
		$compra->setTelefoneCliente(utf8_encode($data->sheets[0]['cells'][$i][16])); //Telefone
		$compra->setEmailCliente(utf8_encode($data->sheets[0]['cells'][$i][17])); //E-mail

		$marca = $compra->getMarca();
		$custoDolar = $compra->getCustoDolar();
		$venda = $compra->getVenda();
		
		if(!empty($marca) && !empty($custoDolar) && !empty($venda) && $calcula){
			$compra->realizaCalculos();
			$compraDAO->gravaCompra($compra);
		}
		else if(!empty($marca)){
			$compraDAO->gravaCompra($compra);
		}
	}
	
	echo $fechamento->getId();
	
}
else{
	$fechamentoDAO->excluiFechamento($fechamento);
	echo "erro_arquivo_excel";
}