<?php
require_once '../classes/inclui-classes.php';
require_once '../daos/inclui-daos.php';

$id_fechamento = $_POST["idFechamento"];

$fechamentoDAO = new FechamentoDAO();
$fechamento = new Fechamento();

$fechamento->setId($id_fechamento);
$fechamentoDAO->excluirFechamento($fechamento);