<?php
require_once '../classes/inclui-classes.php';
require_once '../daos/inclui-daos.php';

$campo = $_POST["campo"];
$valor = $_POST["valor"];
$id_fechamento = $_POST["idFechamento"];

$fechamentoDAO = new FechamentoDAO();
$fechamento = new Fechamento();
$fechamento->setId($id_fechamento);

if($campo == "encerrado"){
	switch ($valor) {
		case "1":
			$data = date("Y-m-d H:i:s");
			$dataTabela = date_format(new DateTime($data), "d/m/Y H:i:s");
			$fechamentoDAO->updFechamento($fechamento, "data_termino", $data);
			break;
		case "0":
			$data = "";
			$dataTabela = "";
			$fechamentoDAO->updFechamento($fechamento, "data_termino", $data);
			break;
		default:
			;
			break;
	}
}

$fechamentoDAO->updFechamento($fechamento, $campo, $valor);
echo $dataTabela;