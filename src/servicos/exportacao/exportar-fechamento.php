<?php
require_once '../../../resources/php_excel/Classes/PHPExcel.php';
require_once '../../classes/inclui-classes.php';
require_once '../../daos/inclui-daos.php';

//Exclui o arquivo antes de criar um novo
/* $diretorio = dirname(__FILE__).'\exportacao.xls';
if(file_exists($diretorio)){
	unlink($diretorio);
} */

$id_fechamento = $_POST["idFechamento"];

$fechamento = new Fechamento();
$fechamento->setId($id_fechamento);

$fechamentoDAO = new FechamentoDAO();
echo $fechamentoDAO->exportarFechamento($fechamento);