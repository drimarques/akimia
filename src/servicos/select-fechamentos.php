<?php
require_once '../classes/inclui-classes.php';
require_once '../daos/inclui-daos.php';

$idFechamento = $_POST["idFechamento"];
$fechamento = new Fechamento();
$fechamento->setId($idFechamento);

$fechamentoDAO = new FechamentoDAO();
echo $fechamentoDAO->populaSelectFechamentos($fechamento);