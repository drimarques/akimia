<?php
require_once '../classes/inclui-classes.php';
require_once '../daos/inclui-daos.php';

$id_fechamento = $_POST["idFechamento"];

$fechamentoDAO = new FechamentoDAO();
$fechamento = new Fechamento();

$fechamento->setId($id_fechamento);
$fechamento = $fechamentoDAO->buscaInfFechamento($fechamento);

echo "{$fechamento->getNomeFechamento()}<br><span>Data de inicio: ".date('d/m/Y H:i:s', strtotime($fechamento->getDtInicio()))."</span><span id='id-fechamento-hidden'>{$fechamento->getId()}</span>";