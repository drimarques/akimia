<?php

error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);

require_once '../classes/inclui-classes.php';
require_once '../daos/inclui-daos.php';

$idProdutoAlterado = $_POST["idProdutoAlterado"];
$campo = $_POST["campo"];
$valor = $_POST["valor"];

$compra = new ListaDeCompras();
$compra->setId($idProdutoAlterado);

$comprasDAO = new ListaDeComprasDAO();
$comprasDAO->updCampoCompra($compra, $campo, $valor);

echo $comprasDAO->getProdutoLista($compra);

