<?php
require_once '../classes/inclui-classes.php';
require_once '../daos/inclui-daos.php';

$ehFechamento = $_POST["ehFechamento"];

$marca = $_POST["marca"];
$sexo = $_POST["sexo"];
$nomeProduto = $_POST["produto"];
$tipo = $_POST["tipo"];
$modelo = $_POST["modelo"];
$cor = $_POST["cor"];
$tamanho = $_POST["tamanho"];
$quantidade = $_POST["quantidade"];
$cliente = $_POST["cliente"];
$telefone = $_POST["telefone"];
$email = $_POST["email"];

$produto = new ListaDeCompras();
$produto->setMarca($marca);
$produto->setSexo($sexo);
$produto->setProduto($nomeProduto);
$produto->setTipo($tipo);
$produto->setModelo($modelo);
$produto->setCor($cor);
$produto->setTamanho($tamanho);
$produto->setQuantidadeTotal($quantidade);
$produto->setCliente($cliente);
$produto->setTelefoneCliente($telefone);
$produto->setEmailCliente($email);

$dao = new ListaDeComprasDAO();

if($ehFechamento == "true"){
	$idFechamento = $_POST["idFechamento"];
	
	$cotacao_dolar = $_POST["cotacao_dolar"];
	$imposto_usa = $_POST["imposto_usa"];
	$iof = $_POST["iof"];
	
	$custoEua = $_POST["custoEua"];
	$venda = $_POST["venda"];

	$fechamento = new Fechamento();
	$fechamento->setId($idFechamento);
	
	$parametros = new ParametrosDeCalculo();
	$parametros->setCotacaoDolar($cotacao_dolar);
	$parametros->setImpostoUsa($imposto_usa);
	$parametros->setIof($iof);
	
	$produto->setIdFechamento($fechamento);
	$produto->setParametrosDeCalculo($parametros);
	
	$produto->setCustoDolar($custoEua);
	$produto->setVenda($venda);
	
	$produto->realizaCalculos();
	$dao->gravaCompra($produto);
}
else if($ehFechamento == "false"){
	$dao->cadastraItemListaCompras($produto);
}






