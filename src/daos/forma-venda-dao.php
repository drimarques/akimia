<?php

class FormaVendaDAO{
	public function buscaFormaVenda($campo, $valor){
		$array = array();
	
		$conexao = new Conexao();
		$query = "SELECT * FROM forma_venda WHERE {$campo} = '{$valor}'";
		$retorno = mysqli_query($conexao->getConexao(), $query);
	
		while($linhas = mysqli_fetch_array($retorno)){
			$formaVenda = new FormaVenda();
			$formaVenda->setId($linhas["id"]);
			$formaVenda->setNome($linhas["nome"]);
			$formaVenda->setSigla($linhas["sigla"]);
			$formaVenda->setAtiva($linhas["ativa"]);
			
			array_push($array, $formaVenda);
		}
	
		$formasVenda = new ArrayObject($array);
		return $formasVenda;
	}
	
	public function getFormasVenda($ativas){
		$conexao = new Conexao();
		$query = ($ativas == "ativas") ? "SELECT * FROM forma_venda WHERE ativa = 1 ORDER BY nome ASC" : "SELECT * FROM forma_venda ORDER BY ativa DESC, nome ASC";
		$retorno = mysqli_query($conexao->getConexao(), $query);
	
		$array = array();
		while($linhas = mysqli_fetch_array($retorno)){
			$formaVenda = new FormaVenda();
			$formaVenda->setId($linhas["id"]);
			$formaVenda->setNome($linhas["nome"]);
			$formaVenda->setSigla($linhas["sigla"]);
			$formaVenda->setAtiva($linhas["ativa"]);
				
			array_push($array, $formaVenda);
		}
	
		$formasEnvio = new ArrayObject($array);
		return $formasEnvio;
	}
	
	public function montaFormaVenda(Rastreamento $rastreamento, $array_formasVenda){
		$optionsFormaVenda = "";
		#Senão tem forma de venda cadastrada
		#Inclui a linha "SELECIONE" e coloca todas as outras forma de venda
		if($rastreamento->getFormaVenda()->getId() == null){
			$optionsFormaVenda .= "<option selected='selected' value='null' disabled='disabled'>Selecione:</option>";
				
			foreach ($array_formasVenda as $linha => $formaVenda) {
				if($formaVenda->getAtiva() == "1"){
					$optionsFormaVenda .= "<option value='{$formaVenda->getId()}'>{$formaVenda->getNome()}</option>";
				}
			}
		}
		else{
			$optionsFormaVenda .= "<option value='null' disabled='disabled'>Selecione:</option>";
				
			foreach ($array_formasVenda as $linha => $formaVenda) {
				if($rastreamento->getFormaVenda()->getId() == $formaVenda->getId()){
					$optionsFormaVenda .= "<option selected='selected' value='{$formaVenda->getId()}'>{$formaVenda->getNome()}</option>";
				}
				else{
					if($formaVenda->getAtiva() == "1"){
						$optionsFormaVenda .= "<option value='{$formaVenda->getId()}'>{$formaVenda->getNome()}</option>";
					}
				}
			}
		}
	
		return $optionsFormaVenda;
	}
	
	public function getTabelaFormaVenda(){
		$array_formasVenda = $this->getFormasVenda("todas");
	
		$tabela = null;
		foreach ($array_formasVenda as $linha => $formaVenda) {
			if($formaVenda->getAtiva() == "1"){
				$ativaCheck = " checked='checked' ";
			}
			else{
				$ativaCheck = null;
			}
				
			$tabela .= "<tr id='{$formaVenda->getId()}'>
							<td>{$formaVenda->getNome()}</td>
							<td>{$formaVenda->getSigla()}</td>
							<td><input type='checkbox' class='check-ativa-forma-venda' $ativaCheck></td>
						</tr>";
		}
	
		return $tabela;
	}
	
	public function updateFormaVenda($campo, $valor, FormaVenda $formaVenda){
		try{
			$conn = new Conexao();
			$update = "UPDATE forma_venda SET $campo = '{$valor}' WHERE id = '{$formaVenda->getId()}'";
	
			if(mysqli_query($conn->getConexao(), $update)){
				return true;
			}
		}
		catch (Exception $ex){
			echo $ex->getMessage();
			return false;
		}
	
	}
	
	public function cadastraFormaVenda(FormaVenda $formaVenda){
		try{
			$conn = new Conexao();
			$insert = "INSERT INTO forma_venda (nome, sigla) VALUES ('{$formaVenda->getNome()}', '{$formaVenda->getSigla()}')";
	
			if(mysqli_query($conn->getConexao(), $insert)){
				$formaVenda->setId(mysqli_insert_id($conn->getConexao()));
				return $formaVenda;
			}
		}
		catch (Exception $ex){
			echo $ex->getMessage();
			return false;
		}
	}
}