<?php

date_default_timezone_set('America/Sao_Paulo');

class FechamentoDAO{
	public function gravaFechamento(Fechamento $f){
		try{
			$conn = new Conexao();
			$query_insert = "INSERT INTO fechamentos (nome_fechamento, data_inicio)
			VALUES (
			'{$f->getNomeFechamento()}',
			'{$f->getDtInicio()}'			
			)";
		
			if(mysqli_query($conn->getConexao(), $query_insert)){
				$f->setId(mysqli_insert_id($conn->getConexao()));
				return $f;
			}
		}
		catch (Exception $ex){
			echo $ex->getMessage();
			return false;
		}
	}
	
	public function excluirFechamento(Fechamento $f){
		try{
			$conn = new Conexao();
			$query = "DELETE FROM lista_de_compras WHERE id_fechamento = '{$f->getId()}'";
			mysqli_query($conn->getConexao(), $query);
			
			$query2 = "DELETE FROM parametros_de_calculo WHERE id_fechamento = '{$f->getId()}'";
			mysqli_query($conn->getConexao(), $query2);
			
			$query3 = "DELETE FROM fechamentos WHERE id_fechamento = '{$f->getId()}'";
			mysqli_query($conn->getConexao(), $query3);			
		}
		catch (Exception $ex){
			echo $ex->getMessage();
			return false;
		}
	}
	
	public function getTabelaResumo(Fechamento $f){
		try{
			$comprasDAO = new ListaDeComprasDAO();
			$query = "SELECT * FROM lista_de_compras WHERE id_fechamento = '{$f->getId()}' ORDER BY marca ASC";
			$lista_compras = $comprasDAO->getListaCompras($f, $query);
				
			$custoTotalEUA = 0;
			$custoTotalBR = 0;
			$vendaTotal = 0;
			$lucroTotal = 0;
			$lucroReal = 0;
			$despesaViagem = $lista_compras[0]->getParametrosDeCalculo()->getDespesaViagem();
			
			foreach ($lista_compras as $linha => $compra){
				$custoTotalEUA += $compra->getCustoDolar();
				$custoTotalBR += $compra->getCustoBR();
				$vendaTotal += $compra->getVendaTotal();
				$lucroTotal += $compra->getLucroTotal();					
			}
			
			$lucroReal = $lucroTotal - $despesaViagem;
	
			$tabela = "	<tr>
							<th>Custo total EUA</th>
							<td>$ ".number_format($custoTotalEUA, 2, '.', ',')."</td>
						</tr>
						<tr>
							<th>Custo total</th>
							<td>R$ ".number_format($custoTotalBR, 2, ',', '.')."</td>
						</tr>
						<tr>
							<th>Venda total</th>
							<td>R$ ".number_format($vendaTotal, 2, ',', '.')."</td>
						</tr>
						<tr>
							<th>Lucro real</th>
							<td>R$ ".number_format($lucroReal, 2, ',', '.')."</td>
						</tr>";
			
			return $tabela;
		}
		catch (Exception $ex){
			echo $ex->getMessage();
			return false;
		}
	}

	public function getValoresTabelaResumo(Fechamento $f){
		try{
			$comprasDAO = new ListaDeComprasDAO();
			$query = "SELECT * FROM lista_de_compras WHERE id_fechamento = '{$f->getId()}' ORDER BY marca ASC";
			$lista_compras = $comprasDAO->getListaCompras($f, $query);
	
			$custoTotalEUA = 0;
			$custoTotalBR = 0;
			$vendaTotal = 0;
			$lucroTotal = 0;
			$lucroReal = 0;
			$despesaViagem = $lista_compras[0]->getParametrosDeCalculo()->getDespesaViagem();
				
			foreach ($lista_compras as $linha => $compra){
				$custoTotalEUA += $compra->getCustoDolar();
				$custoTotalBR += $compra->getCustoBR();
				$vendaTotal += $compra->getVendaTotal();
				$lucroTotal += $compra->getLucroTotal();
			}
				
			$lucroReal = $lucroTotal - $despesaViagem;
				
			$valores_resumo = array();
			array_push($valores_resumo, $custoTotalEUA);
			array_push($valores_resumo, $custoTotalBR);
			array_push($valores_resumo, $vendaTotal);
			array_push($valores_resumo, $lucroReal);
			
			return $valores_resumo;
		}
		catch (Exception $ex){
			echo $ex->getMessage();
			return false;
		}
	}
	
	public function getTabelaParametrosCalculo(Fechamento $f){
		try{
			$parametros = new ParametrosDeCalculo();
			$parametrosDAO = new ParametrosDeCalculoDAO();
			$parametros = $parametrosDAO->getParametrosCalculo($f);
				
			$classCotacaoDolar = ($parametros->getCotacaoDolar() == "0.00") ? "class='bg-azul'" : "class='bg-verde'";
			$classImpostoUsa = ($parametros->getImpostoUsa() == "0.00") ? "class='bg-azul'" : "class='bg-verde'";
			$classIOFParametros = ($parametros->getIof() == "0.00") ? "class='bg-azul'" : "class='bg-verde'";
			$classDespesaViagem = ($parametros->getDespesaViagem() == "0.00") ? "class='bg-azul'" : "class='bg-verde'";
			
			$tabela = "	<tr class='{$parametros->getId()}idParametroCalculo'>
							<th>Cotação dólar</th>
							<td {$classCotacaoDolar}><input type='text' id='cotacao-dolar' class='campo-form real upd-parametro-calculo' value='".number_format($parametros->getCotacaoDolar(), 2, ',', '.')."'></td>
						</tr>
						<tr class='{$parametros->getId()}idParametroCalculo'>
							<th>Imposto USA</th>
							<td {$classImpostoUsa}><input type='text' id='imposto-usa' class='campo-form porcentagem upd-parametro-calculo' value='".number_format($parametros->getImpostoUsa(), 2, ',', '.')."'></td>
						</tr>
						<tr class='{$parametros->getId()}idParametroCalculo'>
							<th>IOF</th>
							<td {$classIOFParametros}><input type='text' id='iof-parametros-calculo' class='campo-form porcentagem upd-parametro-calculo' value='".number_format($parametros->getIof(), 2, ',', '.')."'></td>
						</tr> 
						<tr class='{$parametros->getId()}idParametroCalculo'>
							<th>Despesa de importação</th>
							<td {$classDespesaViagem}><input type='text' id='despesa-viagem' class='campo-form real upd-parametro-calculo' value='".number_format($parametros->getDespesaViagem(), 2, ',', '.')."'></td>
						</tr>";
				
			return $tabela;
		}
		catch (Exception $ex){
			echo $ex->getMessage();
			return false;
		}
	}
		
	public function getUltimoFechamento(){
		//Alterado para pegar o ultimo fechamento, independente de qual seja		
		/* $query = "SELECT * FROM fechamentos ORDER BY id_fechamento DESC";
		$lista_fechamentos = $this->getListaFechamentos($query);
				
		foreach ($lista_fechamentos as $linha => $fechamento) {
			if($fechamento->getEncerrado() == 0){
				$ultimofechamentoNaoEncerrado = $fechamento;
				break;
			}
		}
		
		if(!empty($ultimofechamentoNaoEncerrado)){
			echo $ultimofechamentoNaoEncerrado->getId();
		}
		else{
			echo "null"; 
		} */
		
		$query = "SELECT * FROM fechamentos ORDER BY id_fechamento DESC LIMIT 1";
		$lista_fechamentos = $this->getListaFechamentos($query);
		
		foreach ($lista_fechamentos as $linha => $fechamento) {
			$ultimoFechamento = $fechamento;
		}
				
		echo $ultimoFechamento->getId();			
	}
	
	public function getListaFechamentos($query){
		try{
			$conexao = new Conexao();			
			$retorno = mysqli_query($conexao->getConexao(), $query);
			
			$array_fechamentos = array();
			while($linhas = mysqli_fetch_array($retorno)){
				$fechamento = new Fechamento();
				$fechamento->setId($linhas["id_fechamento"]);
				$fechamento->setNomeFechamento($linhas["nome_fechamento"]);
				$fechamento->setDtInicio($linhas["data_inicio"]);
				$fechamento->setDtTermino($linhas["data_termino"]);
				$fechamento->setEncerrado($linhas["encerrado"]);
			
				array_push($array_fechamentos, $fechamento);
			}
				
			$fechamentos = new ArrayObject($array_fechamentos);
			return $fechamentos;
				
		}
		catch (Exception $ex){
			echo $ex->getMessage();
			return false;
		}				
	}
	
	public function buscaInfFechamento(Fechamento $f){
		try{
			$conexao = new Conexao();
			$query = "SELECT * FROM fechamentos WHERE id_fechamento = {$f->getId()}";
			$retorno = mysqli_query($conexao->getConexao(), $query);					
			$linha = mysqli_fetch_array($retorno);
			
			$f->setId($linha["id_fechamento"]);
			$f->setNomeFechamento($linha["nome_fechamento"]);
			$f->setDtInicio($linha["data_inicio"]);
			$f->setDtTermino($linha["data_termino"]);
			$f->setEncerrado($linha["encerrado"]);
	
			return $f;
	
		}
		catch (Exception $ex){
			echo $ex->getMessage();
			return false;
		}
	}
	
	public function populaSelectFechamentos(Fechamento $fechamentoAberto){
		$query = "SELECT * FROM fechamentos ORDER BY id_fechamento DESC";
		$array_fechamentos = $this->getListaFechamentos($query);			
		
		$option .= "<option value='null' selected='selected' disabled='disabled'>Abertos:</option>";
		foreach ($array_fechamentos as $linha => $fechamento) {			
			if($fechamento->getEncerrado() == "0"){
				$disabled = ($fechamento->getId() == $fechamentoAberto->getId()) ? " disabled='disabled' style='color: blue'" : null;			
				$option .= "<option value='{$fechamento->getId()}' $disabled>".$fechamento->getNomeFechamento()." - ".date_format(new DateTime($fechamento->getDtInicio()), "d/m/Y H:i")."</option>";
			}
		}
		
		$option .= "<option value='null' disabled='disabled'><hr></option>";
		
		$option .= "<option value='null' disabled='disabled'>Encerrados:</option>";
		foreach ($array_fechamentos as $linha => $fechamento) {			
			if($fechamento->getEncerrado() == "1"){
				$disabled = ($fechamento->getId() == $fechamentoAberto->getId()) ? " disabled='disabled' style='color: blue'" : null;
				$option .= "<option value='{$fechamento->getId()}' $disabled>".$fechamento->getNomeFechamento()." - ".date_format(new DateTime($fechamento->getDtTermino()), "d/m/Y H:i")."</option>";
			}
		}
		
		return $option;		
	}
	
	public function populaSelectFechamentosListaCompras(){
		$query = "SELECT * FROM fechamentos ORDER BY id_fechamento DESC";
		$array_fechamentos = $this->getListaFechamentos($query);			
		
		$option .= "<option value='null' selected='selected' disabled='disabled'>Abertos:</option>";
		foreach ($array_fechamentos as $linha => $fechamento) {			
			if($fechamento->getEncerrado() == "0"){						
				$option .= "<option value='{$fechamento->getId()}'>".$fechamento->getNomeFechamento()." - ".date_format(new DateTime($fechamento->getDtInicio()), "d/m/Y H:i")."</option>";
			}
		}
		
		$option .= "<option value='null' disabled='disabled'><hr></option>";
		
		$option .= "<option value='null' disabled='disabled'>Encerrados:</option>";
		foreach ($array_fechamentos as $linha => $fechamento) {			
			if($fechamento->getEncerrado() == "1"){				
				$option .= "<option value='{$fechamento->getId()}'>".$fechamento->getNomeFechamento()." - ".date_format(new DateTime($fechamento->getDtTermino()), "d/m/Y H:i")."</option>";
			}
		}
		
		return $option;
	}
	
	public function getTabelaExportarFechamento($query){
		$fechamentos = $this->getListaFechamentos($query);
		$tabela = $this->montaLayoutTabela($fechamentos);
		echo $tabela;
	}
	
	public function montaLayoutTabela($fechamentos){
		$tabela = null;
		foreach ($fechamentos as $linha => $fechamento) {
			$dt_termino = ($fechamento->getDtTermino() != "0000-00-00 00:00:00") ? date_format(new DateTime($fechamento->getDtTermino()), "d/m/Y H:i:s") : null;
			$encerrado = ($fechamento->getEncerrado() == "0") ? "fechamento-aberto'>ABERTO" : "fechamento-encerrado'>ENCERRADO";
		
			$tabela .= "<tr class='seleciona-fechamento-export' id='{$fechamento->getId()}'>
							<td><span class='edit-nome-fechamento'>{$fechamento->getNomeFechamento()}</span></td>
							<td>".date_format(new DateTime($fechamento->getDtInicio()), "d/m/Y H:i:s")."</td>
							<td class='data-termino-fechamento{$fechamento->getId()}'>{$dt_termino}</td>
							<td><span class='encerra-fechamento hover {$encerrado}</span></td>
						</tr>";
		}
		
		return $tabela;
	}
	
	public function getTabelaHistoricoFechamento($query){
		$fechamentos = $this->getListaFechamentos($query);
		$tabela = $this->montaLayoutTabelaHistoricoFechamentos($fechamentos);
		echo $tabela;
	}
	
	public function montaLayoutTabelaHistoricoFechamentos($fechamentos){
		$tabela = null;
		foreach ($fechamentos as $linha => $fechamento) {
			$dt_termino = ($fechamento->getDtTermino() != "0000-00-00 00:00:00") ? date_format(new DateTime($fechamento->getDtTermino()), "d/m/Y H:i:s") : null;
			$encerrado = ($fechamento->getEncerrado() == "0") ? "fechamento-aberto'>ABERTO" : "fechamento-encerrado'>ENCERRADO";
	
			$tabela .= "<tr class='seleciona-fechamento-historico' id='{$fechamento->getId()}'>
							<td><span class='edit-nome-fechamento'>{$fechamento->getNomeFechamento()}</span></td>
							<td>".date_format(new DateTime($fechamento->getDtInicio()), "d/m/Y H:i:s")."</td>
							<td class='data-termino-fechamento{$fechamento->getId()}'>{$dt_termino}</td>
							<td><span class='encerra-fechamento hover {$encerrado}</span></td>
							<td><img src='../img/edit.png' alt='Editar fechamento' class='hover edita-fechamento'></td>
							<td><img src='../img/trash.png' alt='Excluir fechamento' class='hover excluir-fechamento'></td>
						</tr>";
		}
	
		return $tabela;
	}

	public function exportarFechamento(Fechamento $fechamento){		
		$fechamento = $this->buscaInfFechamento($fechamento);
		
		//$data_hora = date("d-m-Y H-i-s");
		
		$objPHPExcel = new PHPExcel();
		
		//Cores
		$fundo_branco = array('fill' =>
			 array(
			 		'type' => PHPExcel_Style_Fill::FILL_SOLID,
			 		'color' => array('rgb' => 'ffffff')
			 ),
		);
		
		$fundo_azul = array('fill' =>
			 array(
			 		'type' => PHPExcel_Style_Fill::FILL_SOLID,
			 		'color' => array('rgb' => '5478b2')
			 ),
		);
		
		$fundo_rosa = array('fill' =>
			 array(
			 		'type' => PHPExcel_Style_Fill::FILL_SOLID,
			 		'color' => array('rgb' => 'e31b6e')
			 ),
		);
		
		$fonte_branca = array(
			 'font' => array(
			 		'color' => array('rgb' => 'ffffff')
			 ));
		
		$borda_preta = array(
			 'borders' => array(
			 		'allborders' => array(
			 				'style' => PHPExcel_Style_Border::BORDER_THIN
			 		)
			 )
		);
		
		//Deixando fundo brancão da planilha toda
		$objPHPExcel->getDefaultStyle()->applyFromArray($fundo_branco);
		
		//Cores de fundo
		$objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($fundo_azul);
		$objPHPExcel->getActiveSheet()->getStyle('B8')->applyFromArray($fundo_azul);
		$objPHPExcel->getActiveSheet()->getStyle('E2:X2')->applyFromArray($fundo_rosa);
		$objPHPExcel->getActiveSheet()->getStyle('B3')->applyFromArray($fundo_rosa);
		$objPHPExcel->getActiveSheet()->getStyle('B4')->applyFromArray($fundo_rosa);
		$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray($fundo_rosa);
		$objPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray($fundo_rosa);
		$objPHPExcel->getActiveSheet()->getStyle('B9')->applyFromArray($fundo_rosa);
		$objPHPExcel->getActiveSheet()->getStyle('B10')->applyFromArray($fundo_rosa);
		$objPHPExcel->getActiveSheet()->getStyle('B11')->applyFromArray($fundo_rosa);
		$objPHPExcel->getActiveSheet()->getStyle('B12')->applyFromArray($fundo_rosa);
		
		//Cores da fonte
		$objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($fonte_branca);
		$objPHPExcel->getActiveSheet()->getStyle('B8')->applyFromArray($fonte_branca);
		$objPHPExcel->getActiveSheet()->getStyle('E2:X2')->applyFromArray($fonte_branca);
		$objPHPExcel->getActiveSheet()->getStyle('B3')->applyFromArray($fonte_branca);
		$objPHPExcel->getActiveSheet()->getStyle('B4')->applyFromArray($fonte_branca);
		$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray($fonte_branca);
		$objPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray($fonte_branca);
		$objPHPExcel->getActiveSheet()->getStyle('B9')->applyFromArray($fonte_branca);
		$objPHPExcel->getActiveSheet()->getStyle('B10')->applyFromArray($fonte_branca);
		$objPHPExcel->getActiveSheet()->getStyle('B11')->applyFromArray($fonte_branca);
		$objPHPExcel->getActiveSheet()->getStyle('B12')->applyFromArray($fonte_branca);
		
		// Definimos o estilo da fonte negrito
		$objPHPExcel->getActiveSheet()->getStyle('B2')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B4')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B5')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B6')->getFont()->setBold(true);
		
		$objPHPExcel->getActiveSheet()->getStyle('B8')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B9')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B10')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B11')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B12')->getFont()->setBold(true);
		
		$objPHPExcel->getActiveSheet()->getStyle('E2:X2')->getFont()->setBold(true);
		
		//Borda
		$objPHPExcel->getActiveSheet()->getStyle('B2:C2')->applyFromArray($borda_preta);
		$objPHPExcel->getActiveSheet()->getStyle('B3:C3')->applyFromArray($borda_preta);
		$objPHPExcel->getActiveSheet()->getStyle('B4:C4')->applyFromArray($borda_preta);
		$objPHPExcel->getActiveSheet()->getStyle('B5:C5')->applyFromArray($borda_preta);
		$objPHPExcel->getActiveSheet()->getStyle('B6:C6')->applyFromArray($borda_preta);
		
		$objPHPExcel->getActiveSheet()->getStyle('B8:C8')->applyFromArray($borda_preta);
		$objPHPExcel->getActiveSheet()->getStyle('B9:C9')->applyFromArray($borda_preta);
		$objPHPExcel->getActiveSheet()->getStyle('B10:C10')->applyFromArray($borda_preta);
		$objPHPExcel->getActiveSheet()->getStyle('B11:C11')->applyFromArray($borda_preta);
		$objPHPExcel->getActiveSheet()->getStyle('B12:C12')->applyFromArray($borda_preta);
		
		$objPHPExcel->getActiveSheet()->getStyle('E2:X2')->applyFromArray($borda_preta);
		
		// Criamos as colunas
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('B2', "PARÂMETROS DE CÁLCULO")
		->setCellValue("B3", "Cotação Dolar")
		->setCellValue("B4", "Imposto USA")
		->setCellValue('B5', "IOF")
		->setCellValue("B6", "Despesa com Importação")
		->setCellValue('B8', "RESUMO")
		->setCellValue('B9', "Custo Total EUA (USD)")
		->setCellValue("B10","Custo Total (R$)")
		->setCellValue("B11", "Venda Total (R$)")
		->setCellValue("B12",  "Lucro Real (R$)")
		->setCellValue("E2", "Marca")
		->setCellValue("F2", "Sexo")
		->setCellValue("G2", "Tipo")
		->setCellValue("H2", "Produto")
		->setCellValue("I2", "Modelo")
		->setCellValue("J2", "Cor")
		->setCellValue("K2", "Tamanho")
		->setCellValue("L2", "Quantidade")
		->setCellValue("M2", "IOF")
		->setCellValue("N2", "Custo (U$)")
		->setCellValue("O2", "Custo (R$)")
		->setCellValue("P2", "Custo Total (R$)")
		->setCellValue("Q2", "Markup %")
		->setCellValue("R2", "Venda (R$)")
		->setCellValue("S2", "Venda Total (R$)")
		->setCellValue("T2", "Lucro (R$)")
		->setCellValue("U2", "Lucro Total (R$)")
		->setCellValue("V2", "Cliente")
		->setCellValue("W2", "Telefone")
		->setCellValue("X2", "E-mail");
		
		$mesclar_centralizar = array(
			 'alignment' => array(
			 		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			 )
		);
		 
		$objPHPExcel->getActiveSheet()->mergeCells("B2:C2");
		$objPHPExcel->getActiveSheet()->mergeCells("B8:C8");
		$objPHPExcel->getActiveSheet()->getStyle("B2:C2")->applyFromArray($mesclar_centralizar);
		$objPHPExcel->getActiveSheet()->getStyle("B8:C8")->applyFromArray($mesclar_centralizar);
		
		// Podemos configurar diferentes larguras paras as colunas como padrão
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(0.7);
		$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(5);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(0.7);
		
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(23);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(11);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(26);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(11);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(19);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(57);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(16);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(16);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(16);
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(16);
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(5);
		$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(16);
		$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(16);
		$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(16);
		$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(16);
		$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(16);
		$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(16);
		$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(16);
		$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(16);
		$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(30);
		 
		$formato_moeda = array(
			 'code' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
		);
		 
		//Resumo formato de moeda
		$objPHPExcel->getActiveSheet()->getStyle('C9')->getNumberFormat()->applyFromArray($formato_moeda);
		$objPHPExcel->getActiveSheet()->getStyle('C10')->getNumberFormat()->applyFromArray($formato_moeda);
		$objPHPExcel->getActiveSheet()->getStyle('C11')->getNumberFormat()->applyFromArray($formato_moeda);
		$objPHPExcel->getActiveSheet()->getStyle('C12')->getNumberFormat()->applyFromArray($formato_moeda);

		$conexao = new Conexao();
 		$conn = $conexao->getConexao();
		
		 //Parametros de calculo
		$parametrosDAO = new ParametrosDeCalculoDAO();
		$parametro = $parametrosDAO->getParametrosCalculo($fechamento);
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 3, $parametro->getCotacaoDolar());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 4, $parametro->getImpostoUsa());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 5, $parametro->getIof());
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 6, $parametro->getDespesaViagem());
		
		$valores_resumo = $this->getValoresTabelaResumo($fechamento);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 9, $valores_resumo[0]); //CustoTotalEUA
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 10, $valores_resumo[1]); //CustoTotalBR
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 11, $valores_resumo[2]); //VendaTotal
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 12, $valores_resumo[3]); //LucroReal
		
		$listaDAO = new ListaDeComprasDAO();
		$lista_de_compras = $listaDAO->getListaCompras($fechamento, "SELECT * FROM lista_de_compras WHERE id_fechamento = '{$fechamento->getId()}' ORDER BY marca ASC");
		
		$contador = 3;
		foreach ($lista_de_compras as $linha => $compra) {					
			$iof = ($compra->getIof() == "1") ? "Sim" : "Não";
			
			//Borda
			$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(4,  $contador)->applyFromArray($borda_preta);
			$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(5,  $contador)->applyFromArray($borda_preta);
			$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(6,  $contador)->applyFromArray($borda_preta);
			$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(7,  $contador)->applyFromArray($borda_preta);
			$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(8,  $contador)->applyFromArray($borda_preta);
			$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(9,  $contador)->applyFromArray($borda_preta);
			$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(10, $contador)->applyFromArray($borda_preta);
			$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(11, $contador)->applyFromArray($borda_preta);
			$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(12, $contador)->applyFromArray($borda_preta);
			$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(13, $contador)->applyFromArray($borda_preta);
			$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(14, $contador)->applyFromArray($borda_preta);
			$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(15, $contador)->applyFromArray($borda_preta);
			$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(16, $contador)->applyFromArray($borda_preta);
			$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(17, $contador)->applyFromArray($borda_preta);
			$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(18, $contador)->applyFromArray($borda_preta);
			$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(19, $contador)->applyFromArray($borda_preta);
			$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(20, $contador)->applyFromArray($borda_preta);
			$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(21, $contador)->applyFromArray($borda_preta);
			$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(22, $contador)->applyFromArray($borda_preta);
			$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(23, $contador)->applyFromArray($borda_preta);
			
			//Formato dos campos
			$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(14, $contador)->getNumberFormat()->applyFromArray($formato_moeda);
			$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(15, $contador)->getNumberFormat()->applyFromArray($formato_moeda);
			$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(18, $contador)->getNumberFormat()->applyFromArray($formato_moeda);
			$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(19, $contador)->getNumberFormat()->applyFromArray($formato_moeda);
			$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(20, $contador)->getNumberFormat()->applyFromArray($formato_moeda);
			
			// Também podemos escolher a posição exata aonde o dado será inserido (coluna, linha, dado);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4,  $contador, $compra->getMarca());
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5,  $contador, $compra->getSexo());
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6,  $contador, $compra->getTipo());
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7,  $contador, $compra->getProduto());
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8,  $contador, $compra->getModelo());
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9,  $contador, $compra->getCor());
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $contador, $compra->getTamanho());
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $contador, $compra->getQuantidadeTotal());
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $contador, $iof);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $contador, $compra->getCustoDolar());
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $contador, $compra->getCustoBR());
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, $contador, $compra->getCustoTotal());
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, $contador, $compra->getMarkup());
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(17, $contador, $compra->getVenda());
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(18, $contador, $compra->getVendaTotal());
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(19, $contador, $compra->getLucro());
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(20, $contador, $compra->getLucroTotal());
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(21, $contador, $compra->getCliente());
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(22, $contador, $compra->getTelefoneCliente());
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(23, $contador, $compra->getEmailCliente());
			
			$contador++;
		}
		
		// Podemos renomear o nome das planilha atual, lembrando que um único arquivo pode ter várias planilhas
		$objPHPExcel->getActiveSheet()->setTitle("Fechamento - Akimia");
		
		// Cabeçalho do arquivo quando for download automático
		//header('Content-Type: application/vnd.ms-excel');
		//header('Content-Disposition: attachment;filename="ExportacaoFechamento_Data.xls"');
		//header('Cache-Control: max-age=0');
		//header('Cache-Control: max-age=1'); //Se for o IE9, isso talvez seja necessário
		
		// Acessamos o 'Writer' para poder salvar o arquivo
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		
		//Saída de download
		//$objWriter->save('php://output');
		//exit;
		 
		//Gravando o arquivo
		$nome_arquivo = "Exportacao_{$fechamento->getNomeFechamento()}.xls";
		$objWriter->save($nome_arquivo);
		
		return $nome_arquivo;
	}
	
	public function updFechamento(Fechamento $fechamento, $campo, $valor){
		try{
			$conn = new Conexao();
			$query = "UPDATE fechamentos SET {$campo} = '{$valor}' WHERE id_fechamento = '{$fechamento->getId()}'";
			
			if(mysqli_query($conn->getConexao(), $query)){
				return $f;
			}
		}
		catch (Exception $ex){
			echo $ex->getMessage();
			return false;
		}
	}
	
	public function getGraficoResumo($fechamentos){
		$in .= "in (";
		for ($i = 0; $i < count($fechamentos); $i++){
			$in .= $fechamentos[$i].",";
		}		
		$in = substr($in, 0, strlen($in) - 1);		
		$in .= ") ORDER BY id_fechamento ASC";
		
		$query = "SELECT * FROM fechamentos WHERE id_fechamento {$in}";		
		$fechamentos = $this->getListaFechamentos($query);		
		
		$script = "<script>
		function mostraGrafico(){
			$('#grafico').highcharts({
		        chart: {
		            type: 'column'
		        },
				style: {
            		fontFamily: '\"Open Sans\"',
					fontSize: '12px'
        		},				
		        title: {
		            text: 'Me dê um título'
		        },
		        subtitle: {
		            text: 'Me dê um subtítulo'
		        },
		        xAxis: {
		            categories: [";
		                
		for ($i = 0; $i < count($fechamentos); $i++){
			$script .= "'".$fechamentos[$i]->getNomeFechamento()."',";
		}
		$script = substr($script, 0, strlen($script) - 1);		                		               
		$script .= "],
		            crosshair: true
		        },
		        yAxis: {
		            min: 0,
		            title: {
		                text: 'R$'
		            }
		        },
		        tooltip: {
		            headerFormat: '<span style=\"font-size:13px\">{point.key}</span><table>',
		            pointFormat: '<tr><td style=\"color:{series.color};padding:0; font-weight: bold\">{series.name}: </td>' +
		                '<td style=\"padding:0\"><b> {point.y:.2f}</b></td></tr>',
		            footerFormat: '</table>',
		            shared: true,
		            useHTML: true
		        },
		        plotOptions: {
		            column: {
		                pointPadding: 0.2,
		                borderWidth: 0
		            }
		        },
		        series: [{";
		
		$custoTotalEUAGrafico = "data: [";
		$custoTotalBRGrafico = "data: [";
		$vendaTotalGrafico = "data: [";
		$lucroRealGrafico = "data: [";
		
		for ($i = 0; $i < count($fechamentos); $i++){
			$comprasDAO = new ListaDeComprasDAO();
			$query = "SELECT * FROM lista_de_compras WHERE id_fechamento = '{$fechamentos[$i]->getId()}' ORDER BY marca ASC";
			$lista_compras = $comprasDAO->getListaCompras($fechamentos[$i], $query);
				
			$custoTotalEUA = 0;
			$custoTotalBR = 0;
			$vendaTotal = 0;
			$lucroTotal = 0;
			$lucroReal = 0;
			$despesaViagem = $lista_compras[0]->getParametrosDeCalculo()->getDespesaViagem();
		
			foreach ($lista_compras as $linha => $compra){
				$custoTotalEUA += $compra->getCustoDolar();
				$custoTotalBR += $compra->getCustoBR();
				$vendaTotal += $compra->getVendaTotal();
				$lucroTotal += $compra->getLucroTotal();
			}
		
			$lucroReal = $lucroTotal - $despesaViagem;
						
			$custoTotalEUAGrafico .= $custoTotalEUA.",";
			$custoTotalBRGrafico .= $custoTotalBR.",";
			$vendaTotalGrafico .= $vendaTotal.",";
			$lucroRealGrafico .= $lucroReal.",";
		}
		
		$custoTotalEUAGrafico = substr($custoTotalEUAGrafico, 0, strlen($custoTotalEUAGrafico) - 1);
		$custoTotalEUAGrafico .= "]";
		
		$custoTotalBRGrafico = substr($custoTotalBRGrafico, 0, strlen($custoTotalBRGrafico) - 1);
		$custoTotalBRGrafico .= "]";
		
		$vendaTotalGrafico = substr($vendaTotalGrafico, 0, strlen($vendaTotalGrafico) - 1);
		$vendaTotalGrafico .= "]";
		
		$lucroRealGrafico = substr($lucroRealGrafico, 0, strlen($lucroRealGrafico) - 1);
		$lucroRealGrafico .= "]";

		$script .= "name: 'Custo total EUA',
		            {$custoTotalEUAGrafico}
		        	}, {
		            name: 'Custo total',
		            {$custoTotalBRGrafico}	
		        	}, {
		            name: 'Venda total',
		            {$vendaTotalGrafico}
		        	}, {
		            name: 'Lucro real',
		            {$lucroRealGrafico}		
		        }]
	    	})
	    }
	    </script>";
		            
		return $script;
	}
}
