<?php

class ParametrosDeCalculoDAO{
	public function gravaParametrosDeCalculo(ParametrosDeCalculo $p){
		try{			
			$conn = new Conexao();
			$query_insert = "INSERT INTO parametros_de_calculo (id_fechamento, cotacao_dolar, imposto_usa, iof, despesa_viagem)
			VALUES (
				'{$p->getIdFechamento()}',
				'{$p->getCotacaoDolar()}',
				'{$p->getImpostoUsa()}',
				'{$p->getIof()}',
				'{$p->getDespesaViagem()}'
			)";
		
			if(mysqli_query($conn->getConexao(), $query_insert)){
				$p->setId(mysqli_insert_id($conn->getConexao()));
				return $p;
			}
		}
		catch (Exception $ex){
			echo $ex->getMessage();
			return false;
		}
	}

	public function getParametrosCalculo(Fechamento $f){
		try{
			$conexao = new Conexao();
			$query = "SELECT * FROM parametros_de_calculo WHERE id_fechamento = '{$f->getId()}'";
			$retorno = mysqli_query($conexao->getConexao(), $query);						
			$linhas = mysqli_fetch_array($retorno);
			
			$parametros = new ParametrosDeCalculo();
			$parametros->setId($linhas["id_parametro_calculo"]);
			$parametros->setIdFechamento($f);
			$parametros->setCotacaoDolar($linhas["cotacao_dolar"]);
			$parametros->setImpostoUsa($linhas["imposto_usa"]);
			$parametros->setIof($linhas["iof"]);
			$parametros->setDespesaViagem($linhas["despesa_viagem"]);
			
			return $parametros;
		
		}
		catch (Exception $ex){
			echo $ex->getMessage();
			return false;
		}
	}
	
	public function updParametroCalculo(ParametrosDeCalculo $p, $campo, $valor){
		try{
			$conexao = new Conexao();
			$query = "UPDATE parametros_de_calculo SET {$campo} = '{$valor}' WHERE id_parametro_calculo = {$p->getId()}";
		
			if(mysqli_query($conexao->getConexao(), $query)){				
				return true;
			}
	
		}
		catch (Exception $ex){
			echo $ex->getMessage();
			return false;
		}
	}
}