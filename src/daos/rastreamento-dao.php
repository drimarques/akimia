<?php

class RastreamentoDAO{
	public function gravaRastreio(Rastreamento $r){		
		try{
			$formaVenda = $r->getFormaVenda();
			if($formaVenda->getId() == "" || $formaVenda->getId() == NULL) {
				$idFormaVenda = "NULL";
			}
			else{
				$idFormaVenda = $formaVenda->getId();
			}
			
			$conn = new Conexao();
			$query_insert = "INSERT INTO rastreamentos (
								numero_pedido,
								cliente,
								email,
								id_forma_venda,
								cod_rastreio,
								entregue,
								data_cadastro
							)
							VALUES (
								'{$r->getNumeroPedido()}',
								'{$r->getCliente()}',
								'{$r->getEmail()}',
								 {$idFormaVenda},
								'{$r->getCodRastreio()}',
								'{$r->getEntregue()}',
								'{$r->getDataCadastro()}'
							)";
			
			if(mysqli_query($conn->getConexao(), $query_insert)){
				//$r->setId(mysqli_insert_id($conn->getConexao()));
				return true;
			}
		}
		catch (Exception $ex){
			echo $ex->getMessage();
			return false;
		}
	}
	
	public function buscaRastreamentos($campo, $valor){
		$array = array();
	
		$conexao = new Conexao();
		$query = "SELECT * FROM rastreamentos WHERE {$campo} = '{$valor}' ORDER BY id DESC";
		$retorno = mysqli_query($conexao->getConexao(), $query);
	
		while($linhas = mysqli_fetch_array($retorno)){
			$rastreamento = new Rastreamento();
 			$rastreamento->setId($linhas["id"]);
 			$rastreamento->setNumeroPedido($linhas["numero_pedido"]);
			$rastreamento->setCliente($linhas["cliente"]);
 			$rastreamento->setEmail($linhas["email"]);
 			$rastreamento->setCodRastreio($linhas["cod_rastreio"]);
 			$rastreamento->setEntregue($linhas["entregue"]);
			
 			$formaVendaDAO = new FormaVendaDAO();
 			$formaVenda = new FormaVenda();
 			
 			if($linhas["id_forma_venda"] != ""){
 				$retornoFormaVenda = $formaVendaDAO->buscaFormaVenda("id", $linhas["id_forma_venda"]);
 				$formaVenda = $retornoFormaVenda[0];
 			}
 			
			$rastreamento->setFormaVenda($formaVenda);
			
			//Historico rastreio
			$conexao = new Conexao();
			$query_historico = "SELECT * FROM historico_rastreio WHERE id_rastreamento = '{$rastreamento->getId()}' ORDER BY id DESC";
			$retorno_historico = mysqli_query($conexao->getConexao(), $query_historico);
			while($linhas_historico = mysqli_fetch_array($retorno_historico)){
				$historicoRastreamentos = new HistoricoRastreio();
				$historicoRastreamentos->setId($linhas_historico["id"]);
				$historicoRastreamentos->setRastreamento($rastreamento);
				$historicoRastreamentos->setData($linhas_historico["data"]);
				$historicoRastreamentos->setLocalizacao($linhas_historico["localizacao"]);
				$historicoRastreamentos->setAcao($linhas_historico["acao"]);
				$historicoRastreamentos->setDetalhes($linhas_historico["detalhes"]);
				
				$rastreamento->adicionaHistoricoRastreamentos($historicoRastreamentos);
			}
			
			array_push($array, $rastreamento);
		}
	
		$rastreamentos = new ArrayObject($array);
		return $rastreamentos;
	}
	
	public function getUltimoIdRastreio(){
		$conexao = new Conexao();
		$query = "SELECT id FROM rastreamentos ORDER BY id DESC LIMIT 0, 1";
		$retorno = mysqli_query($conexao->getConexao(), $query);
		$linha = mysqli_fetch_array($retorno);
		return (int)$linha["id"] + 1;
	}
	
	public function getTabelaRastreamentos($campo, $valor){
		$rastreamentos = $this->buscaRastreamentos($campo, $valor);
		$tabela = $this->montaLayoutTabelaRastreamentos($rastreamentos);
		echo $tabela;
	}
	
	public function getTabelaBuscaRastreamentos($query){
		$array = array();
		
		$conexao = new Conexao();
		$retorno = mysqli_query($conexao->getConexao(), $query);
		
		while($linhas = mysqli_fetch_array($retorno)){
			$rastreamento = new Rastreamento();
			$rastreamento->setId($linhas["id"]);
			$rastreamento->setNumeroPedido($linhas["numero_pedido"]);
			$rastreamento->setCliente($linhas["cliente"]);
			$rastreamento->setEmail($linhas["email"]);
			$rastreamento->setCodRastreio($linhas["cod_rastreio"]);
			$rastreamento->setEntregue($linhas["entregue"]);
				
			 $formaVendaDAO = new FormaVendaDAO();
 			$formaVenda = new FormaVenda();
 			
 			if($linhas["id_forma_venda"] != ""){
 				$retornoFormaVenda = $formaVendaDAO->buscaFormaVenda("id", $linhas["id_forma_venda"]);
 				$formaVenda = $retornoFormaVenda[0];
 			}
 			
			$rastreamento->setFormaVenda($formaVenda);
			
			//Historico rastreio
			$conexao = new Conexao();
			$query_historico = "SELECT * FROM historico_rastreio WHERE id_rastreamento = '{$rastreamento->getId()}' ORDER BY id DESC";
			$retorno_historico = mysqli_query($conexao->getConexao(), $query_historico);
			while($linhas_historico = mysqli_fetch_array($retorno_historico)){
				$historicoRastreamentos = new HistoricoRastreio();
				$historicoRastreamentos->setId($linhas_historico["id"]);
				$historicoRastreamentos->setRastreamento($rastreamento);
				$historicoRastreamentos->setData($linhas_historico["data"]);
				$historicoRastreamentos->setLocalizacao($linhas_historico["localizacao"]);
				$historicoRastreamentos->setAcao($linhas_historico["acao"]);
				$historicoRastreamentos->setDetalhes($linhas_historico["detalhes"]);
			
				$rastreamento->adicionaHistoricoRastreamentos($historicoRastreamentos);
			}
				
			array_push($array, $rastreamento);
		}
		
		$rastreamentos = new ArrayObject($array);
		$tabela = $this->montaLayoutTabelaRastreamentos($rastreamentos);
		
		echo $tabela;
	}
	
	public function montaLayoutTabelaRastreamentos($rastreamentos){
		$tabela = null;
		
		$formaVendaDAO = new FormaVendaDAO();
		$arrayFormaVenda = $formaVendaDAO->getFormasVenda("todas");
		
		foreach ($rastreamentos as $linha => $rastreamento) {
			//Forma de venda
			$selectFormaVenda = $formaVendaDAO->montaFormaVenda($rastreamento, $arrayFormaVenda);
			
			//Status
			$ultimaAcao = null;
			$imgUltimaAcao = null;
			$historicoRastreio = $rastreamento->getHistoricoRastreamentos();
			if(count($historicoRastreio) > 0){
				$ultimoRastreio = $historicoRastreio[0];
				$ultimaAcao = $ultimoRastreio->getAcao();
			
				if($ultimaAcao == "entrega efetuada"){
					$imgUltimaAcao = "<img src='../img/entrega-efetuada.png' alt='Objeto entregue' class='tooltip-relatorio hover abre-modal-historico-rastreio' title='".ucfirst($ultimaAcao)."'/>";
				}
				else{
					$imgUltimaAcao = "<img src='../img/transport.png' alt='Objeto em transporte' class='tooltip-relatorio hover abre-modal-historico-rastreio' title='".ucfirst($ultimaAcao)."'/>";
				}
			}
			
			$tabela .= "<tr id='{$rastreamento->getId()}' class='seleciona-rastreio'>
							<td class='coluna-mobile'>
								<input type='text' value='{$rastreamento->getNumeroPedido()}' class='campo-form numero-pedido'>
							</td>
							<td>
								<p>{$rastreamento->getCliente()}</p>
								<p>{$rastreamento->getEmail()}</p>
							</td>
							<td class='coluna-mobile'>
								<select name='forma-venda' class='campo-form forma-venda'>
									{$selectFormaVenda}
									<option value='outro' class='abre-modal-forma-venda'>Outra (Cadastro)</option>
								</select>
							</td>
							<td>{$rastreamento->getCodRastreio()}</td>
							<td>
								{$imgUltimaAcao}
							</td>
						</tr>";
		}
	
		return $tabela;
	}
	
	public function updateRastreio($campo, $valor, Rastreamento $rastreamento){
		try{
			$conn = new Conexao();
			$update = "UPDATE rastreamentos SET $campo = '$valor' WHERE id = '{$rastreamento->getId()}'";
	
			if(mysqli_query($conn->getConexao(), $update)){
				return true;
			}
		}
		catch (Exception $ex){
			echo $ex->getMessage();
			return false;
		}
	}
	
	public function deleteRastreio(Rastreamento $rastreamento){
		try{
			$conn = new Conexao();
			$delete = "DELETE FROM historico_rastreio WHERE id_rastreamento = '{$rastreamento->getId()}'";
			mysqli_query($conn->getConexao(), $delete);
			
			$delete2 = "DELETE FROM rastreamentos WHERE id = '{$rastreamento->getId()}'";
			mysqli_query($conn->getConexao(), $delete2);
		}
		catch (Exception $ex){
			echo $ex->getMessage();
			return false;
		}
	}
	
	public function lerXmlRastreamentoCorreiosAGENCIAIDEIAS($xml, Rastreamento $rastreamento){
		$total_movs_bd = $this->getQuantidadeMovimentacoes($rastreamento);
		$total_movs_rastreio = count($xml->evento);	
	
		if($total_movs_rastreio > $total_movs_bd){
			while($total_movs_rastreio > $total_movs_bd){
				$historicoRastreio = new HistoricoRastreio();
					
				$historicoRastreio->setRastreamento($rastreamento);
				$historicoRastreio->setData(trim((string)$xml->evento[(int)$total_movs_bd]->data));
				$historicoRastreio->setLocalizacao(trim((string)$xml->evento[(int)$total_movs_bd]->local));
				$historicoRastreio->setAcao(trim((string)$xml->evento[(int)$total_movs_bd]->acao));
				$historicoRastreio->setDetalhes(trim((string)$xml->evento[(int)$total_movs_bd]->detalhes));
					
				if($historicoRastreio->getAcao() == "entrega efetuada"){
					$this->updateRastreio("entregue", "1", $rastreamento);
				}
	
				$this->atualizaStatusRastreamento($historicoRastreio);
				$total_movs_bd++;
			}

			$this->notificaRastreio($rastreamento->getId());
		}
		else{
			//"Sem movimentações novas.
		}
	}
	
	public function lerXmlRastreamentoCorreios(Rastreamento $rastreamento){
		try {
			$track = new Tracking();
			$track->setTrack(trim($rastreamento->getCodRastreio()));
			$array = $track->trackObject();

	 		$total_movs_bd = $this->getQuantidadeMovimentacoes($rastreamento);
			$total_movs_rastreio = count($array);
		
			if($total_movs_rastreio > $total_movs_bd){			
				while($total_movs_rastreio > $total_movs_bd){
					$historicoRastreio = new HistoricoRastreio();
					
					$historicoRastreio->setRastreamento($rastreamento);
					$historicoRastreio->setData(trim((string)$array[(int)$total_movs_bd]->date));
					$historicoRastreio->setLocalizacao(trim((string)$array[(int)$total_movs_bd]->local));
					$historicoRastreio->setAcao(trim((string)$array[(int)$total_movs_bd]->action));
					$historicoRastreio->setDetalhes(trim((string)$array[(int)$total_movs_bd]->details));
						
					if($historicoRastreio->getAcao() == "entrega efetuada"){
						$this->updateRastreio("entregue", "1", $rastreamento);
					}
		
					$this->atualizaStatusRastreamento($historicoRastreio);
					$total_movs_bd++;
				}

				$this->notificaRastreio($rastreamento->getId());
			}
			else{
				//"Sem movimentações novas.
			}

			return true;

		} catch (Exception $e) {
			return false;
		}
	}

	public function getQuantidadeMovimentacoes(Rastreamento $rastreamento){
		$conexao = new Conexao();
		$query = "SELECT count(*) as totalRegistros FROM historico_rastreio WHERE id_rastreamento = '{$rastreamento->getId()}'";
		$retorno = mysqli_query($conexao->getConexao(), $query);
		$linha = mysqli_fetch_array($retorno);
	
		return $linha['totalRegistros'];
	}

	public function atualizaStatusRastreamento(HistoricoRastreio $historicoRastreio){
		try{
			$rastreamento = $historicoRastreio->getRastreamento();
				
			$array_data = explode("/", substr($historicoRastreio->getData(), 0, strlen($historicoRastreio->getData()) -6));
			$array_hora = explode(":", substr($historicoRastreio->getData(), 11, strlen($historicoRastreio->getData())));
				
			$data = $array_data[2]."-".$array_data[1]."-".$array_data[0]." ".$array_hora[0].":".$array_hora[1].":00";

			$conn = new Conexao();
			$query_insert = "INSERT INTO historico_rastreio (id_rastreamento, data, localizacao, acao, detalhes)
					VALUES (
					'{$rastreamento->getId()}',
					'{$data}',
					'{$historicoRastreio->getLocalizacao()}',
					'{$historicoRastreio->getAcao()}',
					'{$historicoRastreio->getDetalhes()}'
			)";

			mysqli_query($conn->getConexao(), $query_insert);
			
		}
		catch (Exception $ex){
			echo $ex->getMessage();
			return false;
		}
	}

	public function notificaRastreio($idRastreio){
		$array = $this->buscaRastreamentos("id", $idRastreio);
		$rastreamento = $array[0];		
		$tabela = $this->montaTabelaHistoricoRastreio($rastreamento);
		
		$mail = new EnvioEmail();
		$mail->adicionaDestinatario($rastreamento->getEmail());
		// $mail->setCopiaOculta("contato@akimia.com.br");
	
		$mail->setFrom("Akimia Rastreabilidade");
	
		$mail->setAssunto("Akimia Importados - Pedido nº {$rastreamento->getNumeroPedido()} - {$rastreamento->getCodRastreio()}");
		$mail->setMensagem("
							<html>
								<head>
									<style>										
									</style>				
								</head>
								<body>
									<div style='margin-bottom: 25px;'>
										<table style='width: 100%; border-collapse: collapse; background-color: #FFF;'>
											<tr>
												<td style='width: 50%'><img src='http://adm.akimia.com.br/img/logo-akimia-small2.png' alt='Akimia Produtos Importados'></td>
												<td style='width: 50%; text-align: right; padding-top: 15px'>
													Código de rastreio: <a style='text-decoration: none; color: #000; font-weight: bold; font-size: 18px; font-family: Arial, sans-serif;' target='_blank' href='http://websro.correios.com.br/sro_bin/txect01$.QueryList?P_TIPO=001&P_COD_UNI={$rastreamento->getCodRastreio()}&P_LINGUA=001'> {$rastreamento->getCodRastreio()}</a>										
												</td>
											</tr>
										</table>
									</div>
									<div style='clear: both'></div>
									<div style='margin-bottom: 25px; margin-top: 25px;'>
										<p style='font-size: 15px; font-family: Arial, sans-serif;'>Olá <b>{$rastreamento->getCliente()}</b>.</p>
										<p style='font-size: 15px; font-family: Arial, sans-serif;'>Muito obrigado por comprar conosco. Ficamos felizes por ter o produto que procurastes e é um enorme prazer lhe atender.</p>
										<p style='font-size: 15px; font-family: Arial, sans-serif;'>O seu pedido já foi enviado. Abaixo você pode verificar algumas informações e acompanhar o passo a passo da sua entrega.</p>
										<br>
										<table style='margin-bottom: 30px; border-top: solid 2px #000; width: 100%; border-collapse: collapse; background-color: #FFF;'>
											<tr>
												<td style='padding-top: 15px'><b>CÓDIGO DE RASTREIO</b></td>
											</tr>
											<tr>
												<td><a style='text-decoration: none; color: #000; font-family: Arial, sans-serif;' target='_blank' href='http://websro.correios.com.br/sro_bin/txect01$.QueryList?P_TIPO=001&P_COD_UNI={$rastreamento->getCodRastreio()}&P_LINGUA=001'>{$rastreamento->getCodRastreio()}</a></td>
											</tr>
										</table>
										<table style='margin-bottom: 30px; border-top: solid 2px #000; width: 100%; border-collapse: collapse; background-color: #FFF;'>
											<tr>
												<td style='padding-top: 15px'><b>FORMA DE ENVIO</b></td>
											</tr>
											<tr>
												<td>CORREIOS</td>
											</tr>
										</table>
										<table style='margin-bottom: 10px; border-top: solid 2px #000; width: 100%; border-collapse: collapse; background-color: #FFF;'>
											<tr>
												<td style='padding-top: 15px'><b>RASTREIO</b></td>
											</tr>
										</table>
									</div>
									<div style='margin-bottom: 25px'>
										{$tabela}
									</div>
									<div style='margin-bottom: 25px; margin-top: 25px;'>
										<p style='font-size: 15px; font-family: Arial, sans-serif;'>A cada novo status você receberá a nossa notificação. Se clicar no código de rastreio conseguirá consultar a informação direto no site dos Correios.</p>
									</div>
									<div style='margin-bottom: 25px; margin-top: 10px;'>									
										<p style='font-size: 15px; font-family: Arial, sans-serif;'>Agradecemos pelo carinho e confiança.</p>
										<p style='font-size: 15px; font-family: Arial, sans-serif;'>Muito obrigado.</p>
										<p style='font-size: 15px; font-family: Arial, sans-serif;'><a href='http://www.akimia.com.br' target='_blank' style='text-decoration: underline; color: #000; font-family: Arial, sans-serif;'>Akimia Importados</a></p>
									</div>
								</body>
							</html>"
		);
	
		//echo $mail->getMensagem();
		$mail->enviaEmail();
	}

	
	public function montaTabelaHistoricoRastreio(Rastreamento $rastreamento){
		$tbody = null;
		for($i = 0; $i < count($rastreamento->getHistoricoRastreamentos()); $i++){
			$rastreamento_array = $rastreamento->getHistoricoRastreamentos();
			$historicoRastreio = $rastreamento_array[$i];
				
			$destaque = ($i == 0) ? " style='background-color: #ffc'" : null;
				
			$tbody .= "<tr {$destaque}>
							<td style='font-size: 14px; font-family: Arial, sans-serif; color: #555555; padding: 7px 15px; text-align: left; border: solid 1px #b2b1b7;'>{$historicoRastreio->getData()}</td>
							<td style='font-size: 14px; font-family: Arial, sans-serif; color: #555555; padding: 7px 15px; text-align: left; border: solid 1px #b2b1b7;'>{$historicoRastreio->getLocalizacao()}</td>
							<td style='font-size: 14px; font-family: Arial, sans-serif; color: #555555; padding: 7px 15px; text-align: left; border: solid 1px #b2b1b7;'>{$historicoRastreio->getDetalhes()}</td>
							<td style='font-size: 14px; font-family: Arial, sans-serif; color: #555555; padding: 7px 15px; text-align: left; border: solid 1px #b2b1b7;'>".ucfirst($historicoRastreio->getAcao())."</td>
						</tr>";
		
		}
		
		$tabela = "<table style='width: 100%; border-collapse: collapse; background-color: #FFF;' id='tabelaRastreio{$rastreamento->getId()}'>
						<thead style='background-color: #e61b72; border: solid 1px #e61b72;'>
							<tr>
								<th style='font-size: 14px; font-family: Arial, sans-serif; color: #FFF; border-right: solid 1px #FFF; border-left: solid 1px #e61b72; padding: 7px 15px; text-align: left;'>Data</th>												
								<th style='font-size: 14px; font-family: Arial, sans-serif; color: #FFF; border-right: solid 1px #FFF; padding: 7px 15px; text-align: left;'>Local</th>
								<th style='font-size: 14px; font-family: Arial, sans-serif; color: #FFF; border-right: solid 1px #FFF; padding: 7px 15px; text-align: left;'>Detalhes</th>
								<th style='font-size: 14px; font-family: Arial, sans-serif; color: #FFF; border-right: solid 1px #e61b72; padding: 7px 15px; text-align: left;'>Ação</th>
							</tr>				
						</thead>
						<tbody>
							{$tbody}
						</tbody>
					</table>";
							
		return $tabela;
	}
}