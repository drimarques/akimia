<?php
date_default_timezone_set('Brazil/East');
date_default_timezone_set('America/Sao_Paulo');

class UsuarioDAO {
	function validaLogin(Usuario $usuario) {
		$conexao = new Conexao ();
		$query = "SELECT * FROM usuarios WHERE email = '{$usuario->getEmail()}' AND senha = '{$usuario->getSenha()}'";
		$retorno = mysqli_query($conexao->getConexao(), $query);
		$qdtLinhas = mysqli_num_rows($retorno);
		
		if ($qdtLinhas == 1) {
			return true;
		} else {
			return false;
		}
	}
	public function logaUsuario(Usuario $usuario) {
		session_start();
		$_SESSION ["usuario_logado"] = $usuario->getEmail();
		
		// Tempo em segundos
		$tempoInatividade = 60 * 30;
		
		$_SESSION ['inicio'] = time();
		$_SESSION ['limite_inatividade'] = $tempoInatividade;
		
		echo "logado com sucesso";
	}
	public function logOut() {
		session_start();
		unset ( $_SESSION ["usuario_logado"] );
		unset ( $_SESSION ["inicio"] );
		unset ( $_SESSION ["limite_inatividade"] );
		session_destroy();
		// header("Location: http://www.lowcost.com.br/login.aspx?tipofunc=lowcost");
		// die();
	}
	function validaTempoLogado() {
		$tempo = time () - $_SESSION ['inicio'];
				
		if ($tempo > $_SESSION ['limite_inatividade']) {
			echo "faz logout";
		} else {
			$diferença = $_SESSION ['limite_inatividade'] - $tempo;
			echo "está logado à $tempo segundos - ";
			echo "restam $diferença segundos...";
		}
	}
	function renovaTempoLogado() {
		$_SESSION ['inicio'] = time ();
	}
}
     