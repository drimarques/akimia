<?php

class ListaDeComprasDAO{
	public function gravaCompra(ListaDeCompras $c){
		try{
			$conn = new Conexao();
			$query_insert = "INSERT INTO lista_de_compras (
				id_fechamento, 
				marca,
				sexo,
				tipo,
				produto,
				modelo,
				cor,
				tamanho,
				quantidade_total,
				quantidade_comprada,
				comprada,
				iof,
				custo_dolar,
				custo_br,
				custo_total,
				markup,
				venda,
				venda_total,
				lucro,
				lucro_total,
				cliente,
				telefone_cliente,
				email_cliente				
			)
			VALUES (
				'{$c->getIdFechamento()}',
				'{$c->getMarca()}',
				'{$c->getSexo()}',
				'{$c->getTipo()}',
				'{$c->getProduto()}',
				'{$c->getModelo()}',
				'{$c->getCor()}',
				'{$c->getTamanho()}',
				'{$c->getQuantidadeTotal()}',
				'{$c->getQuantidadeComprada()}',
				'{$c->getComprada()}',
				'{$c->getIof()}',
				'{$c->getCustoDolar()}',
				'{$c->getCustoBR()}',
				'{$c->getCustoTotal()}',
				'{$c->getMarkup()}',
				'{$c->getVenda()}',
				'{$c->getVendaTotal()}',
				'{$c->getLucro()}',
				'{$c->getLucroTotal()}',
				'{$c->getCliente()}',
				'{$c->getTelefoneCliente()}',
				'{$c->getEmailCliente()}'
			)";
		
			if(mysqli_query($conn->getConexao(), $query_insert)){
				$c->setId(mysqli_insert_id($conn->getConexao()));
				return $c;
			}
		}
		catch (Exception $ex){
			echo $ex->getMessage();
			return false;
		}
	}

	public function getListaCompras(Fechamento $f, $query){		
		$conexao = new Conexao();		
		$retorno = mysqli_query($conexao->getConexao(), $query);
		
		$parametrosDAO = new ParametrosDeCalculoDAO();
		$p = $parametrosDAO->getParametrosCalculo($f);
		
		$array_compras = array();
		while($linhas = mysqli_fetch_array($retorno)){
			$compra = new ListaDeCompras();

			$compra->setId($linhas["id_compra"]);
			$compra->setIdFechamento($f);
			$compra->setParametrosDeCalculo($p);
			$compra->setMarca($linhas["marca"]);
			$compra->setSexo($linhas["sexo"]);
			$compra->setTipo($linhas["tipo"]);
			$compra->setProduto($linhas["produto"]);
			$compra->setModelo($linhas["modelo"]);
			$compra->setCor($linhas["cor"]);
			$compra->setTamanho($linhas["tamanho"]);
			$compra->setQuantidadeTotal($linhas["quantidade_total"]);
			$compra->setQuantidadeComprada($linhas["quantidade_comprada"]);
			$compra->setComprada($linhas["comprada"]);
			$compra->setIof($linhas["iof"]);
			$compra->setCustoDolar($linhas["custo_dolar"]);
			$compra->setCustoBR($linhas["custo_br"]);
			$compra->setCustoTotal($linhas["custo_total"]);
			$compra->setMarkup($linhas["markup"]);
			$compra->setVenda($linhas["venda"]);
			$compra->setVendaTotal($linhas["venda_total"]);
			$compra->setLucro($linhas["lucro"]);
			$compra->setLucroTotal($linhas["lucro_total"]);
			$compra->setCliente($linhas["cliente"]);
			$compra->setTelefoneCliente($linhas["telefone_cliente"]);
			$compra->setEmailCliente($linhas["email_cliente"]);				
			
			array_push($array_compras, $compra);
		}
			
		$compras = new ArrayObject($array_compras);
		return $compras;
	}

	public function getProdutoPorId($idProduto){						
		$conexao = new Conexao();
		$query = "SELECT * FROM lista_de_compras WHERE id_compra = '{$idProduto}'";
		$retorno = mysqli_query($conexao->getConexao(), $query);
		
		$linhas = mysqli_fetch_array($retorno);
		
		$f = new Fechamento();
		$f->setId($linhas["id_fechamento"]);
		
		$parametrosDAO = new ParametrosDeCalculoDAO();
		$p = $parametrosDAO->getParametrosCalculo($f);
		
		$compra = new ListaDeCompras();
		$compra->setIdFechamento($f);
		$compra->setParametrosDeCalculo($p);
		$compra->setId($linhas["id_compra"]);		
		$compra->setMarca($linhas["marca"]);
		$compra->setSexo($linhas["sexo"]);
		$compra->setTipo($linhas["tipo"]);
		$compra->setProduto($linhas["produto"]);
		$compra->setModelo($linhas["modelo"]);
		$compra->setCor($linhas["cor"]);
		$compra->setTamanho($linhas["tamanho"]);
		$compra->setQuantidadeTotal($linhas["quantidade_total"]);
		$compra->setQuantidadeComprada($linhas["quantidade_comprada"]);
		$compra->setComprada($linhas["comprada"]);
		$compra->setIof($linhas["iof"]);
		$compra->setCustoDolar($linhas["custo_dolar"]);
		$compra->setCustoBR($linhas["custo_br"]);
		$compra->setCustoTotal($linhas["custo_total"]);
		$compra->setMarkup($linhas["markup"]);
		$compra->setVenda($linhas["venda"]);
		$compra->setVendaTotal($linhas["venda_total"]);
		$compra->setLucro($linhas["lucro"]);
		$compra->setLucroTotal($linhas["lucro_total"]);
		$compra->setCliente($linhas["cliente"]);
		$compra->setTelefoneCliente($linhas["telefone_cliente"]);
		$compra->setEmailCliente($linhas["email_cliente"]);
		
		return $compra;
	}
	
	public function getRelatorioListaCompras(Fechamento $f, $query){
		try{
			$comprasDAO = new ListaDeComprasDAO();
			$lista_compras = $comprasDAO->getListaCompras($f, $query);
			$tabela = $this->montaLayoutTabela($lista_compras);
			
			echo $tabela;
		}
		catch (Exception $ex){
			echo $ex->getMessage();
			return false;
		}
	}

	public function montaLayoutTabela($lista_compras){
		
		$tabela = null;
		
		/*Pegando todas as marcas*/
		$marcas = array();
		foreach ($lista_compras as $linha => $compra){
			if (!in_array($compra->getMarca(), $marcas)) {
				array_push($marcas, $compra->getMarca());
			}
		}
			
		foreach ($marcas as $linha => $marca){
			$tabela .= "<tr>
			<td colspan='10'><span class='marca-destaque'>{$marca}</span></td>
			</tr>";
		
			foreach ($lista_compras as $linha => $compra){
				if($compra->getMarca() == $marca){
		
					if($compra->getIof() == "0"){
						$options = "<option value='1'>Sim</option>".
								"<option value='0' selected='selected'>Não</option>";
					}
					else{
						$options = "<option value='1' selected='selected'>Sim</option>".
								"<option value='0'>Não</option>";
					}
		
					$classIOFLista= ($compra->getIof() == "0") ? "class='bg-azul'" : "class='bg-verde'";
					$classCustoEUA= ($compra->getCustoDolar() == "0.00") ? "class='bg-azul'" : "class='bg-verde'";
					$classVenda= ($compra->getVenda() == "0.00") ? "class='bg-azul'" : "class='bg-verde'";
		
					$tabela .= "<tr id='{$compra->getId()}idProduto'>
									<td>
										<div class='produto-destaque'>{$compra->getProduto()} {$compra->getModelo()} - {$compra->getCor()}</div>
										<div class='produto-descricao'>{$compra->getSexo()} - Tamanho: {$compra->getTamanho()} - Quantidade: {$compra->getQuantidadeTotal()} <img src='../img/pencil.png' alt='Editar quantidade' class='hover abre-modal-qtd-lista-compras'></div>
										<div class='mini-modal-quantidade-lista-compras' id='modalQuantidade{$compra->getId()}'>
											<form class='form-quantidade-lista-compras form-busca' id='FormularioQuantidadeListaCompras{$compra->getId()}' autocomplete='off'>
												<input type='number' class='campo-form' id='imputQtd{$compra->getId()}' value='{$compra->getQuantidadeTotal()}' required='required'>
												<input type='submit' class='botao hover' value='Ok'>
											</form>
										</div>
									</td>
									<td {$classIOFLista}>
										<select name='iof' class='campo-form upt-iof-produto-lista'>
											{$options}
										</select>
									</td>
									<td {$classCustoEUA}>
										<input type='text' name='custo_dolar' class='campo-form dolar upd-produto-lista' value='{$compra->getCustoDolar()}'>
									</td>
									<td>R$ ".number_format($compra->getCustoBR(), 2, ',', '.')."</td>
									<td>R$ ".number_format($compra->getCustoTotal(), 2, ',', '.')."</td>
									<td>".number_format($compra->getMarkup(), 2, ',', '.')." %</td>
									<td {$classVenda}>
										<input type='text' name='venda' class='campo-form real upd-produto-lista' value='".number_format($compra->getVenda(), 2, ',', '.')."'>
									</td>
									<td>R$ ".number_format($compra->getVendaTotal(), 2, ',', '.')."</td>
									<td>R$ ".number_format($compra->getLucro(), 2, ',', '.')."</td>
									<td>R$ ".number_format($compra->getLucroTotal(), 2, ',', '.')."</td>
								</tr>";
				}
			}
		}
			
		return $tabela;
	}
	
	public function getProdutoLista(ListaDeCompras $c){
		$compra = $this->getProdutoPorId($c->getId());		
		$compra = $compra->realizaCalculos();	
		$this->updCompra($compra);
		$compra = $this->getProdutoPorId($c->getId());
				
		if($compra->getIof() == "0"){
			$options = "<option value='1'>Sim</option>".
						"<option value='0' selected='selected'>Não</option>";
		}
		else{
			$options = "<option value='1' selected='selected'>Sim</option>".
					"<option value='0'>Não</option>";
		}
		
		$classIOFLista= ($compra->getIof() == "0") ? "class='bg-azul'" : "class='bg-verde'";
		$classCustoEUA= ($compra->getCustoDolar() == "0.00") ? "class='bg-azul'" : "class='bg-verde'";
		$classVenda= ($compra->getVenda() == "0.00") ? "class='bg-azul'" : "class='bg-verde'";
		
		$linhaTd = "<td>
						<div class='produto-destaque'>{$compra->getProduto()} {$compra->getModelo()} - {$compra->getCor()}</div>
						<div class='produto-descricao'>{$compra->getSexo()} - Tamanho: {$compra->getTamanho()} - Quantidade: {$compra->getQuantidadeTotal()} <img src='../img/pencil.png' alt='Editar quantidade' class='hover abre-modal-qtd-lista-compras' ></div>
						<div class='mini-modal-quantidade-lista-compras' id='modalQuantidade{$compra->getId()}'>
							<form class='form-quantidade-lista-compras form-busca' id='FormularioQuantidadeListaCompras{$compra->getId()}' autocomplete='off'>
								<input type='number' class='campo-form' id='imputQtd{$compra->getId()}' value='{$compra->getQuantidadeTotal()}' required='required'>
								<input type='submit' class='botao hover' value='Ok'>
							</form>
						</div>
					 </td>
					<td {$classIOFLista}>
						<select name='iof' class='campo-form upt-iof-produto-lista'>
							{$options}
						</select>
					</td>
					<td {$classCustoEUA}>
						<input type='text' name='custo_dolar' class='campo-form dolar upd-produto-lista' value='{$compra->getCustoDolar()}'>
					</td>
					<td>R$ ".number_format($compra->getCustoBR(), 2, ',', '.')."</td>
					<td>R$ ".number_format($compra->getCustoTotal(), 2, ',', '.')."</td>
					<td>".number_format($compra->getMarkup(), 2, ',', '.')." %</td>
					<td {$classVenda}>
					<input type='text' name='venda' class='campo-form real upd-produto-lista' value='".number_format($compra->getVenda(), 2, ',', '.')."'>
					</td>
					<td>R$ ".number_format($compra->getVendaTotal(), 2, ',', '.')."</td>
					<td>R$ ".number_format($compra->getLucro(), 2, ',', '.')."</td>
					<td>R$ ".number_format($compra->getLucroTotal(), 2, ',', '.')."</td>";
		
		return $linhaTd;
	}
	
	public function getTotalRegistros($query){
		try{
			$conexao = new Conexao();								
			$retorno = mysqli_query($conexao->getConexao(), $query);
			$linha = mysqli_fetch_array($retorno);
			
			switch ($linha["totalRegistros"]) {
				case "0":
					return "Nenhum registro encontrado.";
				break;
				case "1":
					return "{$linha['totalRegistros']} registro encontrado.";
				default:
					return "{$linha['totalRegistros']} registros encontrados.";
				break;
			}					
			
		}
		catch (Exception $ex){
			echo $ex->getMessage();
			return false;
		}
	}
	
	public function updValoresListaCompras(Fechamento $f, $query){
		try{						
			$lista_compras = $this->getListaCompras($f, $query);			
			foreach ($lista_compras as $linha => $compra){
				
				$venda = $compra->getVenda();
				$custo_dolar = $compra->getVenda();
				
				if(!empty($custo_dolar) && !empty($venda) && $compra->getCustoDolar() != "0.00" && $compra->getVenda() != "0.00"){					
					$compra = $compra->realizaCalculos();					
					$this->updCompra($compra);
				}				
			}			
		}
		catch (Exception $ex){
			echo $ex->getMessage();
			return false;
		}
	}
	
	public function updCompra(ListaDeCompras $c){
		try{
			$conexao = new Conexao();			
			$query = "UPDATE lista_de_compras SET 
					custo_dolar = '{$c->getCustoDolar()}',
					custo_br = '{$c->getCustoBR()}',
					custo_total = '{$c->getCustoTotal()}',
					markup = '{$c->getMarkup()}',
					venda = '{$c->getVenda()}',
					venda_total = '{$c->getVendaTotal()}',
					lucro = '{$c->getLucro()}',
					lucro_total = '{$c->getLucroTotal()}'
					WHERE id_compra = '{$c->getId()}'
					"; 
			
			if(mysqli_query($conexao->getConexao(), $query)){
				return true;
			}			
		}
		catch (Exception $ex){
			echo $ex->getMessage();
			return false;
		}
	}

	public function updCampoCompra(ListaDeCompras $c, $campo, $valor){
		try{
			$conexao = new Conexao();
			$query = "UPDATE lista_de_compras SET {$campo} = '{$valor}' WHERE id_compra = {$c->getId()}";
	
			if(mysqli_query($conexao->getConexao(), $query)){
				return true;
			}
	
		}
		catch (Exception $ex){
			echo $ex->getMessage();
			return false;
		}
	}
	
	public function criaListaCompras(Fechamento $f){
		try{
			$conexao = new Conexao();
			$query = "INSERT INTO checklist_compras (marca, sexo, tipo, produto, modelo, cor, tamanho, quantidade_comprada, quantidade_total, comprada, cliente, telefone, email)
					  SELECT marca, sexo, tipo, produto, modelo, cor, tamanho, quantidade_comprada, quantidade_total, comprada, cliente, telefone_cliente, email_cliente FROM lista_de_compras WHERE id_fechamento = '{$f->getId()}'";
	
			if(mysqli_query($conexao->getConexao(), $query)){
				return true;
			}
	
		}
		catch (Exception $ex){
			echo $ex->getMessage();
			return false;
		}
	}
	
	public function limpaListaCompras(){
		try{
			$conexao = new Conexao();
			$query = "truncate table checklist_compras";			
	
			if(mysqli_query($conexao->getConexao(), $query)){
				return true;
			}
	
		}
		catch (Exception $ex){
			echo $ex->getMessage();
			return false;
		}
	}
	
	
	public function checaListaCompras(){
		try{
			$conexao = new Conexao();
			$query = "SELECT count(*) as totalResultados FROM checklist_compras";
			
			$retorno = mysqli_query($conexao->getConexao(), $query);
			
			if($retorno){
				$rowCount = mysqli_num_rows($retorno);
				if($rowCount > 0){
					$linha = mysqli_fetch_array($retorno);
					if($linha["totalResultados"] == "0"){
						echo "nao_tem_lista";
					}
					else{
						echo "tem_lista";
					}
				}
			}
			else{
				echo "nao_tem_lista";
			}
		}
		catch (Exception $ex){
			echo $ex->getMessage();			
		}
	}
	
	public function cadastraItemListaCompras(ListaDeCompras $c){
		try{
			$conn = new Conexao();
			$query_insert = "INSERT INTO checklist_compras (				
				marca,
				sexo,
				tipo,
				produto,
				modelo,
				cor,
				tamanho,
				quantidade_total,
				cliente,
				telefone,
				email
			)
			VALUES (				
				'{$c->getMarca()}',
				'{$c->getSexo()}',
				'{$c->getTipo()}',
				'{$c->getProduto()}',
				'{$c->getModelo()}',
				'{$c->getCor()}',
				'{$c->getTamanho()}',
				'{$c->getQuantidadeTotal()}',
				'{$c->getCliente()}',
				'{$c->getTelefoneCliente()}',
				'{$c->getEmailCliente()}'
			)";
					
			if(mysqli_query($conn->getConexao(), $query_insert)){
				$c->setId(mysqli_insert_id($conn->getConexao()));
				return $c;
			}
		}
		catch (Exception $ex){
			echo $ex->getMessage();			
		}
	}
	
	public function geraFechamentoPorListaCompras(Fechamento $f){
		try{
			$conn = new Conexao();
			$query_insert = "INSERT INTO lista_de_compras (id_fechamento, marca, sexo, tipo, produto, modelo, cor, tamanho, quantidade_comprada, quantidade_total, comprada, cliente, telefone_cliente, email_cliente)
							SELECT {$f->getId()}, marca, sexo, tipo, produto, modelo, cor, tamanho, quantidade_comprada, quantidade_total, comprada, cliente, telefone, email FROM checklist_compras";
						
			if(mysqli_query($conn->getConexao(), $query_insert)){				
				return true;
			}
		}
		catch (Exception $ex){
			echo $ex->getMessage();
		}
	}
	
}