<?php

require_once '../classes/inclui-classes.php';
require_once '../daos/inclui-daos.php';

$usuarioDAO = new UsuarioDAO();
$usuarioDAO->logOut();

header("Location: ../../index.php");
die();