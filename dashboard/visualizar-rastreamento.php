<?php

require_once '../src/classes/inclui-classes.php';
require_once '../src/daos/inclui-daos.php';

$rastreio = $_GET["rastreio"];
if($rastreio != null && $rastreio != ""){
	$dao = new RastreamentoDAO();
	$array = $dao->buscaRastreamentos("cod_rastreio", $rastreio);
	$rastreamento = $array[0];
	$tabela = $dao->montaTabelaHistoricoRastreio($rastreamento);	
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Akimia - Histórico de rastreio</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="shortcut icon" href="../img/logo-akimia-small.png" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,700,700italic,800italic,800' rel='stylesheet' type='text/css'>
<link href="css/padroes.css" rel="stylesheet" type="text/css" />
<link href="css/index.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<header class="centralizer">
		<h1>
			<a href="index.php">
				<img alt="Akimia Produtos Importados" src="../img/logo-akimia-small.png">
			</a>
		</h1>
	</header>
	<section class="centralizer" id="paineis">
		<p>Olá <b><?php echo $rastreamento->getCliente()?>.</b></p>
		<br>
		<p>Muito obrigado por comprar conosco. Ficamos felizes por ter o produto que procurastes e é um enorme prazer lhe atender.</p>
		<p>O seu pedido já foi enviado. Abaixo você pode acompanhar o passo a passo da sua entrega.</p>
		<br>
		<p>
			<?php 
				echo "Código de rastreio: <a style='font-weight: bold; text-decoration: none; color: #000; font-family: Arial, sans-serif;' target='_blank' href='http://websro.correios.com.br/sro_bin/txect01$.QueryList?P_TIPO=001&P_COD_UNI={$rastreio}&P_LINGUA=001'>{$rastreio}</a>."; 
			?>
		</p>
		<br>
		<?php 
			echo $tabela
		?>
		<br>
		<p>Agradecemos pelo carinho e confiança.</p>
		<p>Muito obrigado.</p>
		<br>
		<p><a href='http://www.akimia.com.br/' target='blank' style='text-decoration: underline;'>Akimia Importados</a></p>
		
	</section>
</body>
</html>