function limpaListaENotificacao() {
     $("#notificacao_lista_compras").css({"display": "none"});
     document.getElementById("lista_compras").innerHTML = "";
}

function temItensNaoCompradosMarcados() {
     var verificador = false;

     $(".item_naocomprado_selecionado").each(function () {
	  verificador = true;
     });

     return verificador;
}

function temItensCompradosMarcados() {
     var verificador = false;

     $(".item_comprado_selecionado").each(function () {
	  verificador = true;
     });

     return verificador;
}


function notificacao(texto){
    $("#notificacao_lista_compras").finish();
    document.getElementById("notificacao_lista_compras").innerHTML = texto;
    $("#notificacao_lista_compras").fadeIn("fast").delay(4000).fadeOut(200, function () {

    });
}

function notificacaoErro(texto) {
     $("#notificacao_lista_compras").finish();
     document.getElementById("notificacao_lista_compras").innerHTML = "Marque pelo menos 1 item para " + texto + ".";
     $("#notificacao_lista_compras").fadeIn("fast").delay(1000).fadeOut(200, function () {

     });
}

function notificacaoSucesso(texto, busca) {
     $("#notificacao_lista_compras").finish();
     document.getElementById("notificacao_lista_compras").innerHTML = "Itens " + texto + ".";
     $("#notificacao_lista_compras").fadeIn("fast").delay(1000).fadeOut(200, function () {
	  if (busca !== null) {
	       listaComprasBusca(busca);
	  }
	  else {
	       verificaLocalStorage();
	  }

     });
}

function listaComprasBusca(busca) {
     if (busca !== "") {
	  limpaListaENotificacao();

	  $.ajax({
	       url: "compras/busca.php",
	       data: {filtro_busca: busca},
	       type: 'POST',
	       context: $('#lista_compras'),
	       success: function (relatorio) { //success: function () {
		    $("#lista_compras").html(relatorio);
	       }
	  });
     }
}

function listaCompras(n_comprados, comprados, masculino, feminino, unissex, marcas, tipos) {
     $.ajax({
	  url: "compras/gera-lista-compras.php",
	  data: {n_comprados: n_comprados, comprados: comprados, masculino: masculino, feminino: feminino, unissex: unissex, marcas: marcas, tipos: tipos},
	  type: 'POST',
	  context: $('#lista_compras'),
	  success: function (relatorio) { //success: function () {
	       $("#lista_compras").html(relatorio);
	  }
     });
}

function validaFiltros() {
     mataLocalStorage();
     limpaListaENotificacao();

     var n_comprados = $("#btn_n_comprados").hasClass('filtro_selecionado').toString();
     var comprados = $("#btn_comprados").hasClass('filtro_selecionado').toString();

     var masculino = $("#btn_masculino").hasClass('sexo_selecionado').toString();
     var feminino = $("#btn_feminino").hasClass('sexo_selecionado').toString();
     var unissex = $("#btn_unissex").hasClass('sexo_selecionado').toString();

     //Buscando marcas selecionadas
     var temosMarcas = false;
     $(".marca_selecionada").each(function () {
	  temosMarcas = true;
     });

     if (temosMarcas === true) {
	  var marcas = new Array();
	  $(".marca_selecionada").each(function () {
	       marcas.push(this.innerHTML);
	  });
     }

     //Buscando tipos selecionados
     var temosTipos = false;
     $(".tipo_selecionado").each(function () {
	  temosTipos = true;
     });

     if (temosTipos === true) {
	  var tipos = new Array();
	  $(".tipo_selecionado").each(function () {
	       tipos.push(this.innerHTML);
	  });
     }

     listaCompras(n_comprados, comprados, masculino, feminino, unissex, marcas, tipos);

}

function marcaComprado() {
     if (temItensNaoCompradosMarcados()) {
	  $(".item_naocomprado_selecionado").each(function () {
	       var id_item = this.id;
	       $.ajax({
		    url: "compras/marca-comprado.php",
		    data: {id_item: id_item},
		    type: 'POST'
	       });
	  });

	  var busca = localStorage.getItem("busca");
	  console.log(busca);
	  notificacaoSucesso("comprados", busca);

     }
     else {
	  notificacaoErro("comprar");
     }
}

function marcaNaoComprado() {
     if (temItensCompradosMarcados()) {
	  $(".item_comprado_selecionado").each(function () {
	       var id_item = this.id;
	       $.ajax({
		    url: "compras/marca-nao-comprado.php",
		    data: {id_item: id_item},
		    type: 'POST'
	       });
	  });

	  var busca = localStorage.getItem("busca");
	  notificacaoSucesso("não comprados", busca);
     }
     else {
	  notificacaoErro("não comprar");
     }
}

function verificaLocalStorage() {
     var naoComprados = localStorage.getItem("queryNaoComprados");
     var comprados = localStorage.getItem("queryComprados");

     if (naoComprados !== null && comprados !== null) {
	  $("#btn_n_comprados").removeClass("filtro_selecionado");
	  verificaFiltroPrimeiroNivel();
	  limpaListaENotificacao();

	  $.ajax({
	       url: "compras/gera-lista-compras-nao-comprados.php",
	       data: {queryNaoComprados: naoComprados},
	       type: 'POST',
	       context: $('#lista_compras'),
	       success: function (relatorio) { //success: function () {
		    $("#lista_compras").html(relatorio);

		    //Listando comprados
		    $.ajax({
			 url: "compras/gera-lista-compras-comprados.php",
			 data: {queryComprados: comprados},
			 type: 'POST',
			 context: $('#lista_compras'),
			 success: function (relatorio) { //success: function () {
			      $("#lista_compras").append(relatorio);
			 }
		    });

	       }
	  });

     }
     else if (naoComprados !== null && comprados === null) {
	  $("#btn_n_comprados").removeClass("filtro_selecionado");
	  verificaFiltroPrimeiroNivel();
	  limpaListaENotificacao();

	  $.ajax({
	       url: "compras/gera-lista-compras-nao-comprados.php",
	       data: {queryNaoComprados: naoComprados},
	       type: 'POST',
	       context: $('#lista_compras'),
	       success: function (relatorio) { //success: function () {
		    $("#lista_compras").html(relatorio);
	       }
	  });
     }
     else if (comprados !== null && naoComprados === null) {
	  $("#btn_n_comprados").removeClass("filtro_selecionado");
	  verificaFiltroPrimeiroNivel();
	  limpaListaENotificacao();

	  $.ajax({
	       url: "compras/gera-lista-compras-comprados.php",
	       data: {queryComprados: comprados},
	       type: 'POST',
	       context: $('#lista_compras'),
	       success: function (relatorio) { //success: function () {
		    $("#lista_compras").html(relatorio);
	       }
	  });
     }
     else {
	  validaFiltros();
     }
}

function mataLocalStorage() {
     $("#txtBusca").val("");
     localStorage.clear();
}

$(document).ready(function () {
     $("#hamburguer_open").click(function () {

	  $("#notificacao_lista_compras").fadeOut("fast");

	  var tamanhoDoBody = $("body").width();
	  var widthMenuLateral;
	  
	  console.log(tamanhoDoBody);
	  if(tamanhoDoBody < 450){
		  widthMenuLateral = "85%";
	  }
	  else if(tamanhoDoBody < 800){
		  widthMenuLateral = "45%";
	  }
	  else if(tamanhoDoBody < 1400){
		  widthMenuLateral = "30%";
	  }
	  else{
		  widthMenuLateral = "20%";
	  }
	  
	  $("#menu-mobile").css({"display": "block"});
	  $("#menu-mobile").animate({
	       width: widthMenuLateral
	  },
	  500,
		  function () {

		  });
     });

     $("#fecha-menu-mobile").click(function () {
	  $("#menu-mobile").animate({
	       width: "0"
	  },
	  500,
		  function () {
		       $("#hamburguer_open").css({"display": "block"});
		       $("#menu-mobile").css({"display": "none"});
		  });
     });

     $("#hamburguer_close").click(function () {
	  $("#menu-mobile").animate({
	       width: "0"
	  },
	  500,
		  function () {
		       $("#hamburguer_open").css({"display": "block"});
		       $("#menu-mobile").css({"display": "none"});
		       validaFiltros();
		  });
     });

     $("#img_buscar").click(function () {
	  $("#img_buscar").css({"display": "none"});
	  $("#img_menu_suspenso").css({"display": "none"});
	  $("#hamburguer_open").css({"display": "none"});
	  $(".btns_menu").css({"display": "none"});


	  $("#filtro_pesquisa").css({"display": "block"});
	  $("#filtro_pesquisa").animate({
	       width: "80%"
	  }, 500,
		  function () {
		       $("#close_busca").css({"display": "block"});
		       $("#txtBusca").focus();
		  });
     });

     $("#close_busca").click(function () {
	  $("#close_busca").css({"display": "none"});

	  $("#filtro_pesquisa").animate({
	       width: "0"
	  }, 500,
		  function () {
		       $("#filtro_pesquisa").css({"display": "none"});
		       $("#img_buscar").css({"display": "block"});
		       $("#img_menu_suspenso").css({"display": "block"});
		       $("#hamburguer_open").css({"display": "block"});
		       $(".btns_menu").css({"display": "block"});
		  });
     });

     $("#txtBusca").keyup(function () {
	  var busca = document.getElementById("txtBusca");
	  if (busca.value.toString() !== "") {
	       localStorage.setItem("busca", busca.value.toString());
	       listaComprasBusca(busca.value.toString());
	  }
	  else {
	       mataLocalStorage();
	       limpaListaENotificacao();
	       document.getElementById("notificacao_lista_compras").innerHTML = "Digite ou selecione um filtro, vamos começar :D";
	       $("#notificacao_lista_compras").fadeIn("fast");
	  }

     });

     //Botoes comprados n comprados comprados
     $("#btn_n_comprados").click(function () {
	  $("#btn_n_comprados").toggleClass('filtro_selecionado');
	  $("#abre_sexo").fadeIn();
	  verificaFiltroPrimeiroNivel();
     });

     $("#btn_comprados").click(function () {
	  $("#btn_comprados").toggleClass('filtro_selecionado');
	  $("#abre_sexo").fadeIn();
	  verificaFiltroPrimeiroNivel();
     });

     $("#abre_sexo").click(function () {
	  $(".filtro_sexo").fadeIn();
     });

     //Botoes de sexo
     $("#btn_masculino").click(function () {
	  $("#btn_masculino").toggleClass('sexo_selecionado');
	  verificaFiltroSexo();
     });

     $("#btn_feminino").click(function () {
	  $("#btn_feminino").toggleClass('sexo_selecionado');
	  verificaFiltroSexo();
     });

     $("#btn_unissex").click(function () {
	  $("#btn_unissex").toggleClass('sexo_selecionado');
	  verificaFiltroSexo();
     });

     $("#abre_marcas").click(function () {
	  if ($("#espaco_marcas").html() === "") {
	       $.ajax({
		    url: "compras/get-marcas.php",
		    //data: {n_comprados: n_comprados, comprados: comprados},
		    type: 'POST',
		    context: $('#espaco_marcas'),
		    success: function (marcas) { //success: function () {
			 $("#espaco_marcas").html(marcas);
		    }
	       });
	  }
     });

     $("#abre_tipo").click(function () {
	  if ($("#espaco_tipos").html() === "") {
	       $.ajax({
		    url: "compras/get-tipos.php",
		    //data: {n_comprados: n_comprados, comprados: comprados},
		    type: 'POST',
		    context: $('#espaco_tipos'),
		    success: function (tipos) { //success: function () {
			 $("#espaco_tipos").html(tipos);
		    }
	       });
	  }
     });

     //logout
     $("#btn_logout_mobile").click(function () {
//	  $.ajax({
//	       url: "src/autenticacao/logout.php",
//	       type: 'POST',
//	       context: $("autenticacao"),
//	       success: function () {
//	    	   location.href = "../../index.php";
//	       }
//	  });
    	 location.href = "../index.php";
     });
     

	$('.tooltip').tooltipster({        	
        position: 'bottom',        
        theme: 'tooltipster-light'       
    });
	
	$("header").delegate("#abre-modal-limpar-lista", "click", function(){
		$('#modalLimparLista').modal('show');
    });
	
	$("header").delegate("#img_menu_suspenso", "click", function(){
		$("#menu-suspenso").fadeToggle("fast");
	});
	
	$("header").delegate("#img-adiciona-item", "click", function(){
		abreModalAddItem();
	});
	
	$("header").delegate("#abre-modal-importar-planilha", "click", function(){
		$('#modalImportarPlanilha').modal('show');
	});
	
	
	document.getElementById("arquivo-lista-compras").onchange = function () {
	    document.getElementById("nome-arquivo-lista-compras").value = this.value;
	};
	
	$("#lista-compras-lista").on('submit',(function(e) {
		e.preventDefault();	
		
		var planilha = $("#arquivo-lista-compras").val();			
		if(planilha == ""){
			notificacao("Por favor, selecione um arquivo.");
		}		
		else{
			notificacao("Aguarde, importando planilha...");
			
			$.ajax({
				url: "../../src/servicos/lista-compras/importa-lista-compras.php",
				type: "POST",
				data: new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
				success: function(retorno){						
					if(retorno == "erro_arquivo_excel"){
						notificacao("Somente arquivos no formado .xls");
					}
					else{										
						notificacao("Planilha importada com sucesso :D");
						
						setTimeout(function(){
							$("#modal-lista-compras nav ul li").removeClass("menu-selecionado");
							$("#modal-lista-compras nav ul li").removeAttr("style");						
													
							location.href = "index.php";
						}, 1000);
					}
				}
			});
		}
	}));

});