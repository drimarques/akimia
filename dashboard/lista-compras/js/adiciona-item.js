function abreModalAddItem() {
     $('#modalAddItem').modal('show');
}

function addItem() {
     var marca = $("#marca_cadastro").val();
     var sexo = $("#sexo_cadastro").val();
     var tipo = $("#tipo_cadastro").val();
     var produto = $("#produto_cadastro").val();
     var modelo = $("#modelo_cadastro").val();
     var cor = $("#cor_cadastro").val();
     var tamanho = $("#tamanho_cadastro").val();
     var quantidade = $("#quantidade_cadastro").val();     
     var cliente = $("#cliente_cadastro").val();
     var telefone = $("#telefone_cliente").val();
     var email = $("#email_cliente").val();    

     if (!validaCadastroItem(marca, sexo, tipo, produto, modelo, cor, tamanho, quantidade)) {
	  $.ajax({
	       url: "compras/adiciona-item.php",
	       data: {marca: marca, sexo: sexo, tipo: tipo, produto: produto, modelo: modelo, cor: cor, tamanho: tamanho, quantidade: quantidade, cliente: cliente, telefone: telefone, email: email},
	       type: 'POST',
	       //context: $('#resultmodalQuantidadeado'),
	       success: function () { //success: function (retorno) {
		    $("#notificacao_add_item").removeClass('alert-danger');
		    $("#notificacao_add_item").addClass('alert-success');
		    $("#notificacao_add_item").text("Item adicionado com sucesso.");
		    $("#notificacao_add_item").fadeIn("fast").delay(1000).fadeOut(200, function () {
			 location.href = "index.php";
		    });
	       }
	  });

     }
     else {
	  $("#notificacao_add_item").removeClass('alert-success');
	  $("#notificacao_add_item").addClass('alert-danger');
	  $("#notificacao_add_item").text("Preencha todos os campos.");
	  $("#notificacao_add_item").fadeIn("fast");
     }
}

function validaCadastroItem(marca, sexo, tipo, produto, modelo, cor, tamanho, quantidade) {
     var teve_erro = false;

     if (teve_erro === false) {
	  teve_erro = (marca === "") ? true : false;
	  (marca === "") ? $("#marca_cadastro").addClass('borda_erro') : $("#marca_cadastro").removeClass('borda_erro');
     }

     if (teve_erro === false) {	  
	  teve_erro = (sexo === null) ? true : false;
	  (sexo === null) ? $("#sexo_cadastro").addClass('borda_erro') : $("#sexo_cadastro").removeClass('borda_erro');
     }

     if (teve_erro === false) {
	  teve_erro = (tipo === "") ? true : false;
	  (tipo === "") ? $("#tipo_cadastro").addClass('borda_erro') : $("#tipo_cadastro").removeClass('borda_erro');
     }

     if (teve_erro === false) {
	  teve_erro = (produto === "") ? true : false;
	  (produto === "") ? $("#produto_cadastro").addClass('borda_erro') : $("#produto_cadastro").removeClass('borda_erro');
     }

     if (teve_erro === false) {
	  teve_erro = (modelo === "") ? true : false;
	  (modelo === "") ? $("#modelo_cadastro").addClass('borda_erro') : $("#modelo_cadastro").removeClass('borda_erro');
     }

     if (teve_erro === false) {
	  teve_erro = (cor === "") ? true : false;
	  (cor === "") ? $("#cor_cadastro").addClass('borda_erro') : $("#cor_cadastro").removeClass('borda_erro');
     }

     if (teve_erro === false) {
	  teve_erro = (tamanho === "") ? true : false;
	  (tamanho === "") ? $("#tamanho_cadastro").addClass('borda_erro') : $("#tamanho_cadastro").removeClass('borda_erro');
     }

     if (teve_erro === false) {
	  teve_erro = (quantidade === "") ? true : false;
	  (quantidade === "") ? $("#quantidade_cadastro").addClass('borda_erro') : $("#quantidade_cadastro").removeClass('borda_erro');
     }

     return teve_erro;
}