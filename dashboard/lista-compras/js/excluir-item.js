$(function() {
	$("header").delegate("#excluirItemLista", "click",function(){
		if (!temItensCompradosMarcados() && !temItensNaoCompradosMarcados()) {
			notificacao("Marque pelo menos 1 item para excluir");
		} 
		else {
			notificacao("Excluindo itens...");
			var ids = [];
			
			$(".item_naocomprado_selecionado").each(function() {
				var id_item = this.id;
				ids.push(id_item);
			});

			$(".item_comprado_selecionado").each(function() {
				var id_item = this.id;
				ids.push(id_item);
			});
			
			for(var i = 0; i < ids.length; i++){
				var idItem = ids[i];
				$.ajax({
					url : "compras/delete-item-compra.php",
					data : {"id_item" : idItem},
					type : 'POST',
					success: function () {
						
					}
				});
			}
			
			setTimeout(function(){
				location.href = "index.php";
			}, 5000)
		}
	});
});