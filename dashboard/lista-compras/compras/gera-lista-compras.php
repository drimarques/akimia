<?php

     require_once '../src/classes/conexao.class.php';
     require_once '../src/classes/compra.class.php';
     require_once '../src/dao/comprasDAO.php';     
     
     $dao = new comprasDAO();

     $n_comprados = $_POST["n_comprados"];
     $comprados = $_POST["comprados"];

     $masculino = $_POST["masculino"];
     $feminino = $_POST["feminino"];
     $unissex = $_POST["unissex"];
               
     $marcas = $_POST["marcas"];
     
     $tipos = $_POST["tipos"];
     
     $queryNaoComprados = "SELECT * FROM checklist_compras WHERE comprada = 0";
     $queryComprados = "SELECT * FROM checklist_compras WHERE (comprada = 1 OR quantidade_comprada <> 0)";

     function tratamentoSexo($query, $masculino, $feminino, $unissex) {
	  if ($masculino == "true" && $feminino == "true" && $unissex == "true") {	       
	       return $query;
	  }
	  else if($masculino == "true"){
	       $query .= " AND (sexo = 'Masculino'";
	       if($feminino == "true"){
		    $query .= " OR sexo = 'Feminino'";
	       }	       
	       if($unissex == "true"){
		    $query .= " OR sexo = '-'";
	       }	       
	       $query .= ")";	       
	       return $query;
	  }
	  else if($feminino == "true"){
	       $query .= " AND (sexo = 'Feminino'";
	       if($unissex == "true"){
		    $query .= " OR sexo = '-'";
	       }	       
	       $query .= ")";	       
	       return $query;
	  }
	  else if($unissex == "true"){
	       $query .= " AND (sexo = '-')";
	       return $query;
	  }
	  else{
	       return $query;
	  }
     }
     
     function tratamentoMarca($query, $marcas) {
	  if($marcas != ""){
	       $query .= " AND ( ";
	       for($i = 0; $i < count($marcas); $i++){
		    $query .= "marca = '$marcas[$i]' OR ";
	       }
	       
	       $query = substr($query, 0, -3);
	       $query .= ")";
	       return $query;	       
	  }
	  else{	       
	       return $query;
	  }
     }
     
     function tratamentoTipo($query, $tipos) {
	  if($tipos != ""){
	       $query .= " AND ( ";
	       for($i = 0; $i < count($tipos); $i++){
		    $query .= "tipo = '$tipos[$i]' OR ";
	       }
	       
	       $query = substr($query, 0, -3);
	       $query .= ")";
	       return $query;	       
	  }
	  else{	       
	       return $query;
	  }
     }

     if ($n_comprados == "true") {
	  $queryNaoComprados = tratamentoSexo($queryNaoComprados, $masculino, $feminino, $unissex);
	  $queryNaoComprados = tratamentoMarca($queryNaoComprados, $marcas);
	  $queryNaoComprados = tratamentoTipo($queryNaoComprados, $tipos);
	  ?>
	  <script>
	       localStorage.setItem("queryNaoComprados", "<?php echo $queryNaoComprados ?>");
	  </script>
	  <?php
	  $dao->listaNaoComprados($queryNaoComprados);
     }

     if ($comprados == "true") {
	  $queryComprados = tratamentoSexo($queryComprados, $masculino, $feminino, $unissex);
	  $queryComprados = tratamentoMarca($queryComprados, $marcas);	  	  
	  $queryComprados = tratamentoTipo($queryComprados, $tipos);
	  ?>
	  <script>
	       localStorage.setItem("queryComprados", "<?php echo $queryComprados ?>");
	  </script>
	  <?php
	  $dao->listaComprados($queryComprados);
     }          


     