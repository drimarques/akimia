<?php
     class comprasDAO {

	  function cadastraListaCompra(Compra $compra) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $conn->query("INSERT INTO checklist_compras (tipo, sexo, produto, marca, modelo, cor, quantidade_total, tamanho, cliente, telefone, email)
	       VALUES ('{$compra->Tipo}', '{$compra->Sexo}', '{$compra->Produto}', '{$compra->Marca}', '{$compra->Modelo}', '{$compra->Cor}', '{$compra->QuantidadeTotal}', '{$compra->Tamanho}', '{$compra->Cliente}', '{$compra->Telefone}', '{$compra->Email}')");

	       unset($conn);
	  }

	  function listaCompras() {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $retorno = $conn->query("SELECT * FROM checklist_compras ORDER BY marca, produto asc");

	       $tabela = "<table class='table-responsive table-hover tabelas2' id='table-lista-compras'>";
	       $tabela .= '<thead>
			      <tr>
				   <th>Marca</th>
				   <th>Sexo</th>
				   <th>Tipo</th>				   
				   <th>Produto</th>				 
				   <th>Modelo</th>
				   <th>Cor</th>
				   <th>Tamanho</th>
				   <th>Quantidade</th>				   			      
			      </tr>
			   </thead>';

	       while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
		    $tabela .= '<tr>
				   <td>' . $linhas[marca] . '</td>
				   <td>' . $linhas[sexo] . '</td>
				   <td>' . $linhas[tipo] . '</td>
				   <td>' . $linhas[produto] . '</td>				   
				   <td>' . $linhas[modelo] . '</td>
				   <td>' . $linhas[cor] . '</td>				   
				   <td>' . $linhas[tamanho] . '</td>
				   <td>' . $linhas[quantidade_total] . '</td>
			        </tr>';
	       }

	       $tabela .= "</table>";
	       unset($conn);

	       echo $tabela;
	  }

	  function getDadosCompra($id_item) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();

	       $retorno = $conn->query("SELECT * FROM checklist_compras WHERE id_compra = '$id_item'");
	       $linhas = $retorno->fetch(PDO::FETCH_ASSOC);

	       $compra = new Compra();
	       
	       $compra->setTipo($linhas[tipo]);
	       $compra->setSexo($linhas[sexo]);
	       $compra->setProduto($linhas[produto]);
	       $compra->setMarca($linhas[marca]);
	       $compra->setModelo($linhas[modelo]);
	       $compra->setCor($linhas[cor]);
	       $compra->setQuantidadeTotal($linhas[quantidade_total]);
	       $compra->setQuantidadeComprada($linhas[quantidade_comprada]);
	       $compra->setTamanho($linhas[tamanho]);
	       $compra->setComprado($linhas[comprada]);

	       $compra->setCliente($linhas[cliente]);
	       $compra->setTelefone($linhas[telefone]);
	       $compra->setEmail($linhas[email]);

	       unset($conn);
	       return $compra;
	  }
	  	 	 	 
	  function getIdsCompras() {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $retorno = $conn->query("SELECT * FROM checklist_compras ORDER BY id_item ASC");

	       $ids = array();

	       while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
		    array_push($ids, $linhas[id_compra]);
	       }

	       unset($conn);
	       return $ids;
	  }

//	  ******************************************** Mobile	  
	  function marcaComprado($id_item) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();

	       $compra = $this->getDadosCompra($id_item);
	       $conn->query("UPDATE checklist_compras SET comprada = 1, quantidade_comprada = '$compra->QuantidadeTotal' WHERE id_compra = '$id_item'");

	       unset($conn);
	  }

	  function marcaNaoComprado($id_item) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();

	       $conn->query("UPDATE checklist_compras SET comprada = 0, quantidade_comprada = 0 WHERE id_compra = '$id_item'");

	       unset($conn);
	  }

	  function listaNaoComprados($query) {
	       $distinctMarcas = str_replace("*", "distinct marca", $query);
	       $distinctMarcas .= " ORDER BY marca ASC";
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $marcas = $conn->query($distinctMarcas);

	       //Verifica se existe resultados
	       if ($marcas->rowCount() > 0) {
		    $lista = "";

		    //Percorrendo as marcas e montando as tabelas
		    while ($marca = $marcas->fetch(PDO::FETCH_ASSOC)) {

			 $lista.= "<div class='marca'>$marca[marca]</div>";
			 //Remove a marca e adiciona uma nova marca
			 $marcaAnterior = $marca[marca];
			 $queryNova = str_replace(" AND marca = '$marcaAnterior'", " ", $query);
			 $queryNova .= " AND marca = '$marca[marca]'";
			 $retorno = $conn->query($queryNova);

			 while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {

			      if ($linhas[quantidade_total] > $linhas[quantidade_comprada]) {
				   $quantidade_restante = $linhas[quantidade_total] - $linhas[quantidade_comprada];

				   $lista .= "<div class='produto_lista' id='$linhas[id_compra]' onclick=\"marcaItemNaoComprado(this)\">
						  <div class='titulo_produto'>$linhas[produto] $linhas[modelo] - $linhas[cor]</div>
						  <span class='descricao_produto'>Tamanho: $linhas[tamanho] - Quantidade: $quantidade_restante</span>						  
					     </div>
					     <div onclick=\"abreModalClienteCompra(this)\" id='$linhas[id_compra]' class='abre_cliente_compra'><img src='img/3pontos.png' alt='Cliente compra'></div>
					     <div class='clear'></div>";
			      }
			 }
		    }

		    echo "<div id='separacao-nao-comprados' class='separacao'>Não comprados</div>";
		    echo $lista;

		    unset($conn);
	       } else {
		    echo "";
	       }
	  }

	  function listaComprados($query) {
	       $distinctMarcas = str_replace("*", "distinct marca", $query);
	       $distinctMarcas .= " ORDER BY marca ASC";
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $marcas = $conn->query($distinctMarcas);

	       //Verifica se existe resultados
	       if ($marcas->rowCount() > 0) {
		    $lista = "";

		    //Percorrendo as marcas e montando as tabelas
		    while ($marca = $marcas->fetch(PDO::FETCH_ASSOC)) {

			 $lista.= "<div class='marca'>$marca[marca]</div>";
			 //Remove a marca e adiciona uma nova marca
			 $marcaAnterior = $marca[marca];
			 $queryNova = str_replace(" AND marca = '$marcaAnterior'", " ", $query);
			 $queryNova .= " AND marca = '$marca[marca]'";
			 $retorno = $conn->query($queryNova);

			 while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
			      $lista .= "<div class='produto_lista' id='$linhas[id_compra]' onclick=\"marcaItemJaComprado(this)\">
					     <div class='titulo_produto'><s>$linhas[produto] $linhas[modelo] - $linhas[cor]</s></div>
					     <span class='descricao_produto  '><s>Tamanho: $linhas[tamanho] - Quantidade: $linhas[quantidade_comprada]</s></span>
					</div>
					<div onclick=\"abreModalClienteCompra(this)\" id='$linhas[id_compra]' class='abre_cliente_compra'><img src='img/3pontos.png' alt='Cliente compra'></div>
					<div class='clear'></div>";
			 }
		    }

		    echo "<div class='separacao'>Comprados</div>";
		    echo $lista;

		    unset($conn);
	       } else {
		    echo "";
	       }
	  }

	  function listaComprasBusca($busca) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();

	       //Monta SQL de busca
	       $sql = "SELECT * FROM checklist_compras WHERE (";
	       $sql .= "tipo LIKE '%$busca%' OR ";
	       $sql .= "sexo LIKE '%$busca%' OR ";
	       $sql .= "produto LIKE '%$busca%  ' OR ";
	       $sql .= "marca LIKE '%$busca%' OR ";
	       $sql .= "modelo LIKE '%$busca%' OR ";
	       $sql .= "cor LIKE '%$busca%' OR ";
	       $sqlLike = substr($sql, 0, -4);

	       $sqlLike2 = "$sqlLike) ORDER BY marca ASC";

	       $retorno = $conn->query($sqlLike2);

	       $lista = "";
	       $totalRegistros = $retorno->rowCount();
	       /* Se tiverem registros encontrados, mostra a tabela */
	       if ($totalRegistros > 0) {
		    while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
			 $titulo_produto = "";
			 $descricao_produto = "";

			 //Verifica se está comprado
			 if ($linhas['comprada'] == '1') {
			      $titulo_produto = "<div class='titulo_produto'><s>$linhas[marca]<br>$linhas[produto] $linhas[modelo] - $linhas[cor]</s></div>";
			      $descricao_produto = "<span class='descricao_produto'><s>Tamanho: $linhas[tamanho] - Quantidade: $linhas[quantidade_total]</s></span>";
			 } else {
			      $titulo_produto = "<div class='titulo_produto  '>$linhas[marca]<br>$linhas[produto] $linhas[modelo] - $linhas[cor]</div>";
			      $descricao_produto = "<span class='descricao_produto'>Tamanho: $linhas[tamanho] - Quantidade: $linhas[quantidade_total]</span>";
			 }



			 $lista .= "<div class='produto_lista' id='$linhas[id_compra]' onclick=\"marcaItem(this)\">
					$titulo_produto
					$descricao_produto
				   </div>";
		    }

		    echo $lista;
	       } else {
		    echo "";
	       }
	  }

	  function getMarcas($query) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $retorno = $conn->query($query);
	       while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
		    echo "<div class='filtro_marca' onclick='selecionaMarca(this)'>$linhas[marca]</div>";
	       }
	  }

	  function getTipos($query) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $retorno = $conn->query($query);
	       while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
		    echo "<div class='filtro_tipo' onclick='selecionaTipo(  this)'>$linhas[tipo]</div>";
	       }
	  }

	  function upd_quantidadeComprada(Compra $compra, $id_item) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();

	       if ($conn->query("UPDATE checklist_compras SET quantidade_total = '{$compra->QuantidadeTotal}', quantidade_comprada = '{$compra->QuantidadeComprada}', comprada = '{$compra->Comprado}' WHERE id_compra = '$id_item'")) {
		    unset($conn);
		    return true;
	       } else {
		    unset($conn);
		    return false;
	       }
	  }
	  
	  function deleteItemCompra($id_item) {
	  	$conexao = new Conexao();
	  	$conn = $conexao->getConexao();
	  
	  	$conn->query("DELETE FROM checklist_compras WHERE id_compra = '$id_item'");
	  
	  	unset($conn);
	  }

     }
     