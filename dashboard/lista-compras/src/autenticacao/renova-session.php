<?php

     session_start();
     date_default_timezone_set('Brazil/East');
     date_default_timezone_set('America/Sao_Paulo');

     require_once '../dao/usuarioDAO.php';
     
     $usuarioDAO = new UsuarioDAO();
     $usuarioDAO->validaTempoSession();