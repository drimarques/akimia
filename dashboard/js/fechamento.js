function getSelectFechamentos(idFechamento){
	$.ajax({
		url: "../src/servicos/select-fechamentos.php",
		data : {"idFechamento" : idFechamento},			
		type : 'POST',
		context : $("select#select-fechamentos"),
		success : function(retorno) {
			$("select#select-fechamentos").html(retorno);
		},
		error : function(retorno) {
			
		}
	});
}

function abreModalFechamento(idFechamento){			
	if(idFechamento === undefined){		
		idFechamento = getLocalStorage("idUltimoFechamento");
		setLocalStorage("idFechamentoResumo", idFechamento);
	}
	else{
		setLocalStorage("idFechamentoResumo", idFechamento);
	}
	
	localStorage.removeItem("valorAnterior");
	
	if(idFechamento != null && idFechamento != "null" && idFechamento != ""){
		preparandoModal(true);
				
		//getTabelaResumo(idFechamento);
		getImagemCadeado(idFechamento);		
		getTabelaParametrosCalculo(idFechamento);
		getInfFechamento(idFechamento);
		getSelectFechamentos(idFechamento);
		getTotalRegistros(idFechamento);
		
		/*Lista de compras*/		
		$.ajax({
			url: "../src/servicos/tabelas/tabela-lista-de-compras.php",
			data : {"idFechamento" : idFechamento},			
			type : 'POST',
			context : $("#tabela-fechamento tbody"),
			success : function(retorno) {
				$("#tabela-fechamento tbody").html(retorno);
				
				notificacao(false, null);
				
				$('.dolar').maskMoney({prefix:'$ ', thousands:',', decimal:'.', affixesStay: false});
				$('.real').maskMoney({prefix:'R$ ', thousands:'.', decimal:',', affixesStay: false});
				$('.porcentagem').maskMoney({prefix:'% ', thousands:'.', decimal:',', affixesStay: false});
				
				$('#modal-fechamento').bPopup({
			        positionStyle: 'fixed',
			        onOpen: function () {
			        	
			        },
			        onClose: function () {
			            //console.log("modal foi fechado");
			        }
			    },
			    function () {		    			    
			    	preparandoModal(false);		    			    
			    });				
			},
			error : function(retorno) {
				return false;
			}
		});					
		
	}
	else{
		alert("Não há fechamento em aberto");
	}
	
}

function mudaCorBg(elemento, valor){	
	if(valor == "0.00" || valor == "0"){			
		$(elemento).addClass("bg-azul");
		$(elemento).removeClass("bg-verde");
		
		return false;
	}
	else {
		$(elemento).removeClass("bg-azul");
		$(elemento).addClass("bg-verde");
		
		return true;
	}
}

function limpaRelatorioFechamento(){
	$("#tabela-fechamento tbody").html("");
}

$(function(){
	
	// abreModalFechamento(8);
	
	/*Quando atualiza algum parametro de cálculo*/
	$("#modal-fechamento").delegate(".upd-parametro-calculo", "blur", function(){										
		var trParametroCalculo = $(this).parent().parent();
		var tdParametro = $(this).parent();
		
		var idParametroCalculo = $(trParametroCalculo).attr("class").substring(0, $(trParametroCalculo).attr("class").length - 18);		
		var idParametroAlterado = $(this).attr("id");
										
		//Se for uma porcentagem... tira "% " senão tira "R$ "
		var valor = ($(this).val().search("%") >= 0) ? $(this).val().substring(2, $(this).val().length) : $(this).val().substring(3, $(this).val().length); 		

		var valor = valor.replace(".", "");
		var valor = valor.replace(",", ".");
		
		if(valor != getLocalStorage("valorAnterior")){
			if(mudaCorBg(tdParametro, valor)){
				notificacao(true, "Atualizando valores da lista de compras...");
				
				limpaRelatorioFechamento();
				
				var fechamentoAberto = $("#id-fechamento-hidden").text();		
				/*Esse ajax retorna a tabela do fechamento recalculada*/
				$.ajax({
					url: "../src/servicos/upd-parametro-calculo.php",
					data : {"idFechamento": fechamentoAberto, "idParametroCalculo" : idParametroCalculo, "nomeParametro": idParametroAlterado, "valor": valor},			
					type : 'POST',
					context : $("#tabela-fechamento tbody"),
					success : function(retorno) {
						$("#tabela-fechamento tbody").html(retorno);
						
						$('.dolar').maskMoney({prefix:'$ ', thousands:',', decimal:'.', affixesStay: false});
						$('.real').maskMoney({prefix:'R$ ', thousands:'.', decimal:',', affixesStay: false});
						$('.porcentagem').maskMoney({prefix:'% ', thousands:'.', decimal:',', affixesStay: false});
						
						notificacao(false);				
					},
					error : function(retorno) {
						
					}
				});
			}
		}					
	});
	
	$("#modal-fechamento").delegate(".upd-parametro-calculo", "focus", function(){										
		var valorAnterior = $(this).val();
		var valorAnterior = valorAnterior.replace(".", "");
		var valorAnterior = valorAnterior.replace(",", ".");
		
		setLocalStorage("valorAnterior", valorAnterior);
	});
	
	$("#modal-fechamento").delegate(".upd-produto-lista", "focus", function(){										
		var trProduto = $(this).parent().parent();
		var tdProduto = $(this).parent();
						
		var idProdutoAlterado = $(trProduto).attr("id").substring(0, $(trProduto).attr("id").length - 9);		
		var nameProdutoAlterado = $(this).attr("name");
					
		//console.log(nameProdutoAlterado);
		
		//Se for uma porcentagem... tira "% " senão tira "R$ "
		if(nameProdutoAlterado == "custo_dolar"){			
			valorAnterior = $(this).val();
			valorAnterior = valorAnterior.replace(",", "");
		}		
		else if(nameProdutoAlterado == "venda"){					
			valorAnterior = $(this).val();
			valorAnterior = valorAnterior.replace(".", "");
			valorAnterior = valorAnterior.replace(",", ".");
		}
				
		setLocalStorage("valorAnterior", valorAnterior);
	});
	
	
	/*Quando atualiza algum item da tabela*/
	$("#modal-fechamento").delegate(".upd-produto-lista", "blur", function(){		
		var trProduto = $(this).parent().parent();
		var tdProduto = $(this).parent();
						
		var idLinhaProduto = $(trProduto).attr("id");
		var idProdutoAlterado = $(trProduto).attr("id").substring(0, $(trProduto).attr("id").length - 9);		
		var nameProdutoAlterado = $(this).attr("name");
						
		//Se for uma porcentagem... tira "% " senão tira "R$ "
		if(nameProdutoAlterado == "custo_dolar"){			
			valor = $(this).val().substring(2, $(this).val().length);
			valor = valor.replace(",", "");
		}		
		else if(nameProdutoAlterado == "venda"){					
			valor = $(this).val().substring(3, $(this).val().length);
			valor = valor.replace(".", "");
			valor = valor.replace(",", ".");
		}
				
		if(valor != getLocalStorage("valorAnterior")){
			if(mudaCorBg(tdProduto, valor)){
				notificacao(true, "Atualizando valores do produto...");
				
				$.ajax({
					url: "../src/servicos/upd-produto-lista.php",
					data : {"idProdutoAlterado" : idProdutoAlterado, "campo": nameProdutoAlterado, "valor": valor},			
					type : 'POST',
					context : $("#tabela-fechamento tbody #"+ idLinhaProduto),
					success : function(retorno){						
						$("#tabela-fechamento tbody #"+ idLinhaProduto).html(retorno);												
						
						$('.dolar').maskMoney({prefix:'$ ', thousands:',', decimal:'.', affixesStay: false});
						$('.real').maskMoney({prefix:'R$ ', thousands:'.', decimal:',', affixesStay: false});
						$('.porcentagem').maskMoney({prefix:'% ', thousands:'.', decimal:',', affixesStay: false});
						
						notificacao(false);
					},
					error : function(retorno) {
						
					}
				});
			}		
		}
	});
	
	
	/*Quando atualiza o iof da tabela*/
	$("#modal-fechamento").delegate(".upt-iof-produto-lista", "change", function(){
		notificacao(true, "Atualizando valores do produto...");
						
		var trProduto = $(this).parent().parent();
		var tdProduto = $(this).parent();
		var valor = $(this).val();
		
		var idLinhaProduto = $(trProduto).attr("id");
						
		var idProdutoAlterado = $(trProduto).attr("id").substring(0, $(trProduto).attr("id").length - 9);		
		var nameProdutoAlterado = $(this).attr("name");
		
		$.ajax({
			url: "../src/servicos/upd-produto-lista.php",
			data : {"idProdutoAlterado" : idProdutoAlterado, "campo": nameProdutoAlterado, "valor": valor},			
			type : 'POST',
			context : $("#tabela-fechamento tbody #"+ idLinhaProduto),
			success : function(retorno) {							
				$("#tabela-fechamento tbody #"+ idLinhaProduto).html(retorno);
				
				$('.dolar').maskMoney({prefix:'$ ', thousands:',', decimal:'.', affixesStay: false});
				$('.real').maskMoney({prefix:'R$ ', thousands:'.', decimal:',', affixesStay: false});
				$('.porcentagem').maskMoney({prefix:'% ', thousands:'.', decimal:',', affixesStay: false});
				
				notificacao(false);
			},
			error : function(retorno) {
				
			}
		});
	});
	
	/*Quando seleciona outro fechamento no select*/
	$("#select-fechamentos").change(function(){
		var idFechamentoSelecionado = $(this).val();
		
		var modalFechar = $("#modal-fechamento").bPopup();
        modalFechar.close();					        
        abreModalFechamento(idFechamentoSelecionado);
	});
	
	$("#modal-fechamento").scroll(function() {		
		if($("#tabela-fechamento").offset().top < 20){
			$("#subir-top-fechamento").fadeIn("fast");
		}
		else{
			$("#subir-top-fechamento").css({"display": "none"});
		}
	});
	
	$("#subir-top-fechamento").click(function(){				
		$('#modal-fechamento').animate({
			scrollTop : $("#top-fechamento").offset().top
		}, "swing");
	});

	$("#tabela-fechamento").delegate(".abre-modal-qtd-lista-compras", "click", function(){
		var imgFechamento = $(".cadeado-fechamento").attr("src");

		if(imgFechamento == "../img/cadeado-open.png") {
			var modalQtd = $(this).parent().next();
			$(modalQtd).fadeToggle("fast");	
		} else {
			notificacao(true, "Fechamento encerrado! Não é possível editar a quantidade.");
			setTimeout(function(){
				notificacao(false, null);
			}, 3000);
		}
	});

	$("#tabela-fechamento").delegate(".form-quantidade-lista-compras", "submit", function(e){
		e.preventDefault();
		notificacao(true, "Atualizando quantidade de produtos...");
		var idCompra = $(this).parent().attr("id");
		var idProduto = idCompra.replace("modalQuantidade", "");
		var idInput = "#imputQtd"+idProduto;

		var qtd = $(idInput).val();
		$(this).parent().fadeOut("fast");
	
		$.ajax({
			url: "../src/servicos/upd-produto-lista.php",
			data : {"idProdutoAlterado" : idProduto, "campo": "quantidade_total", "valor": qtd},			
			type : 'POST',
			context : $("#tabela-fechamento tbody #"+ idProduto),
			success : function(retorno) {
				$("#tabela-fechamento tbody #"+idProduto+"idProduto" ).html(retorno);
				
				$('.dolar').maskMoney({prefix:'$ ', thousands:',', decimal:'.', affixesStay: false});
				$('.real').maskMoney({prefix:'R$ ', thousands:'.', decimal:',', affixesStay: false});
				$('.porcentagem').maskMoney({prefix:'% ', thousands:'.', decimal:',', affixesStay: false});
				
				notificacao(false);
			},
			error : function(retorno) {
				
			}
		});
	});
});
