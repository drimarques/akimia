function buscaFechamentoExportar(busca){
	notificacao(true, "Buscando fechamento...");
		
	$("#tabela-exportar-fechamento tbody").html("");
	
	$.ajax({
		url: "../src/servicos/tabelas/tabela-busca-fechamento.php",
		data : {"busca": busca},			
		type : 'POST',
		context : $("#tabela-exportar-fechamento tbody"),
		success : function(retorno) {			
			$("#tabela-exportar-fechamento tbody").html(retorno);
						
			notificacao(false);			
		},
		error : function(retorno) {
			
		}
	});	
}

$(function(){
	$("#txt-busca-fechamento").keypress(function(e) {				
		if (e.which === 13) {										
			e.preventDefault();
			var busca = $(this).val();
			buscaFechamentoExportar(busca);
		}
	});

	$("#btn-buscar-fechamento").click(function(e) {
		e.preventDefault();
		var busca = $("#txt-busca-fechamento").val();
		buscaFechamentoExportar(busca);
	});
});
