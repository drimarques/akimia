function abreModalExportarFechamento(){
	preparandoModal(true);
	
	$(".linha-selecionada").each(function(){
		$(this).removeClass("linha-selecionada");
	});
	
	$.ajax({
		url: "../src/servicos/tabelas/tabela-exportar-fechamentos.php",
		//data : {"idFechamento" : idFechamento, "query" : query},			
		type : 'POST',
		context : $("#tabela-exportar-fechamento tbody"),
		success : function(retorno) {			
			$('#modal-exportar-fechamento').bPopup({
		        positionStyle: 'fixed',
		        onOpen: function () {
		        	$("#tabela-exportar-fechamento tbody").html(retorno);		        	
		        },
		        onClose: function () {
		            //console.log("modal foi fechado");
		        }
		    },
		    function () {		    			    
		    	preparandoModal(false);		    			    
		    });
		},
		error : function(retorno) {
			
		}
	});
	
	
};

$(function(){
	
	$("#tabela-exportar-fechamento").delegate(".seleciona-fechamento-export", "click", function(){		
		$(".linha-selecionada").each(function(){
			$(this).removeClass("linha-selecionada");
		});
		
		$(this).addClass("linha-selecionada");		
		$("#fechamento-download").fadeOut("fast");
	});
	
	$("#btn-exportar-fechamento").click(function(){
		var cont = 0;
		var idFechamento;
		
		$(".linha-selecionada").each(function(){
			idFechamento = $(this).attr("id");
			cont++;					
		});
		
		if(cont == 0){
			notificacao(true, "Por favor, selecione um fechamento para exportar.");
		}
		else{
			notificacao(true, "Aguarde, criando planilha...");
			
			$.ajax({
				url: "../src/servicos/exportacao/exportar-fechamento.php",
				data : {"idFechamento" : idFechamento},			
				type : 'POST',
				//context : $("#tabela-exportar-fechamento tbody"),
				success : function(retorno) {			
					notificacao(false);
					$("#fechamento-download").html("Arquivo criado com sucesso! <a href='../src/servicos/exportacao/"+retorno+"'>Clique aqui</a> para fazer o download.");
					$("#fechamento-download").fadeIn("fast");
				},
				error : function(retorno) {
					
				}
			});
		}
	});
})