function buscaProdutoFechamento(busca){
	notificacao(true, "Buscando produto...");
	var idFechamento = $("#id-fechamento-hidden").text();
	
	var query = "SELECT * FROM lista_de_compras WHERE (marca LIKE '%"+ busca +"%' OR sexo LIKE '%"+ busca +"%' OR produto LIKE '%"+ busca +"%' OR modelo LIKE '%"+ busca +"%' OR cor LIKE '%"+ busca +"%') AND id_fechamento = "+ idFechamento + " ORDER BY marca ASC";			
	getTotalRegistros(idFechamento, query);
	
	limpaRelatorioFechamento();
	
	$.ajax({
		url: "../src/servicos/tabelas/tabela-busca-lista-de-compras.php",
		data : {"idFechamento" : idFechamento, "query" : query},			
		type : 'POST',
		context : $("#tabela-fechamento tbody"),
		success : function(retorno) {			
			$("#tabela-fechamento tbody").html(retorno);
			
			$('.dolar').maskMoney({prefix:'$ ', thousands:',', decimal:'.', affixesStay: false});
			$('.real').maskMoney({prefix:'R$ ', thousands:'.', decimal:',', affixesStay: false});
			$('.porcentagem').maskMoney({prefix:'% ', thousands:'.', decimal:',', affixesStay: false});
			
			notificacao(false);			
		},
		error : function(retorno) {
			
		}
	});	
}

$(function(){
	$("#txt-busca-produto").keypress(function(e) {				
		if (e.which === 13) {										
			e.preventDefault();
			var busca = $(this).val();
			buscaProdutoFechamento(busca);
		}
	});

	$("#btn-buscar-produto").click(function(e) {
		e.preventDefault();
		var busca = $("#txt-busca-produto").val();
		buscaProdutoFechamento(busca);
	});
});
