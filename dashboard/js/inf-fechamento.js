function getInfFechamento(idFechamento){	
	$.ajax({
		url: "../src/servicos/inf-fechamento.php",
		data : {"idFechamento" : idFechamento},			
		type : 'POST',
		context : $(".inf-fechamento-modal"),
		success : function(retorno) {					
			$(".inf-fechamento-modal").html(retorno);
			//id-fechamento-hidden -- saber qual fechamento está aberto se precisar...
		},
		error : function(retorno) {
			
		}
	});
}