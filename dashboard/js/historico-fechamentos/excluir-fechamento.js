function abreModalExcluirFechamento(idFechamento, nomeFechamento){
	notificacaoExcluir(false, null);
	
	$("#id-fechamento-excluir").val(idFechamento);
	$("#nome-fechamento-excluir").html("Deseja realmente excluir o fechamento <b>" + nomeFechamento + "</b>?");
	
	$('#modal-excluir-fechamento').bPopup({
        positionStyle: 'fixed',        
        onOpen: function () {
        			        	
        },
        onClose: function () {        
        }
    },
    function () {		    			    
    			    			    
    });
}

$(function(){
	$("#btn-excluir-fechamento").click(function(e){
		e.preventDefault();
		
		notificacaoExcluir(true, "Aguarde... excluindo fechamento.");		
        var idFechamento = $("#id-fechamento-excluir").val();               
        
        $.ajax({
    		url: "../src/servicos/excluir-fechamento.php",
    		data : {"idFechamento": idFechamento},			
    		type : 'POST',
    		//context : $("#tabela-historico-fechamentos tbody"),
    		success : function(retorno) {    			
    			notificacaoExcluir(false, null);    			
    			$("#"+idFechamento).css({"display": "none"});    			
    			var modalFechar = $("#modal-excluir-fechamento").bPopup();
    	        modalFechar.close();
    		},
    		error : function(retorno) {
    			
    		}
    	});
	});
	
	$("#btn-cancelar").click(function(){
        var modalFechar = $("#modal-excluir-fechamento").bPopup();
        modalFechar.close();
	});
});
