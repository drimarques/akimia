function buscaFechamentoHistorico(busca){
	notificacao(true, "Buscando fechamento...");
		
	$("#tabela-historico-fechamentos tbody").html("");
	
	$.ajax({
		url: "../src/servicos/tabelas/tabela-busca-fechamento-historico.php",
		data : {"busca": busca},			
		type : 'POST',
		context : $("#tabela-historico-fechamentos tbody"),
		success : function(retorno) {			
			$("#tabela-historico-fechamentos tbody").html(retorno);
			notificacao(false);			
		},
		error : function(retorno) {
			
		}
	});	
}

$(function(){
	$("#txt-busca-fechamento-historico").keypress(function(e) {				
		if (e.which === 13) {										
			e.preventDefault();
			var busca = $(this).val();
									
			buscaFechamentoHistorico(busca);			
		}
	});

	$("#btn-buscar-fechamento-historico").click(function(e) {
		e.preventDefault();
		var busca = $("#txt-busca-fechamento-historico").val();
		buscaFechamentoHistorico(busca);
	});
});
