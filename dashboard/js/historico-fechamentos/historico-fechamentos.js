function abreModalHistoricoFechamentos(){
	preparandoModal(true);
	
	$(".linha-selecionada").each(function(){
		$(this).removeClass("linha-selecionada");
	});
	
	$('#modal-historico-fechamentos').bPopup({
        positionStyle: 'fixed',
        onOpen: function () {        			        
        },
        onClose: function () {
            //console.log("modal foi fechado");
        }
    },
    function () {		    			    
    	preparandoModal(false);		    			    
    });	
	
/*	$.ajax({
		url: "../src/servicos/tabelas/tabela-historico-fechamentos.php",
		//data : {"idFechamento" : idFechamento, "query" : query},			
		type : 'POST',
		context : $("#tabela-exportar-fechamento tbody"),
		success : function(retorno) {			
			$('#modal-historico-fechamentos').bPopup({
		        positionStyle: 'fixed',
		        onOpen: function () {
		        	$("#tabela-historico-fechamentos tbody").html(retorno);		        	
		        },
		        onClose: function () {
		            //console.log("modal foi fechado");
		        }
		    },
		    function () {		    			    
		    	preparandoModal(false);		    			    
		    });			
		},
		error : function(retorno) {
			
		}
	});	*/
}

$(function(){
	//abreModalHistoricoFechamentos();
	
	$("#tabela-historico-fechamentos").delegate(".seleciona-fechamento-historico", "click", function(){						
		$(this).toggleClass("linha-selecionada");
	});
	
	$("#tabela-historico-fechamentos").delegate(".edita-fechamento", "click", function(){						
		var linhaClicada = $(this).parent().parent();
		var idFechamento = $(linhaClicada).attr("id");
		var statusFechamento = $(linhaClicada).children()[3];
		
		if($(statusFechamento).text() == "ENCERRADO"){
			notificacao(true, "Este fechamento está encerrado, não é possível editá-lo.");
			
			setTimeout(function(){
				notificacao(false, null);
			}, 5000);
		}
		else{
			notificacao(true, "Aguarde... abrindo fechamento.");
			
			setTimeout(function(){
				abreModalFechamento(idFechamento);
			}, 2500);			
						
		}
	});
	
	$("#tabela-historico-fechamentos").delegate(".excluir-fechamento", "click", function(){						
		var linhaClicada = $(this).parent().parent();
		var idFechamento = $(linhaClicada).attr("id");
		var nomeFechamento = $(linhaClicada).children()[0];
		var nomeFechamentoText = $(nomeFechamento).text();
		abreModalExcluirFechamento(idFechamento, nomeFechamentoText);		
	});
	
	
	$("#btn-gerar-grafico").click(function(){
		var fechamentos = new Array();
		$(".linha-selecionada").each(function(){
			fechamentos.push($(this).attr("id"));
		});
		
		if(fechamentos.length > 0){
			notificacao(true, "Aguarde, criando gráfico...");		
			$("#grafico").html("");
			
			$.ajax({
				url: "../src/servicos/get-grafico-historico.php",
				data : {"fechamentos" : fechamentos},			
				type : 'POST',
				context : $("#grafico"),
				success : function(retorno) {
					$("#grafico").html(retorno);			
					mostraGrafico();
					notificacao(false, null);
				},
				error : function(retorno) {
					
				}
			});
		}
		else{
			notificacao(true, "Por favor, selecione ao menos um fechamento.");
			setTimeout(function(){
				notificacao(false, null);
			}, 3000);
		}
		
			
	});	
})