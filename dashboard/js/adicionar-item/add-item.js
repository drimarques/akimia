function validaCampos(){
	var value;
	var flErro = false;		
	
	$(".campo-add-item").removeAttr("style");
	$(".campo-add-item").each(function(){
		value = $(this).val();
		if(value == ""){			
			$(this).css({"border": "solid 1px red"});
			flErro = true;
		}
	});
	
	//flErro = false;	
	return flErro;
}

function cadastraItem(){	
	var marca = $("#txt-marca-produto").val();
	// var sexo = $("#txt-sexo-produto").val();
	// var tipo = $("#txt-tipo-produto").val();
	var produto = $("#txt-produto-produto").val();	
	// var modelo = $("#txt-modelo-produto").val();
	// var cor = $("#txt-cor-produto").val();
	// var tamanho = $("#txt-tamanho-produto").val();
	var quantidade = $("#txt-quantidade-produto").val();
	var custoEua = $("#txt-custo-eua-produto").val();
	var venda = $("#txt-venda-produto").val();
	// var cliente = $("#txt-cliente-produto").val();
	// var telefone = $("#txt-telefone-cliente-produto").val();
	// var email = $("#txt-email-cliente-produto").val();
	
	custoEua = custoEua.replace(",", "");	
	
	venda = venda.replace(".", "");
	venda = venda.replace(",", ".");
	
	var idFechamento = $("#id-fechamento-hidden").text();
	
	var cotacao_dolar = $("#cotacao-dolar").val();
	var imposto_usa = $("#imposto-usa").val();
	var iof = $("#iof-parametros-calculo").val();
	
	cotacao_dolar = cotacao_dolar.replace(".", "");
	cotacao_dolar = cotacao_dolar.replace(",", ".");
	
	imposto_usa = imposto_usa.replace(".", "");
	imposto_usa = imposto_usa.replace(",", ".");
	
	iof = iof.replace(".", "");
	iof = iof.replace(",", ".");
			
	notificacaoAddItem(true, "Aguarde, cadastrando produto...");
	
	$.ajax({
		url: "../src/servicos/add-item.php",
		// data : {"ehFechamento": "true", "idFechamento": idFechamento, "cotacao_dolar" : cotacao_dolar, "imposto_usa" : imposto_usa, "iof" : iof, "marca" : marca, "sexo" : sexo, "tipo" : tipo, "produto": produto, "modelo" : modelo, "cor" : cor, "tamanho" : tamanho, "quantidade" : quantidade, "custoEua" : custoEua, "venda" : venda, "cliente" : cliente, "telefone" : telefone, "email" : email},
		data : {"ehFechamento": "true", "idFechamento": idFechamento, "cotacao_dolar" : cotacao_dolar, "imposto_usa" : imposto_usa, "iof" : iof, "marca" : marca, "produto": produto, "quantidade" : quantidade, "custoEua" : custoEua, "venda" : venda},
		type : 'POST',
		//context : $("#tabela-fechamento tbody"),
		success : function(retorno) {						
			$('#form-add-item').each(function(){
				this.reset();
			});
			
			var modalFechar = $("#modal-adiciona-item-fechamento").bPopup();
	        modalFechar.close();
	        
	        var modalFechar2 = $("#modal-fechamento").bPopup();
	        modalFechar2.close();
	        
	        abreModalFechamento(idFechamento);
		},
		error : function(retorno) {
			return false;
		}
	});
}

$(function(){
	$("#adiciona-item").click(function(){				
		$('#modal-adiciona-item-fechamento').bPopup({
	        positionStyle: 'fixed',
	        onOpen: function () {
	        	
	        },
	        onClose: function () {

	        }
	    },
	    function () {		    			    
	    	$("#txt-marca-produto").focus();	    			    
	    });
	});
	
	$("#btn-add-item-fechamento").click(function (e) {
		e.preventDefault();	
		
		notificacaoAddItem(false, null);
		
		if(!validaCampos()){
			cadastraItem();
		}
		else{
			notificacaoAddItem(true, "Por favor preencha todos os campos.");
		}
    });
});