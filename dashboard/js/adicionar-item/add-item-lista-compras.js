function validaCamposListaCompras(){
	var value;
	var flErro = false;		
	
	$(".campo-add-item-lista-compras").removeAttr("style");
	$(".campo-add-item-lista-compras").each(function(){
		value = $(this).val();
		if(value == ""){			
			$(this).css({"border": "solid 1px red"});
			flErro = true;				
		}
	});
	
	//flErro = false;	
	return flErro;
}

function cadastraItemListaCompras(){	
	var marca = $("#txt-marca-lista-compras").val();
	var sexo = $("#txt-sexo-lista-compras").val();
	var tipo = $("#txt-tipo-lista-compras").val();
	var produto = $("#txt-lista-compras-lista-compras").val();	
	var modelo = $("#txt-modelo-lista-compras").val();
	var cor = $("#txt-cor-lista-compras").val();
	var tamanho = $("#txt-tamanho-lista-compras").val();
	var quantidade = $("#txt-quantidade-lista-compras").val();	
	var cliente = $("#txt-cliente-lista-compras").val();
	var telefone = $("#txt-telefone-cliente-lista-compras").val();
	var email = $("#txt-email-cliente-lista-compras").val();
			
	notificacaoAddItem(true, "Aguarde, cadastrando produto...");
	
	$.ajax({
		url: "../src/servicos/add-item.php",
		data : {"ehFechamento": "false", "marca" : marca, "sexo" : sexo, "tipo" : tipo, "produto": produto, "modelo" : modelo, "cor" : cor, "tamanho" : tamanho, "quantidade" : quantidade, "cliente" : cliente, "telefone" : telefone, "email" : email},			
		type : 'POST',
		//context : $("#tabela-fechamento tbody"),
		success : function(retorno) {
			notificacaoAddItem(true, "Item adicionado com sucesso.");
			
			setTimeout(function(){
				notificacaoAddItem(false, null);				
				$('#form-add-item-lista-compras').each(function(){
					this.reset();
				});
				
				var modalFechar = $("#modal-adiciona-item-avulso-lista-compras").bPopup();
		        modalFechar.close();
		        
		        location.href = "lista-compras/";
			}, 1000);
		},
		error : function(retorno) {
			return false;
		}
	});
}

$(function(){	
	$("#btn-add-item-lista-compras").click(function (e) {
		e.preventDefault();	
		
		notificacaoAddItem(false, null);
		
		if(!validaCamposListaCompras()){
			cadastraItemListaCompras();
		}
		else{
			notificacaoAddItem(true, "Por favor preencha todos os campos.");
		}
    });
});