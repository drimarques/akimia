function validaCamposRatreio(){
	var value;
	var flErro = false;		
	
	$(".campo-add-item-rastreio").removeAttr("style");
	$(".campo-add-item-rastreio").each(function(){
		value = $(this).val();
		
		if(value == ""){
			$(this).css({"border": "solid 1px red"});
			flErro = true;				
		}
	});
	
	//flErro = false;	
	return flErro;
}

function cadastraItemRastreio(){	
	var numeroPedido = $("#txt-numero-pedido-rastreio").val();
	var cliente = $("#txt-cliente-rastreio").val();
	var email = $("#txt-email-rastreio").val();
	var formaVenda = $("#forma-venda-cadastro-rastreio").children(":selected").val();
	var codRastreio = $("#txt-cod-rastreio").val();
	
	if(email == ""){
		email = "contato@akimia.com.br";
	}

	notificacaoAddItem(true, "Aguarde, cadastrando rastreio...");
	
	$.ajax({
		url: "../src/servicos/rastreamento/add-item-rastreio.php",
		data : {"numeroPedido": numeroPedido, "cliente": cliente, "email": email, "formaVenda": formaVenda, "codRastreio": codRastreio},			
		type : 'POST',
		success : function(retorno) {
			if(retorno == "erro"){
				notificacaoAddItem(true, "Erro ao adicionar o rastreio.");
			}
			else{
				notificacaoAddItem(true, "Rastreio adicionado com sucesso.");
				
				setTimeout(function(){
					notificacaoAddItem(false, null);				
					$('#form-add-rastreio').each(function(){
						this.reset();
					});
					
					var modalFechar = $("#modal-adiciona-rastreio").bPopup();
			        modalFechar.close();
			        
			        populaTabelaRastreamentos("andamento");
				}, 1200);
			}
		},
		error : function(retorno) {
			return false;
		}
	});
}

$(function(){	
	$("#btn-add-rastreio").click(function (e) {
		e.preventDefault();	
		
		notificacaoAddItem(false, null);
		
		if(!validaCamposRatreio()){
			cadastraItemRastreio();
		}
		else{
			notificacaoAddItem(true, "Por favor preencha todos os campos.");
		}
    });
});