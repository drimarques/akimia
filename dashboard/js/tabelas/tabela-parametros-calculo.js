function getTabelaParametrosCalculo(idFechamento){	
	$.ajax({
		url: "../src/servicos/tabelas/tabela-parametros-calculo.php",
		data : {"idFechamento" : idFechamento},			
		type : 'POST',
		context : $("#tabela-parametros-calculo tbody"),
		success : function(retorno) {					
			$("#tabela-parametros-calculo tbody").html(retorno);							
		},
		error : function(retorno) {
			
		}
	});
}