/*function getTabelaResumo(idFechamento) {
	if (idFechamento == undefined) {
		idFechamento = getLocalStorage("idUltimoFechamento");
	}
	
	setInterval(function() {
		if (idFechamento != null) {
			$.ajax({
				url : "../src/servicos/tabelas/tabela-resumo.php",
				data : {
					"idFechamento" : idFechamento
				},
				type : 'POST',
				context : $("#tabela-resumo tbody"),
				success : function(retorno) {
					$("#tabela-resumo tbody").html(retorno);

				},
				error : function(retorno) {

				}
			});
		}
	}, 500);
}*/

setInterval(function() {	
	var idFechamento = getLocalStorage("idFechamentoResumo");
	
	if (idFechamento != null) {
		$.ajax({
			url : "../src/servicos/tabelas/tabela-resumo.php",
			data : {
				"idFechamento" : idFechamento
			},
			type : 'POST',
			context : $("#tabela-resumo tbody"),
			success : function(retorno) {
				$("#tabela-resumo tbody").html(retorno);

			},
			error : function(retorno) {

			}
		});
	}
}, 500);