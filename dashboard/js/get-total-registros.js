function getTotalRegistros(idFechamento, query){	
	if(query == undefined){		
		query = "SELECT count(*) as totalRegistros FROM lista_de_compras WHERE id_fechamento = " + idFechamento;				
	}
	else{
		query = query.replace("SELECT *", "SELECT count(*) as totalRegistros");
	}
		
	$.ajax({
		url: "../src/servicos/total-registros.php",
		data : {"query" : query},			
		type : 'POST',
		context : $("#total-registros"),
		success : function(retorno) {
			$("#total-registros").html(retorno);
		},
		error : function(retorno) {
			
		}
	});
}

$(function(){
	
});
