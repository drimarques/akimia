function populaSelectFechamentosListaDeCompras(){
	$.ajax({
		url: "../src/servicos/select-fechamentos-lista-compras.php",
		//data : {"idFechamento" : idFechamento},			
		type : 'POST',
		context : $("#select-fechamentos-lista-compras"),
		success : function(retorno) {
			$("#select-fechamentos-lista-compras").html(retorno);
			
			$('#modal-lista-compras').bPopup({
		        positionStyle: 'fixed',
		        onOpen: function () {
		        	
		        },
		        onClose: function () {
		        	
		        }
		    },
		    function () {		    			    
		    	preparandoModal(false);
		    });
		},
		error : function(retorno) {
			
		}
	});
}


function abreModalListaCompras(){
	preparandoModal(true);
	verificaListaCompras();
}

function verificaListaCompras(){		
	$.ajax({
		url: "../src/servicos/lista-compras/checa-lista-compras.php",
		//data : {"idFechamento" : idFechamento},			
		type : 'POST',
		//context : $("#select-fechamentos-lista-compras"),
		success : function(retorno) {
			if(retorno == "nao_tem_lista"){			
				populaSelectFechamentosListaDeCompras();
			}
			else{				
				location.href = "lista-compras/";
			}					
		},
		error : function(retorno) {
			
		}
	});
}

function criaListaComprasPorFechamentoExistente(idFechamento){
	notificacao(true, "Aguarde, criando lista de compras...");
	
	$.ajax({
		url: "../src/servicos/lista-compras/cria-lista-compras.php",
		data : {"idFechamento" : idFechamento},			
		type : 'POST',
		//context : $("#select-fechamentos-lista-compras"),
		success : function(retorno) {
			notificacao(true, "Lista de compras criada com sucesso :D");
			
			setTimeout(function(){
				notificacao(false, null);
				$("#modal-lista-compras nav ul li").removeClass("menu-selecionado");
				$("#modal-lista-compras nav ul li").removeAttr("style");						
										
				location.href = "lista-compras/";
			}, 1000);		
		},
		error : function(retorno) {
			
		}
	});
}


function abreModalAddItensAvulsos(){
	$('#modal-adiciona-item-avulso-lista-compras').bPopup({
        positionStyle: 'fixed',
        onOpen: function () {
        	
        },
        onClose: function () {
        	
        }
    },
    function () {		    			    
    	$("#txt-marca-lista-compras").focus();
    });
}

$(function(){
	//Form submit
	$("#lista-compras").on('submit',(function(e) {
		e.preventDefault();		
		notificacao(true, "Aguarde... Importando planilha.");			
				
		var idMenu;		
		$(".menu-selecionado").each(function(){			 
		    idMenu = $(this).attr('id');		 		    
		});		
		
		if(idMenu == "menu-fechamento-existente"){
			var idFechamento = $("#select-fechamentos-lista-compras").val();
			if(idFechamento != null){								
				criaListaComprasPorFechamentoExistente(idFechamento);
			}
			else{
				notificacao(true, "Por favor, selecione um fechamento.");
			}
		}
		else if(idMenu == "menu-importar-planilha"){
			var planilha = $("#arquivo-lista-compras").val();			
			if(planilha == ""){
				notificacao(true, "Por favor, selecione um arquivo.");
			}		
			else{
				$.ajax({
					url: "../src/servicos/lista-compras/importa-lista-compras.php",
					type: "POST",
					data: new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					success: function(retorno){						
						if(retorno == "erro_arquivo_excel"){
							notificacao(true, "Somente arquivos no formado .xls");
						}
						else{										
							notificacao(true, "Planilha importada com sucesso :D");
							
							setTimeout(function(){
								notificacao(false, null);
								$("#modal-lista-compras nav ul li").removeClass("menu-selecionado");
								$("#modal-lista-compras nav ul li").removeAttr("style");						
														
								location.href = "lista-compras/";
							}, 1000);
						}
					}
				});
			}
		}								
	}));
	
	
	
	$("#modal-lista-compras nav ul li").click(function(){		
		$("#modal-lista-compras nav ul li").removeClass("menu-selecionado");
		$("#modal-lista-compras nav ul li").removeAttr("style");
		
		$(this).css({
			"background-color": "#e61b72",
			"color": "#FFF",
			
		    "-moz-transition": "all 300ms ease-in",    
	    	"-webkit-transition": "all 300ms ease-in",    
	    	"-o-transition": "all 300ms ease-in",
	    	"transition": "all 300ms ease-in"
		});
		
		$(this).addClass("menu-selecionado");		
		
		if($(this).attr("id") == "menu-fechamento-existente"){
			$("#sessao-usar-planilha").fadeOut("fast", function(){
				$("#sessao-usar-fechamento").fadeIn("fast");
			});
						
		}
		else if($(this).attr("id") == "menu-importar-planilha"){
			$("#sessao-usar-fechamento").fadeOut("fast", function(){
				$("#sessao-usar-planilha").fadeIn("fast");
			});			
		}
		else if($(this).attr("class") == "hover menu-adicionar-itens-avulsos menu-selecionado"){
			/*$("#sessao-usar-fechamento").fadeOut("fast");
			$("#sessao-usar-planilha").fadeOut("fast");*/
			abreModalAddItensAvulsos();
		}
	});

	
	$("#btn-limpar-lista").click(function(e){
		e.preventDefault();
		
		notificacao("Aguarde, limpando lista...");
		
		$.ajax({
			url: "../../src/servicos/lista-compras/limpar-lista-compras.php",
			//data : {"idFechamento" : idFechamento},			
			type : 'POST',
			success : function(retorno) {				
				notificacao("Lista de compras vazia.");
				setTimeout(function(){
					$("#modal-lista-compras nav ul li").removeClass("menu-selecionado");
					$("#modal-lista-compras nav ul li").removeAttr("style");
					
					location.href = "../index.php";
				}, 1000);
				
			},
			error : function(retorno) {
				
			}
		});
	});	
});