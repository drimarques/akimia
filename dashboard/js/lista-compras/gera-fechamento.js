$(function(){
	$("header").delegate("#abre-modal-gerar-fechamento", "click", function(){
		$('#modalGerarFechamento').modal('show');
	});
	
	
	$("#btn-gera-fechamento").click(function(e){
		e.preventDefault();
		
		var nomeFechamento = $("#txt-nome-fechamento-lista-compras").val();		
		if(nomeFechamento == ""){
			notificacao("Por favor, dê um nome ao fechamento.");
		}		
		else{
			notificacao("Aguarde... gerando fechamento.");
			
			$.ajax({
				url: "../../src/servicos/lista-compras/gera-fechamento.php",
				data : {"nomeFechamento" : nomeFechamento},			
				type : 'POST',
				//context : $("#select-fechamentos-lista-compras"),
				success : function(retorno) {
					notificacao("Fechamento gerado com sucesso.");
					
					location.href = "../index.php";
					
//					setTimeout(function(){
//						
//						var modalFechar = $("#modal-gera-fechamento").bPopup();
//				        modalFechar.close();
//				        
//				        var modalFechar2 = $("#modal-lista-compras").bPopup();
//				        modalFechar2.close();
//				        
//				        populaTabelaHistorico();				        
//				        abreModalFechamento(retorno);
//						
//					}, 1200);
				},
				error : function(retorno) {
					
				}
			});
		}
	});	
});