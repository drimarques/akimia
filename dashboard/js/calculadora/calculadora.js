function abreModalCalculadora() {
	var largura = $(document).width();

	//1024px
	if (largura <= 1008) {
		location.href = "calculadora.php";
	} else {
		$('#modal-calculadora').bPopup({
	        positionStyle: 'fixed',
	        onOpen: function () {        			        
	        },
	        onClose: function () {
	        }
	    },
	    function () {
	    	
	    });	
	}
}

function formatReal(int){
    var tmp = int+'';
    tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
    if( tmp.length > 6 )
            tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");

    return tmp;
}


function calculaTipoPeso() {	
	var tipoPeso = $("#select-tipo-peso").children(":selected").val();
	var valorPeso = $("#valor-peso-calculadora").val();
	var valorCalculado = 0;
	
	valorPeso = valorPeso.replace(",", ".");
	
	switch (tipoPeso) {
	case "kilos":
		valorCalculado = valorPeso * 100;
		break;
	case "gramas":
		valorCalculado = valorPeso / 1000 * 100;
		break;
	case "libras":
		valorCalculado = valorPeso * 0.454 * 100;
		break;
	case "pounds":
		valorCalculado = valorPeso * 0.454 * 100;
		break;
	case "litros":
		valorCalculado = valorPeso * 1.05 * 100;
		break;
	case "mililitros":
		valorCalculado = valorPeso / 1000 * 1.05 * 100;
	default:
		break;
	}
	
	return valorCalculado;
}

function realizaCalculos(valorProduto, valorDolar, percentualCalculo) {	
	var valorPeso = calculaTipoPeso();
	var dolar = valorDolar.replace(",", ".");
	var percentualLucro = percentualCalculo.replace(",", ".");
	
	//Inicio de calculos
	//Rever esse calculo
	var valorSemLucro = (valorProduto * 1.1338) * dolar;
	
	//Calculo para 30%
	var lucroFixo = valorSemLucro * 30 / 100;
	var totalLucroFixo = (valorSemLucro + lucroFixo) + valorPeso;
	totalLucroFixo = totalLucroFixo.toFixed(2).replace(".", "");
	
	//Calculo variavel
	var lucroVariavel = valorSemLucro * percentualLucro / 100; //lucro variavel
	var totalLucroVariavel = (valorSemLucro + lucroVariavel) + valorPeso;
	totalLucroVariavel = totalLucroVariavel.toFixed(2).replace(".", "");

	$('#lucro-fixo').text("R$ " + formatReal(totalLucroFixo));
	$('#lucro-variavel').text("R$ " + formatReal(totalLucroVariavel));
}

$(function (){
	//abreModalCalculadora();
	
	$('.decimal-calculadora').maskMoney({prefix:'', thousands:'', decimal:',', affixesStay: false});
	$('.real-calculadora').maskMoney({prefix:'R$ ', thousands:'.', decimal:',', affixesStay: false});
	$('.dolar-calculadora').maskMoney({prefix:'$ ', thousands:',', decimal:'.', affixesStay: false});
	$('.porcentagem-calculadora').maskMoney({prefix:'% ', thousands:'.', decimal:',', affixesStay: false});
		
	$("#form-calculadora").on('submit',(function(e) {
		e.preventDefault();	
		
		var valorProduto = $("#valor-produto-calculadora").val();
		valorProduto = valorProduto.replace("$ ", "");
		
		var valorDolar = $("#valor-dolar").val();
		valorDolar = valorDolar.replace("R$ ", "");
		
		var percentualCalculo = $("#percentual-lucro").val();
		percentualCalculo = percentualCalculo.replace("% ", "");
		
		realizaCalculos(valorProduto, valorDolar, percentualCalculo);
	}));
	
	$("#select-tipo-peso").change(function(){
		$("#form-calculadora").submit();
	})
	
	$(".campo-rolagem").focus(function(){
		var rolarAteAqui = "#label-" + $(this).attr("id");

		$("body").animate({
			scrollTop : $(rolarAteAqui).offset().top
		}, "swing");
	});
});