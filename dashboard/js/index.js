function populaTabelaHistorico(){
	$.ajax({
		url: "../src/servicos/tabelas/tabela-historico-fechamentos.php",
		//data : {"idFechamento" : idFechamento, "query" : query},			
		type : 'POST',
		context : $("#tabela-exportar-fechamento tbody"),
		success : function(retorno) {						
			$("#tabela-historico-fechamentos tbody").html(retorno);		    		
		},
		error : function(retorno) {
			
		}
	});
}

$(function(){
	$("#usuario-logado").click(function(){		
		var larguraBody = $("body").width();		
		if(larguraBody >= 584){
			$("#opcoes-usuario").fadeToggle("fast");
		}			
	});
	
	$('.modal-close').bind('click', function (e) {
        e.preventDefault();
        notificacao(false);
        
        //Obtem qual modal está sendo fechado
        var modal = ($(this).parent().parent());
        //Faz esse modal ser um objeto do tipo bPopup pra ele recceber o método close
        var modalFechar = $(modal).bPopup();
        //Fecha ele
        modalFechar.close();
        
        var idModal = $(modal).attr("id");
        switch (idModal) {
		case "modal-imp-plan-rastreamento":
			populaTabelaRastreamentos("andamento");
			break;
		case "modal-forma-venda":
			populaSelectFormaVenda();
			
			$("#menu-filtro-rastreamento ul li").each(function(){
				$(this).removeClass("selecionado");
			});
			
			$("#menu-filtro-rastreamento ul li:first-child").addClass("selecionado");
			populaTabelaRastreamentos("andamento");
			break;	
		default:
			break;
		}
    });
		
	$('#paineis').on('click', "#open-modal-plan", function (e) {		
        $('#modal-imp-plan').bPopup({
            positionStyle: 'fixed',
            onOpen: function () {                
                
            },
            onClose: function () {

            }
        },
        function () {
        	
        });
    });
		

	$('#paineis').on('click', "#open-modal-fechamento", function (e) {
		abreModalFechamento();
    });
	
	$('#paineis').on('click', "#open-modal-exportar", function (e) {
		abreModalExportarFechamento();
    });
	
	$('#paineis').on('click', "#open-modal-historico", function (e) {
		abreModalHistoricoFechamentos();
    });
	
	$('#paineis').on('click', "#open-modal-lista-compras", function (e) {
		abreModalListaCompras();
    });
	
	$('#paineis').on('click', "#open-modal-calculadora", function (e) {
		abreModalCalculadora();
    });
	
	$('#paineis').on('click', "#open-modal-rastreamento", function (e) {
		$('#modal-rastreamento').bPopup({
            positionStyle: 'fixed',
            onOpen: function () {                
                
            },
            onClose: function () {

            }
        },
        function () {
        	
        });
    });
	
//	$('#modal-rastreamento').bPopup({
//        positionStyle: 'fixed',
//        onOpen: function () {                
//            
//        },
//        onClose: function () {
//
//        }
//    },
//    function () {
//    	
//    });
	
	populaTabelaHistorico();	
	
})