function mascara(t, mask){
	 var i = t.value.length;
	 var saida = mask.substring(1,0);
	 var texto = mask.substring(i)
	 if (texto.substring(0,1) != saida){
		 t.value += texto.substring(0,1);
	 }
 }

function editaNomeFechamento(elemento){
	notificacao(true, "Aguarde... alterando nome do fechamento");
	
	$(elemento).parent().addClass("edit-nome-fechamento");
	
	var nomeNovo = $(elemento).val();
	var idFechamento;
	
	if($(elemento).attr("id") == "edit-nome-fech-modal-fechamento"){ 
		$(elemento).parent().html(nomeNovo + "<br>" + $(elemento).attr("name"));
		idFechamento = $("#id-fechamento-hidden").text();
	}
	else{		  
		$(elemento).parent().html(nomeNovo);
		idFechamento = $(".linha-selecionada").attr("id");
	}
	
	$.ajax({
		url : "../src/servicos/upd-fechamento.php",
		data : {"idFechamento": idFechamento, "campo" : "nome_fechamento", "valor": nomeNovo},			
		type : 'POST',
		//context : $(".inf-fechamento-modal"),
		success : function(retorno) {					
			notificacao(false, null);
		}
	});
}

function deslogaUsuario(){		
	location.href = "../src/autenticacao/logout.php";
}

function preparandoModal(estado){
	if(estado){
		$(".preparando-modal").fadeIn("fast");
	}
	else{
		$(".preparando-modal").fadeOut("fast");
	}
}


function notificacao(carregando, texto){
	if(carregando == true){
		$(".carregando").html(texto);
		$(".carregando").fadeIn("fast");				
	}
	else{
		$(".carregando").html("");
		$(".carregando").css({"display": "none"});			
	}	
}

function notificacaoExcluir(carregando, texto){
	if(carregando == true){
		$(".notificacao-excluir").html(texto);
		$(".notificacao-excluir").fadeIn("fast");				
	}
	else{
		$(".notificacao-excluir").html("");
		$(".notificacao-excluir").css({"display": "none"});			
	}
}

function notificacaoAddItem(carregando, texto){	
	if(carregando == true){
		$(".notificacao-add-item").html(texto);
		$(".notificacao-add-item").fadeIn("fast");				
	}
	else{
		$(".notificacao-add-item").css({"display": "none"});
		$(".notificacao-add-item").html("");
		$(".notificacao-add-item").css({"display": "none"});			
	}
					
	$('#modal-adiciona-item-fechamento').animate({
		scrollTop : $(".notificacao-add-item").offset().top
	}, "swing");
	
	$('#modal-adiciona-item-avulso-lista-compras').animate({
		scrollTop : $(".notificacao-add-item").offset().top
	}, "swing");
}



$(function() {
	$("html").mousemove(function() {
		$.ajax({
			url : "../src/autenticacao/renova-tempo-logado.php",
			success : function() {

			}
		});
	});
	
	/* Verificando tempo logado */
	setInterval(function() {
		$.ajax({
			url : "../src/autenticacao/valida-tempo-logado.php",
			success : function(retorno) {
				//console.log(retorno); //saber o tempo logado
				if (retorno === "faz logout") {
					deslogaUsuario();
				}
			}
		});

	}, 10000);
	
	/*Inpus file*/
	document.getElementById("arquivo-excel").onchange = function () {
	    document.getElementById("nome-arquivo").value = this.value;
	};
	
	document.getElementById("arquivo-lista-compras").onchange = function () {
	    document.getElementById("nome-arquivo-lista-compras").value = this.value;
	};
	
	document.getElementById("arquivo-rastreio").onchange = function () {
	    document.getElementById("nome-arquivo-rastreio").value = this.value;
	};
	
	$("body").delegate(".encerra-fechamento", "click", function(){					
		var valor = null;
		
		if($(this).hasClass("fechamento-aberto")){
			notificacao(true, "Aguarde... encerrando fechamento.");
			
			$(this).removeClass("fechamento-aberto");
			$(this).addClass("fechamento-encerrado");
			$(this).text("ENCERRADO");
			
			valor = "1";
		}
		else{
			notificacao(true, "Aguarde... reabrindo fechamento.");			
			$(this).removeClass("fechamento-encerrado");
			$(this).addClass("fechamento-aberto");
			$(this).text("ABERTO");
			
			valor = "0";
		}
				
		var idFechamento = $(this).parent().parent().attr("id");		
		
		$.ajax({
			url : "../src/servicos/upd-fechamento.php",
			data : {"idFechamento": idFechamento, "campo" : "encerrado", "valor": valor},			
			type : 'POST',
			//context : $(".inf-fechamento-modal"),
			success : function(retorno) {
				$(".data-termino-fechamento"+idFechamento).text(retorno);
				notificacao(false, null);
			}
		});
				
	});
	
	$("body").delegate(".edit-nome-fechamento", "click", function(){
		$(this).removeClass("edit-nome-fechamento");
		
		if($(this).attr("id") == "nome-fech-modal-fechamento"){
			var arrayTexto = $(this).html().split("<br>");
			var span = arrayTexto[1];			
			$(this).html("<input type='text' name='"+span+"' class='campo-form edita-nome-fechamento' id='edit-nome-fech-modal-fechamento' value='"+arrayTexto[0]+"'>");
			$("#edit-nome-fech-modal-fechamento").focus();
		}
		else{
			$(this).attr("name", $(this).text());
			var nomeAnterior = $(this).text();
			$(this).html("<input type='text' class='campo-form edita-nome-fechamento' id='edit-nome-fechamento' value='"+nomeAnterior+"'>");
			$("#edit-nome-fechamento").focus();
		}
	});
	
	
	$("body").delegate(".edita-nome-fechamento", "keypress", function(e){		
		if (e.which === 13) {										
			e.preventDefault();			
			editaNomeFechamento(this);
		}			
	});
	
	$('.tooltip').tooltipster({        	
        position: 'bottom',        
        theme: 'tooltipster-light'       
    });
	
});
