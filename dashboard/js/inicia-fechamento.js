$(function(){
	//Form submit
	$("#inicio-fechamento").on('submit',(function(e) {
		e.preventDefault();		
		notificacao(true, "Aguarde... Importando planilha.");			
				
		var nomeFechamento = $("#nome-fechamento").val();
		var planilha = $("#arquivo-excel").val();
				
		if(nomeFechamento == "" || planilha == ""){
			notificacao(true, "Preencha todos os campos.");
		}		
		else{		
			$.ajax({
				url: "../src/servicos/inicia-fechamento.php?nomeFechamento=" + nomeFechamento,
				type: "POST",
				data: new FormData(this),
				contentType: false,
				cache: false,
				processData: false,
				success: function(retorno){									
					if(retorno == "erro_arquivo_excel"){																		
						notificacao(true, "Somente arquivos no formado .xls");
					}
					else{
						populaTabelaHistorico();						
						notificacao(true, "Planilha importada com sucesso :D");
						
						setTimeout(function(){
							notificacao(false);						
							var modalFechar = $("#modal-imp-plan").bPopup();
					        modalFechar.close();					        
					        abreModalFechamento(retorno);
						}, 1000);											
					}						
				}
			});		
		}
		
	}));	
})