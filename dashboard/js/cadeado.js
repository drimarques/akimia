function getImagemCadeado(idFechamento){
	$.ajax({
		url: "../src/servicos/get-imagem-cadeado.php",
		data : {"idFechamento" : idFechamento},			
		type : 'POST',
		//context : $("#tabela-fechamento tbody"),
		success : function(retorno) {
			$(".cadeado-fechamento").attr("src", retorno);
			$(".cadeado-fechamento").attr("id", idFechamento+"cadeadoFechamento");
			
			/*Some com botao ADD caso fechamento encerrado*/
			if(retorno == "../img/cadeado-close.png"){
				$("#adiciona-item").fadeOut("fast");
			}
			else{
				$("#adiciona-item").fadeIn("fast");
			}
			
		},
		error : function(retorno) {
			return false;
		}
	});
}

$(function(){
	
	$("#modal-fechamento").on('click', ".cadeado-fechamento", function (e) {
		var idFechamento = $("#id-fechamento-hidden").text();
		var valor;
		var texto;
		
		if($(this).attr("src") == "../img/cadeado-close.png"){
			valor = "0";
			texto = "Aguarde, abrindo fechamento..."
		}
		else{
			valor = "1";
			texto = "Aguarde, encerrando fechamento..."
		}			
		
		notificacao(true, texto);
		
		$.ajax({
			url: "../src/servicos/upd-fechamento.php",
			data : {"idFechamento" : idFechamento, "campo": "encerrado", "valor": valor},			
			type : 'POST',
			//context : $("#tabela-fechamento tbody"),
			success : function(retorno) {
				
				var modalFechar = $("#modal-fechamento").bPopup();
		        modalFechar.close();					        
		        abreModalFechamento(idFechamento);
			},
			error : function(retorno) {
				return false;
			}
		});			
        
    });
});