function abreModalFormaVenda(){
	$.ajax({
		url : "../src/servicos/rastreamento/get-tabela-forma-venda.php",
		type : 'POST',
		success : function(retorno) {
			notificacao(false, null);
			$("#tabela-forma-venda tbody").html(retorno);
			
			$('#modal-forma-venda').bPopup({
		        positionStyle: 'fixed',
		        onOpen: function () {
		        	
		        },
		        onClose: function () {
		        	
		        }
		    },
		    function () {		    			    
		    		    			    
		    });
		},
		error : function(retorno) {
			
		}
	});
}

$(function(){
	$("#modal-forma-venda").delegate(".check-ativa-forma-venda", "change", function(){
		notificacaoAddItem(true, "Aguarde, alterando...");
		
		var idFormaVenda = $(this).parent().parent().attr("id");
		
		var checked = $(this).is(":checked");
		var ativa = (checked) ? "1" : "0";
		
		$.ajax({
			url : "../src/servicos/rastreamento/upd-forma-venda.php",
			data : {"campo": "ativa", "valor": ativa, "idFormaVenda": idFormaVenda},
			type : 'POST',
			success : function(retorno) {
				notificacaoAddItem(false, null);
			},
			error : function(retorno) {
				
			}
		});
	});
	
	$("#btn-cadastro-forma-venda").click(function(e){
		e.preventDefault();
		notificacaoAddItem(false, null);
		
		var sigla = $("#sigla-forma-venda").val();
		var CadastroVenda = $("#nome-forma-venda").val();
		
		if(CadastroVenda != "" && sigla != ""){
			var txtCadastroFormaVenda = CadastroVenda.trim().toLowerCase();
			
			var erro = false;
			$("#tabela-forma-venda tbody tr").each(function(){
				var tds = $(this).children();
				var nomeFormaVendaTabela = tds[0].innerHTML.toLowerCase(); 
				
				if(nomeFormaVendaTabela == txtCadastroFormaVenda){
					erro = true;
				}
			});
			
			if(!erro){
				$.ajax({
					url : "../src/servicos/rastreamento/cadastro-forma-venda.php",
					data : {"nome": CadastroVenda, "sigla": sigla},
					type : 'POST',
					success : function(retorno) {
						notificacaoAddItem(false, null);
						
						notificacaoAddItem(true, "Forma de venda cadastrada com sucesso.");
						$("#tabela-forma-venda").append("<tr id='"+retorno+"'>"+
															"<td>" + CadastroVenda + "</td>"+
															"<td>" + sigla + "</td>"+
															"<td><input type='checkbox' class='check-ativo-forma-envio' checked='checked'></td>"+
														"</tr>");
						
						$("#nome-forma-venda").val("");
						$("#sigla-forma-venda").val("");
						$("#nome-forma-venda").focus();
						
						setTimeout(function(){
							notificacaoAddItem(false, null);
						}, 2500)
					},
					error : function(retorno) {
						
					}
				});
			}
			else{
				notificacaoAddItem(true, "Erro! Forma de venda já cadastrada.");
			}
		}
		else{
			notificacaoAddItem(true, "Erro! Preencha todos os campos.");
			setTimeout(function(){
				notificacaoAddItem(false, null);
			}, 1500);
		}
		
		
	});
	
	$("#tabela-rastreamento").delegate(".forma-venda", "change", function(){
		var idRastreio = $(this).parent().parent().attr("id");
		var idFormaVenda = $(this).children(":selected").val();
		
		if(idFormaVenda != "outro"){
			notificacao(true, "Aguarde, alterando forma de venda...");
			
			$.ajax({
				url : "../src/servicos/rastreamento/upd-rastreamento.php",
				data : {"campo": "id_forma_venda", "valor": idFormaVenda, "idRastreio": idRastreio},
				type : 'POST',
				success : function(retorno) {
					notificacao(false, null);
				},
				error : function(retorno) {
					
				}
			});
		}
		else{
			abreModalFormaVenda();
		}
	});
	
	$("#modal-adiciona-rastreio").delegate("#forma-venda-cadastro-rastreio", "change", function(){
		var idFormaVenda = $(this).children(":selected").val();
		
		if(idFormaVenda == "outro"){
			abreModalFormaVenda();
		}
	});
});
