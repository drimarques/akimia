$(function(){
	$(".reenviar-email").click(function(){
		var ehViaModal = $(this).parent().parent().parent().parent().attr("id");
		var idsFormaEnvio = new Array();
		
		if(ehViaModal == "modal-historico-rastreamento"){
			var idTabelaRastreio = $(".tabela-historico-rastreio").children().attr("id");
			var idRastreio = idTabelaRastreio.replace("tabelaRastreio", "");
			
			idsFormaEnvio.push(idRastreio);
			window.open("../src/servicos/rastreamento/reenvio-email.php?idsRastreio=" + idsFormaEnvio, '_blank');
			done = true;
		}
		else{
			var temSelecionada = false;
			$("#tabela-rastreamento .linha-selecionada").each(function() {
				temSelecionada = true;
			});
			
			if(temSelecionada){
				$("#tabela-rastreamento .linha-selecionada").each(function() {
					idsFormaEnvio.push(this.id);
				});
				
				window.open("../src/servicos/rastreamento/reenvio-email.php?idsRastreio=" + idsFormaEnvio, '_blank');
		        done = true;
			}
			else{
				notificacao(true, "Selecione uma linha...");
				setTimeout(function(){
					notificacao(false, null);
				}, 1500);
			}
		}
	})
});