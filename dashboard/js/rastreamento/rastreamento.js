function integracaoCorreios(){
	return $.ajax({
		url: "../src/servicos/rastreamento/integracao-correios.php",			
		type : 'POST',
		success : function() {
		},
		error : function() {
		}
	});
}

function populaSelectFormaVenda(){ 
	$.ajax({
		url: "../src/servicos/rastreamento/get-select-forma-venda.php",			
		type : 'POST',
		success : function(retorno) {
			$("#forma-venda-cadastro-rastreio").html(retorno);
		},
		error : function(retorno) {
		}
	});
}

function buscaRastreamento(busca){
	notificacao(true, "Buscando rastreamento...");
	
	var entregue = ($("#menu-filtro-rastreamento ul li.selecionado").html() == "Finalizados") ? "1" : "0";	
	
	$.ajax({
		url: "../src/servicos/tabelas/tabela-busca-rastreamentos.php",
		data : {"busca": busca, "entregue": entregue},			
		type : 'POST',
		context : $("#tabela-rastreamento tbody"),
		success : function(retorno) {			
			if(retorno == ""){
				$("#tabela-rastreamento tbody").html(null);
				notificacao(true, "Nenhum rastreio encontrado...");
			}
			else{
				notificacao(false, null);
				$("#tabela-rastreamento tbody").html(retorno);
				loadingTooltips();
				
				$("#tabela-rastreamento").highlight(busca);
			}
		},
		error : function(retorno) {
		}
	});	
}

function loadingTooltips(){
	$('.tooltip-relatorio').tooltipster({        	
        position: 'bottom',        
        theme: 'tooltipster-light'       
    });
}

function populaTabelaRastreamentos(filtro){
	notificacao(true, "Aguarde, atualizando...");
	var url = (filtro == "andamento") ? '../src/servicos/tabelas/tabela-rastreamentos-em-andamento.php' : '../src/servicos/tabelas/tabela-rastreamentos-finalizados.php'; 
	
	$.ajax({
		url: url,
		type : 'POST',
		context : $("#tabela-rastreamento tbody"),
		success : function(retorno) {
			if(retorno == ""){
				$("#tabela-rastreamento tbody").html(null);
				notificacao(true, "Nenhum rastreio encontrado...");
				setTimeout(function(){
					notificacao(false, null);
				}, 2000);
			}
			else{
				notificacao(false, null);
				$("#tabela-rastreamento tbody").html(retorno);
				loadingTooltips();
			}
		},
		error : function(retorno) {
			
		}
	});
}

$(function(){
	populaTabelaRastreamentos("andamento");
	
	$("#hamburger-button-rastreio").click(function(){
		$("#menu-rastreamento-mobile").fadeToggle("fast");
	})
	
	$(".abre-modal-adicionar-lote-rastreio").click(function(){
		$('#modal-imp-plan-rastreamento').bPopup({
	        positionStyle: 'fixed',
	        onOpen: function () {
	        	
	        },
	        onClose: function () {

	        }
	    },
	    function () {		    			    
	    		    			    
	    });
	});
	
	$(".reload-integracao-correios").click(function(){
		notificacao(true, "Aguarde, verificando com os correios...");
		
		if(integracaoCorreios()){
			setTimeout(function(){
				$("#menu-filtro-rastreamento ul li").each(function(){
					$(this).removeClass("selecionado");
				});
				
				$("#menu-filtro-rastreamento ul li:first-child").addClass("selecionado");
				populaTabelaRastreamentos("andamento");	
			}, 1000);
		}
	});
	
	$(".gerar-link").click(function(){
		var temSelecionada = false;
		var contador = 0;
		$("#tabela-rastreamento .linha-selecionada").each(function() {
			temSelecionada = true;
			contador++;
		});
		
		if(temSelecionada && contador == 1){
			var tds;
			$("#tabela-rastreamento .linha-selecionada").each(function() {
				 tds = $(this).children();
			});
			var codRastreio = $(tds[3]).text();
			var url = "http://adm.akimia.com.br/dashboard/visualizar-rastreamento.php?rastreio=" + codRastreio;
			//var url = "http://localhost/akimia/dashboard/visualizar-rastreamento.php?rastreio=" + codRastreio;
			
			$("#link-gerado").val(url);
			
			$('#modal-link-gerado').bPopup({
		        positionStyle: 'fixed',
		        onOpen: function () {
		        	
		        },
		        onClose: function () {
		        
		        }
		    },
		    function () {		    			    
		    	notificacao(false, null);
		    });
			
			
		} else{
			notificacao(true, "Por favor selecione uma linha...");
			setTimeout(function(){
				notificacao(false, null);
			}, 2500);
		}
	});
	
	
	$("#btn-copiar-link").click(function(){
		var copyTextarea = document.getElementById("link-gerado");
		copyTextarea.select();

		try {
			var successful = document.execCommand('copy');
		    if(successful){
		    	notificacaoAddItem(true, "Link copiado");
		    	setTimeout(function(){
		    		notificacaoAddItem(false, null);
		    	}, 1500)
		    }		    
		} catch (err) {
			
		}
	});
	
	$(".abre-modal-adicionar-rastreio").click(function(){
		notificacao(true, "Cuidando de algumas coisas...");
		notificacaoAddItem(false, null);
		
		populaSelectFormaVenda();
		
		$.ajax({
    		url: "../src/servicos/rastreamento/get-ultimo-id-pedido.php",			
    		type : 'POST',
    		success : function(retorno) {
    			$("#txt-numero-pedido-rastreio").val(retorno);
    			
    			$('#modal-adiciona-rastreio').bPopup({
    		        positionStyle: 'fixed',
    		        onOpen: function () {
    		        	
    		        },
    		        onClose: function () {
    		        
    		        }
    		    },
    		    function () {		    			    
    		    	notificacao(false, null);
    		    });
    		},
    		error : function(retorno) {
    		}
    	});
	});
	
	
	$("#menu-filtro-rastreamento ul li").click(function(){
		$("#menu-filtro-rastreamento ul li").each(function(){
			$(this).removeClass("selecionado");
		});
		
		$(this).addClass("selecionado");
		
		if($(this).text() == "Em andamento"){
			populaTabelaRastreamentos("andamento");
		}
		else{
			populaTabelaRastreamentos("finalizados");
		}
	});
	
	$("#tabela-rastreamento").delegate(".numero-pedido", "blur", function(){
		var idRastreio = $(this).parent().parent().attr("id");
		var numeroPedido = $(this).val();
		
		notificacao(true, "Aguarde, alterando número do pedido...");
		
		$.ajax({
			url : "../src/servicos/rastreamento/upd-rastreamento.php",
			data : {"campo": "numero_pedido", "valor": numeroPedido, "idRastreio": idRastreio},
			type : 'POST',
			success : function(retorno) {
				notificacao(false, null);
			},
			error : function(retorno) {
				
			}
		});
	})
	
	$("#tabela-rastreamento").delegate(".abre-modal-historico-rastreio", "click", function(){
		var idRastreio = $(this).parent().parent().attr("id");
		
		notificacao(true, "Aguarde, buscando histórico do rastreamento...");
		
		$.ajax({
			url : "../src/servicos/rastreamento/get-tabela-rastreio.php",
			data : {"idRastreio": idRastreio},
			type : 'POST',
			success : function(retorno) {
				notificacao(false, null);
				$('#modal-historico-rastreamento .modal-body div.tabela-historico-rastreio').html(retorno);
				
				$('#modal-historico-rastreamento').bPopup({
			        positionStyle: 'fixed',
			        onOpen: function () {
			        	
			        },
			        onClose: function () {

			        }
			    },
			    function () {		    			    
			    		    			    
			    });
			},
			error : function(retorno) {
				
			}
		});
	})
	
	$("#tabela-rastreamento").delegate(".seleciona-rastreio", "click", function(){
		$(this).toggleClass("linha-selecionada");
	});
	
	$(".excluir-rastreio").click(function(){		
		var temLinhaSelecionada = false;
		$("#tabela-rastreamento .linha-selecionada").each(function() {
			temLinhaSelecionada = true;
		});
		
		if (!temLinhaSelecionada) {
			notificacao(true, "Marque pelo menos 1 rastreio para excluir.");
			setTimeout(function(){
				notificacao(false, null);
			}, 2000);
		} 
		else {
			notificacao(true, "Aguarde excluindo rastreios...");
			var ids = [];
			
			$("#tabela-rastreamento .linha-selecionada").each(function() {
				var id_item = this.id;
				ids.push(id_item);
			});
			
			for(var i = 0; i < ids.length; i++){
				var idRastreio = ids[i];
				$.ajax({
					url : "../src/servicos/rastreamento/delete-rastreio.php",
					data : {"idRastreio" : idRastreio},
					type : 'POST',
					success: function (retorno) {
						
					}
				});
			}
			
			setTimeout(function(){
				$("#menu-filtro-rastreamento ul li").each(function(){
					$(this).removeClass("selecionado");
				});
				
				$("#menu-filtro-rastreamento ul li:first-child").addClass("selecionado");
				populaTabelaRastreamentos("andamento");
			}, 3000)
		}
	});
	
	$("#txt-busca-rastreamento").keypress(function(e) {				
		if (e.which === 13) {										
			e.preventDefault();
			var busca = $(this).val();
			buscaRastreamento(busca);
		}
	});

	$("#btn-buscar-rastreamento").click(function(e) {
		e.preventDefault();
		var busca = $("#txt-busca-rastreamento").val();
		buscaRastreamento(busca);
	});
	
	//Form submit
	$("#planilha-rastreamento").on('submit',(function(e) {
		e.preventDefault();		
		notificacaoAddItem(true, "Aguarde, importando planilha...")			
			
		var planilha = $("#arquivo-rastreio").val();
				
		if(planilha == ""){
			notificacaoAddItem(true, "Por favor, selecione uma planilha.");
		}		
		else{		
			$.ajax({
				url: "../src/servicos/rastreamento/importacao-rastreamentos.php",
				type: "POST",
				data: new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
				success: function(retorno){
					if(retorno == "erro_arquivo_excel"){																		
						notificacaoAddItem(true, "Somente arquivos no formado .xls");
					}
					else if(retorno.match(/erro_forma_venda/)){
						var erros = retorno.split("__");
						var textoErros = "";
						for (var i = 0; i < erros.length - 1; i++) {
							var separaErros = erros[i].split(",");
							textoErros += "Linha " +  separaErros[2] + " não inserida. Obs.: Forma de venda \"" + separaErros[1] + "\" não encontrada. <br>";
						}
						notificacaoAddItem(true, textoErros);
					}
					else{
						notificacaoAddItem(true, "Planilha importada com sucesso :D");
						setTimeout(function(){
							notificacaoAddItem(true, "Aguarde, verificando com os correios...");
							
							if(integracaoCorreios()){
								setTimeout(function(){
									notificacaoAddItem(false, null);
									var modalFechar = $("#modal-imp-plan-rastreamento").bPopup();
							        modalFechar.close();
							        populaTabelaRastreamentos("andamento");
								}, 1200);
							}
						}, 1000);
					}
					
					$("#menu-filtro-rastreamento ul li").each(function(){
						$(this).removeClass("selecionado");
					});
					$("#menu-filtro-rastreamento ul li:first-child").addClass("selecionado");
					
				}
			});		
		}
		
	}));
});