<?php
session_start();

if (!isset($_SESSION["usuario_logado"])) {
	header("Location: ../index.php");
	die();
}
else{
	require_once '../src/classes/inclui-classes.php';
	require_once '../src/daos/inclui-daos.php';
	$dolar = new CotacaoDolar();
}

?>
<!DOCTYPE html>
<html>
		<head>
		<meta charset="UTF-8">
		<title>Painel - Akimia</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="shortcut icon" href="../img/logo-akimia-small.png" />
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,700,700italic,800italic,800' rel='stylesheet' type='text/css'>
		<link href="css/padroes.css" rel="stylesheet" type="text/css" />
		<link href="css/index.css" rel="stylesheet" type="text/css" />
		<link href="css/calculadora.css" rel="stylesheet" type="text/css" />
	</head>
	<body>
		<header class="centralizer">
			<h1>
				<a href="index.php">
					<img alt="Akimia Produtos Importados" src="../img/logo-akimia-small.png">
				</a>
			</h1>
		</header>
		
		<article id="calculadora">
			<form name="form-calculadora" id="form-calculadora" method="post" autocomplete="off">
				<label class="label" id="label-valor-dolar">Dólar:</label>
				<input type="text" name="valor-dolar" id="valor-dolar" class="campo-form real-calculadora campo-rolagem" placeholder="Valor do dólar" value='<?php echo $dolar->getValorDolar() ?>'>
			
				<label class="label">Tipo do peso:</label>
				<select name="select-tipo-peso" id="select-tipo-peso" class="campo-form">
					<option value="libras">Libras (lb)</option>
					<option value="pounds">Pounds (lb)</option>
					<option value="kilos" selected="selected">Kilos (kg)</option>
					<option value="gramas">Gramas</option>
					<option value="litros">Litros (l)</option>
					<option value="mililitros">Mililitros (ml)</option>
				</select>
				
				<label class="label" id="label-valor-peso-calculadora">Valor peso:</label>
				<input type="text" name="valor-peso-calculadora" id="valor-peso-calculadora" class="campo-form decimal-calculadora campo-rolagem" placeholder="Digite o valor do peso" value="0,00">
				
				<label class="label" id="label-valor-produto-calculadora">Valor produto (U$):</label>
				<input type="text" name="valor-produto-calculadora" id="valor-produto-calculadora" class="campo-form dolar-calculadora campo-rolagem" placeholder="Digite o valor do produto" value='0.00'>
				
				<table class="tabela" id="tabela-lucro-calculadora">
					<thead>
						<tr>
							<th>Percentual desejado</th>
							<th>Valor de Venda</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><input type="text" name="percentual-lucro" id="percentual-lucro" class="campo-form porcentagem-calculadora campo-rolagem" value="0,00"></td>
							<td id="lucro-variavel"></td>
						</tr>
						<tr>
							<td id="label-percentual-lucro">% 30,00</td>
							<td id="lucro-fixo"></td>
						</tr>
					</tbody>
				</table>
				
				<input type="submit" name="btn-calcula-valores" id="btn-calcula-valores" class="botao hover" value="Calcular">
			</form>
		</article>


		<script src="../resources/jquery/jquery-2.1.4.min.js"></script>
		<script src="../resources/jquery/jquery.easing.min.js"></script>
		<script src="../resources/jquery/jquery.maskMoney.js"></script>
		<script src="../resources/jquery/jquery.bpopup.min.js"></script>
		<script src="js/calculadora/calculadora.js" type="text/javascript"></script>
		<script src="https://code.highcharts.com/highcharts.js"></script>
		<script src="https://code.highcharts.com/modules/exporting.js"></script>
	</body>
</html>