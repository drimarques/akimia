<?php
session_start();

if (!isset($_SESSION["usuario_logado"])) {
	header("Location: ../index.php");
	die();
}
else{
	require_once '../src/classes/inclui-classes.php';
	require_once '../src/daos/inclui-daos.php';
	$dolar = new CotacaoDolar();
}

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Painel - Akimia</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="shortcut icon" href="../img/logo-akimia-small.png" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,700,700italic,800italic,800' rel='stylesheet' type='text/css'>
<link href="css/padroes.css" rel="stylesheet" type="text/css" />
<link href="css/index.css" rel="stylesheet" type="text/css" />
<link href="css/modal-fechamento.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" type="text/css" href="../resources/tooltipster/css/tooltipster.css" />
<link rel="stylesheet" type="text/css" href="../resources/tooltipster/css/themes/tooltipster-light.css" />
</head>

	<script>
		function alerta(){
			alert("Função em desenvolvimento.");
		}			
	</script>
	
<body>
	<header class="centralizer">
		<h1>
			<a href="index.php">
				<img alt="Akimia Produtos Importados" src="../img/logo-akimia-small.png">
			</a>
		</h1>
		
		<section id="usuario-logado">
			<span class="hover">contato@akimia.com.br <span class="arrow-bottom"></span></span>
		</section>
		
		<section id="opcoes-usuario">
			<div class="arrow-top"></div>
			<ul>
				<li><a href="#" onclick="alerta()">Alterar senha</a></li>
				<li><a href="#" onclick="deslogaUsuario();">Sair</a></li>
			</ul>
		</section>		
	</header>
	
	<section class="centralizer" id="paineis">
		<ul>
			<li class="painel">
				<a href="http://www.akimia.com.br/site/adm/index.php" target="_blank">
					<img alt="" src="../img/clientes.png">
					<h2>Clientes</h2>
					<p>Visualize clientes cadastrados no site.</p>
				</a>
			</li>
			<li class="painel" id="open-modal-plan">
				<a href="#">
					<img alt="" src="../img/excel.png">
					<h2>Importar planilha</h2>
					<p>Importe a planilha para iniciar um fechamento.</p>
				</a>
			</li>
			<li class="painel" id="open-modal-fechamento">
				<a href="#">
					<img alt="" src="../img/fechamento.png">
					<h2>Fechamento</h2>
					<p>Visualize o seu fechamento atual.</p>
				</a>
			</li>
			<li class="painel" id="open-modal-historico">
				<a href="#">
					<img alt="" src="../img/historico.png">
					<h2>Histórico de fechamentos</h2>
					<p>Busque seu histórico de fechamentos</p>
				</a>
			</li>						
			<li class="painel">
				<a href="#" id="open-modal-exportar">
					<img alt="" src="../img/download.png">
					<h2>Exportar fechamento</h2>
					<p>Faca o download em excel do fechamento desejado.</p>
				</a>
			</li>
			<li class="painel">
				<a href="#" id="open-modal-lista-compras">
					<img alt="" src="../img/lista-de-compras.png">
					<h2>Lista de compras</h2>
					<p>Faça um check list de suas compras.</p>
				</a>
			</li>
			<li class="painel">
				<a href="#" id="open-modal-rastreamento">
					<img alt="" src="../img/box.svg">
					<h2>Rastreamento</h2>
					<p>Rastreie suas compras.</p>
				</a>
			</li>
			<li class="painel">
				<a href="#" id="open-modal-calculadora">
					<img alt="" src="../img/calculadora.png">
					<h2>Calculadora</h2>
					<p></p>
				</a>
			</li>
		</ul>
	</section>
	
	<section class="rodape">
		<div id="copy">
			Akimia Produtos Importados &#169;2012 - <?php echo date('Y') ?>
		</div>
		<div id="developer">
			Desenvolvido e mantido por <a target="_blank" href="https://www.linkedin.com/pub/adriano-marques/b6/522/4b">Adriano Marques.</a>
		</div>
	</section>
	
	<div class="preparando-modal">Cuidando de algumas coisas, aguarde...</div>
	
	<div id="modal-imp-plan" class="modal">
    	<h1>Importar planilha<span class="modal-close hover">x</span></h1>            
			<section class="modal-body">
				<form name="inicio-fechamento" id="inicio-fechamento" method="post" enctype="multipart/form-data" autocomplete="off">
                	<div>
                		<label for="nome-fechamento" class="label">Nome do fechamento:</label>
                		<input type="text" name="nome-fechamento" id="nome-fechamento" class="campo-form" placeholder="Dê um nome ao fechamento">
                		
                		<label for="arquivo-excel" class="label">Planilha:</label>
                		<input id="nome-arquivo" placeholder="Nenhum arquivo selecionado" class="campo-form nome-arquivo" disabled="disabled" />
						<div class="botao-upload">
						    <span>Escolher arquivo</span>
						    <input type="file" id="arquivo-excel" class="botao-upload" name="arquivo-excel" accept=".xls">
						</div>
                		                		
                		<div class="carregando notificacao"></div>                		
                		
                		<input type="submit" id="btn-importar-planilha" name="btn-importar-planilha" value="Enviar" class="botao hover">    
                	</div>
				</form>
            </section>
	</div>

	<div id="modal-fechamento" class="modal">
    	<h1>Fechamento <img alt="Cadeado fechamento" src="" class="hover cadeado-fechamento"><span class="modal-close hover">x</span></h1>            
			<section class="modal-body">
				<div id="top-fechamento">
					<form name="form-parametros-calculo" id="form-parametros-calculo" method="post">
						<table id="tabela-parametros-calculo" class="tabela">
							<tbody>
								
							</tbody>
						</table>
					</form>	
					<form name="form-resumo" id="form-resumo" method="post">
						<table id="tabela-resumo" class="tabela">
							<tbody>
								
							</tbody>
						</table>
					</form>		
					<form class="form-busca" name="form-busca-produto" id="form-busca-produto" method="post" autocomplete="off">
						<input type="text" class="campo-form" id="txt-busca-produto" name="txt-busca-produto" placeholder="Buscar produto..." autofocus="autofocus">
						<input type="submit" value="Buscar" id="btn-buscar-produto" name="btn-buscar-produto" class="botao hover"/>
						
						<h1 class="inf-fechamento-modal edit-nome-fechamento" id="nome-fech-modal-fechamento"></h1>
												
						<label for="select-fechamentos" class="label">Fechamentos:</label>
						<select name="select-fechamentos" id="select-fechamentos" class="campo-form">							
						</select>
					</form>
				</div>
				
				<div class="carregando notificacao"></div>
				
            	<div>
            		<form name="form-fechamento" id="form-fechamento">
            		
            		<h2 id="total-registros"></h2>            		
	            		<table id="tabela-fechamento" class="tabela">
	            			<thead>
	            				<tr>
	            					<th>Produto</th>
	            					<th>IOF</th>
	            					<th>Custo EUA</th>
	            					<th>Custo</th>
	            					<th>Custo total</th>
	            					<th>Markup</th>
	            					<th>Venda</th>
	            					<th>Venda total</th>
	            					<th>Lucro</th>
	            					<th>Lucro total</th>            				
	            			</thead>
	            			<tbody>
	            				
	            			</tbody>
	            		</table>
	            	</form>
	            	
	            	<!-- <div id="subir-top-fechamento" class="hover">
	            		<img alt="subir-topo" src="../img/scrolltop.png">
	            	</div> -->
	            	
	            	<div id="adiciona-item" class="hover">
	            		<img alt="Adicionar item" src="../img/add-button.png">
	            	</div>
	            	
            	</div>
            </section>
	</div>

	<div id="modal-exportar-fechamento" class="modal">
		<h1>Exportar fechamento<span class="modal-close hover">x</span></h1>
		<section class="modal-body">
			<div>
				<form class="form-busca" name="form-busca-fechamento" id="form-busca-fechamento" method="post" autocomplete="off">
					<input type="text" class="campo-form" id="txt-busca-fechamento" name="txt-busca-fechamento" placeholder="Buscar fechamento..." autofocus="autofocus">
					<input type="submit" value="Buscar" id="btn-buscar-fechamento" name="btn-buscar-fechamento" class="botao hover"/>
				</form>							
			</div>
			<div>
				<table class="tabela" id="tabela-exportar-fechamento">
					<thead>
						<tr>
							<th>Nome</th>
							<th>Início</th>
							<th>Término</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>						
					</tbody>							
				</table>
			</div>
			
			<div class="botao hover" id="btn-exportar-fechamento">Exportar</div>			
			<div class="carregando notificacao"></div>
			<div id="fechamento-download" class="notificacao"></div>
	    </section>
	</div>
	
	<div id="modal-historico-fechamentos" class="modal">
		<h1>Histórico de fechamentos<span class="modal-close hover">x</span></h1>
		<section class="modal-body">
			<div>
				<form class="form-busca" name="form-busca-fechamento" id="form-busca-fechamento" method="post" autocomplete="off">
					<input type="text" class="campo-form" id="txt-busca-fechamento-historico" name="txt-busca-fechamento-historico" placeholder="Buscar fechamento..." autofocus="autofocus">
					<input type="submit" value="Buscar" id="btn-buscar-fechamento-historico" name="btn-buscar-fechamento-historico" class="botao hover"/>
				</form>							
			</div>
			<div>
				<table class="tabela sortable" id="tabela-historico-fechamentos">
					<thead>
						<tr>
							<th>Nome</th>
							<th>Início</th>
							<th>Término</th>
							<th>Status</th>
							<th>Editar</th>
							<th>Excluir</th>
						</tr>
					</thead>
					<tbody>						
					</tbody>							
				</table>
			</div>
			
			<div class="botao hover" id="btn-gerar-grafico">Gerar gráfico</div>			
			<div class="carregando notificacao"></div>
			
			<div id="grafico">
			</div>
						
	    </section>
	</div>

	<div id="modal-excluir-fechamento" class="modal">
		<h1>Excluir fechamento<span class="modal-close hover">x</span></h1>
		<section class="modal-body">
			<div>
				<form name="form-excluir-fechamento" id="form-excluir-fechamento" method="post" autocomplete="off">
					<label class="label" id="nome-fechamento-excluir"></label>
				
					<input type="hidden" class="campo-form" id="id-fechamento-excluir" name="id-fechamento-excluir">					
					<input type="submit" value="Sim" id="btn-excluir-fechamento" name="btn-excluir-fechamento" class="botao botao-paralelo hover"/>
					<input type="button" value="Não" id="btn-cancelar" name="btn-cancelar" class="botao-2 botao-paralelo hover"/>
				</form>							
			</div>
			<div>
			</div>
								
			<div class="carregando notificacao notificacao-excluir"></div>
	    </section>
	</div>
	
	
	<div id="modal-adiciona-item-fechamento" class="modal">
		<h1>Adicionar ítem<span class="modal-close hover">x</span></h1>
		<section class="modal-body">
			<div class="carregando notificacao notificacao-add-item"></div>		
			<div>
				<form method="post" name="form-add-item" id="form-add-item" autocomplete="off">
					<span class="input-required-alert">Atenção! Campos com * são obrigatórios.</span>
					<label class="label" for="txt-marca-produto">*Marca:</label>
					<input type="text" class="campo-form campo-add-item" id="txt-marca-produto" name="txt-marca-produto" placeholder="Digite aqui a marca do produto">
					
					<!-- <label class="label" for="txt-sexo-produto">Sexo:</label>
					<input type="text" class="campo-form campo-add-item" id="txt-sexo-produto" name="txt-sexo-produto" placeholder="Digite aqui o sexo">
					
					<label class="label" for="txt-tipo-produto">Tipo:</label>
					<input type="text" class="campo-form campo-add-item" id="txt-tipo-produto" name="txt-tipo-produto" placeholder="Digite aqui o tipo do produto"> -->
					
					<label class="label" for="txt-produto-produto">*Produto:</label>
					<input type="text" class="campo-form campo-add-item" id="txt-produto-produto" name="txt-produto-produto" placeholder="Digite aqui o nome do produto">
					
					<!-- <label class="label" for="txt-modelo-produto">Modelo:</label>
					<input type="text" class="campo-form campo-add-item" id="txt-modelo-produto" name="txt-modelo-produto" placeholder="Digite aqui o modelo do produto">
					
					<label class="label" for="txt-cor-produto">Cor:</label>
					<input type="text" class="campo-form campo-add-item" id="txt-cor-produto" name="txt-cor-produto" placeholder="Digite aqui a cor do produto">
					
					<label class="label" for="txt-tamanho-produto">Tamanho:</label>
					<input type="text" class="campo-form campo-add-item" id="txt-tamanho-produto" name="txt-tamanho-produto" placeholder="Digite aqui o tamanho do produto"> -->
					
					<label class="label" for="txt-quantidade-produto">Quantidade:</label>
					<input type="number" class="campo-form" id="txt-quantidade-produto" name="txt-quantidade-produto" placeholder="Digite aqui a quantidade de produtos" min="1">
					
					<label class="label" for="txt-custo-eua-produto">Custo EUA:</label>
					<input type="text" class="campo-form dolar" id="txt-custo-eua-produto" name="txt-custo-eua-produto" placeholder="Digite aqui o custo em dólar do produto">
					
					<label class="label" for="txt-venda-produto">Venda:</label>
					<input type="text" class="campo-form real" id="txt-venda-produto"  name="txt-venda-produto" placeholder="Digite aqui valor da venda do produto">
					
					<!-- <label class="label" for="txt-cliente-produto">Cliente:</label>
					<input type="text" class="campo-form" id="txt-cliente-produto"  name="txt-cliente-produto" placeholder="Digite aqui nome do cliente">
					
					<label class="label" for="txt-telefone-cliente-produto">Telefone:</label>
					<input type="text" class="campo-form tel" onkeypress="mascara(this, '## #####-####')" maxlength="13" id="txt-telefone-cliente-produto"  name="txt-telefone-cliente-produto" placeholder="Somente números">
					
					<label class="label" for="txt-email-cliente-produto">E-mail:</label>
					<input type="text" class="campo-form" id="txt-email-cliente-produto"  name="txt-email-cliente-produto" placeholder="Digite aqui e-mail do cliente"> -->
					
					<input type="submit" value="Adicionar" id="btn-add-item-fechamento" name="btn-add-item-fechamento" class="botao hover"/>
				</form>							
			</div>			
	    </section>
	</div>
	
	<div id="modal-lista-compras" class="modal">
		<h1>Lista de compras<span class="modal-close hover">x</span></h1>
		<section class="modal-body">
			<!-- <div id="acoes-para-lista-compras-existente">
				<h1>Sua lista foi criada :)</h1>
				<p>O que deseja fazer?</p>				
				<nav id="menu-lista-existente">							
                	<ul>
                		<li class="hover" onclick="location.href='lista-compras/'">Ver lista</li>                		
                		<li class="hover menu-adicionar-itens-avulsos">Adicionar itens avulsos</li>                	
                		<li class="hover" id="menu-abre-modal-gerar-fechamento">Gerar fechamento</li>
                		<li class="hover" id="btn-limpar-lista">Limpar lista</li>
                	</ul>
				</nav>
			</div> -->
			<div id="acoes-nao-tenho-lista">
				<h1>Você ainda não tem uma lista de compras :(</h1>
				<p>Criar uma lista:</p>
				<nav id="menu-nao-tenho-lista">							
                	<ul>                		
                		<li class="hover menu-selecionado" id="menu-importar-planilha">Importando planilha</li>
                		<li class="hover" id="menu-fechamento-existente">Com fechamento existente</li>
                		<li class="hover menu-adicionar-itens-avulsos">Adicionando itens avulsos</li>                		                		
                	</ul>
				</nav>
				<form name="lista-compras" id="lista-compras" method="post" enctype="multipart/form-data" autocomplete="off">
                	<div id="sessao-usar-fechamento">
                		<label for="select-fechamentos-lista-compras" class="label">Fechamentos:</label>
                		<select class="campo-form" name="select-fechamentos-lista-compras" id="select-fechamentos-lista-compras">                		
                		</select>
                	</div>
                	<div id="sessao-usar-planilha">
                		<label for="arquivo-lista-compras" class="label">Planilha:</label>
                		<input id="nome-arquivo-lista-compras" placeholder="Nenhum arquivo selecionado" class="campo-form nome-arquivo" disabled="disabled" />
						<div class="botao-upload">
						    <span>Escolher arquivo</span>
						    <input type="file" id="arquivo-lista-compras" class="botao-upload" name="arquivo-lista-compras" accept=".xls">
						</div>                		                		                		                                		                		  
                	</div>
                	
                	<input type="submit" id="btn-importar-lista-compras" name="btn-importar-lista-compras" value="Importar lista" class="botao hover">                	
				</form>
				
			</div>
			<div>							
				<div class="carregando notificacao"></div>
			</div>						
	    </section>
	</div>
	
	<div id="modal-adiciona-item-avulso-lista-compras" class="modal">
		<h1>Adicionar ítem<span class="modal-close hover">x</span></h1>
		<section class="modal-body">
			<div class="carregando notificacao notificacao-add-item"></div>		
			<div>
				<form method="post" name="form-add-item-lista-compras" id="form-add-item-lista-compras" autocomplete="off">
					<label class="label" for="txt-marca-lista-compras">Marca:</label>
					<input type="text" class="campo-form campo-add-item-lista-compras" id="txt-marca-lista-compras" name="txt-marca-lista-compras" placeholder="Digite aqui a marca do produto">
					
					<label class="label" for="txt-sexo-lista-compras">Sexo:</label>
					<input type="text" class="campo-form campo-add-item-lista-compras" id="txt-sexo-lista-compras" name="txt-sexo-lista-compras" placeholder="Digite aqui o sexo">
					
					<label class="label" for="txt-tipo-lista-compras">Tipo:</label>
					<input type="text" class="campo-form campo-add-item-lista-compras" id="txt-tipo-lista-compras" name="txt-tipo-lista-compras" placeholder="Digite aqui o tipo do produto">
					
					<label class="label" for="txt-lista-compras-lista-compras">Produto:</label>
					<input type="text" class="campo-form campo-add-item-lista-compras" id="txt-lista-compras-lista-compras" name="txt-lista-compras-lista-compras" placeholder="Digite aqui o nome do produto">
					
					<label class="label" for="txt-modelo-lista-compras">Modelo:</label>
					<input type="text" class="campo-form campo-add-item-lista-compras" id="txt-modelo-lista-compras" name="txt-modelo-lista-compras" placeholder="Digite aqui o modelo do produto">
					
					<label class="label" for="txt-cor-lista-compras">Cor:</label>
					<input type="text" class="campo-form campo-add-item-lista-compras" id="txt-cor-lista-compras" name="txt-cor-lista-compras" placeholder="Digite aqui a cor do produto">
					
					<label class="label" for="txt-tamanho-lista-compras">Tamanho:</label>
					<input type="text" class="campo-form campo-add-item-lista-compras" id="txt-tamanho-lista-compras" name="txt-tamanho-lista-compras" placeholder="Digite aqui o tamanho do produto">
					
					<label class="label" for="txt-quantidade-lista-compras">Quantidade:</label>
					<input type="number" class="campo-form campo-add-item-lista-compras" id="txt-quantidade-lista-compras" name="txt-quantidade-lista-compras" placeholder="Digite aqui a quantidade de produtos" min="1">
					
					<label class="label" for="txt-cliente-lista-compras">Cliente:</label>
					<input type="text" class="campo-form" id="txt-cliente-lista-compras"  name="txt-cliente-lista-compras" placeholder="Digite aqui nome do cliente">
					
					<label class="label" for="txt-telefone-cliente-lista-compras">Telefone:</label>
					<input type="text" class="campo-form tel" onkeypress="mascara(this, '## #####-####')" maxlength="13" id="txt-telefone-cliente-lista-compras"  name="txt-telefone-cliente-lista-compras" placeholder="Somente números">
					
					<label class="label" for="txt-email-cliente-lista-compras">E-mail:</label>
					<input type="text" class="campo-form" id="txt-email-cliente-lista-compras"  name="txt-email-cliente-lista-compras" placeholder="Digite aqui e-mail do cliente">
					
					<input type="submit" value="Adicionar" id="btn-add-item-lista-compras" name="btn-add-item-lista-compras" class="botao hover"/>
				</form>							
			</div>			
	    </section>
	</div>
	
	<div id="modal-gera-fechamento" class="modal">
		<h1>Gerar fechamento<span class="modal-close hover">x</span></h1>
		<section class="modal-body">
			<div class="carregando notificacao notificacao-add-item"></div>		
			<div>
				<form method="post" name="form-gera-fechamento" id="form-gera-fechamento" autocomplete="off">
					<label class="label" for="txt-nome-fechamento-lista-compras"">Nome do fechamento:</label>
					<input type="text" class="campo-form" id="txt-nome-fechamento-lista-compras" name="txt-nome-fechamento-lista-compras" placeholder="Digite aqui o nome do fechamento">
					
					<input type="submit" value="Salvar" id="btn-gera-fechamento" name="btn-gera-fechamento" class="botao hover"/>
				</form>							
			</div>			
	    </section>
	</div>
	
	
	<div id="modal-rastreamento" class="modal">
		<h1>Rastreamento<span class="modal-close hover">x</span></h1>
		<section class="modal-body">
			<div>
				<img alt="Hamburger Button Rastreio" src="../img/hamburger-button.svg" id="hamburger-button-rastreio" class="hover">
				<nav id="menu-rastreamento">
					<ul>
						<li class="hover tooltip abre-modal-adicionar-rastreio" title="Adicionar rastreio"><img alt="Incluir rastreamento" src="../img/add-button.png"></li>
						<li class="hover tooltip abre-modal-adicionar-lote-rastreio" title="Importar lote de rastreio"><img alt="Importar planilha de rastreamento" src="../img/excel-small.png"></li>
						<li class="hover tooltip excluir-rastreio" title="Excluir rastreio"><img alt="Excluir item" src="../img/trash.png"></li>
						<li class="hover tooltip reenviar-email" title="Reenviar e-mail de rastreio"><img alt="Encaminhar e-mail" src="../img/encaminhar.svg"></li>
						<li class="hover tooltip reload-integracao-correios" title="Atualizar rastreio"><img alt="Integração correios" src="../img/reload.svg"></li>
						<li class="hover tooltip gerar-link" title="Gerar link"><img alt="Gerar link" src="../img/link.svg"></li>
					</ul>
				</nav>
				<nav id="menu-rastreamento-mobile">
					<ul>
						<li class="hover abre-modal-adicionar-rastreio"><img alt="Incluir rastreamento" src="../img/add-button.png"> <span>Adicionar rastreio</span></li>
						<li class="hover excluir-rastreio"><img alt="Excluir item" src="../img/trash.png"> <span>Excluir rastreio</span></li>
						<li class="hover reenviar-email"><img alt="Encaminhar e-mail" src="../img/encaminhar.svg"> <span>Reenviar e-mail</span></li>
						<li class="hover tooltip reload-integracao-correios"><img alt="Integração correios" src="../img/reload.svg"> <span>Atualizar rastreio</span></li>
						<li class="hover tooltip gerar-link"><img alt="Gerar link" src="../img/link.svg"> <span>Gerar link</span></li>
					</ul>
				</nav>
				<form method="post" name="form-busca-rastreamento" id="form-busca-rastreamento" autocomplete="off" class="form-busca">				
					<input type="text" class="campo-form" id="txt-busca-rastreamento" name="txt-busca-rastreamento" placeholder="Buscar...">
					<input type="submit" value="Buscar" id="btn-buscar-rastreamento" name="btn-buscar-rastreamento" class="botao hover"/>
				</form>
				<nav id="menu-filtro-rastreamento">
					<ul>
						<li class="hover selecionado">Em andamento</li>
						<li class="hover">Finalizados</li>
					</ul>
				</nav>
			</div>
			<div>
				<div class="carregando notificacao"></div>
				<table id="tabela-rastreamento">
					<thead>
						<tr>
							<th class='coluna-mobile'>N° do pedido</th>
							<th>Cliente</th>
							<th class='coluna-mobile'>Forma de venda</th>
							<th>Cod. de Rastreio</th>
							<th class="coluna-icone">Status</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
	    </section>
	</div>

	<div id="modal-imp-plan-rastreamento" class="modal">
    	<h1>Importar planilha de rastreio<span class="modal-close hover">x</span></h1>            
			<section class="modal-body">
				<form name="planilha-rastreamento" id="planilha-rastreamento" method="post" enctype="multipart/form-data" autocomplete="off">
                	<div>
                		<label for="nome-arquivo-rastreio" class="label">Planilha:</label>
                		<input id="nome-arquivo-rastreio" placeholder="Nenhum arquivo selecionado" class="campo-form nome-arquivo" disabled="disabled" />
						<div class="botao-upload">
						    <span>Escolher arquivo</span>
						    <input type="file" id="arquivo-rastreio" class="botao-upload" name="arquivo-rastreio" accept=".xls">
						</div>
                		                		
                		<div class="carregando notificacao notificacao-add-item"></div>                		
                		
                		<input type="submit" id="btn-importar-planilha" name="btn-importar-planilha" value="Enviar" class="botao hover">    
                	</div>
				</form>
            </section>
	</div>

		<div id="modal-adiciona-rastreio" class="modal">
			<h1>Adicionar Rastreio<span class="modal-close hover">x</span></h1>
			<section class="modal-body">
				<div class="carregando notificacao notificacao-add-item"></div>
				<div>
					<form method="post" name="form-add-rastreio" id="form-add-rastreio" autocomplete="off">
						<label class="label" for="txt-numero-pedido">Número do pedido:</label>
						<input type="text" class="campo-form campo-add-item-rastreio" id="txt-numero-pedido-rastreio" name="txt-numero-pedido-rastreio" placeholder="Digite o número do pedido">
						
						<label class="label" for="txt-cliente-rastreio">Cliente:</label>
						<input type="text" class="campo-form campo-add-item-rastreio" id="txt-cliente-rastreio" name="txt-cliente-rastreio" placeholder="Ex: Danilo Parreira">
						
						<label class="label" for="txt-email-rastreio">E-mail:</label>
						<input type="text" class="campo-form" id="txt-email-rastreio" name="txt-email-rastreio" placeholder="Ex: contato@akimia.com.br">
						
						<label class="label" for="forma-venda-cadastro-rastreio">Forma de Venda:</label>
						<select name="forma-venda-cadastro-rastreio" class="campo-form" id="forma-venda-cadastro-rastreio">
						</select>
						
						<label class="label" for="txt-cod-rastreio">Cód. de Rastreio:</label>
						<input type="text" class="campo-form campo-add-item-rastreio" id="txt-cod-rastreio" name="txt-cod-rastreio" placeholder="Ex: BR13NLSMA122I">
						
						<input type="submit" value="Adicionar" id="btn-add-rastreio" name="btn-add-rastreio" class="botao hover"/>
					</form>							
				</div>			
		    </section>
		</div>
		
		<div id="modal-historico-rastreamento" class="modal">
			<h1>Histórico de Rastreamento<span class="modal-close hover">x</span></h1>
			<section class="modal-body">
				<div>
					<ul>
						<li class="hover tooltip reenviar-email" title="Reenviar e-mail de rastreio"><img alt="Encaminhar e-mail" src="../img/encaminhar.svg"></li>
					</ul>
				</div>	
				<div class="tabela-historico-rastreio">
					
				</div>						
		    </section>
		</div>
		
		<div id="modal-forma-venda" class="modal">
			<h1>Forma de Venda<span class="modal-close hover">x</span></h1>
			<section class="modal-body">
				<div>
					<form name="form-forma-venda" id="form-forma-venda" method="post" autocomplete="off">
						<label class="label">Nome:</label>
						<input type="text" class="campo-form" placeholder="Ex: Mercado Livre" name="nome-forma-venda" id="nome-forma-venda" maxlength="30" required="required">

						<label class="label">Sigla:</label>
						<input type="text" class="campo-form" placeholder="Ex: ML" name="sigla-forma-venda" id="sigla-forma-venda" maxlength="2" required="required">
						
						<input type="submit" class="botao hover" name="btn-cadastro-forma-venda" id="btn-cadastro-forma-venda" value="Cadastrar">
					</form>
				</div>
				<div>
					<div class="carregando notificacao notificacao-add-item"></div>
					<table class="tabela" id="tabela-forma-venda">
						<thead>
							<tr>
								<th>Nome</th>
								<th>Sigla</th>
								<th>Ativa</th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</section>
		</div>
		
		
	<div id="modal-link-gerado" class="modal">
    	<h1>Link Gerado<span class="modal-close hover">x</span></h1>            
			<section class="modal-body">
				<div class="carregando notificacao notificacao-add-item"></div>
				<form name="form-forma-venda" id="form-forma-venda" method="post" autocomplete="off">
					<input type='text' class='campo-form' id='link-gerado' value=''>
					<input type="button" value="Copiar link" class="botao hover" id="btn-copiar-link">
				</form>
            </section>
	</div>
	
	<div id="modal-calculadora" class="modal">
    	<h1>Calculadora<span class="modal-close hover">x</span></h1>            
			<section class="modal-body">
				<div>
					<form name="form-calculadora" id="form-calculadora" method="post" autocomplete="off">
						<label class="label" id="label-valor-dolar">Dólar:</label>
						<input type="text" name="valor-dolar" id="valor-dolar" class="campo-form real-calculadora campo-rolagem" placeholder="Valor do dólar" value='<?php echo $dolar->getValorDolar() ?>'>
					
						<label class="label">Tipo do peso:</label>
						<select name="select-tipo-peso" id="select-tipo-peso" class="campo-form">
							<option value="libras">Libras (lb)</option>
							<option value="pounds">Pounds (lb)</option>
							<option value="kilos" selected="selected">Kilos (kg)</option>
							<option value="gramas">Gramas</option>
							<option value="litros">Litros (l)</option>
							<option value="mililitros">Mililitros (ml)</option>
						</select>
						
						<label class="label" id="label-valor-peso-calculadora">Valor peso:</label>
						<input type="text" name="valor-peso-calculadora" id="valor-peso-calculadora" class="campo-form decimal-calculadora campo-rolagem" placeholder="Digite o valor do peso" value="0,00">
						
						<label class="label" id="label-valor-produto-calculadora">Valor produto (U$):</label>
						<input type="text" name="valor-produto-calculadora" id="valor-produto-calculadora" class="campo-form dolar-calculadora campo-rolagem" placeholder="Digite o valor do produto" value='0.00'>
						
						<table class="tabela" id="tabela-lucro-calculadora">
							<thead>
								<tr>
									<th>Percentual desejado</th>
									<th>Valor de Venda</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><input type="text" name="percentual-lucro" id="percentual-lucro" class="campo-form porcentagem-calculadora campo-rolagem" value="0,00"></td>
									<td id="lucro-variavel"></td>
								</tr>
								<tr>
									<td id="label-percentual-lucro">% 30,00</td>
									<td id="lucro-fixo"></td>
								</tr>
							</tbody>
						</table>
						
						<input type="submit" name="btn-calcula-valores" id="btn-calcula-valores" class="botao hover" value="Calcular">
						
					</form>
				</div>
            </section>
	</div>

	<script src="../resources/jquery/jquery-2.1.4.min.js"></script>
	<script src="../resources/jquery/jquery.easing.min.js"></script>
	<script src="../resources/jquery/jquery.maskMoney.js"></script>
	<script src="../resources/jquery/jquery.bpopup.min.js"></script>
	<script type="text/javascript" src="../resources/tooltipster/js/jquery.tooltipster.js"></script>
	<script src="../resources/jquery/jquery.highlight.js"></script>
	<script src="js/padroes.js" type="text/javascript"></script>
	<script src="js/index.js" type="text/javascript"></script>
	<script src="js/local-storage.js" type="text/javascript"></script>
	<script src="js/inicia-fechamento.js" type="text/javascript"></script>
	<script src="js/fechamento.js" type="text/javascript"></script>
	<script src="js/tabelas/tabela-resumo.js" type="text/javascript"></script>
	<script src="js/tabelas/tabela-parametros-calculo.js" type="text/javascript"></script>
	<script src="js/inf-fechamento.js" type="text/javascript"></script>	
	<script src="js/get-total-registros.js" type="text/javascript"></script>
	<script src="js/cadeado.js" type="text/javascript"></script>
	<script src="js/busca-produto-fechamento.js" type="text/javascript"></script>
	<script src="js/exportar-fechamento/exportar-fechamento.js" type="text/javascript"></script>
	<script src="js/exportar-fechamento/busca-fechamento-exportar.js" type="text/javascript"></script>
	<script src="js/historico-fechamentos/historico-fechamentos.js" type="text/javascript"></script>
	<script src="js/historico-fechamentos/busca-fechamento-historico.js" type="text/javascript"></script>	
	<script src="js/historico-fechamentos/excluir-fechamento.js" type="text/javascript"></script>
	<script src="js/lista-compras/lista-compras.js" type="text/javascript"></script>
	<script src="js/adicionar-item/add-item.js" type="text/javascript"></script>
	<script src="js/adicionar-item/add-item-lista-compras.js" type="text/javascript"></script>
	<script src="js/rastreamento/rastreamento.js" type="text/javascript"></script>
	<script src="js/rastreamento/forma-venda.js" type="text/javascript"></script>
	<script src="js/adicionar-item/add-item-rastreio.js" type="text/javascript"></script>
	<script src="js/rastreamento/reenviar-email.js" type="text/javascript"></script>
	<script src="js/calculadora/calculadora.js" type="text/javascript"></script>
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
</body>
</html>
