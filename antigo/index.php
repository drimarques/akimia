<?php
session_start ();

date_default_timezone_set ( 'Brazil/East' );
date_default_timezone_set ( 'America/Sao_Paulo' );

require_once 'src/classes/conexao.class.php';
require_once 'src/classes/cliente.class.php';
require_once 'src/classes/enviar-email.class.php';
require_once 'src/classes/contato.class.php';

require_once 'adm/excel-reader/reader.php';

require_once 'src/classes/compra.class.php';
require_once 'src/dao/comprasDAO.php';

require_once 'src/classes/parametros_calculo.class.php';
require_once 'src/dao/parametrosDAO.php';

require_once 'src/classes/fechamento.class.php';
require_once 'src/dao/fechamentoDAO.php';

require_once 'src/dao/usuarioDAO.php';

$usuarioDAO = new UsuarioDAO ();
$usuarioDAO->validaTempoSession ();
?>
<!DOCTYPE html>
<html>
<head>
<title>Akimia - Produtos Importados</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="Adriano Marques">
<link rel="shortcut icon" href="img/Logo.Akimia.transp.750x750.png">
<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="bootstrap/bootstrap-theme.min.css">
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<script src="jquery/jquery-2.1.3.min.js"></script>
<script src="js/akimia.js"></script>

<script type="text/javascript">
		function googleTranslateElementInit() {
  			new google.translate.TranslateElement({pageLanguage: 'pt', includedLanguages: 'en,es,pt', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
  			$("body").removeAttr("style");
		}		
</script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

</head>
<body onload="verificaDevice()">	  
	  <?php
			if (isset ( $_SESSION ["notificacao_lista"] )) {
				echo "<div class='alert alert-warning' id='notifica_importacao_lista'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>{$_SESSION["notificacao_lista"]}</div>";
				unset ( $_SESSION ["notificacao_lista"] );
			}
			
			if (isset ( $_SESSION ["notificacao_sem_fechamento"] )) {
				echo "<div class='alert alert-warning' id='notifica_sem_fechamento'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>{$_SESSION["notificacao_sem_fechamento"]}</div>";
				unset ( $_SESSION ["notificacao_sem_fechamento"] );
			}
			?>

	  <div id="social">
		<table>
			<tr>
				<td><a href="https://www.facebook.com/akimiaimportados"
					target="_blank"><img src="img/facebook.png" alt="Facebook Akimia"
						id="img_notifica_facebook"></a></td>
				<td><a href="https://www.facebook.com/akimiaimportados"
					target="_blank" id="notifica_facebook">facebook.com/akimiaimportados</a></td>
			</tr>
			<tr>
				<td><a href="https://instagram.com/akimia_importados"
					target="_blank"><img src="img/instagram.png" alt="Instagram Akimia"
						id="img_notifica_intagram"></a></td>
				<td><a href="https://instagram.com/akimia_importados"
					target="_blank" id="notifica_instagram">@akimia_importados</a></td>
			</tr>
			<tr>
				<td><a href="#" onclick="selecionaMenuFaleConosco('desktop');"
					class="chama-fale-conosco"><img src="img/mail.png"
						alt="Email Akimia" id="img_notifica_email"></a></td>
				<td><a href="#" onclick="selecionaMenuFaleConosco('desktop');"
					class="chama-fale-conosco" id="notifica_email">contato@akimia.com.br</a></td>
			</tr>
			<tr>
				<td><a href="#" onclick="selecionaMenuFaleConosco('desktop');"
					class="chama-fale-conosco"><img src="img/whatsapp.png"
						alt="WhatsApp Akimia" id="img_notifica_whatsapp"></a></td>
				<td><a href="#" onclick="selecionaMenuFaleConosco('desktop');"
					class="chama-fale-conosco" id="notifica_whatsapp">+55 11 98931-9713</a></td>
			</tr>
			<tr>
				<td><img src="img/3pontos.png" alt="Acesso restrito Akimia"
					id="img-acesso-restrito"></td>
			</tr>
		</table>
	</div>

	<div id="google_translate_element"></div>

	<section id="logo">
		<a href="index.php"><img src="img/Logo.Akimia.transp.750x750.png"
			alt="Akimia Produtos Importados"></a>
	</section>

	<!--Painel de acesso restrito desktop-->
	<section id="sessao-restrito">
		<div id="autenticacao">
			<h2>
				<img src="img/password.png" alt="Acesso restrito Akimia">Acesso
				restrito
			</h2>
			<form name="form_autenticao" id="form_autenticao" method="post"
				autocomplete="off">
				<input type="email" id="email_login_desktop"
					name="email_login_desktop" placeholder="email@exemplo.com"
					class="form-control"> <input type="password"
					id="senha_login_desktop" name="senha_login_desktop"
					placeholder="Digite sua senha" class="form-control"> <input
					type="button" class="btn btn-default" value="Entrar"
					id="btn_login_desktop" name="btn_login_desktop">
				<div class="clear"></div>
				<div id="notifica_erro_login_desktop"
					class="alert alert-danger notificacao"></div>
			</form>
		</div>
	</section>

	  <?php
			require_once 'src/importa_planilha.php';
			?>

	  <!--Menu mobile-->
	<nav class="navbar navbar-inverse">
		<div class="container">
			<div class="navbar-header">

				<!--Responsivo-->
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>

				<img onclick="selecionaAcessoRestrito()" id="acesso_restrito_mobile"
					src="img/password_mobile.png" alt="Acesso restrito"> <a
					class="navbar-brand" href="index.php"> <img alt="Logo Akimia"
					src="adm/img/Logo.Akimia.Mini.png">
				</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li id="menu-cadastro-mobile" class="active"><a href="#cadastro"
						onclick="selecionaMenuCadastro('mobile');">Cadastro</a></li>
					<li id="menu-como-encomendar-mobile"><a href="#como-encomendar"
						onclick="selecionaMenuComoEncomendar('mobile');">Como encomendar?</a></li>
					<li id="menu-a-akimia-mobile"><a href="#a-akimia"
						onclick="selecionaMenuAAkimia('mobile');">A Akimia</a></li>
					<li id="menu-fale-conosco-mobile"><a href="#fale-conosco"
						onclick="selecionaMenuFaleConosco('mobile');">Fale Conosco</a></li>
				</ul>
			</div>
		</div>
	</nav>

	<section id="principal">
		<!--Menu desktop-->
		<ul class="menu-desktop">
			<li id="menu-a-akimia-desktop"><a href="#"
				onclick="selecionaMenuAAkimia('desktop');">A Akimia</a></li>
			<li id="menu-como-encomendar-desktop"><a href="#"
				onclick="selecionaMenuComoEncomendar('desktop');">Como encomendar?</a></li>
			<li class="active" id="menu-cadastro-desktop"><a href="#"
				onclick="selecionaMenuCadastro('desktop');">Cadastro</a></li>
			<!--<li id="menu-fale-conosco-desktop"><a href="#" onclick="selecionaMenuFaleConosco('desktop');">Fale Conosco</a></li>-->
		</ul>

		<!--Cadastro-->
		<div id="cadastro" class="central">
			<h1>Cadastro</h1>
			<h4>
				<b>Atenção!</b> Campos com (<span class="campos-obrigatorios">*</span>)
				são obrigatórios
			</h4>
			<form class="form-horizontal" method="post" id="formCadastro"
				name="formCadastro" autocomplete="off">
				<div class="form-group">
					<label for="nomeCadastro" class="col-sm-2 control-label"><span
						class="campos-obrigatorios">*</span>Nome completo</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="nomeCadastro"
							name="nomeCadastro" placeholder="Digite seu nome" maxlength="50"
							required="required">
					</div>
				</div>
				<div class="form-group">
					<label for="apelidoCadastro" class="col-sm-2 control-label">Como
						gosta de ser chamado?</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="apelidoCadastro"
							name="apelidoCadastro" placeholder="Digite aqui" maxlength="20">
					</div>
				</div>
				<div class="form-group">
					<label for="emailCadastro" class="col-sm-2 control-label"><span
						class="campos-obrigatorios">*</span>E-mail</label>
					<div class="col-sm-10">
						<input type="email" class="form-control" id="emailCadastro"
							name="emailCadastro" placeholder="Digite seu e-mail"
							required="required">
					</div>
				</div>
				<div class="form-group">
					<label for="whatsAppCadastro" class="col-sm-2 control-label">WhatsApp</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="whatsAppCadastro"
							name="whatsAppCadastro" placeholder="Apenas números">
					</div>
				</div>
				<div class="form-group">
					<label for="telCadastro" class="col-sm-2 control-label">Telefone
						fixo</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="telCadastro"
							name="telCadastro" placeholder="Apenas números">
					</div>
				</div>
				<div class="form-group">
					<label for="celCadastro" class="col-sm-2 control-label">Celular</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="celCadastro"
							name="celCadastro" placeholder="Apenas números">
					</div>
				</div>
				<div class="form-group">
					<label for="cepCadastro" class="col-sm-2 control-label">CEP</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="cepCadastro"
							name="cepCadastro" placeholder="Somente números">
					</div>
				</div>
				<div class="form-group">
					<label for="enderecoCadastro" class="col-sm-2 control-label">Endereço</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="enderecoCadastro"
							name="enderecoCadastro" placeholder="Digite seu endereço"
							readonly="readonly">
					</div>
				</div>
				<div class="form-group">
					<label for="numEnderecoCadastro" class="col-sm-2 control-label">Número</label>
					<div class="col-sm-10">
						<input type="number" class="form-control" id="numEnderecoCadastro"
							name="numEnderecoCadastro" min="1" placeholder="Digite o número"
							pattern="[0-9]+$" readonly="readonly">
					</div>
				</div>
				<div class="form-group">
					<label for="complementoCadastro" class="col-sm-2 control-label">Complemento</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="complementoCadastro"
							name="complementoCadastro" placeholder="Complemento"
							maxlength="100" readonly="readonly">
					</div>
				</div>
				<div class="form-group">
					<label for="referenciaCadastro" class="col-sm-2 control-label">Referência</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="referenciaCadastro"
							name="referenciaCadastro" placeholder="Referência"
							maxlength="100" readonly="readonly">
					</div>
				</div>
				<div class="form-group">
					<label for="estadoCadastro" class="col-sm-2 control-label">Estado</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="estadoCadastro"
							name="estadoCadastro" placeholder="Digite seu estado"
							readonly="readonly">
					</div>
				</div>
				<div class="form-group">
					<label for="cidadeCadastro" class="col-sm-2 control-label">Cidade</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="cidadeCadastro"
							name="cidadeCadastro" placeholder="Digite sua cidade"
							readonly="readonly">
					</div>
				</div>
				<div class="form-group">
					<label for="bairroCadastro" class="col-sm-2 control-label">Bairro</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="bairroCadastro"
							name="bairroCadastro" placeholder="Digite seu bairro"
							readonly="readonly">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-default"
							name="btnCadastrarCliente" id="btnCadastrarCliente">Cadastrar</button>
					</div>
				</div>
			</form>

		    <?php
						// Botão btnCadastrarCliente
						require_once 'src/botao-cadastrar-cliente.click.php';
						?>
	       </div>

		<!--Como encomendar-->
		<div id="como-encomendar" class="central">
			<h1>Como encomendar?</h1>
			<section id="conteudo-como-encomendar">
				<ol>
					<li>Envie-nos email (contato@akimia.com.br) ou mensagem inbox, com
						o produto desejado com o CEP de destino;</li>
					<li>Faremos um orçamento e responderemos seu email/mensagem
						informando o valor do produto e frete;</li>
					<li>Aprovando o orçamento, solicitamos um depósito de 30% do
						total da compra;</li>
					<li>Feito o depósito, envie o comprovante do depósito e assim sua
						encomenda estará confirmada;</li>
					<li>Quando o produto chegar, mandaremos um email/mensagem
						confirmando a compra e solicitando o depósito do valor restante +
						frete;</li>
					<li>Envie-nos o comprovante do pagamento que providenciaremos a
						entrega do produto;</li>
				</ol>
				<h3>Observações</h3>
				<ul>
					<li>A entrega do produto seguirá a ordem de confirmação do
						pedido (pagamento). Para receber primeiro confirme seu pedido com
						antecedência;</li>
					<li>Caso não encontremos o produto solicitado, faremos a
						devolução integral do valor adiantado (30%);</li>
					<li>O prazo para pagamento do valor restante é de 30 dias. Em caso
						de não pagamento o produto será disponibilizado para venda sem
						aviso prévio e o valor do adiantamento não será devolvido;</li>
					<li>Em caso de desistência o valor do adiantamento não será
						devolvido;</li>
					<li>O frete será cobrado inclusive para as entregas realizadas na
						cidade de São Paulo;</li>
				</ul>
			</section>
		</div>

		<!--A Akimia-->
		<div id="a-akimia" class="central">
			<h1>A Akimia</h1>

			<section id="conteudo-a-akimia">
				<h2>História</h2>
				<p>Em 2012 na comemoração do Natal, três primas brincavam que no
					ano seguinte programariam uma viagem à Miami para passear e fazer
					compras.</p>
				<p>Durante o planejamento da tal viagem, muitos amigos e parentes
					começaram a pedir que lhes trouxessem os mais diversos produtos e
					então, surgiu a idéia de atender à essas encomendas.</p>
				<p>O sucesso foi tão grande que nossa fama se espalhou rapidamente
					e todos cobravam uma nova viagem para que pudessem novamente fazer
					suas encomendas.</p>
				<p>Investimos então na criação da AKIMIA PRODUTOS IMPORTADOS. Uma
					empresa que nos proporciona estar mais perto de nosso filhos e
					casas e ainda nos traz a satisfação de atender a todos os nossos
					clientes.</p>

				<h2>Nome Akimia</h2>
				<p>Uma brincadeira com a as palavras.</p>
				<p>Algo como Miami (MIA) está aqui (AKI).</p>

				<h2>Missão</h2>
				<p>Atender os nossos clientes de forma personalizada e
					individualizada identificando as reais necessidades sem deixar de
					lado o respeito, a honestidade e o carinho proporcionando as
					pessoas que não podem viajar ao exterior a chance de adquirir
					produtos originais e de qualidade diretamente do Brasil com preço
					justo e entrega garantida.</p>

				<h2>Visão</h2>
				<p>Tornar-se referência em importação de produtos de grandes
					marcas com comprometimento e qualidade.</p>

				<h2>Valores</h2>
				<p>Respeito</p>
				<p>Honestidade</p>
				<p>Originalidade</p>
				<p>Personalização</p>
				<p>Agilidade</p>
				<p>Atendimento eficaz</p>
			</section>
		</div>

		<!--Fale conosco-->
		<div id="fale-conosco" class="central">
			<h1>Fale Conosco</h1>
			<section id="conteudo-fale-conosco">
				<ul id="nav-contats">
					<!--<li><a href="http://www.akimia.com.br" target="_blank"><img src="img/site.png" alt="Url site Akimia">www.akimia.com.br</a></li>-->
					<!--<li><a href="https://www.facebook.com/akimiaimportados" target="_blank"><img src="img/facebook.png" alt="Facebook Akimia">facebook.com/akimiaimportados</a></li>-->
					<!--<li><a href="http://instagram.com/akimia_importados" target="_blank"><img src="img/instagram.png" alt="Instagram Akimia">@akimia_importados</a></li>-->
					<li><a href="#" class="chama-fale-conosco"><img src="img/mail.png"
							alt="E-mail Akimia">contato@akimia.com.br</a></li>
					<li><a href="#fale-conosco"><img src="img/whatsapp.png"
							alt="WhatsApp Akimia">+55 11 98931-9713</a></li>
				</ul>
				<br>
				<h4>
					<b>Atenção!</b> Campos com (<span class="campos-obrigatorios">*</span>)
					são obrigatórios
				</h4>
				<form class="form-horizontal" method="post" id="formContato"
					name="formContato">
					<div class="form-group">
						<label for="nomeContato" class="col-sm-2 control-label"><span
							class="campos-obrigatorios">*</span>Nome</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="nomeContato"
								name="nomeContato" placeholder="Digite seu nome"
								required="required" maxlength="50">
						</div>
					</div>
					<div class="form-group">
						<label for="emailContato" class="col-sm-2 control-label"><span
							class="campos-obrigatorios">*</span>E-mail</label>
						<div class="col-sm-10">
							<input type="email" class="form-control" id="emailContato"
								name="emailContato" placeholder="Digite seu e-mail"
								required="required">
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-sm-2 control-label"><span
							class="campos-obrigatorios">*</span>Assunto</label>
						<div class="col-sm-10">
							<select class="form-control" name="assuntoContato"
								id="assuntoContato">
								<option disabled="disabled" value="" selected="selected">Selecione
									um assunto:</option>
								<option value="Críticas">Críticas</option>
								<option value="Dúvidas">Dúvidas</option>
								<option value="Garantia">Garantia</option>
								<option value="Orçamento">Orçamento</option>
								<option value="Sugestão/Elogio">Sugestão/Elogio</option>
								<option value="Outro">Outro</option>
							</select>
						</div>
					</div>
					<!--		    <div class="form-group">
						       <label for="assuntoContato" class="col-sm-2 control-label">Assunto</label>
						       <div class="col-sm-10">
							    <input type="text" class="form-control" id="assuntoContato" name="assuntoContato" placeholder="Digite o assunto" required="required" maxlength="20">
						       </div>
						  </div>-->
					<div class="form-group">
						<label for="mensagemContato" class="col-sm-2 control-label"><span
							class="campos-obrigatorios">*</span>Mensagem</label>
						<div class="col-sm-10">
							<textarea class="form-control" rows="4"
								placeholder="Digite sua mensagem" name="mensagemContato"
								id="mensagemContato" required="required" maxlength="400"></textarea>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-default"
								name="btnEnviarFaleConosco" id="btnEnviarFaleConosco">Enviar</button>
						</div>
					</div>
				</form>
			 <?php
				/* Botão btnEnviarFaleConosco */
				require_once 'src/botao-enviar-email.click.php';
				?>
		    </section>
		</div>


		<!--Login mobile-->
		<div id="sessao-restrito-mobile" class="central">
			<h1 id="title-acesso-restrito">Acesso restrito</h1>
			<form name="form_autenticao_mobile" id="form_autenticao_mobile"
				method="post" autocomplete="off">
				<input type="email" id="email_login_mobile"
					name="email_login_mobile" placeholder="email@exemplo.com"
					class="form-control"> <input type="password"
					id="senha_login_mobile" name="senha_login_mobile"
					placeholder="Digite sua senha" class="form-control"> <input
					type="button" class="btn btn-default" value="Entrar"
					id="btn_login_mobile" name="btn_login_mobile">
				<div class="clear"></div>
				<div id="notifica_erro_login_mobile"
					class="alert alert-danger notificacao"></div>
			</form>
		</div>
	</section>

	<!--Rodap�-->
	<footer>
		<div id="copyright">Copyright &#169; 2012 - <?php echo date('Y') ?> - Akimia Produtos Importados</div>
		<div id="dev"> Desenvolvido por <a href="http://www.facebook.com.br/adrianomarques9" target="_blank">Adriano Marques</a>.</div>
	</footer>

	<script src="jquery/jquery-2.1.3.min.js" type="text/javascript"></script>
	<script src="bootstrap/bootstrap.min.js" type="text/javascript"></script>

	<!--Validar CEP-->
	<!--<script src="http://code.jquery.com/jquery-1.7.1.min.js" type="text/javascript"></script> S� rodou nessa versão do jQuery-->
	<script src="jquery/jquery-1.7.1.min.js" type="text/javascript"></script>
	<!--Biblioteca do scrol suave, juntamente com jQuery-->
	<script src="jquery/jquery.nicescroll.min.js"></script>
	<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
	<script src="js/gmaps.js" type="text/javascript"></script>
	<script src="js/cep.js" type="text/javascript"></script>
	<script>
			 $(function () {
			      wscep({map: 'map1', auto: true});
			 });
	  </script>
	<script src="js/akimia.js" type="text/javascript"></script>
	<script src="js/autenticacao.js" type="text/javascript"></script>
</body>
</html>