function notificaErroLogin(mensagem, id_campo, device) {
     var campo_notificacao = "notifica_erro_login_" + device;
     $("#" + campo_notificacao).text(mensagem);
     $("#" + campo_notificacao).fadeIn("fast");
     $("#" + id_campo).css({"border": "solid 1px red"});
}

function limpaErros(device) {
     var campo_email = "email_login_" + device;
     var campo_senha = "senha_login_" + device;
     var campo_notificacao = "notifica_erro_login_" + device;
     $("#" + campo_email).css({"border": "solid 1px #ccc"});
     $("#" + campo_senha).css({"border": "solid 1px #ccc"});
     $("#" + campo_notificacao).css({"display": "none"});
}

function validaLogin(email, senha, device) {
     limpaErros(device);
     var campo_email = "email_login_" + device;
     var campo_senha = "senha_login_" + device;
     var teve_erro = false;
     if ((email === null) || (email === "")) {
	  teve_erro = true;
	  notificaErroLogin("O e-mail não pode ser vazio.", campo_email, device);
     }
     else {
	  validaEmail = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2}/;
	  if (validaEmail.exec(email)) {

	  }
	  else {
	       teve_erro = true;
	       notificaErroLogin("Formato de e-mail inválido", campo_email, device);
	  }
     }

     if (teve_erro === false) {
	  if ((senha === null) || (senha === "")) {
	       teve_erro = true;
	       notificaErroLogin("A senha não pode ser vazia.", campo_senha, device);
	  }
     }

     return teve_erro;
}

function validaEmailSenha(email, senha, device) {
     $.ajax({
	  url: "src/autenticacao/valida-login.php",
	  data: {email: email, senha: senha},
	  type: 'POST',
	  context: $("#form_autenticao"),
	  success: function (retorno) {
	       if (retorno === "erro") {
		    var campo_email = "email_login_" + device;
		    var campo_senha = "senha_login_" + device;
		    $("#" + campo_email).css({"border": "solid 1px red"});
		    $("#" + campo_senha).css({"border": "solid 1px red"});
		    notificaErroLogin("E-mail ou senha incorretos", null, device);
	       }
	       else {
		    //Mostra o novo painel
		    verificaUsuarioLogado(device);
	       }
	  }
     });
}

function verificaUsuarioLogado(device) {
     $.ajax({
	  url: "src/autenticacao/usuario-logado.php",
	  type: 'POST',
	  context: $("#sessao-restrito"),
	  success: function (retorno) {
	       if (retorno !== "") {
		    switch (device) {
			 case "desktop":
			      console.log("logado logado com sucesso");
			      $("#autenticacao").css({"display": "none"});
			      $("#sessao-restrito").html(retorno);
			      break;
			 case "mobile":
			      location.href = "adm/compras.php";
			      break;
		    }
	       }
	       else {
		    console.log("não está logado");
//		    location.href = "index.php";
	       }
	  }
     });
}

function verificaDevice() {
     var largura = $('body').width();
     //Se for menor que 1007px ele redireciona
     if (largura < 1007) {
	  verificaUsuarioLogado("mobile");
     }
     else if (largura >= 1008) {
	  verificaUsuarioLogado("desktop");
     }
}

$(document).ready(function () {
     $("#img-acesso-restrito").click(function () {
	  $("#sessao-restrito").fadeToggle("fast");
	  $("#email_login").focus();
     });
     $("#btn_login_desktop").click(function () {
	  var email = $("#email_login_desktop").val();
	  var senha = $("#senha_login_desktop").val();
	  if (!validaLogin(email, senha, "desktop")) {
	       validaEmailSenha(email, senha, "desktop");
	  }
     });
     $("#btn_login_mobile").click(function () {
	  var email = $("#email_login_mobile").val();
	  var senha = $("#senha_login_mobile").val();
	  if (!validaLogin(email, senha, "mobile")) {
	       validaEmailSenha(email, senha, "mobile");
	  }
     });
     //Deslogar
     $("#sessao-restrito").delegate('#btn_logout', 'click', function () {
	  $.ajax({
	       url: "src/autenticacao/logout.php",
	       type: 'POST',
	       context: $("autenticacao"),
	       success: function () {
		    location.href = "index.php";
	       }
	  });
     });
     //Fazendo abrir a caixa de selecao de arquivo
     $("#sessao-restrito").delegate('#importar_lista', 'click', function () {
	  var botao = document.getElementById("arquivo_excel");
	  botao.click();
     });


     $("#sessao-restrito").delegate("#arquivo_excel", 'change', function () {
	  var botao = document.getElementById("btnEnviarExcel");
	  botao.click();
     });


     $("body").mousemove(function () {
	  $.ajax({
	       url: "src/autenticacao/renova-session.php",
	       success: function () {

	       }
	  });
     });


//     $("#sessao-restrito").delegate("#abre_fechamento", 'click', function () {
//	  $.ajax({
//	       url: "src/autenticacao/valida-fechamento.php",
//	       data: {tela: "fechamento"},
//	       type: 'POST',
//	       success: function (retorno) {
//		    if (retorno === "sem fechamento") {
//			 location.href = "index.php";
//		    }
//		    else {
//			 window.open("adm/fechamento.php");
//		    }
//	       }
//	  });
//     });


//     $("#sessao-restrito").delegate("#abre_listacompras", 'click', function () {
//	  $.ajax({
//	       url: "src/autenticacao/valida-fechamento.php",
//	       data: {tela: "lista de compras"},
//	       type: 'POST',
//	       success: function (retorno) {
//		    if (retorno === "sem lista de compras") {
//			 location.href = "index.php";
//		    }
//		    else {
//			 window.open("adm/compras.php");
//		    }
//	       }
//	  });
//     });


});