<?php
     session_start();
     if (!isset($_SESSION["usuario_logado"])) {
	  header("Location: ../index.php");
	  die();
     }

     require_once '../src/classes/conexao.class.php';

     require_once '../src/classes/fechamento.class.php';
     require_once '../src/dao/fechamentoDAO.php';

     require_once '../src/dao/usuarioDAO.php';

     $usuarioDAO = new UsuarioDAO();
     $usuarioDAO->validaTempoSession();

     $daoFECHAMENTO = new fechamentoDAO();
?>
<!DOCTYPE html>
<html lang="pt-br">
     <head>
	  <title>Histórico de Fechamentos - Akimia</title>
	  <meta charset="UTF-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	  <meta name="author" content="Adriano Marques">
	  <link rel="shortcut icon" href="img/Logo.Akimia.Mini.png">
	  <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	  <link rel="stylesheet" href="../bootstrap/bootstrap.min.css">
	  <link rel="stylesheet" href="../bootstrap/bootstrap-theme.min.css">
	  <link href="css/historico-fechamentos.css" rel="stylesheet" type="text/css">
	  <script src="../jquery/jquery.min.js"></script>
	  <script type="text/javascript" src="google/jsapi"></script>
	  <script type='text/javascript'>
	       var google;
	       google.load('visualization', '1', {packages: ['corechart']});
//	       google.setOnLoadCallback(drawVisualization);	      
	  </script>
     </head>
     <body>
	  <header>
	       <h1>
		    <img onclick="location.href = 'historico-fechamentos.php'" src="img/Logo.Akimia.Mini.png" alt="Akimia">Histórico de Fechamentos		    
	       </h1>	  	      
	  </header>

	  <section id="painel-fechamentos">
	       <h2 id="abre-lista-fechamentos">Temporadas fechadas <img src="img/seta-black.png" alt="Abre temporadas"></h2>
	       <ul id="lista-painel-fechamentos">
		    <?php
			 $daoFECHAMENTO->listaFechamentos();
		    ?>		    
	       </ul>
	       <div id='btn-gerar-tudo'>GERAR</div>
	       <div class="clear"></div>
	       <div class="alert alert-warning" id='sem-periodo-selecionado'>Por favor selecione uma temporada.</div>
	  </section>

	  
	  <div id="loading-resultados">
	       
	  </div>
	  
	  <section id="painel-resumos">

	  </section>	 

	  <section id="painel-grafico">
	       
	  </section>

	  <script src="../jquery/jquery.min.js"></script>
	  <script src="../bootstrap/bootstrap.min.js" type="text/javascript"></script>
	  <script src="js/renova-session.js"></script>
	  <script src="../jquery/jquery-color-animation/mainfile"></script>
	  <script src=js/historico-fechamentos.js></script>
     </body>
</html>