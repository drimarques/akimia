<?php

     require_once '../../src/classes/conexao.class.php';

     require_once '../../src/classes/compra.class.php';
     require_once '../../src/dao/comprasDAO.php';

     require_once '../../src/classes/parametros_calculo.class.php';
     require_once '../../src/dao/parametrosDAO.php';

     require_once '../../src/classes/fechamento.class.php';
     require_once '../../src/dao/fechamentoDAO.php';

     $fechamentos = $_POST['fechamentos'];

     $script = "<script>
		    function mostraGrafico() {
			 // Some raw data (not necessarily accurate)
			 var data = google.visualization.arrayToDataTable([
			      ['Temporada', 'Custo Total EUA', 'Custo Total', 'Venda Total', 'Lucro real'],";

     for ($i = 0; $i < count($fechamentos); $i++) {
	  $fechamento = new Fechamento();
	  $fechamentoDAO = new fechamentoDAO();
	  $comprasDAO = new comprasDAO();
	  $parametrosDAO = new parametrosDAO();
	  
	  $fechamento = $fechamentoDAO->getDadosFechamento($fechamentos[$i]);
	  
	  $parametro = $parametrosDAO->getDadosParametroPorIdFechamento($fechamentos[$i]);
	  $despesaViagem = $parametro->GetDespesaViagem();
	  $lucroVerdadeiro = $comprasDAO->getTotalLucroTotalPorIdFechamento($fechamentos[$i]) - $despesaViagem;

	  $script .= "['{$fechamento->GetNomeFechamento()}', {$comprasDAO->getTotalValorDolarPorIdFechamento($fechamentos[$i])}, {$comprasDAO->getTotalCustoTotalPorIdFechamento($fechamentos[$i])}, {$comprasDAO->getTotalVendaTotalPorIdFechamento($fechamentos[$i])}, $lucroVerdadeiro],";
     }

     $script2 = substr($script, 0, -1);

     $script2 .= "]);
			 var options = {
			      title: 'Evolução das temporadas',
			      vAxis: {title: 'Grana'},
			      hAxis: {title: 'Temporada'},
			      seriesType: 'bars'
				      //series: {5: {type: 'line'}}
			 };

			 var chart = new google.visualization.ComboChart(document.getElementById('painel-grafico'));
			 chart.draw(data, options);
		    }
	       </script>";


     echo $script2;

     