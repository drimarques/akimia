<?php

     require_once '../../src/classes/conexao.class.php';
     
     require_once '../../src/classes/compra.class.php';
     require_once '../../src/dao/comprasDAO.php';

     require_once '../../src/classes/parametros_calculo.class.php';
     require_once '../../src/dao/parametrosDAO.php';

     require_once '../../src/classes/fechamento.class.php';
     require_once '../../src/dao/fechamentoDAO.php';

     $fechamentos = $_POST['fechamentos'];
     $comprasDAO = new comprasDAO();

     for ($i = 0; $i < count($fechamentos); $i++) {	  
	  $comprasDAO->getTabelaResumoPorIdFechamento($fechamentos[$i]);
     }
