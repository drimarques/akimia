<?php
     session_start();
     if (!isset($_SESSION["usuario_logado"])) {
	  header("Location: ../index.php");
	  die();
     }

     require_once '../src/classes/conexao.class.php';
     require_once '../src/classes/compra.class.php';
     require_once '../src/dao/comprasDAO.php';
     
     require_once '../src/classes/parametros_calculo.class.php';
     require_once '../src/dao/parametrosDAO.php';
     
     require_once '../src/classes/fechamento.class.php';
     require_once '../src/dao/fechamentoDAO.php';

     require_once '../src/dao/usuarioDAO.php';

     $usuarioDAO = new UsuarioDAO();
     $usuarioDAO->validaTempoSession();

     $dao = new comprasDAO();
     $parametrosDAO = new parametrosDAO();

     $daoFECHAMENTO = new fechamentoDAO();
     $fechamento = $daoFECHAMENTO->getFechamentoAtual();
     if($fechamento->GetIdFechamento() == null){
	  header("Location: ../index.php");
	  die();
     }
?>
<!DOCTYPE html>
<html lang="pt-br">
     <head>
	  <title>Lista de compras - Akimia</title>
	  <meta charset="UTF-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	  <meta name="author" content="Adriano Marques">
	  <link rel="shortcut icon" href="img/Logo.Akimia.Mini.png">
	  <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	  <link rel="stylesheet" href="../bootstrap/bootstrap.min.css">
	  <link rel="stylesheet" href="../bootstrap/bootstrap-theme.min.css">
	  <link href="css/estilo.css" rel="stylesheet" type="text/css">
	  <script src="../jquery/jquery-2.1.3.min.js"></script>
	  <script src="js/renova-session.js"></script>
     </head>
     <body>	  
	  <header class="header">	       
	       <h1><img onclick="location.href = 'index.php'" src="img/Logo.Akimia.Mini.png" alt="Akimia">Lista de Compras</h1>
	  </header>
	  <?php
	       $parametrosDAO->listaParametros();
	       $dao->listaCompras();
	  ?>

	  <script src="../jquery/jquery-2.1.3.min.js" type="text/javascript"></script>
	  <script src="../bootstrap/bootstrap.min.js" type="text/javascript"></script>
	  <!--Biblioteca do scrol suave, juntamente com jQuery-->
	  <script src="../jquery/jquery.nicescroll.min.js"></script> 	   
     </body>
</html>


