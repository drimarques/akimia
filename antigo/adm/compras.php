<?php
     session_start();
     if (!isset($_SESSION["usuario_logado"])) {
	  header("Location: ../index.php");
	  die();
     }

     require_once '../src/classes/conexao.class.php';
     require_once '../src/classes/compra.class.php';
     require_once '../src/dao/comprasDAO.php';

     require_once '../src/classes/fechamento.class.php';
     require_once '../src/dao/fechamentoDAO.php';

     require_once '../src/dao/usuarioDAO.php';

     $usuarioDAO = new UsuarioDAO();
     $usuarioDAO->validaTempoSession();

     $daoFECHAMENTO = new fechamentoDAO();
     $fechamento = $daoFECHAMENTO->getFechamentoAtual();
?>
<!DOCTYPE html>
<html lang="pt-br">
     <head>
	  <title>Compras - Akimia</title>
	  <meta charset="UTF-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	  <meta name="author" content="Adriano Marques">
	  <link rel="shortcut icon" href="img/Logo.Akimia.Mini.png">
	  <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	  <link rel="stylesheet" href="../bootstrap/bootstrap.min.css">
	  <link rel="stylesheet" href="../bootstrap/bootstrap-theme.min.css">
	  <link href="css/compras.css" rel="stylesheet" type="text/css">
	  <script src="../jquery/jquery-2.1.3.min.js"></script>
	  <script src="js/selecionadores.js"></script>
	  <script src="js/verificadores.js"></script>
	  <script src="js/compras.js" type="text/javascript"></script>
	  <script src="js/adiciona-item.js" type="text/javascript"></script>
	  <script src="js/informa-qtd-comprada.js" type="text/javascript"></script>
	  <script src="js/get-cliente-compra.js" type="text/javascript"></script>
	  <?php
	       if ($fechamento->GetIdFechamento() == null) {
		    echo "Não há lista de compras. Necessário importar planilha.";
		    die();
	       }
	  ?>

	  <script>
	       function mascaraTelefone(t, mask) {
		    var i = t.value.length;
		    var saida = mask.substring(1, 0);
		    var texto = mask.substring(i)
		    if (texto.substring(0, 1) != saida) {
			 t.value += texto.substring(0, 1);
		    }
	       }
	  </script>
     </head>
     <body onload="verificaLocalStorage()">
	  <header>
	       <section id="menu-mobile">
		    <div id="close_menu_mobile">
			 <img src="img/logout.png" alt="Botão de logout" id="btn_logout_mobile">
			 <img src="img/fecha-menu-mobile.png" alt="Menu" id="fecha-menu-mobile">
			 <img src="img/close_menu_mobile.png" alt="Menu" id="hamburguer_close">			
		    </div>

		    <div id="div-filtros">
			 <section class="filtros">
			      <div id="btn_n_comprados" class="filtro_selecionado"><img src="img/unlike.png" alt="Like">Não comprados</div>
			      <div id="btn_comprados"><img src="img/like.png" alt="Like">Comprados</div>
			 </section>

			 <section class="titulo_filtro" id="abre_sexo">Sexo <img src="img/seta.png"></section>
			 <section class="filtros" id="espaco_sexo">
			      <div id="btn_masculino" class="filtro_sexo"><img src="img/masculino.png">Masculino</div>
			      <div id="btn_feminino" class="filtro_sexo"><img src="img/feminino.png">Feminino</div>
			      <div id="btn_unissex" class="filtro_sexo"><img src="img/indiferente.png">Unissex</div>
			 </section>

			 <section class="titulo_filtro" id="abre_marcas">Marcas <img src="img/seta.png"></section>			 
			 <section class="filtros" id="espaco_marcas"></section>

			 <section class="titulo_filtro" id="abre_tipo">Tipo <img src="img/seta.png"></section>			 
			 <section class="filtros" id="espaco_tipos"></section>
		    </div>
	       </section>

	       <h1 id="menu-header">
		    <img onclick="location.href = 'compras.php'" src="img/Logo.Akimia.Mini.png" alt="Akimia">
	       </h1>

	       <img src="img/3pontos.png" alt="Menu" id="hamburguer_open">

	       <img src="img/buscar.png" alt="Buscar" id="img_buscar">	       	  
	       <div id="filtro_pesquisa">
		    <form method="post" name="form_busca" id="form_busca" autocomplete="off">
			 <input type="text" placeholder="Buscar" id="txtBusca" name="txtBusca" class="form-control" autofocus="autofocus">
			 <img src="img/close.png" alt="Fechar busca" id="close_busca">
		    </form>
	       </div>

	       <div class="btns_menu">
		    <img src="img/like.png" alt="Marca comprado" onclick="marcaComprado()">
	       </div>

	       <div class="btns_menu">
		    <img src="img/unlike.png" alt="Marca não comprado" onclick="marcaNaoComprado()">
	       </div>

	       <div class="btns_menu">
		    <img src="img/compras.png" alt="Informa quantidade" onclick="abreModalQtdComprada()">
	       </div>

	       <div class="btns_menu">
		    <img src="img/add_item.png" alt="Adiciona item" onclick="abreModal()">
	       </div>

	  </header>

	  <article id="lista_compras"></article>

	  <div id='notificacao_lista_compras'></div>

	  <!--Modal Add Item-->
	  <div id="modalAddItem" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	       <div class="modal-dialog modal-sm">
		    <div class="modal-content">
			 <div class="modal-header">
			      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      <div class="modal-title" id="myModalLabel"><b>Adicionar novo item</b></div>
			 </div>
			 <div class="modal-body">
			      <form class="form-horizontal" method="post" id="form_cadastro_item" name="form_cadastro_item" autocomplete="off">

				   <div class="form-group">
					<label for="marca_cadastro" class="col-sm-2 control-label">Marca</label>
					<div class="col-sm-10">
					     <input type="text" class="form-control" id="marca_cadastro" name="marca_cadastro" placeholder="Digite a marca" required="required"> 
					</div>
				   </div>
				   <div class="form-group">
					<label for="sexo_cadastro" class="col-sm-2 control-label">Sexo</label>
					<div class="col-sm-10">
					     <select class="form-control" name="sexo_cadastro" id="sexo_cadastro" required="required">
						  <option disabled="disabled" selected="selected">Selecione um sexo:</option>
						  <option value="Masculino">Masculino</option>
						  <option value="Feminino">Feminino</option>
						  <option value="-">Unissex</option>
					     </select>
					</div>
				   </div>
				   <div class="form-group">
					<label for="tipo_cadastro" class="col-sm-2 control-label">Tipo</label>
					<div class="col-sm-10">
					     <input type="text" class="form-control" id="tipo_cadastro" name="tipo_cadastro" placeholder="Digite o tipo" required="required">
					</div>
				   </div>
				   <div class="form-group">
					<label for="produto_cadastro" class="col-sm-2 control-label">Produto</label>
					<div class="col-sm-10">
					     <input type="text" class="form-control" id="produto_cadastro" name="produto_cadastro" placeholder="Digite o nome do produto" required="required">
					</div>
				   </div>
				   <div class="form-group">
					<label for="modelo_cadastro" class="col-sm-2 control-label">Modelo</label>
					<div class="col-sm-10">
					     <input type="text" class="form-control" id="modelo_cadastro" name="modelo_cadastro" placeholder="Digite o modelo do produto" required="required">
					</div>
				   </div>
				   <div class="form-group">
					<label for="cor_cadastro" class="col-sm-2 control-label">Cor</label>
					<div class="col-sm-10">
					     <input type="text" class="form-control" id="cor_cadastro" name="cor_cadastro" placeholder="Digite a cor do produto" required="required">
					</div>
				   </div>
				   <div class="form-group">
					<label for="tamanho_cadastro" class="col-sm-2 control-label">Tamanho</label>
					<div class="col-sm-10">
					     <input type="text" class="form-control" id="tamanho_cadastro" name="tamanho_cadastro" placeholder="Digite o tamanho" required="required">
					</div>
				   </div>
				   <div class="form-group">
					<label for="quantidade_cadastro" class="col-sm-2 control-label">Quantidade</label>
					<div class="col-sm-10">
					     <input type="number" class="form-control" id="quantidade_cadastro" name="quantidade_cadastro" min="1" placeholder="Digite a quantidade" required="required" pattern="[0-9]+$">
					</div>
				   </div>
				   <div class="form-group">
					<label for="cliente_cadastro" class="col-sm-2 control-label">Cliente</label>
					<div class="col-sm-10">
					     <input type="text" class="form-control" id="cliente_cadastro" name="cliente_cadastro" placeholder="Digite o cliente">
					</div>
				   </div>
				   <div class="form-group">
					<label for="telefone_cliente" class="col-sm-2 control-label">Telefone</label>
					<div class="col-sm-10">
					     <input type="text" class="form-control" id="telefone_cliente" name="telefone_cliente" placeholder="Digite o telefone" onkeypress="mascaraTelefone(this, '## #####-####')" maxlength="13">
					</div>
				   </div>
				   <div class="form-group">
					<label for="email_cliente" class="col-sm-2 control-label">E-mail</label>
					<div class="col-sm-10">
					     <input type="email" class="form-control" id="email_cliente" name="email_cliente" placeholder="Digite o e-mail" >
					</div>
				   </div>
				   <!--alertas-->
				   <div class="alert" id="notificacao_add_item" role="alert"></div>				  
			      </form>
			 </div>
			 <div class="modal-footer">			      
			      <button type="button" class="btn btn-default" onclick="addItem()">Adicionar</button>
			 </div>
		    </div>
	       </div>
	  </div>

	  <!--Modal Quantidade-->
	  <div id="modalQuantidade" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	       <div class="modal-dialog modal-sm">
		    <div class="modal-content">
			 <div class="modal-header">
			      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      <div class="modal-title" id="myModalLabel"><b>Quantidade comprada</b></div>
			 </div>
			 <div class="modal-body">

			      <section id="info_item">
			      </section>			      			      

			      <form class="form-horizontal" method="post" id="form_quantidade_comprada" name="form_quantidade_comprada" autocomplete="off">  
				   <div class="form-group">
					<label for="quantidade_comprada" class="col-sm-2 control-label">Quantos você comprou?</label>
					<div class="col-sm-10">
					     <input type="number" class="form-control" id="quantidade_comprada" name="quantidade_comprada" min="1" required="required" pattern="[0-9]+$">
					</div>
				   </div>

				   <!--alertas-->
				   <div class="alert" id="notificacao_quantidade_comprada" role="alert"></div>			 
			      </form>			      			      
			 </div>
			 <div class="modal-footer">			      
			      <button type="button" class="btn btn-default" onclick="validaQuantidade()">Salvar</button>
			 </div>


		    </div>
	       </div>
	  </div>

	  <!--Modal com dados do cliente-->
	  <div id="modalDadosCliente" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	       <div class="modal-dialog modal-sm">
		    <div class="modal-content">
			 <div class="modal-header">
			      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      <div class="modal-title" id="myModalLabel"><b>Dados do Cliente</b></div>
			 </div>
			 <div class="modal-body">
			      <section id="info_cliente">
			      </section>			      			      
			 </div>
		    </div>
	       </div>
	  </div>

	  <script src="../jquery/jquery-2.1.3.min.js" type="text/javascript"></script>
	  <script src="../bootstrap/bootstrap.min.js" type="text/javascript"></script>
	  <!--Biblioteca do scrol suave, juntamente com jQuery-->
	  <script src="../jquery/jquery.nicescroll.min.js"></script> 	  
	  <script src="js/renova-session.js"></script>
     </body>
</html>