<?php
     
     require_once '../../src/classes/conexao.class.php';
     require_once '../../src/classes/compra.class.php';
     require_once '../../src/dao/comprasDAO.php';
     
     require_once '../../src/classes/fechamento.class.php';
     require_once '../../src/dao/fechamentoDAO.php';
     
     $daoFECHAMENTO = new fechamentoDAO();
     $fechamento = $daoFECHAMENTO->getFechamentoAtual();

     $dao = new comprasDAO();
         
     $query = "SELECT distinct marca FROM lista_compras WHERE id_fechamento = '{$fechamento->GetIdFechamento()}' ORDER BY marca ASC";     
     $dao->getMarcas($query);
     
