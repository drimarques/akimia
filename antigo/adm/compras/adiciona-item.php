<?php

     require_once '../../src/classes/conexao.class.php';
     require_once '../../src/classes/compra.class.php';
     require_once '../../src/dao/comprasDAO.php';

     require_once '../../src/classes/fechamento.class.php';
     require_once '../../src/dao/fechamentoDAO.php';

     $marca = $_POST['marca'];
     $sexo = $_POST['sexo'];
     $tipo = $_POST['tipo'];
     $produto = $_POST['produto'];
     $modelo = $_POST['modelo'];
     $cor = $_POST['cor'];
     $tamanho = $_POST['tamanho'];
     $quantidade = $_POST['quantidade'];
     $cliente = $_POST['cliente'];
     $telefone = $_POST['telefone'];
     $email = $_POST['email'];

     $compra = new Compra();
     $dao = new comprasDAO();

     $compra->setMarca($marca);
     $compra->setSexo($sexo);
     $compra->setTipo($tipo);
     $compra->setProduto($produto);
     $compra->setModelo($modelo);
     $compra->setCor($cor);
     $compra->setTamanho($tamanho);
     $compra->setQuantidadeTotal($quantidade);
     $compra->setCliente($cliente);
     $compra->setTelefone($telefone);
     $compra->setEmail($email);

     $daoFECHAMENTO = new fechamentoDAO();
     $fechamento = $daoFECHAMENTO->getFechamentoAtual();

     if ($fechamento->IdFechamento == "") {
	  $daoFECHAMENTO->iniciaFechamento();
	  $fechamento = $daoFECHAMENTO->getFechamentoAtual();
     }

     $dao->cadastraListaCompra($compra, $fechamento->GetIdFechamento());




     