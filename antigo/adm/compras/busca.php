<?php

     require_once '../../src/classes/conexao.class.php';
     require_once '../../src/classes/compra.class.php';
     require_once '../../src/dao/comprasDAO.php';
    
     require_once '../../src/classes/fechamento.class.php';
     require_once '../../src/dao/fechamentoDAO.php';

     $dao = new comprasDAO();
     $busca = $_POST["filtro_busca"];
     
     $daoFECHAMENTO = new fechamentoDAO();
     $fechamento = $daoFECHAMENTO->getFechamentoAtual();

     if ($busca != "") {
	  $dao->listaComprasBusca($busca, $fechamento->GetIdFechamento());
     }