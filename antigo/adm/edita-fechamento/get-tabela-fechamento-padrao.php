<?php
     require_once '../../src/classes/conexao.class.php';
     require_once '../../src/classes/compra.class.php';
     require_once '../../src/dao/comprasDAO.php';
     
     require_once '../../src/classes/fechamento.class.php';
     require_once '../../src/dao/fechamentoDAO.php';
               
     $daoFECHAMENTO = new fechamentoDAO();
     $fechamento = $daoFECHAMENTO->getFechamentoAtual();
     
     $daoCOMPRA = new comprasDAO();
     $daoCOMPRA->listaRelatorioFechamento($fechamento);