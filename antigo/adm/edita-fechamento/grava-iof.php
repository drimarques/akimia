<?php
     require_once '../../src/classes/conexao.class.php';
     require_once '../../src/classes/compra.class.php';
     require_once '../../src/dao/comprasDAO.php';

     require_once '../../src/classes/parametros_calculo.class.php';
     require_once '../../src/dao/parametrosDAO.php';
     
     require_once '../../src/classes/fechamento.class.php';
     require_once '../../src/dao/fechamentoDAO.php';
     
     $dao = new comprasDAO();     
     $valor = $_POST['IOF'];     
     $valor = str_replace("% ", " ", $valor);
     $valor = str_replace(",", ".", $valor);

     $parametrosDAO = new parametrosDAO();
     if($parametrosDAO->temParametros()){
	  $parametrosDAO->upd_IOF($valor);
     }
     else{
	  $parametrosDAO->ins_IOF($valor);
     }
     
