<?php
     session_start();
     if (!isset($_SESSION["usuario_logado"])) {
	  header("Location: ../index.php");
	  die();
     }

     require_once '../src/classes/conexao.class.php';
     require_once '../src/classes/compra.class.php';
     require_once '../src/dao/comprasDAO.php';

     require_once '../src/classes/parametros_calculo.class.php';
     require_once '../src/dao/parametrosDAO.php';

     require_once '../src/classes/fechamento.class.php';
     require_once '../src/dao/fechamentoDAO.php';

     require_once '../src/dao/usuarioDAO.php';

     $usuarioDAO = new UsuarioDAO();
     $usuarioDAO->validaTempoSession();

     $dao = new comprasDAO();
     $parametrosDAO = new parametrosDAO();

     $daoFECHAMENTO = new fechamentoDAO();
     $fechamento = $daoFECHAMENTO->getFechamentoAtual();
?>
<!DOCTYPE html>
<html lang="pt-br">
     <head>
	  <title>Fechamento - Akimia</title>
	  <meta charset="UTF-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	  <meta name="author" content="Adriano Marques">
	  <link rel="shortcut icon" href="img/Logo.Akimia.Mini.png">
	  <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	  <link rel="stylesheet" href="../bootstrap/bootstrap.min.css">
	  <link rel="stylesheet" href="../bootstrap/bootstrap-theme.min.css">
	  <link href="css/estilo.css" rel="stylesheet" type="text/css">
	  <script src="../jquery/jquery.min.js"></script>
	  <script src="js/fechamento.js"></script>
	  <?php
	       if($fechamento->GetIdFechamento() == null){
		    echo "Não há informações para fechamento. Necessário importar planilha.";
		    die();
	       }
	  ?>

     </head>
     <body onload="getTabelaResumo();">	
	  <header class="header">
	       <h1>
		    <img onclick="location.href = 'fechamento.php'" src="img/Logo.Akimia.Mini.png" alt="Akimia">Fechamento
		    <input type="text" class="form-control" id="txtBuscaProduto" name="txtBuscaProduto" placeholder="Procurar produto" autocomplete="off" autofocus="autofocus">
		    <div class="btn btn-default" id="btnBuscaProduto"><img src="img/search.png" alt="Buscar produto"></div>

		    <div id="btn-fechar-fechamento">FECHAR TEMPORADA</div>
	       </h1>	  

	       <div id="atualizando_tabela" ><img src="img/loading.gif" alt="Loading">Atualizando lista...</div>
	       <?php
		    $parametrosDAO->listaParametrosFechamento($fechamento);
	       ?>
	       <table id="tabela-resumo" class="table-responsive"></table>
	  </header>

	  <section id="fechamento-fixo">	       
	       <?php
		    $dao->listaRelatorioFechamento($fechamento);
	       ?>
	  </section>
	  <div id="loading-busca"></div>

	  <div class="clear"></div>	  	 

	  <div id="modal-encerra-periodo" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	       <div class="modal-dialog modal-sm">
		    <div class="modal-content">
			 <div class="modal-header">
			      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      <h4 class="modal-title" id="myModalLabel">Fechar temporada</h4>
			 </div>
			 <div class="modal-body">			      
			      <?php
				   $data_inicio = date_create($fechamento->GetDataInicio());
				   if ($fechamento->GetDataInicio() != null) {
					echo "<div id='inicio'>Início: <span class='float-rigth'>" . date_format($data_inicio, "d/m/Y - H:i:s") . "</span></div>";
				   }
			      ?>
			      <input type="text" id="nome_fechamento" name="nome_fechamento" class="form-control" placeholder="Dê um nome ao fechamento..." autocomplete="off">

			      <div class="alert alert-danger" id="erro_nome_fechamento_null">Por favor, dê um nome ao fechamento.</div>
			      <div class="alert alert-success" id="sucesso_encerra_fechamento">Aguarde, criando o arquivo...<br>Essa janela será fechada em alguns instantes...</div>
			 </div>
			 <div class="modal-footer">			      
			      <button type="button" class="btn btn-default" id="btn-salvar-fechamento">FECHAR</button>
			      <button type="button" class="btn" data-dismiss="modal">Cancelar</button>
			 </div>
		    </div>
	       </div>
	  </div>

	  <script src="../jquery/jquery.min.js"></script>
	  <script src="../jquery/jquery.maskMoney.js"></script>	  	  
	  <script src="../bootstrap/bootstrap.min.js" type="text/javascript"></script>
	  <script src="js/renova-session.js"></script>
	  <script src="../jquery/jquery-color-animation/mainfile"></script>
     </body>
</html>