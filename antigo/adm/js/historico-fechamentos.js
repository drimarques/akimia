function selecionaFechamento(item) {
     $(item).toggleClass("fechamento_selecionado");
}

function geraResumos(fechamentos) {
     $("#loading-resultados").html("<img src='img/loading.gif' alt='Loading'>");
     $("#loading-resultados").append("<span>Aguarde, gerando resumo...</span>");

     $("#loading-resultados").fadeIn("fast");

     $.ajax({
	  url: "historico-fechamentos/get-resumos.php",
	  data: {fechamentos: fechamentos},
	  type: 'POST',
	  context: $('#painel-resumos'),
	  success: function (retorno) {
	       $("#painel-resumos").html("<h2 id='vem-ate-aqui'>Resumo</h2>");
	       $("#painel-resumos").append(retorno);

	       geraGrafico(fechamentos);
	  }
     });
}

function geraGrafico(fechamentos) {
     $("#loading-resultados").html("");    
     $("#loading-resultados").html("<img src='img/loading.gif' alt='Loading'>");
     $("#loading-resultados").append("<span>Aguarde, criando gráfico...</span>");

     $.ajax({
	  url: "historico-fechamentos/get-grafico.php",
	  data: {fechamentos: fechamentos},
	  type: 'POST',
	  context: $('#painel-grafico2'),
	  success: function (retorno) {
	       $("#painel-grafico").html(retorno);
	       mostraGrafico();

	       $('body').animate({
		    scrollTop: $("#vem-ate-aqui").offset().top
	       }, 1200);

	       $("#loading-resultados").fadeOut("fast");

	  }
     });
}

$(document).ready(function () {
     $("#abre-lista-fechamentos").click(function () {
	  $("#lista-painel-fechamentos").slideToggle("fast");
	  $("#btn-gerar-tudo").fadeToggle("fast");

     });

     $("#btn-gerar-tudo").mouseover(function () {
	  $("#btn-gerar-tudo").animate({
	       backgroundColor: '#e72b7b',
	       color: '#ffffff',
	       borderColor: '#ffffff'
	  }, 800);
     });

     $("#btn-gerar-tudo").mouseout(function () {
	  $("#btn-gerar-tudo").finish();
	  $("#btn-gerar-tudo").css({
	       "background-color": "#ffffff",
	       "color": "#e72b7b",
	       "border": "solid 1px #e72b7b"
	  });
     });

     $("#btn-gerar-tudo").click(function () {
	  var fechamentos = new Array();

	  var verificador = false;
	  $(".fechamento_selecionado").each(function () {
	       verificador = true;
	       fechamentos.push(this.id);
	  });

	  if (verificador !== false) {
	       $("#sem-periodo-selecionado").fadeOut("fast");

	       geraResumos(fechamentos);

	  }
	  else {
	       $("#sem-periodo-selecionado").fadeIn("fast");
	  }

     });
});