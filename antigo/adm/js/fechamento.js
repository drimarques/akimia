function getTabelaResumo() {
//     var url_atual = window.location.href;
//     var editando = url_atual.search("edita_fechamento=");
//
//     if (editando === -1) {
//	  $.ajax({
//	       url: "fechamento/get-tabela-resumo.php",
//	       success: function (retorno) {
//		    $("#tabela-resumo").html(retorno);
//	       }
//	  });
//     }
//     else {
//	  var array_parametros = url_atual.split("=");
//	  var id_fechamento = array_parametros[1];
//
//	  $.ajax({
//	       url: "edita-fechamento/get-tabela-resumo-por-id-fechamento.php",
//	       data: {id_fechamento: id_fechamento},
//	       type: 'POST',
//	       success: function (retorno) {
//		    $("#tabela-resumo").html(retorno);
//	       }
//	  });
//     }


     $.ajax({
	  url: "fechamento/get-tabela-resumo.php",
	  success: function (retorno) {
	       $("#tabela-resumo").html(retorno);
	  }
     });

}

function atualizaTabelaPorIdItem(id_item) {
     idValorRS = id_item + "valorbr";
     $.ajax({
	  url: "getters-fechamento/get-valorRS.php",
	  data: {id_item: id_item},
	  type: 'POST',
	  context: $("#" + idValorRS),
	  success: function (retorno) {
	       $("#" + idValorRS).html(retorno);
	  }
     });

     idMarkup = id_item + "markup";
     $.ajax({
	  url: "getters-fechamento/get-markup.php",
	  data: {id_item: id_item},
	  type: 'POST',
	  context: $("#" + idMarkup),
	  success: function (retorno) {
	       $("#" + idMarkup).html(retorno);
	  }
     });

     idLucro = id_item + "lucro";
     $.ajax({
	  url: "getters-fechamento/get-lucro.php",
	  data: {id_item: id_item},
	  type: 'POST',
	  context: $("#" + idLucro),
	  success: function (retorno) {
	       $("#" + idLucro).html(retorno);
	  }
     });

     idCustoTotal = id_item + "custo_total";
     $.ajax({
	  url: "getters-fechamento/get-custoTotal.php",
	  data: {id_item: id_item},
	  type: 'POST',
	  context: $("#" + idCustoTotal),
	  success: function (retorno) {
	       $("#" + idCustoTotal).html(retorno);
	  }
     });

     idVendaTotal = id_item + "venda_total";
     $.ajax({
	  url: "getters-fechamento/get-vendaTotal.php",
	  data: {id_item: id_item},
	  type: 'POST',
	  context: $("#" + idVendaTotal),
	  success: function (retorno) {
	       $("#" + idVendaTotal).html(retorno);
	  }
     });

     idLucroTotal = id_item + "lucro_total";
     $.ajax({
	  url: "getters-fechamento/get-lucroTotal.php",
	  data: {id_item: id_item},
	  type: 'POST',
	  context: $("#" + idLucroTotal),
	  success: function (retorno) {
	       $("#" + idLucroTotal).html(retorno);
	  }
     });
}

function atualizaTabelaGeral() {
     $("#atualizando_tabela").fadeIn("fast");
     $.ajax({
	  url: "fechamento/upd-tabela-geral.php",
	  type: 'POST',
	  success: function (retorno) {
	       $("#atualizando_tabela").fadeOut("fast").delay(1500, function () {
		    location.href = "fechamento.php";
	       });
	  }
     });
}

function upd_valorDolarProduto(elemento) {
     var id_item = elemento.id;
     var valor_dolar = elemento.value;

     $.ajax({
	  url: "fechamento/grava-valor-dolar-compra.php",
	  data: {valor: valor_dolar, id_item: id_item},
	  type: 'POST',
	  success: function (retorno) {
	       atualizaTabelaPorIdItem(id_item);

	       if (valor_dolar === "$ 0.00" || valor_dolar === "") {
		    var td = $(elemento);
		    $(td.parent()).addClass("outline_azul");
		    $(td.parent()).removeClass("outline_verde");
	       }
	       else {
		    var td = $(elemento);
		    $(td.parent()).addClass("outline_verde");
		    $(td.parent()).removeClass("outline_azul");
	       }
	  }
     });

}

function upd_IOFProduto(elemento) {
     var iof = elemento.value;
     var id_item = elemento.id;

     $.ajax({
	  url: "fechamento/grava-iof-produto.php",
	  data: {iof: iof, id_item: id_item},
	  type: 'POST',
	  success: function (retorno) {
	       atualizaTabelaPorIdItem(id_item);

	       var td = $(elemento);
	       $(td.parent()).css({
	       });
	  }
     });
}

function upd_valorVendaProduto(elemento) {
     var id_item = elemento.id;
     var valor_venda = elemento.value;

     $.ajax({
	  url: "fechamento/grava-valor-venda-compra.php",
	  data: {valor: valor_venda, id_item: id_item},
	  type: 'POST',
	  success: function (retorno) {
	       atualizaTabelaPorIdItem(id_item);

	       if (valor_venda === "R$ 0,00" || valor_venda === "") {
		    var td = $(elemento);
		    $(td.parent()).addClass("outline_azul");
		    $(td.parent()).removeClass("outline_verde");
	       }
	       else {
		    var td = $(elemento);
		    $(td.parent()).addClass("outline_verde");
		    $(td.parent()).removeClass("outline_azul");
	       }
	  }
     });

}

function animacaoBusca(opacidade, texto) {
     $("#loading-busca").text(texto);
     $("#loading-busca").animate({
	  opacity: opacidade
     }, 1000, function () {
	  animacaoBusca(1);
	  animacaoBusca(0.25);
	  animacaoBusca(1);
	  animacaoBusca(0.25);
	  animacaoBusca(1);
	  animacaoBusca(0.25);
     });
}

function getTabelaFechamentoPadrao() {
     $('#fechamento-fixo').html("");
     $("#loading-busca").css({"display": "block"});
     animacaoBusca(0.25, "Carregando todos produtos...");

     $.ajax({
	  url: "fechamento/get-tabela-fechamento-padrao.php",
	  type: 'POST',
	  context: $('#fechamento-fixo'),
	  success: function (retorno) {
	       $("#loading-busca").stop();
	       $("#loading-busca").css({"display": "none"});

	       $('#fechamento-fixo').html(retorno);
	       $(".dolar").maskMoney({showSymbol: true, symbol: "$ ", decimal: ".", thousands: ","});
	       $(".real").maskMoney({showSymbol: true, symbol: "R$ ", decimal: ",", thousands: "."});
	       $(".porcentagem").maskMoney({showSymbol: true, symbol: "% ", decimal: ",", thousands: "."});
	  }
     });
}

function buscaProduto() {
     $('#fechamento-fixo').html("");
     $("#loading-busca").css({"display": "block"});
     animacaoBusca(0.25, "Buscando produtos...");

     var busca = $("#txtBuscaProduto").val();
     if (busca !== "") {

	  $.ajax({
	       url: "fechamento/busca-produto.php",
	       data: {busca: busca},
	       type: 'POST',
	       context: $('#fechamento-fixo'),
	       success: function (retorno) {
		    $("#loading-busca").stop();
		    $("#loading-busca").css({"display": "none"});

		    if (retorno === "") {
			 getTabelaFechamentoPadrao();
		    }
		    else if (retorno === "Sem resultados") {
			 $('#fechamento-fixo').html("<div class='alert alert-warning' id='resultado-busca-produto'>Nenhum produto encontrado :( redefina sua busca.</div>");
		    }
		    else {
			 $('#fechamento-fixo').html(retorno);
			 $(".dolar").maskMoney({showSymbol: true, symbol: "$ ", decimal: ".", thousands: ","});
			 $(".real").maskMoney({showSymbol: true, symbol: "R$ ", decimal: ",", thousands: "."});
			 $(".porcentagem").maskMoney({showSymbol: true, symbol: "% ", decimal: ",", thousands: "."});
		    }
	       }
	  });
     }
     else {
	  getTabelaFechamentoPadrao();
     }
}

$(document).ready(function () {
     $(".dolar").maskMoney({showSymbol: true, symbol: "$ ", decimal: ".", thousands: ","});
     $(".real").maskMoney({showSymbol: true, symbol: "R$ ", decimal: ",", thousands: "."});
     $(".porcentagem").maskMoney({showSymbol: true, symbol: "% ", decimal: ",", thousands: "."});
     
     $("#cotacao-dolar").focusout(function () {
	  var cotacao_dolar = $("#cotacao-dolar").val();
	  $.ajax({
	       url: "fechamento/grava-cotacao-dolar.php",
	       data: {valor: cotacao_dolar},
	       type: 'POST',
	       success: function (retorno) {
		    var td = $("#cotacao-dolar");
		    $(td.parent()).css({
		    });
		    atualizaTabelaGeral();
	       }
	  });
     });
     $("#imposto-usa").focusout(function () {
	  var imposto_usa = $("#imposto-usa").val();
	  $.ajax({
	       url: "fechamento/grava-imposto-usa.php",
	       data: {imposto_usa: imposto_usa},
	       type: 'POST',
//	       context: $('#teste'),
	       success: function (retorno) { //success: function () {	
		    var td = $("#imposto-usa");
		    $(td.parent()).css({
		    });
		    atualizaTabelaGeral();
	       }
	  });
     });
     $("#IOF").focusout(function () {
	  var iof = $("#IOF").val();
	  $.ajax({
	       url: "fechamento/grava-iof.php",
	       data: {IOF: iof},
	       type: 'POST',
//	       context: $('#teste'),
	       success: function (retorno) { //success: function () {
		    var td = $("#IOF");
		    $(td.parent()).css({
		    });
//		    $("#IOF").css({"outline": "4px solid #33cc00"});
		    atualizaTabelaGeral();
	       }
	  });
     });
     $("#despesa_imp").focusout(function () {
	  var despesa_importacao = $("#despesa_imp").val();
	  $.ajax({
	       url: "fechamento/grava-despesa-importacao.php",
	       data: {despesa_importacao: despesa_importacao},
	       type: 'POST',
//	       context: $('#teste'),
	       success: function (retorno) {
		    var td = $("#despesa_imp");
		    $(td.parent()).css({
		    });
//		    $("#IOF").css({"outline": "4px solid #33cc00"});
		    atualizaTabelaGeral();
	       }
	  });
     });

     setInterval(function () {
	  getTabelaResumo();
     }, 500);

     $("#btnBuscaProduto").click(function () {
	  buscaProduto();
     });

     $("#txtBuscaProduto").keypress(function (e) {
	  if (e.which === 13) {
	       buscaProduto();
	  }
     });

     $("#fechamento-fixo").delegate("#limpar-busca", 'click', function () {
	  $("#txtBuscaProduto").val(null);
	  $("#txtBuscaProduto").focus();
	  getTabelaFechamentoPadrao();
     });

     $("#btn-fechar-fechamento").mouseover(function () {
	  $("#btn-fechar-fechamento").animate({
	       backgroundColor: '#e72b7b',
	       color: '#ffffff',
	       borderColor: '#ffffff'
	  }, 800);
     });

     $("#btn-fechar-fechamento").mouseout(function () {
	  $("#btn-fechar-fechamento").finish();
	  $("#btn-fechar-fechamento").css({
	       "background-color": "#f3f3f3",
	       "color": "#e72b7b",
	       "border": "solid 1px #e72b7b"
	  });
     });

     $("#btn-fechar-fechamento").click(function () {
	  $("#modal-encerra-periodo").modal('show');
     });

     $("#btn-salvar-fechamento").click(function () {
	  var nome_fechamento = $("#nome_fechamento").val();

	  if (nome_fechamento === null || nome_fechamento === "") {
	       $("#nome_fechamento").css({"border": "solid 1px red"});
	       $("#sucesso_encerra_fechamento").css({"display": "none"});
	       $("#erro_nome_fechamento_null").fadeIn();
	  }
	  else {
	       //Fecha
	       $("#nome_fechamento").css({"border": "1px solid #ccc"});
	       $("#erro_nome_fechamento_null").css({"display": "none"});
	       $("#sucesso_encerra_fechamento").fadeIn();

	       $.ajax({
		    url: "fechamento/encerrar-fechamento.php",
		    data: {nome_fechamento: nome_fechamento},
		    type: 'POST',
		    success: function (retorno) {
			 var uri = 'src/exportar_fechamento.php?id_fechamento=' + retorno;
//			 window.location = uri;
			 window.open(uri);
			 window.close();
		    }
	       });
	  }
     });


//     $('#modal-encerra-periodo').on('hidden.bs.modal', function () {
//	  location.href = "fechamento.php";
//     });

});