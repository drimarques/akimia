<?php

     date_default_timezone_set('Brazil/East');
     date_default_timezone_set('America/Sao_Paulo');
     
     $nome_fechamento = $_POST["nome_fechamento"];

     require_once '../../src/classes/conexao.class.php';     
     require_once '../../src/classes/fechamento.class.php';
     require_once '../../src/dao/fechamentoDAO.php';

     $fechamentoDAO = new fechamentoDAO();     
     $fechamento = $fechamentoDAO->getFechamentoAtual();     
     $fechamento->SetNomeFechamento($nome_fechamento);
     $fechamento->SetDataTermino(date("Y-m-d H:i:s"));
     $fechamento->SetFechado('1');     
     $fechamentoDAO->encerraFechamento($fechamento); 
     
     echo $fechamento->GetIdFechamento();