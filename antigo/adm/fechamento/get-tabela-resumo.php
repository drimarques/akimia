<?php

     require_once '../../src/classes/conexao.class.php';
     require_once '../../src/classes/compra.class.php';
     require_once '../../src/dao/comprasDAO.php';
     
     require_once '../../src/classes/parametros_calculo.class.php';
     require_once '../../src/dao/parametrosDAO.php';
     
     require_once '../../src/classes/fechamento.class.php';
     require_once '../../src/dao/fechamentoDAO.php';
                   
     $dao = new comprasDAO();
     $dao->getTabelaResumo();
