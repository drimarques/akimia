<?php     

     date_default_timezone_set('America/Sao_Paulo');
     $data_hora = date("d-m-Y_H-i-s");

     require_once '../php_excel/Classes/PHPExcel.php';
     require_once '../../src/classes/conexao.class.php';

     require_once '../../src/dao/comprasDAO.php';

     require_once '../../src/dao/parametrosDAO.php';
     require_once '../../src/classes/parametros_calculo.class.php';

     require_once '../../src/classes/fechamento.class.php';
     require_once '../../src/dao/fechamentoDAO.php';


// Instanciamos a classe
     $objPHPExcel = new PHPExcel();

     //Cores
     $fundo_branco = array('fill' =>
	 array(
	     'type' => PHPExcel_Style_Fill::FILL_SOLID,
	     'color' => array('rgb' => 'ffffff')
	 ),
     );
    
     $fundo_azul = array('fill' =>
	 array(
	     'type' => PHPExcel_Style_Fill::FILL_SOLID,
	     'color' => array('rgb' => '5478b2')
	 ),
     );

     $fundo_rosa = array('fill' =>
	 array(
	     'type' => PHPExcel_Style_Fill::FILL_SOLID,
	     'color' => array('rgb' => 'e31b6e')
	 ),
     );

     $fonte_branca = array(
	 'font' => array(
	     'color' => array('rgb' => 'ffffff')
     ));

     $borda_preta = array(
	 'borders' => array(
	     'allborders' => array(
		 'style' => PHPExcel_Style_Border::BORDER_THIN
	     )
	 )
     );

     //Deixando fundo brancão da planilha toda
     $objPHPExcel->getDefaultStyle()->applyFromArray($fundo_branco);

     //Cores de fundo
     $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($fundo_azul);
     $objPHPExcel->getActiveSheet()->getStyle('B8')->applyFromArray($fundo_azul);
     $objPHPExcel->getActiveSheet()->getStyle('E2:X2')->applyFromArray($fundo_rosa);
     $objPHPExcel->getActiveSheet()->getStyle('B3')->applyFromArray($fundo_rosa);
     $objPHPExcel->getActiveSheet()->getStyle('B4')->applyFromArray($fundo_rosa);
     $objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray($fundo_rosa);
     $objPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray($fundo_rosa);
     $objPHPExcel->getActiveSheet()->getStyle('B9')->applyFromArray($fundo_rosa);
     $objPHPExcel->getActiveSheet()->getStyle('B10')->applyFromArray($fundo_rosa);
     $objPHPExcel->getActiveSheet()->getStyle('B11')->applyFromArray($fundo_rosa);
     $objPHPExcel->getActiveSheet()->getStyle('B12')->applyFromArray($fundo_rosa);

     //Cores da fonte
     $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($fonte_branca);
     $objPHPExcel->getActiveSheet()->getStyle('B8')->applyFromArray($fonte_branca);
     $objPHPExcel->getActiveSheet()->getStyle('E2:X2')->applyFromArray($fonte_branca);
     $objPHPExcel->getActiveSheet()->getStyle('B3')->applyFromArray($fonte_branca);
     $objPHPExcel->getActiveSheet()->getStyle('B4')->applyFromArray($fonte_branca);
     $objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray($fonte_branca);
     $objPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray($fonte_branca);
     $objPHPExcel->getActiveSheet()->getStyle('B9')->applyFromArray($fonte_branca);
     $objPHPExcel->getActiveSheet()->getStyle('B10')->applyFromArray($fonte_branca);
     $objPHPExcel->getActiveSheet()->getStyle('B11')->applyFromArray($fonte_branca);
     $objPHPExcel->getActiveSheet()->getStyle('B12')->applyFromArray($fonte_branca);

// Definimos o estilo da fonte negrito
     $objPHPExcel->getActiveSheet()->getStyle('B2')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('B3')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('B4')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('B5')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('B6')->getFont()->setBold(true);

     $objPHPExcel->getActiveSheet()->getStyle('B8')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('B9')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('B10')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('B11')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('B12')->getFont()->setBold(true);

     $objPHPExcel->getActiveSheet()->getStyle('E2:X2')->getFont()->setBold(true);

     //Borda
     $objPHPExcel->getActiveSheet()->getStyle('B2:C2')->applyFromArray($borda_preta);
     $objPHPExcel->getActiveSheet()->getStyle('B3:C3')->applyFromArray($borda_preta);
     $objPHPExcel->getActiveSheet()->getStyle('B4:C4')->applyFromArray($borda_preta);
     $objPHPExcel->getActiveSheet()->getStyle('B5:C5')->applyFromArray($borda_preta);
     $objPHPExcel->getActiveSheet()->getStyle('B6:C6')->applyFromArray($borda_preta);

     $objPHPExcel->getActiveSheet()->getStyle('B8:C8')->applyFromArray($borda_preta);
     $objPHPExcel->getActiveSheet()->getStyle('B9:C9')->applyFromArray($borda_preta);
     $objPHPExcel->getActiveSheet()->getStyle('B10:C10')->applyFromArray($borda_preta);
     $objPHPExcel->getActiveSheet()->getStyle('B11:C11')->applyFromArray($borda_preta);
     $objPHPExcel->getActiveSheet()->getStyle('B12:C12')->applyFromArray($borda_preta);

     $objPHPExcel->getActiveSheet()->getStyle('E2:X2')->applyFromArray($borda_preta);

// Criamos as colunas
     $objPHPExcel->setActiveSheetIndex(0)
     ->setCellValue('B2', "PARÂMETROS DE CÁLCULO")
     ->setCellValue("B3", "Cotação Dolar")
     ->setCellValue("B4", "Imposto USA")
     ->setCellValue('B5', "IOF")
     ->setCellValue("B6", "Despesa com Importação")
     ->setCellValue('B8', "RESUMO")
     ->setCellValue('B9', "Custo Total EUA (USD)")
     ->setCellValue("B10","Custo Total (R$)")
     ->setCellValue("B11", "Venda Total (R$)")
     ->setCellValue("B12",  "Lucro Real (R$)")
     ->setCellValue("E2", "Marca")
     ->setCellValue("F2", "Sexo")
     ->setCellValue("G2", "Tipo")
     ->setCellValue("H2", "Produto")
     ->setCellValue("I2", "Modelo")
     ->setCellValue("J2", "Cor")
     ->setCellValue("K2", "Tamanho")
     ->setCellValue("L2", "Quantidade")
     ->setCellValue("M2", "IOF")
     ->setCellValue("N2", "Custo (U$)")     
     ->setCellValue("O2", "Custo (R$)")
     ->setCellValue("P2", "Custo Total (R$)")
     ->setCellValue("Q2", "Markup %")
     ->setCellValue("R2", "Venda (R$)")
     ->setCellValue("S2", "Venda Total (R$)")
     ->setCellValue("T2", "Lucro (R$)")          
     ->setCellValue("U2", "Lucro Total (R$)")
     ->setCellValue("V2", "Cliente")
     ->setCellValue("W2", "Telefone")
     ->setCellValue("X2", "E-mail");

     $mesclar_centralizar = array(
	 'alignment' => array(
	     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	 )
     );
     
     $objPHPExcel->getActiveSheet()->mergeCells("B2:C2");
     $objPHPExcel->getActiveSheet()->mergeCells("B8:C8");
     $objPHPExcel->getActiveSheet()->getStyle("B2:C2")->applyFromArray($mesclar_centralizar);
     $objPHPExcel->getActiveSheet()->getStyle("B8:C8")->applyFromArray($mesclar_centralizar);

// Podemos configurar diferentes larguras paras as colunas como padrão
     $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(0.7);
     $objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(5);     
     $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(0.7);

     $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(23);
     $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(11);     
     $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(26);
     $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(11);
     $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(19);
     $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(57);
     $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(16);
     $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(16);
     $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(16);
     $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(16);
     $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(5);
     $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(16);
     $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(16);
     $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(16);
     $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(16);
     $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(16);
     $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(16);
     $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(16);
     $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(16);
     $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(20);
     $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(20);
     $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(30);
     
     $formato_moeda = array(
	 'code' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
     );     
     
     //Resumo formato de moeda
     $objPHPExcel->getActiveSheet()->getStyle('C9')->getNumberFormat()->applyFromArray($formato_moeda);
     $objPHPExcel->getActiveSheet()->getStyle('C10')->getNumberFormat()->applyFromArray($formato_moeda);
     $objPHPExcel->getActiveSheet()->getStyle('C11')->getNumberFormat()->applyFromArray($formato_moeda);
     $objPHPExcel->getActiveSheet()->getStyle('C12')->getNumberFormat()->applyFromArray($formato_moeda);
          
     //Conexao
     $conexao = new Conexao();
     $conn = $conexao->getConexao();
     
     $fechamentoDAO = new fechamentoDAO();
     $fechamento = $fechamentoDAO->getFechamentoAtual();
     
     if(isset($_GET["id_fechamento"])){
	  $fechamento->setIdfechamento($_GET["id_fechamento"]);
     }

     //Parametros de calculo
     $parametros = $conn->query("SELECT * FROM parametros_calculo WHERE id_fechamento = '{$fechamento->GetIdFechamento()}'");
     $valores = $parametros->fetch(PDO::FETCH_ASSOC);
     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 3, $valores[cotacaoDolar]);
     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 4, $valores[impostoUSA]);
     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 5, $valores[IOF]);
     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 6, $valores[despesa_viagem]);

     //Resumo
     $parametrosDAO = new parametrosDAO();
     $comprasDAO = new comprasDAO();

     $parametro = $parametrosDAO->getDadosParametroPorIdFechamento($fechamento->GetIdFechamento());
     $despesaViagem = $parametro->GetDespesaViagem();
     $lucroVerdadeiro = $comprasDAO->getTotalLucroTotalPorIdFechamento($fechamento->GetIdFechamento()) - $despesaViagem;

     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 9, $comprasDAO->getTotalValorDolarPorIdFechamento($fechamento->GetIdFechamento()));
     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 10, $comprasDAO->getTotalCustoTotalPorIdFechamento($fechamento->GetIdFechamento()));
     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 11, $comprasDAO->getTotalVendaTotalPorIdFechamento($fechamento->GetIdFechamento()));
     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 12, $lucroVerdadeiro);

     $retorno = $conn->query("SELECT * FROM lista_compras WHERE id_fechamento = '{$fechamento->GetIdFechamento()}' ORDER BY marca ASC");
     $contador = 3;
     while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {

	  if ($linhas[iof] == "1") {
	       $iof = "Sim";
	  } else {
	       $iof = "Não";
	  }

	  //Borda
	  $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(4,  $contador)->applyFromArray($borda_preta);
	  $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(5,  $contador)->applyFromArray($borda_preta);
	  $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(6,  $contador)->applyFromArray($borda_preta);
	  $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(7,  $contador)->applyFromArray($borda_preta);
	  $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(8,  $contador)->applyFromArray($borda_preta);
	  $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(9,  $contador)->applyFromArray($borda_preta);
	  $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(10, $contador)->applyFromArray($borda_preta);
	  $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(11, $contador)->applyFromArray($borda_preta);
	  $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(12, $contador)->applyFromArray($borda_preta);
	  $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(13, $contador)->applyFromArray($borda_preta);
	  $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(14, $contador)->applyFromArray($borda_preta);
	  $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(15, $contador)->applyFromArray($borda_preta);
	  $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(16, $contador)->applyFromArray($borda_preta);
	  $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(17, $contador)->applyFromArray($borda_preta);
	  $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(18, $contador)->applyFromArray($borda_preta);
	  $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(19, $contador)->applyFromArray($borda_preta);
	  $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(20, $contador)->applyFromArray($borda_preta);
	  $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(21, $contador)->applyFromArray($borda_preta);
	  $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(22, $contador)->applyFromArray($borda_preta);
	  $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(23, $contador)->applyFromArray($borda_preta);	  	  
	  
	  //Formato dos campos
	  $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(14, $contador)->getNumberFormat()->applyFromArray($formato_moeda);
	  $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(15, $contador)->getNumberFormat()->applyFromArray($formato_moeda);
	  $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(18, $contador)->getNumberFormat()->applyFromArray($formato_moeda);
	  $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(19, $contador)->getNumberFormat()->applyFromArray($formato_moeda);
	  $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(20, $contador)->getNumberFormat()->applyFromArray($formato_moeda);


	  // Também podemos escolher a posição exata aonde o dado será inserido (coluna, linha, dado);	  
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4,  $contador, $linhas[marca]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5,  $contador, $linhas[sexo]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6,  $contador, $linhas[tipo]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7,  $contador, $linhas[produto]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8,  $contador, $linhas[modelo]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9,  $contador, $linhas[cor]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $contador, $linhas[tamanho]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $contador, $linhas[quantidade_total]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $contador, $iof);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $contador, $linhas[valor_dolar]);	  
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $contador, $linhas[valor_rs]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, $contador, $linhas[custo_total]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, $contador, $linhas[markup]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(17, $contador, $linhas[venda]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(18, $contador, $linhas[venda_total]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(19, $contador, $linhas[lucro]);	  	  
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(20, $contador, $linhas[lucro_total]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(21, $contador, $linhas[cliente]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(22, $contador, $linhas[telefone_cliente]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(23, $contador, $linhas[email_cliente]);
	  $contador++;
     }

     unset($conn);


// Podemos renomear o nome das planilha atual, lembrando que um único arquivo pode ter várias planilhas
     $objPHPExcel->getActiveSheet()->setTitle('Lista de Compras - Akimia');

// Cabeçalho do arquivo para ele baixar
     header('Content-Type: application/vnd.ms-excel');
     header('Content-Disposition: attachment;filename="ExportacaoFechamento_Data' . $data_hora . '.xls"');
     header('Cache-Control: max-age=0');
// Se for o IE9, isso talvez seja necessário
     header('Cache-Control: max-age=1');

// Acessamos o 'Writer' para poder salvar o arquivo
     $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

// Salva diretamente no output, poderíamos mudar arqui para um nome de arquivo em um diretório ,caso não quisessemos jogar na tela
     $objWriter->save('php://output');   
     
     exit;          