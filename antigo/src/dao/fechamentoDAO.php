<?php
     
     
     class fechamentoDAO {

	  function getFechamentoAtual() {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();

	       $retorno = $conn->query("SELECT * FROM fechamento WHERE fechado = 0 ORDER BY id_fechamento DESC LIMIT 0, 1");
	       $linhas = $retorno->fetch(PDO::FETCH_ASSOC);

	       $fechamento = new Fechamento();
	       $fechamento->SetIdFechamento($linhas[id_fechamento]);
	       $fechamento->SetNomeFechamento($linhas[nome_fechamento]);
	       $fechamento->SetDataInicio($linhas[data_inicio]);
	       $fechamento->SetDataTermino($linhas[data_termino]);
	       $fechamento->SetFechado($linhas[fechado]);

	       unset($conn);
	       return $fechamento;
	  }

	  function iniciaFechamento() {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();	       
	       $data_inicio = date("Y-m-d H:i:s");	       	       
	       $conn->query("INSERT INTO fechamento (data_inicio) VALUES('$data_inicio')");

	       unset($conn);
	  }
	  
	  function encerraFechamento(Fechamento $f) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $query = "UPDATE fechamento SET nome_fechamento = '{$f->GetNomeFechamento()}', data_termino = '{$f->GetDataTermino()}', fechado = '{$f->GetFechado()}' WHERE id_fechamento = '{$f->GetIdFechamento()}'";	       
	       $conn->query($query);	       
	       unset($conn);
	  }
	  
	  function listaFechamentos(){
	       $lista = "<li>
			      <div>Nome</div>
			      <div>Data de início</div>
			      <div>Data de término</div>
			 </li>";
	       
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $retorno = $conn->query("SELECT * FROM fechamento WHERE fechado = 1 ORDER BY id_fechamento ASC");
	       
	       while($linhas = $retorno->fetch(PDO::FETCH_ASSOC)){
		    $data_inicio = date_create($linhas[data_inicio]);
		    $data_termino = date_create($linhas[data_termino]);
		    
		    $lista .= "<li id='$linhas[id_fechamento]' onclick='selecionaFechamento(this)'>
				   <div>$linhas[nome_fechamento]</div>
				   <div>".date_format($data_inicio, 'd/m/Y - H:i:s')."</div>
				   <div>".date_format($data_termino, 'd/m/Y - H:i:s')."</div>
			      </li>";		    
	       }
	       
	       echo $lista;
	  }
	  
	  
	  function getDadosFechamento($id_fechamento){
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();

	       $retorno = $conn->query("SELECT * FROM fechamento WHERE id_fechamento = '$id_fechamento'");
	       $linhas = $retorno->fetch(PDO::FETCH_ASSOC);

	       $fechamento = new Fechamento();

	       $fechamento->SetIdFechamento($linhas[id_fechamento]);
	       $fechamento->SetNomeFechamento($linhas[nome_fechamento]);
	       $fechamento->SetDataInicio($linhas[data_inicio]);
	       $fechamento->SetDataTermino($linhas[data_termino]);

	       unset($conn);
	       return $fechamento;
	  }

     }
     