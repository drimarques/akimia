<?php

     class comprasDAO {

	  function cadastraListaCompra(Compra $compra, $id_fechamento) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $conn->query("INSERT INTO lista_compras (id_fechamento, tipo, sexo, produto, marca, modelo, cor, quantidade_total, tamanho, valor_dolar, venda, cliente, telefone_cliente, email_cliente)
	       VALUES ('$id_fechamento', '{$compra->Tipo}', '{$compra->Sexo}', '{$compra->Produto}', '{$compra->Marca}', '{$compra->Modelo}', '{$compra->Cor}', '{$compra->QuantidadeTotal}', '{$compra->Tamanho}', '{$compra->ValorDolar}', '{$compra->Venda}', '{$compra->Cliente}', '{$compra->Telefone}', '{$compra->Email}')");

	       unset($conn);
	  }

	  function limpaListaCompras($id_fechamento) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();

	       $conn->query("DELETE FROM lista_compras WHERE id_fechamento = '$id_fechamento'");
	       unset($conn);
	  }

	  function listaCompras() {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $retorno = $conn->query("SELECT * FROM lista_compras ORDER BY marca, produto asc");

	       $tabela = "<table class='table-responsive table-hover tabelas2' id='table-lista-compras'>";
	       $tabela .= '<thead>
			      <tr>
				   <th>Marca</th>
				   <th>Sexo</th>
				   <th>Tipo</th>				   
				   <th>Produto</th>				 
				   <th>Modelo</th>
				   <th>Cor</th>
				   <th>Tamanho</th>
				   <th>Quantidade</th>				   			      
			      </tr>
			   </thead>';

	       while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
		    $tabela .= '<tr>
				   <td>' . $linhas[marca] . '</td>
				   <td>' . $linhas[sexo] . '</td>
				   <td>' . $linhas[tipo] . '</td>
				   <td>' . $linhas[produto] . '</td>				   
				   <td>' . $linhas[modelo] . '</td>
				   <td>' . $linhas[cor] . '</td>				   
				   <td>' . $linhas[tamanho] . '</td>
				   <td>' . $linhas[quantidade_total] . '</td>
			        </tr>';
	       }

	       $tabela .= "</table>";
	       unset($conn);

	       echo $tabela;
	  }

	  function getDadosCompra($id_item) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();

	       $retorno = $conn->query("SELECT * FROM lista_compras WHERE id_item = '$id_item'");
	       $linhas = $retorno->fetch(PDO::FETCH_ASSOC);

	       $compra = new Compra();

	       $compra->setFechamento($linhas[id_fechamento]);
	       $compra->setTipo($linhas[tipo]);
	       $compra->setSexo($linhas[sexo]);
	       $compra->setProduto($linhas[produto]);
	       $compra->setMarca($linhas[marca]);
	       $compra->setModelo($linhas[modelo]);
	       $compra->setCor($linhas[cor]);
	       $compra->setQuantidadeTotal($linhas[quantidade_total]);
	       $compra->setQuantidadeComprada($linhas[quantidade_comprada]);
	       $compra->setTamanho($linhas[tamanho]);
	       $compra->setComprado($linhas[comprado]);

	       $compra->setValorDolar($linhas[valor_dolar]);
	       $compra->setIOF($linhas[iof]);
	       $compra->setValorRS($linhas[valor_rs]);
	       $compra->setMarkup($linhas[markup]);
	       $compra->setVenda($linhas[venda]);
	       $compra->setLucro($linhas[lucro]);
	       $compra->setCustoTotal($linhas[custo_total]);
	       $compra->setVendaTotal($linhas[venda_total]);
	       $compra->setLucroTotal($linhas[lucro_total]);

	       $compra->setCliente($linhas[cliente]);
	       $compra->setTelefone($linhas[telefone_cliente]);
	       $compra->setEmail($linhas[email_cliente]);

	       unset($conn);
	       return $compra;
	  }

	  function listaRelatorioFechamento(Fechamento $fechamento) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $marcas = $conn->query("SELECT distinct marca FROM lista_compras WHERE id_fechamento = '{$fechamento->GetIdFechamento()}' ORDER BY marca ASC");

	       $tabela = "<table id='table-fechamento-padrao' class='table-responsive table-hover tabela-fechamento'>";
	       $tabela .= '<thead>
			      <tr>
				   <th id="produto-fechamento"></th>
				   <th>IOF</th>
				   <th>Custo EUA</th>				   
				   <th>Custo</th>
				   <th>Custo Total</th>
				   <th class="bold">Markup</th>
				   <th>Venda</th>
				   <th>Venda Total</th>
				   <th class="bold">Lucro</th>
				   <th>Lucro Total</th>
			      </tr>
			 </thead>
			 <tbody>';

	       while ($marca = $marcas->fetch(PDO::FETCH_ASSOC)) {
		    $tabela .= '<tr>
				   <td colspan="10" class="linha-marca"><div class="marca-fechamento"><b>' . strtoupper($marca[marca]) . '</b></div></td>
			        </tr>';

		    $retorno = $conn->query("SELECT * FROM lista_compras WHERE id_fechamento = '{$fechamento->GetIdFechamento()}' AND marca = '$marca[marca]' ORDER BY produto ASC");

		    while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
			 if ($linhas[iof] == '0') {
			      $iof = '<option value="1">Sim</option>
				 <option value="0" selected>Não</option>';
			 } else if ($linhas[iof] == '1') {
			      $iof = '<option value="1" selected>Sim</option>
				 <option value="0">Não</option>';
			 }

			 if ($linhas[sexo] == "-") {
			      $sexo = "Unissex";
			 } else {
			      $sexo = $linhas[sexo];
			 }

			 $classeCustoEUA = ($linhas[valor_dolar] == "0") ? "outline_azul" : "outline_verde";
			 $classeVenda = ($linhas[venda] == "0") ? "outline_azul" : "outline_verde";

			 $tabela .= '<tr>					
					<td>			      
					     <div>' . $linhas[produto] . ' ' . $linhas[modelo] . ' - ' . $linhas[cor] . '</div>
					     <div class="properts-produto">' . $sexo . ' - Tamanho: ' . $linhas[tamanho] . ' Quantidade: ' . $linhas[quantidade_total] . '</div>
					</td>
					<td class="outline_verde">
					     <select name="iof" class="form-control iof-fechamento" id=' . $linhas[id_item] . ' onchange="upd_IOFProduto(this)">
						  ' . $iof . '
					     </select>
					</td>
					<td class=' . $classeCustoEUA . '><input type="text" class="dolar form-control" id=' . $linhas[id_item] . ' onblur="upd_valorDolarProduto(this)" value=' . $linhas[valor_dolar] . '></td>					
					<td id="' . $linhas[id_item] . 'valorbr">R$ ' . number_format($linhas[valor_rs], 2, ',', '.') . '</td>
					<td id="' . $linhas[id_item] . 'custo_total">R$ ' . number_format($linhas[custo_total], 2, ',', '.') . '</td>
					<td id="' . $linhas[id_item] . 'markup">' . $linhas[markup] . ' %</td>
					<td class=' . $classeVenda . '><input type="text" class="real form-control" id=' . $linhas[id_item] . ' onblur="upd_valorVendaProduto(this)" value=' . number_format($linhas[venda], 2, ',', '.') . '></td>
					     <td id="' . $linhas[id_item] . 'venda_total">R$ ' . number_format($linhas[venda_total], 2, ',', '.') . '</td>
					<td id="' . $linhas[id_item] . 'lucro">R$ ' . number_format($linhas[lucro], 2, ',', '.') . '</td>
					<td id="' . $linhas[id_item] . 'lucro_total">R$ ' . number_format($linhas[lucro_total], 2, ',', '.') . '</td>
			      </tr>';
		    }
	       }

	       $tabela .= "</tbody>";
	       $tabela .= "</table>";

	       unset($conn);
	       echo $tabela;
	  }

	  //Sem separacao por marca
	  function buscaRelatorioFechamento_BKP(Fechamento $fechamento, $busca) {
	       if ($busca == "" || $busca == null) {
		    echo "";
	       } else {
		    $conexao = new Conexao();
		    $conn = $conexao->getConexao();

		    //Monta SQL de busca
		    $sql = "SELECT * FROM lista_compras WHERE id_fechamento = '{$fechamento->GetIdFechamento()}' AND (";
		    $sql .= "marca LIKE '%$busca%' OR ";
		    $sql .= "produto LIKE '%$busca%' OR ";
		    $sql .= "modelo LIKE '%$busca%' OR ";
		    $sql .= "tipo LIKE '%$busca%' OR ";
		    $sql .= "sexo LIKE '%$busca%' OR ";
		    $sqlLike = substr($sql, 0, -4);
		    $sqlLike .= ")";

		    $retorno = $conn->query($sqlLike);

		    //conta o total de itens
		    $totalRegistros = $retorno->rowCount();

		    /* Se tiverem registros encontrados, mostra a tabela */
		    if ($totalRegistros > 0) {
			 $tabela = "<table id='table-fechamento-busca' class='table-responsive table-hover tabela-fechamento'>";
			 $tabela .= '<thead>
			      <tr>
				   <th id="produto-fechamento"></th>
				   <th>Custo EUA</th>
				   <th>IOF</th>
				   <th>Custo</th>
				   <th class="bold">Markup</th>
				   <th>Venda</th>
				   <th class="bold">Lucro</th>
				   <th>Custo Total</th>
				   <th>Venda Total</th>
				   <th>Lucro Total</th>
			      </tr>
			 </thead>
			 <tbody>';

			 while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
			      if ($linhas[iof] == '0') {
				   $iof = '<option value="1">Sim</option>
				 <option value="0" selected>Não</option>';
			      } else if ($linhas[iof] == '1') {
				   $iof = '<option value="1" selected>Sim</option>
				 <option value="0">Não</option>';
			      }

			      if ($linhas[sexo] == "-") {
				   $sexo = "Unissex";
			      } else {
				   $sexo = $linhas[sexo];
			      }

			      $classeCustoEUA = ($linhas[valor_dolar] == "0") ? "outline_azul" : "outline_verde";
			      $classeVenda = ($linhas[venda] == "0") ? "outline_azul" : "outline_verde";

			      $tabela .= '<tr>
					<td>			      
					     <div><b>' . $linhas[marca] . '</b> - ' . $linhas[produto] . ' ' . $linhas[modelo] . ' - ' . $linhas[cor] . '</div>
					     <div class="properts-produto">' . $sexo . ' - Tamanho: ' . $linhas[tamanho] . ' Quantidade: ' . $linhas[quantidade_total] . '</div>
					</td>
					<td class=' . $classeCustoEUA . '><input type="text" class="dolar form-control" id=' . $linhas[id_item] . ' onblur="upd_valorDolarProduto(this)" value=' . $linhas[valor_dolar] . '></td>
					<td class="outline_verde">
					     <select name="iof" class="form-control iof-fechamento" id=' . $linhas[id_item] . ' onchange="upd_IOFProduto(this)">
						  ' . $iof . '
					     </select>
					</td>
					<td id="' . $linhas[id_item] . 'valorbr">R$ ' . number_format($linhas[valor_rs], 2, ',', '.') . '</td>
					<td id="' . $linhas[id_item] . 'markup">' . $linhas[markup] . ' %</td>
					<td class=' . $classeVenda . '><input type="text" class="real form-control" id=' . $linhas[id_item] . ' onblur="upd_valorVendaProduto(this)" value=' . number_format($linhas[venda], 2, ',', '.') . '></td>
					<td id="' . $linhas[id_item] . 'lucro">R$ ' . number_format($linhas[lucro], 2, ',', '.') . '</td>
					<td id="' . $linhas[id_item] . 'custo_total">R$ ' . number_format($linhas[custo_total], 2, ',', '.') . '</td>
					<td id="' . $linhas[id_item] . 'venda_total">R$ ' . number_format($linhas[venda_total], 2, ',', '.') . '</td>
					<td id="' . $linhas[id_item] . 'lucro_total">R$ ' . number_format($linhas[lucro_total], 2, ',', '.') . '</td>
			      </tr>';
			 }

			 $tabela .= "</tbody>";
			 $tabela .= "</table>";

			 unset($conn);
			 echo $tabela;
		    } else {
			 echo "Sem resultados";
		    }
	       }
	  }

	  //Com separação por marca
	  function buscaRelatorioFechamento(Fechamento $fechamento, $busca) {
	       if ($busca == "" || $busca == null) {
		    echo "";
	       } else {
		    $conexao = new Conexao();
		    $conn = $conexao->getConexao();

		    //Monta SQL de distinct marca
		    $sql = "SELECT distinct marca FROM lista_compras WHERE id_fechamento = '{$fechamento->GetIdFechamento()}' AND (";
		    $sql .= "marca LIKE '%$busca%' OR ";
		    $sql .= "produto LIKE '%$busca%' OR ";
		    $sql .= "modelo LIKE '%$busca%' OR ";
		    $sql .= "tipo LIKE '%$busca%' OR ";
		    $sql .= "sexo LIKE '%$busca%' OR ";
		    $sqlLikeMarca = substr($sql, 0, -4);
		    $sqlLikeMarca .= ")";

		    $retorno = $conn->query($sqlLikeMarca);

		    //conta o total de itens
		    $total_marcas = $retorno->rowCount();

		    /* Se tiverem registros encontrados, mostra a tabela */
		    if ($total_marcas > 0) {
			 $tabela = "<table id='table-fechamento-padrao' class='table-responsive table-hover tabela-fechamento'>";
			 $tabela .= '<thead>
					<tr>
					     <th id="produto-fechamento"></th>
					     <th>IOF</th>
					     <th>Custo EUA</th>				   
					     <th>Custo</th>
					     <th>Custo Total</th>
					     <th class="bold">Markup</th>
					     <th>Venda</th>
					     <th>Venda Total</th>
					     <th class="bold">Lucro</th>
					     <th>Lucro Total</th>
					</tr>
				   </thead>
				   <tbody>';

			 while ($marcas = $retorno->fetch(PDO::FETCH_ASSOC)) {
			      //Monta SQL por marca
			      $sql = "SELECT * FROM lista_compras WHERE id_fechamento = '{$fechamento->GetIdFechamento()}' AND (";
			      $sql .= "marca LIKE '%$busca%' OR ";
			      $sql .= "produto LIKE '%$busca%' OR ";
			      $sql .= "modelo LIKE '%$busca%' OR ";
			      $sql .= "tipo LIKE '%$busca%' OR ";
			      $sql .= "sexo LIKE '%$busca%' OR ";
			      $sqlLike = substr($sql, 0, -4);
			      $sqlLike .= ") AND marca = '$marcas[marca]' ORDER BY produto ASC";

			      $tabela .= '<tr>
					     <td colspan="10" class="linha-marca"><div class="marca-fechamento"><b>' . strtoupper($marcas[marca]) . '</b></div></td>
					  </tr>';

			      $retorno2 = $conn->query($sqlLike);

			      while ($linhas = $retorno2->fetch(PDO::FETCH_ASSOC)) {
				   if ($linhas[iof] == '0') {
					$iof = '<option value="1">Sim</option>
				 <option value="0" selected>Não</option>';
				   } else if ($linhas[iof] == '1') {
					$iof = '<option value="1" selected>Sim</option>
				 <option value="0">Não</option>';
				   }

				   if ($linhas[sexo] == "-") {
					$sexo = "Unissex";
				   } else {
					$sexo = $linhas[sexo];
				   }

				   $classeCustoEUA = ($linhas[valor_dolar] == "0") ? "outline_azul" : "outline_verde";
				   $classeVenda = ($linhas[venda] == "0") ? "outline_azul" : "outline_verde";

				   $tabela .= '<tr>					
						  <td>			      
						       <div>' . $linhas[produto] . ' ' . $linhas[modelo] . ' - ' . $linhas[cor] . '</div>
						       <div class="properts-produto">' . $sexo . ' - Tamanho: ' . $linhas[tamanho] . ' Quantidade: ' . $linhas[quantidade_total] . '</div>
						  </td>
						  <td class="outline_verde">
						       <select name="iof" class="form-control iof-fechamento" id=' . $linhas[id_item] . ' onchange="upd_IOFProduto(this)">
							    ' . $iof . '
						       </select>
						  </td>
						  <td class=' . $classeCustoEUA . '><input type="text" class="dolar form-control" id=' . $linhas[id_item] . ' onblur="upd_valorDolarProduto(this)" value=' . $linhas[valor_dolar] . '></td>					
						  <td id="' . $linhas[id_item] . 'valorbr">R$ ' . number_format($linhas[valor_rs], 2, ',', '.') . '</td>
						  <td id="' . $linhas[id_item] . 'custo_total">R$ ' . number_format($linhas[custo_total], 2, ',', '.') . '</td>
						  <td id="' . $linhas[id_item] . 'markup">' . $linhas[markup] . ' %</td>
						  <td class=' . $classeVenda . '><input type="text" class="real form-control" id=' . $linhas[id_item] . ' onblur="upd_valorVendaProduto(this)" value=' . number_format($linhas[venda], 2, ',', '.') . '></td>
						       <td id="' . $linhas[id_item] . 'venda_total">R$ ' . number_format($linhas[venda_total], 2, ',', '.') . '</td>
						  <td id="' . $linhas[id_item] . 'lucro">R$ ' . number_format($linhas[lucro], 2, ',', '.') . '</td>
						  <td id="' . $linhas[id_item] . 'lucro_total">R$ ' . number_format($linhas[lucro_total], 2, ',', '.') . '</td>
					     </tr>';
			      }
			 }

			 $tabela .= "</tbody>";
			 $tabela .= "</table>";

			 unset($conn);
			 echo "<div id='limpar-busca'>Limpar busca de: \"$busca\" <span>X</span></div>";
			 echo $tabela;
		    } else {
			 echo "Sem resultados";
		    }
	       }
	  }

	  function upd_valorDolarCompra($id_item, $valor) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $conn->query("UPDATE lista_compras SET valor_dolar = '$valor' WHERE id_item = '$id_item'");
	       unset($conn);
	  }

	  function upd_IOFCompra($id_item, $iof) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $conn->query("UPDATE lista_compras SET IOF = '$iof' WHERE id_item = '$id_item'");
	       unset($conn);
	  }

	  function upd_valorVendaCompra($id_item, $valor) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $conn->query("UPDATE lista_compras SET venda = '$valor' WHERE id_item = '$id_item'");
	       unset($conn);
	  }

	  function calculaValoresFechamento(Compra $compra, ParametrosCalculo $parametro) {
	       //Se tem IOF
	       if ($compra->getIOF()) {
		    $compra->setValorRS($compra->getValorDolar() * $parametro->GetCotacaoDolar() * (1 + ($parametro->GetImpostoUSA() / 100) + ($parametro->GetIOF() / 100)));
		    $compra->setMarkup(($compra->getVenda() / $compra->getValorRS()) * 100 - 100);
		    $compra->setLucro($compra->getVenda() - $compra->getValorRS());
	       } else {
		    $compra->setValorRS($compra->getValorDolar() * $parametro->GetCotacaoDolar() * (1 + ($parametro->GetImpostoUSA() / 100)));
		    $compra->setMarkup(($compra->getVenda() / $compra->getValorRS()) * 100 - 100);
		    $compra->setLucro($compra->getVenda() - $compra->getValorRS());
	       }

	       return $compra;
	  }

	  function updValoresCompra(Compra $compra, $id_item) {
	       $custo_total = $compra->getValorRS() * $compra->QuantidadeTotal . '<br>';
	       $venda_total = $compra->getVenda() * $compra->QuantidadeTotal . '<br>';
	       $lucro_total = $compra->getLucro() * $compra->QuantidadeTotal . '<br>';

	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $conn->query("UPDATE lista_compras SET valor_rs = '{$compra->getValorRS()}', markup = '{$compra->getMarkup()}', lucro = '{$compra->getLucro()}', custo_total = '$custo_total', venda_total = '$venda_total', lucro_total = '$lucro_total' WHERE id_item = '$id_item'");
	       unset($conn);
	  }

	  function getIdsCompras() {
	       $fechamentoDAO = new fechamentoDAO();
	       $fechamento = $fechamentoDAO->getFechamentoAtual();

	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $retorno = $conn->query("SELECT * FROM lista_compras WHERE id_fechamento = '{$fechamento->GetIdFechamento()}' ORDER BY id_item ASC");

	       $ids = array();

	       while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
		    array_push($ids, $linhas[id_item]);
	       }

	       unset($conn);
	       return $ids;
	  }

	  function getTabelaResumo() {
	       $parametrosDAO = new parametrosDAO();
	       $parametro = $parametrosDAO->getDadosParametro();
	       $despesaViagem = $parametro->GetDespesaViagem();
	       $lucroVerdadeiro = $this->getTotalLucroTotal() - $despesaViagem;
	       $tabela = '<tr>
			      <th>Custo Total EUA</th>
			      <td>$ ' . number_format($this->getTotalValorDolar(), 2, '.', ',') . '</td>
			 </tr>
			 <tr>
			      <th>Custo Total</th>
			      <td>R$ ' . number_format($this->getTotalCustoTotal(), 2, ',', '.') . '</td>
			 </tr>
			 <tr>
			      <th>Venda Total</th>
			      <td>R$ ' . number_format($this->getTotalVendaTotal(), 2, ',', '.') . '</td>
			 </tr>				   		 
			 <tr>
			      <th>Lucro Real</th>
			      <td>R$ ' . number_format($lucroVerdadeiro, 2, ',', '.') . '</td>
			 </tr>';

	       echo $tabela;
	  }

	  function getTabelaResumoPorIdFechamento2($id_fechamento) {
	       $parametrosDAO = new parametrosDAO();
	       $parametro = $parametrosDAO->getDadosParametroPorIdFechamento($id_fechamento);
	       $despesaViagem = $parametro->GetDespesaViagem();
	       $lucroVerdadeiro = $this->getTotalLucroTotalPorIdFechamento($id_fechamento) - $despesaViagem;

	       $fechamentoDAO = new fechamentoDAO();
	       $fechamento = $fechamentoDAO->getDadosFechamento($id_fechamento);

	       $tabela .= '
			 <tr>
			      <th>Custo Total EUA</th>
			      <td>$ ' . number_format($this->getTotalValorDolarPorIdFechamento($id_fechamento), 2, '.', ',') . '</td>
			 </tr>
			 <tr>
			      <th>Custo Total</th>
			      <td>R$ ' . number_format($this->getTotalCustoTotalPorIdFechamento($id_fechamento), 2, ',', '.') . '</td>
			 </tr>
			 <tr>
			      <th>Venda Total</th>
			      <td>R$ ' . number_format($this->getTotalVendaTotalPorIdFechamento($id_fechamento), 2, ',', '.') . '</td>
			 </tr>				   		 
			 <tr>
			      <th>Lucro Real</th>
			      <td>R$ ' . number_format($lucroVerdadeiro, 2, ',', '.') . '</td>
			 </tr>';
	      
	       echo $tabela;
	  }

	  function getTabelaResumoPorIdFechamento($id_fechamento) {
	       $parametrosDAO = new parametrosDAO();
	       $parametro = $parametrosDAO->getDadosParametroPorIdFechamento($id_fechamento);
	       $despesaViagem = $parametro->GetDespesaViagem();
	       $lucroVerdadeiro = $this->getTotalLucroTotalPorIdFechamento($id_fechamento) - $despesaViagem;

	       $fechamentoDAO = new fechamentoDAO();
	       $fechamento = $fechamentoDAO->getDadosFechamento($id_fechamento);

	       $tabela = "<table class='table-resumos table-responsive'>
			      <caption>{$fechamento->GetNomeFechamento()}</caption>
			  <tbody>";

	       $tabela .= '
			 <tr>
			      <th>Custo Total EUA</th>
			      <td>$ ' . number_format($this->getTotalValorDolarPorIdFechamento($id_fechamento), 2, '.', ',') . '</td>
			 </tr>
			 <tr>
			      <th>Custo Total</th>
			      <td>R$ ' . number_format($this->getTotalCustoTotalPorIdFechamento($id_fechamento), 2, ',', '.') . '</td>
			 </tr>
			 <tr>
			      <th>Venda Total</th>
			      <td>R$ ' . number_format($this->getTotalVendaTotalPorIdFechamento($id_fechamento), 2, ',', '.') . '</td>
			 </tr>				   		 
			 <tr>
			      <th>Lucro Real</th>
			      <td>R$ ' . number_format($lucroVerdadeiro, 2, ',', '.') . '</td>
			 </tr>';

	       $tabela .= "</tbody>";
	       $tabela .= "</table>";
	       echo $tabela;
	  }

	  function getTotalValorDolar() {
	       $fechamentoDAO = new fechamentoDAO();
	       $fechamento = $fechamentoDAO->getFechamentoAtual();

	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $retorno = $conn->query("SELECT valor_dolar FROM lista_compras WHERE id_fechamento = '{$fechamento->GetIdFechamento()}'");

	       $total = 0;
	       while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
		    $total += $linhas[valor_dolar];
	       }

	       return $total;
	  }

	  function getTotalValorDolarPorIdFechamento($id_fechamento) {
	       $fechamentoDAO = new fechamentoDAO();
	       $fechamento = $fechamentoDAO->getFechamentoAtual();

	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $retorno = $conn->query("SELECT valor_dolar FROM lista_compras WHERE id_fechamento = '$id_fechamento'");

	       $total = 0;
	       while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
		    $total += $linhas[valor_dolar];
	       }

	       return $total;
	  }

	  function getTotalCustoTotal() {
	       $fechamentoDAO = new fechamentoDAO();
	       $fechamento = $fechamentoDAO->getFechamentoAtual();

	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $retorno = $conn->query("SELECT custo_total FROM lista_compras WHERE id_fechamento = '{$fechamento->GetIdFechamento()}'");

	       $total = 0;
	       while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
		    $total += $linhas[custo_total];
	       }

	       return $total;
	  }

	  function getTotalCustoTotalPorIdFechamento($id_fechamento) {
	       $fechamentoDAO = new fechamentoDAO();
	       $fechamento = $fechamentoDAO->getFechamentoAtual();

	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $retorno = $conn->query("SELECT custo_total FROM lista_compras WHERE id_fechamento = '$id_fechamento'");

	       $total = 0;
	       while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
		    $total += $linhas[custo_total];
	       }

	       return $total;
	  }

	  function getTotalVendaTotal() {
	       $fechamentoDAO = new fechamentoDAO();
	       $fechamento = $fechamentoDAO->getFechamentoAtual();

	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $retorno = $conn->query("SELECT venda_total FROM lista_compras WHERE id_fechamento = '{$fechamento->GetIdFechamento()}'");

	       $total = 0;
	       while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
		    $total += $linhas[venda_total];
	       }

	       return $total;
	  }

	  function getTotalVendaTotalPorIdFechamento($id_fechamento) {
	       $fechamentoDAO = new fechamentoDAO();
	       $fechamento = $fechamentoDAO->getFechamentoAtual();

	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $retorno = $conn->query("SELECT venda_total FROM lista_compras WHERE id_fechamento = '$id_fechamento'");

	       $total = 0;
	       while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
		    $total += $linhas[venda_total];
	       }

	       return $total;
	  }

	  function getTotalLucroTotal() {
	       $fechamentoDAO = new fechamentoDAO();
	       $fechamento = $fechamentoDAO->getFechamentoAtual();

	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $retorno = $conn->query("SELECT lucro_total FROM lista_compras WHERE id_fechamento = '{$fechamento->GetIdFechamento()}'");

	       $total = 0;
	       while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
		    $total += $linhas[lucro_total];
	       }

	       return $total;
	  }

	  function getTotalLucroTotalPorIdFechamento($id_fechamento) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $retorno = $conn->query("SELECT lucro_total FROM lista_compras WHERE id_fechamento = '$id_fechamento'");

	       $total = 0;
	       while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
		    $total += $linhas[lucro_total];
	       }

	       return $total;
	  }

//	  ******************************************** Mobile	  
	  function marcaComprado($id_item) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();

	       $compra = $this->getDadosCompra($id_item);
	       $conn->query("UPDATE lista_compras SET comprado = 1, quantidade_comprada = '$compra->QuantidadeTotal' WHERE id_item = '$id_item'");

	       unset($conn);
	  }

	  function marcaNaoComprado($id_item) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();

	       $conn->query("UPDATE lista_compras SET comprado = 0, quantidade_comprada = 0 WHERE id_item = '$id_item'");

	       unset($conn);
	  }

	  function listaNaoComprados($query) {
	       $distinctMarcas = str_replace("*", "distinct marca", $query);
	       $distinctMarcas .= " ORDER BY marca ASC";
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $marcas = $conn->query($distinctMarcas);

	       //Verifica se existe resultados
	       if ($marcas->rowCount() > 0) {
		    $lista = "";

		    //Percorrendo as marcas e montando as tabelas
		    while ($marca = $marcas->fetch(PDO::FETCH_ASSOC)) {

			 $lista.= "<div class='marca'>$marca[marca]</div>";
			 //Remove a marca e adiciona uma nova marca
			 $marcaAnterior = $marca[marca];
			 $queryNova = str_replace(" AND marca = '$marcaAnterior'", " ", $query);
			 $queryNova .= " AND marca = '$marca[marca]'";
			 $retorno = $conn->query($queryNova);

			 while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {

			      if ($linhas[quantidade_total] > $linhas[quantidade_comprada]) {
				   $quantidade_restante = $linhas[quantidade_total] - $linhas[quantidade_comprada];

				   $lista .= "<div class='produto_lista' id='$linhas[id_item]' onclick=\"marcaItemNaoComprado(this)\">
						  <div class='titulo_produto'>$linhas[produto] $linhas[modelo] - $linhas[cor]</div>
						  <span class='descricao_produto'>Tamanho: $linhas[tamanho] - Quantidade: $quantidade_restante</span>						  
					     </div>
					     <div onclick=\"abreModalClienteCompra(this)\" id='$linhas[id_item]' class='abre_cliente_compra'><img src='img/3pontos.png' alt='Cliente compra'></div>
					     <div class='clear'></div>";
			      }
			 }
		    }

		    echo "<div id='separacao-nao-comprados' class='separacao'>Não comprados</div>";
		    echo $lista;

		    unset($conn);
	       } else {
		    echo "";
	       }
	  }

	  function listaComprados($query) {
	       $distinctMarcas = str_replace("*", "distinct marca", $query);
	       $distinctMarcas .= " ORDER BY marca ASC";
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $marcas = $conn->query($distinctMarcas);

	       //Verifica se existe resultados
	       if ($marcas->rowCount() > 0) {
		    $lista = "";

		    //Percorrendo as marcas e montando as tabelas
		    while ($marca = $marcas->fetch(PDO::FETCH_ASSOC)) {

			 $lista.= "<div class='marca'>$marca[marca]</div>";
			 //Remove a marca e adiciona uma nova marca
			 $marcaAnterior = $marca[marca];
			 $queryNova = str_replace(" AND marca = '$marcaAnterior'", " ", $query);
			 $queryNova .= " AND marca = '$marca[marca]'";
			 $retorno = $conn->query($queryNova);

			 while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
			      $lista .= "<div class='produto_lista' id='$linhas[id_item]' onclick=\"marcaItemJaComprado(this)\">
					     <div class='titulo_produto'><s>$linhas[produto] $linhas[modelo] - $linhas[cor]</s></div>
					     <span class='descricao_produto  '><s>Tamanho: $linhas[tamanho] - Quantidade: $linhas[quantidade_comprada]</s></span>
					</div>
					<div onclick=\"abreModalClienteCompra(this)\" id='$linhas[id_item]' class='abre_cliente_compra'><img src='img/3pontos.png' alt='Cliente compra'></div>
					<div class='clear'></div>";
			 }
		    }

		    echo "<div class='separacao'>Comprados</div>";
		    echo $lista;

		    unset($conn);
	       } else {
		    echo "";
	       }
	  }

	  function listaComprasBusca($busca, $id_fechamento) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();

	       //Monta SQL de busca
	       $sql = "SELECT * FROM lista_compras WHERE id_fechamento = '$id_fechamento' AND (";
	       $sql .= "tipo LIKE '%$busca%' OR ";
	       $sql .= "sexo LIKE '%$busca%' OR ";
	       $sql .= "produto LIKE '%$busca%  ' OR ";
	       $sql .= "marca LIKE '%$busca%' OR ";
	       $sql .= "modelo LIKE '%$busca%' OR ";
	       $sql .= "cor LIKE '%$busca%' OR ";
	       $sqlLike = substr($sql, 0, -4);

	       $sqlLike2 = "$sqlLike) ORDER BY marca ASC";

	       $retorno = $conn->query($sqlLike2);

	       $lista = "";
	       $totalRegistros = $retorno->rowCount();
	       /* Se tiverem registros encontrados, mostra a tabela */
	       if ($totalRegistros > 0) {
		    while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
			 $titulo_produto = "";
			 $descricao_produto = "";

			 //Verifica se está comprado
			 if ($linhas['comprado'] == '1') {
			      $titulo_produto = "<div class='titulo_produto'><s>$linhas[marca]<br>$linhas[produto] $linhas[modelo] - $linhas[cor]</s></div>";
			      $descricao_produto = "<span class='descricao_produto'><s>Tamanho: $linhas[tamanho] - Quantidade: $linhas[quantidade_total]</s></span>";
			 } else {
			      $titulo_produto = "<div class='titulo_produto  '>$linhas[marca]<br>$linhas[produto] $linhas[modelo] - $linhas[cor]</div>";
			      $descricao_produto = "<span class='descricao_produto'>Tamanho: $linhas[tamanho] - Quantidade: $linhas[quantidade_total]</span>";
			 }



			 $lista .= "<div class='produto_lista' id='$linhas[id_item]' onclick=\"marcaItem(this)\">
					$titulo_produto
					$descricao_produto
				   </div>";
		    }

		    echo $lista;
	       } else {
		    echo "";
	       }
	  }

	  function getMarcas($query) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $retorno = $conn->query($query);
	       while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
		    echo "<div class='filtro_marca' onclick='selecionaMarca(this)'>$linhas[marca]</div>";
	       }
	  }

	  function getTipos($query) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $retorno = $conn->query($query);
	       while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
		    echo "<div class='filtro_tipo' onclick='selecionaTipo(  this)'>$linhas[tipo]</div>";
	       }
	  }

	  function upd_quantidadeComprada(Compra $compra, $id_item) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();

	       if ($conn->query("UPDATE lista_compras SET quantidade_total = '{$compra->QuantidadeTotal}', quantidade_comprada = '{$compra->QuantidadeComprada}', comprado = '{$compra->Comprado}' WHERE id_item = '$id_item'")) {
		    unset($conn);
		    return true;
	       } else {
		    unset($conn);
		    return false;
	       }
	  }

     }
     