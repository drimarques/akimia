<?php

     class parametrosDAO {

	  function cadastraParametros(ParametrosCalculo $parametros, $id_fechamento) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();

	       $conn->query("DELETE FROM parametros_calculo WHERE id_fechamento = '$id_fechamento'");
	       $conn->query("INSERT INTO parametros_calculo (id_fechamento, cotacaoDolar, impostoUSA, IOF, despesa_viagem) VALUES ('$id_fechamento', '{$parametros->CotacaoDolar}', '{$parametros->ImpostoUSA}', '{$parametros->IOF}', '{$parametros->DespesaViagem}')");

	       unset($conn);
	  }

	  function listaParametros() {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $retorno = $conn->query("SELECT * FROM parametros_calculo");

	       $tabela = '<table class="table-responsive table-hover tabelas2" id="table-parametros">';
	       $tabela .= "<thead>
			      <tr>
				   <th>Cotação Dólar</th>
				   <th>Imposto USA</th>
				   <th>IOF</th>		      
			      </tr>
			 </thead>
			 <tbody>";

	       while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
		    $tabela .= '<tr>				   
				   <td>R$ ' . number_format($linhas[cotacaoDolar], 2, ',', '.') . '</td>
				   <td>' . number_format($linhas[impostoUSA], 2, ',', '.') . ' %</td>
				   <td>' . number_format($linhas[IOF], 2, ',', '.') . ' %</td>
			      </tr>';
	       }

	       $tabela .= "</tbody>
			 </table>";

	       unset($conn);

	       echo $tabela;
	  }

	  function listaParametrosFechamento(Fechamento $fechamento) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $retorno = $conn->query("SELECT * FROM parametros_calculo WHERE id_fechamento = '{$fechamento->GetIdFechamento()}'");

	       $tabela = '<table class="table-responsive" id="table-parametros-fechamento"><tbody>';
	       if ($retorno->rowCount() == 1) {
		    while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {

			 $classeCotacao = ($linhas[cotacaoDolar] == "0") ? "outline_azul" : "outline_verde";
			 $classeImposto = ($linhas[impostoUSA] == "0") ? "outline_azul" : "outline_verde";
			 $classeIOF = ($linhas[IOF] == "0") ? "outline_azul" : "outline_verde";
			 $classeDespesa = ($linhas[despesa_viagem] == "0") ? "outline_azul" : "outline_verde";


			 $tabela .= '<tr>
					<th>Cotação Dólar</th>
					<td class="' . $classeCotacao . '"><input type="text" class="real form-control" value=' . number_format($linhas[cotacaoDolar], 2, ',', '.') . ' id="cotacao-dolar"></td>					     
				   </tr>
				   <tr>
					<th>Imposto USA</th>
					<td class="' . $classeImposto . '"><input type="text" class="porcentagem form-control" value=' . $linhas[impostoUSA] . ' id="imposto-usa"></td>
				   </tr>	  
				   <tr>     
					<th>IOF</th>
					<td class="' . $classeIOF . '"><input type="text" class="porcentagem form-control" value=' . $linhas[IOF] . ' id="IOF"></td>
				   </tr>	  
				   <tr>	  
					<th>Despesa de Importação</th>
					<td class="' . $classeDespesa . '"><input type="text" class="real form-control" value=' . number_format($linhas[despesa_viagem], 2, ',', '.') . ' id="despesa_imp"></td>
				   </tr>';
		    }
	       } else {
		    $tabela .= '<tr>
				   <th>Cotação Dólar</th>
				   <td><input type="text" class="dolar form-control" id="cotacao-dolar"></td>
			      </tr>
			      <tr>
				   <th>Imposto USA</th>
				   <td><input type="text" class="porcentagem form-control" id="imposto-usa"></td>
			      </tr>	  
			      <tr>     
				   <th>IOF</th>
				   <td><input type="text" class="porcentagem form-control" id="IOF"></td>
			      </tr>	  
			      <tr>	  
				   <th>Despesa de Importação</th>
				   <td><input type="text" class="real form-control" id="despesa_imp"></td>
			      </tr>';
	       }


	       $tabela .= "</tbody>
			 </table>";

	       unset($conn);

	       echo $tabela;
	  }

	  function listaParametrosFechamentobkp(Fechamento $fechamento) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $retorno = $conn->query("SELECT * FROM parametros_calculo WHERE id_fechamento = '{$fechamento->GetIdFechamento()}'");

	       $tabela = '<table class="table-responsive" id="table-parametros-fechamento"><tbody>';
	       if ($retorno->rowCount() == 1) {
		    while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
			 $tabela .= '<tr>
					<th>Cotação Dólar</th>
					<td>R$ ' . number_format($linhas[cotacaoDolar], 2, ',', '.') . '</td>
				   </tr>
				   <tr>
					<th>Imposto USA</th>
					<td>' . number_format($linhas[impostoUSA], 2, ',', '.') . ' %</td>
				   </tr>	  
				   <tr>     
					<th>IOF</th>
					<td>' . number_format($linhas[IOF], 2, ',', '.') . ' %</td>
				   </tr>	  
				   <tr>	  
					<th>Despesa de Importação</th>
					<td>R$ ' . number_format($linhas[despesa_viagem], 2, ',', '.') . '</td>
				   </tr>';
		    }
	       } else {
		    $tabela .= '<tr>
				   <th>Cotação Dólar</th>
				   <td><input type="text" class="dolar form-control" id="cotacao-dolar"></td>
			      </tr>
			      <tr>
				   <th>Imposto USA</th>
				   <td><input type="text" class="porcentagem form-control" id="imposto-usa"></td>
			      </tr>	  
			      <tr>     
				   <th>IOF</th>
				   <td><input type="text" class="porcentagem form-control" id="IOF"></td>
			      </tr>	  
			      <tr>	  
				   <th>Despesa de Importação</th>
				   <td><input type="text" class="real form-control" id="despesa_imp"></td>
			      </tr>';
	       }


	       $tabela .= "</tbody>
			 </table>";

	       unset($conn);

	       echo $tabela;
	  }

	  function temParametros() {
	       $fechamentoDAO = new fechamentoDAO();
	       $fechamento = $fechamentoDAO->getFechamentoAtual();

	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $retorno = $conn->query("SELECT * FROM parametros_calculo WHERE id_fechamento = '{$fechamento->GetIdFechamento()}'");

	       if ($retorno->rowCount() > 0) {
		    unset($conn);
		    return true;
	       } else {
		    unset($conn);
		    return false;
	       }
	  }

	  function upd_cotacaoDolar($valor) {
	       $fechamentoDAO = new fechamentoDAO();
	       $fechamento = $fechamentoDAO->getFechamentoAtual();

	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $conn->query("UPDATE parametros_calculo SET cotacaoDolar = '$valor' WHERE id_fechamento = '{$fechamento->GetIdFechamento()}'");
	       unset($conn);
	  }

	  function ins_cotacaoDolar($valor) {
	       $fechamentoDAO = new fechamentoDAO();
	       $fechamento = $fechamentoDAO->getFechamentoAtual();

	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $conn->query("INSERT INTO parametros_calculo (id_fechamento, cotacaoDolar) VALUES ('{$fechamento->GetIdFechamento()}', '$valor')");
	       unset($conn);
	  }

	  function upd_impostoUsa($valor) {
	       $fechamentoDAO = new fechamentoDAO();
	       $fechamento = $fechamentoDAO->getFechamentoAtual();

	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $conn->query("UPDATE parametros_calculo SET impostoUSA = '$valor' WHERE id_fechamento = '{$fechamento->GetIdFechamento()}'");
	       unset($conn);
	  }

	  function ins_impostoUsa($valor) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $conn->query("INSERT INTO parametros_calculo (id_fechamento, impostoUSA) VALUES ('{$fechamento->GetIdFechamento()}', '$valor')");
	       unset($conn);
	  }

	  function upd_IOF($valor) {
	       $fechamentoDAO = new fechamentoDAO();
	       $fechamento = $fechamentoDAO->getFechamentoAtual();

	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $conn->query("UPDATE parametros_calculo SET IOF = '$valor' WHERE id_fechamento = '{$fechamento->GetIdFechamento()}'");
	       unset($conn);
	  }

	  function ins_IOF($valor) {
	       $fechamentoDAO = new fechamentoDAO();
	       $fechamento = $fechamentoDAO->getFechamentoAtual();

	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $conn->query("INSERT INTO parametros_calculo (id_fechamento, IOF) VALUES ('{$fechamento->GetIdFechamento()}', '$valor')");
	       unset($conn);
	  }

	  function ins_despesaImportacao($valor) {
	       $fechamentoDAO = new fechamentoDAO();
	       $fechamento = $fechamentoDAO->getFechamentoAtual();

	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $conn->query("INSERT INTO parametros_calculo (id_fechamento, despesa_viagem) VALUES ('{$fechamento->GetIdFechamento()}', '$valor')");
	       unset($conn);
	  }

	  function upd_despesaImportacao($valor) {
	       $fechamentoDAO = new fechamentoDAO();
	       $fechamento = $fechamentoDAO->getFechamentoAtual();

	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $conn->query("UPDATE parametros_calculo SET despesa_viagem = '$valor' WHERE id_fechamento = '{$fechamento->GetIdFechamento()}'");
	       unset($conn);
	  }

	  function getDadosParametro() {
	       $fechamentoDAO = new fechamentoDAO();
	       $fechamento = $fechamentoDAO->getFechamentoAtual();

	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();

	       $retorno = $conn->query("SELECT * FROM parametros_calculo WHERE id_fechamento = '{$fechamento->GetIdFechamento()}'");
	       $linhas = $retorno->fetch(PDO::FETCH_ASSOC);

	       $parametro = new ParametrosCalculo();
	       $parametro->SetIdParametro($linhas[idParametro]);
	       $parametro->SetCotacaoDolar($linhas[cotacaoDolar]);
	       $parametro->SetImpostoUSA($linhas[impostoUSA]);
	       $parametro->SetIOF($linhas[IOF]);
	       $parametro->SetDespesaViagem($linhas[despesa_viagem]);

	       unset($conn);
	       return $parametro;
	  }

	  function getDadosParametroPorIdFechamento($id_fechamento) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();

	       $retorno = $conn->query("SELECT * FROM parametros_calculo WHERE id_fechamento = '$id_fechamento'");
	       $linhas = $retorno->fetch(PDO::FETCH_ASSOC);

	       $parametro = new ParametrosCalculo();
	       $parametro->SetIdParametro($linhas[idParametro]);
	       $parametro->SetCotacaoDolar($linhas[cotacaoDolar]);
	       $parametro->SetImpostoUSA($linhas[impostoUSA]);
	       $parametro->SetIOF($linhas[IOF]);
	       $parametro->SetDespesaViagem($linhas[despesa_viagem]);

	       unset($conn);
	       return $parametro;
	  }

     }
     