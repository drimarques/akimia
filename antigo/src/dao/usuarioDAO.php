<?php

     session_start();

     date_default_timezone_set('Brazil/East');
     date_default_timezone_set('America/Sao_Paulo');

     class UsuarioDAO {

	  function validaLogin(Usuario $usuario) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $retorno = $conn->query("SELECT * FROM usuarios WHERE email = '{$usuario->GetEmail()}' AND senha = '{$usuario->GetSenha()}'");

	       if ($retorno->rowCount() > 0) {
		    unset($conn);
		    return true;
	       } else {
		    unset($conn);
		    return false;
	       }
	  }

	  function logaUsuario($email) {
	       $_SESSION["usuario_logado"] = $email;
	       $tempoInatividade = 600;

	       $_SESSION['inicio'] = time();
	       $_SESSION['limite_inatividade'] = $tempoInatividade;
	  }

	  function logOut() {
	       session_destroy();
	  }

	  function validaTempoSession() {
	       $inicio = $_SESSION['inicio'];
	       $limite = $_SESSION['limite_inatividade'];

	       if ($inicio) {
		    $tempo = time() - $inicio;
	       }

	       if ($tempo > $limite) {
		    unset($_SESSION["usuario_logado"]);
		    unset($_SESSION["inicio"]);
		    unset($_SESSION["limite_inatividade"]);		    
	       } 
	       else {
		    $_SESSION['inicio'] = time();		    
	       }
	  }

     }
     