<?php

     class Cliente {
	  
//	  Atributos
	  var $Nome;
	  var $Apelido;
	  var $Email;
	  var $WhatsApp;
	  var $Telefone;
	  var $Celular;
	  var $CEP;
	  var $Endereco;
	  var $NumeroEndereco;
	  var $Complemento;
	  var $Referencia;
	  var $UF;
	  var $Cidade;
	  var $Bairro;

//	  Métodos
	  function SetNome($nome) {
	       $this->Nome = $nome;
	  }

	  function GetNome() {
	       return $this->Nome;
	  }

	  function SetApelido($apelido) {
	       $this->Apelido = $apelido;
	  }

	  function GetApelido() {
	       return $this->Apelido;
	  }

	  function SetEmail($email) {
	       $this->Email = $email;
	  }

	  function GetEmail() {
	       return $this->Email;
	  }

	  function SetWhatsApp($whatsapp) {
	       $this->WhatsApp = $whatsapp;
	  }

	  function GetWhatsApp() {
	       return $this->WhatsApp;
	  }

	  function SetTelefone($telefone) {
	       $this->Telefone = $telefone;
	  }

	  function GetTelefone() {
	       return $this->Telefone;
	  }

	  function SetCelular($celular) {
	       $this->Celular = $celular;
	  }

	  function GetCelular() {
	       return $this->Celular;
	  }

	  function SetCEP($cep) {
	       $this->CEP = $cep;
	  }

	  function GetCEP() {
	       return $this->CEP;
	  }

	  function SetEndereco($endereco) {
	       $this->Endereco = $endereco;
	  }

	  function GetEndereco() {
	       return $this->Endereco;
	  }

	  function SetNumeroEndereco($num) {
	       $this->NumeroEndereco = $num;
	  }

	  function GetNumeroEndereco() {
	       return $this->NumeroEndereco;
	  }

	  function SetComplemento($complemento) {
	       $this->Complemento = $complemento;
	  }

	  function GetComplemento() {
	       return $this->Complemento;
	  }

	  function SetReferencia($ref) {
	       $this->Referencia = $ref;
	  }

	  function GetReferencia() {
	       return $this->Referencia;
	  }

	  function SetUF($UF) {
	       $this->UF = $UF;
	  }

	  function GetUF() {
	       return $this->UF;
	  }

	  function SetCidade($cidade) {
	       $this->Cidade = $cidade;
	  }

	  function GetCidade() {
	       return $this->Cidade;
	  }

	  function SetBairro($bairro) {
	       $this->Bairro = $bairro;
	  }

	  function GetBairro() {
	       return $this->Bairro;
	  }
	  
//	   Verifica se tem cliente cadastrado
	  function existeClientes(){
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $retorno = $conn->query("SELECT * FROM clientes");
	       
	       /* Maior ou igual a 1, existe clientes...*/
	       if ($retorno->rowCount() >= 1) {		    
		    unset($conn);
		    return TRUE;
	       } 
	       else {		    
		    unset($conn);
		    return FALSE;
	       }
	  }

//	  Método que verifica e-mail já cadastrado
	  function getEmailCadastrado($email) {
	       /* Abre conexao */
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();

	       /* Executa query */
	       $retorno = $conn->query("SELECT * FROM clientes WHERE email = '$email'");

	       /* Se retornar 1 e-mail é porque já existe */
	       if ($retorno->rowCount() >= 1) {
		    #Fecha conexão
		    unset($conn);
		    return TRUE;
	       } else {
		    #Fecha conexão
		    unset($conn);
		    return FALSE;
	       }
	  }

//	  Cadastro de clientes
	  function cadastraCliente() {
	       /* Abre conexao */
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();

	       /* Executa query */
	       $conn->query("INSERT INTO clientes (nome, apelido, email, whatsapp, telefone, celular, cep, endereco, numero_endereco, complemento, referencia, uf, cidade, bairro, permite_notificacoes) VALUES
	       ('$this->Nome', '$this->Apelido', '$this->Email', '$this->WhatsApp', '$this->Telefone', '$this->Celular', '$this->CEP', '$this->Endereco', '$this->NumeroEndereco', '$this->Complemento', '$this->Referencia', '$this->UF', '$this->Cidade', '$this->Bairro', '1')");

	       #Fecha conexão
	       unset($conn);
	  }

//	 Função que carrega usuário
	  function getDadosCliente($id_cliente) {
	       /* Abre conexao */
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();

	       /* Executa query */
	       $retorno = $conn->query("SELECT * FROM clientes WHERE id_cliente = '$id_cliente'");

//	       Recebe os dados
	       $linhas = $retorno->fetch(PDO::FETCH_ASSOC);
	       $this->SetNome($linhas['nome']);
	       $this->SetApelido($linhas['apelido']);
	       $this->SetEmail($linhas['email']);
	       $this->SetWhatsApp($linhas['whatsapp']);
	       $this->SetTelefone($linhas['telefone']);
	       $this->SetCelular($linhas['celular']);
	       $this->SetCEP($linhas['cep']);
	       $this->SetEndereco($linhas['endereco']);
	       $this->SetNumeroEndereco($linhas['numero_endereco']);
	       $this->SetComplemento($linhas['complemento']);
	       $this->SetReferencia($linhas['referencia']);
	       $this->SetUF($linhas['uf']);
	       $this->SetCidade($linhas['cidade']);
	       $this->SetBairro($linhas['bairro']);
	  }
	  
//	  Construtor
	  function __construct() {
	       $this->Nome = null;
	       $this->Apelido = null;
	       $this->Email = null;
	       $this->WhatsApp = null;
	       $this->Telefone = null;
	       $this->Celular = null;
	       $this->CEP = null;
	       $this->Endereco = null;
	       $this->NumeroEndereco = null;
	       $this->Complemento = null;
	       $this->Referencia = null;
	       $this->UF = null;
	       $this->Cidade = null;
	       $this->Bairro = null;
	  }

     }
     