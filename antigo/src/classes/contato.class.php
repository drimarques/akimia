<?php

     class Contato {

	  var $Nome;
	  var $Email;
	  var $Assunto;
	  var $Mensagem;

	  /*Atributos*/
	  function setNome($nome) {
	       $this->Nome = $nome;
	  }

	  function getNome() {
	       return $this->Nome;
	  }

	  function setEmail($email) {
	       $this->Email = $email;
	  }
	  
	  function getEmail() {
	       return $this->Email;
	  }

	  function setAssunto($assunto) {
	       $this->Assunto = $assunto;
	  }
	  
	  function getAssunto() {
	       return $this->Assunto;
	  }

	  function setMensagem($mensagem) {
	       $this->Mensagem = $mensagem;
	  }
	  
	  function getMensagem() {
	       return $this->Mensagem;
	  }
	  
	  /*Construtor*/
	  public function __construct() {
	       $this->Nome = null;
	       $this->Email = null;
	       $this->Assunto = null;
	       $this->Mensagem = null;
	  }

     }
     