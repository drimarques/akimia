<?php
     
     class Fechamento {

	  var $IdFechamento;
	  var $NomeFechamento;
	  var $DataInicio;
	  var $DataTermino;
	  var $Fechado;

	  function SetIdFechamento($id) {
	       $this->IdFechamento = $id;
	  }

	  function GetIdFechamento() {
	       return $this->IdFechamento;
	  }
	  
	  function SetNomeFechamento($nome) {
	       $this->NomeFechamento = $nome;
	  }

	  function GetNomeFechamento() {
	       return $this->NomeFechamento;
	  }
	  
	  function SetDataInicio($data) {
	       $this->DataInicio = $data;
	  }

	  function GetDataInicio() {
	       return $this->DataInicio;
	  }
	  
	  function SetDataTermino($data) {
	       $this->DataTermino = $data;
	  }

	  function GetDataTermino() {
	       return $this->DataTermino;
	  }
	  
	  function SetFechado($valor) {
	       $this->Fechado = $valor;
	  }

	  function GetFechado() {
	       return $this->Fechado;
	  }
	  
	  function __construct() {
	       
	  }
     }

