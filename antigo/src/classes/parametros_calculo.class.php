<?php

     class ParametrosCalculo {
	  
	  var $IdParametro;
	  var $CotacaoDolar;
	  var $ImpostoUSA;
	  var $IOF;
	  var $DespesaViagem;

	  function SetIdParametro($valor) {
	       $this->IdParametro = $valor;
	  }

	  function GetIdParametro() {
	       return $this->IdParametro;
	  }
	  
	  function SetCotacaoDolar($valor) {
	       $this->CotacaoDolar = $valor;
	  }

	  function GetCotacaoDolar() {
	       return $this->CotacaoDolar;
	  }
	  
	  function SetImpostoUSA($valor) {
	       $this->ImpostoUSA = $valor;
	  }
	  
	  function GetImpostoUSA() {
	       return $this->ImpostoUSA;
	  }

	  function SetIOF($valor) {
	       $this->IOF = $valor;
	  }
	  
	  function GetIOF() {
	       return $this->IOF;
	  }
	  
	  function SetDespesaViagem($valor) {
	       $this->DespesaViagem = $valor;
	  }

	  function GetDespesaViagem() {
	       return $this->DespesaViagem;
	  }

     }
     

