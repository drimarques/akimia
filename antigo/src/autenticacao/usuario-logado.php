<?php

     session_start();
     
     if(isset($_SESSION["usuario_logado"])){
	  echo "<div id='painel-restrito'>
		    <h4>Bem-vindo! <span id='btn_logout'>Sair</span></h4>
		    <div>{$_SESSION["usuario_logado"]}</div>
		    <ul id='lista-painel-restrito'>
			 <li id='clientes_cadastrados' onclick='window.open(\"adm/index.php\")'><img src='img/clientes.png' alt='Clientes cadastrados'>Clientes cadastrados</li>
			 <li id='importar_lista'><img src='img/excel.png' alt='Importar'>Importar lista
			      <form method='post' hidden='hidden' name='form_arquivo_excel' enctype='multipart/form-data' autocomplete='off'>
				   <input type='file' id='arquivo_excel' name='arquivo_excel' accept='.xls'>
				   <input type='submit' name='btnEnviarExcel' id='btnEnviarExcel'>
			      </form>
			 </li>
			 <li><a href='src/autenticacao/valida-fechamento.php?tela=lista de compras' target='_blank'><img src='img/compras.png' alt='Ver lista'>Ver lista</a></li>
			 <li><a href='src/autenticacao/valida-fechamento.php?tela=fechamento' target='_blank'><img src='img/fechamento.png' alt='Fechamento'>Fechamento</a></li>
			 <li onclick='window.open(\"adm/historico-fechamentos.php\")'><img src='img/historico.png' alt='Histórico de fechamentos'>Histórico de fechamentos</li>
			 <li onclick='javascript:window.location=\"adm/src/exportar_fechamento.php\"'><img src='img/down.png' alt='Exportar fechamento'>Exportar fechamento</li>
		    </ul>
	       </div>";
     }