<?php
     require_once '../classes/conexao.class.php';
     require_once '../classes/usuario.class.php';
     require_once '../dao/usuarioDAO.php';

     $email = $_POST["email"];
     $senha = $_POST["senha"];
     
     $usuario = new Usuario();
     $usuario->SetEmail($email);
     $usuario->SetSenha($senha);
     
     $usuarioDAO = new UsuarioDAO();
     if($usuarioDAO->validaLogin($usuario)){	  
	  $usuarioDAO->logaUsuario($usuario->GetEmail());
     }
     else{	  
	  echo "erro";
     }

     