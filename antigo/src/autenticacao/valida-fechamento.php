<?php
     session_start();
     
     require_once '../classes/conexao.class.php';
     require_once '../classes/fechamento.class.php';
     require_once '../dao/fechamentoDAO.php';

     $tela = $_GET["tela"];
     
     $fechamentoDAO = new fechamentoDAO();
     $fechamento = $fechamentoDAO->getFechamentoAtual();     
     if ($fechamento->GetIdFechamento() == null) {	  
	  $_SESSION["notificacao_sem_fechamento"] = "Não há informações para <b><i>$tela</i></b>. Necessário importar planilha.";
	  header("Location: ../../index.php");
     }
     else{
	  switch ($tela) {
	       case "lista de compras":
		    header("Location: ../../adm/compras.php");
		    break;
	       case "fechamento":
		    header("Location: ../../adm/fechamento.php");
		    break;
	       default:
		    break;
	  }
	  
     }
     
     die();