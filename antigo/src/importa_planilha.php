<?php

     if (isset($_POST['btnEnviarExcel'])) {
	  $dao = new comprasDAO();
	  $compra = new Compra();

	  $daoPARAMETROS = new parametrosDAO();
	  $parametros = new ParametrosCalculo();

	  $daoFECHAMENTO = new fechamentoDAO();
	  $fechamento = $daoFECHAMENTO->getFechamentoAtual();

	  if ($fechamento->IdFechamento == "") {
	       $daoFECHAMENTO->iniciaFechamento();
	       $fechamento = $daoFECHAMENTO->getFechamentoAtual();
	  }

	  $data = new Spreadsheet_Excel_Reader();
	  $data->setOutputEncoding('UTF-8');

	  //Se o arquivo não for vazio e for um arquivo excel
	  if (!empty($_FILES['arquivo_excel']) && ($_FILES['arquivo_excel']['type'] == "application/vnd.ms-excel" || $_FILES['arquivo_excel']['type'] == "application/octet-stream")) {
	       $data->read($_FILES['arquivo_excel']['tmp_name']);

	       //[linha][coluna]
	       if (utf8_encode($data->sheets[0]['cells'][3][2]) == "Cotação Dolar") {
		    $cotacaoDolar = (double) $data->sheets[0]['cells'][3][3];
		    $parametros->SetCotacaoDolar($cotacaoDolar);
		    if($parametros->CotacaoDolar < "0"){
			 $parametros->CotacaoDolar = "0";
		    }
	       }

	       //Obtem impostoUSA
	       if (utf8_encode($data->sheets[0]['cells'][4][2]) == "Imposto USA") {
		    $impostoUSA = (double) $data->sheets[0]['cells'][4][3];
		    $parametros->SetImpostoUSA($impostoUSA);
		    
		    if($parametros->ImpostoUSA < "0"){
			 $parametros->ImpostoUSA = "0";
		    }
	       }

	       //Obtem IOF
	       if (utf8_encode($data->sheets[0]['cells'][5][2]) == "IOF") {
		    $IOF = (double) $data->sheets[0]['cells'][5][3];
		    $parametros->SetIOF($IOF);
		    if($parametros->IOF < "0"){
			 $parametros->IOF = "0";
		    }
	       }

	       //Obtem IOF
	       if (utf8_encode($data->sheets[0]['cells'][6][2]) == "Despesa com Importação") {
		    $despesa_viagem = (double) $data->sheets[0]['cells'][6][3];
		    $parametros->SetDespesaViagem($despesa_viagem);
		    if($parametros->DespesaViagem < "0"){
			 $parametros->DespesaViagem = "0";
		    }
	       }

	       //Cadastra parametros de calculo
	       $daoPARAMETROS->cadastraParametros($parametros, $fechamento->GetIdFechamento());


	       $qtdLinhas = $data->sheets[0]['numRows'];

	       //Limpa lista
	       $dao->limpaListaCompras($fechamento->GetIdFechamento());
//	       //Percorrendo os valores a partir da 3 linha
	       for ($i = 3; $i <= $qtdLinhas; $i++) {
		    $compra = new Compra();
		    $compra->setMarca(utf8_encode($data->sheets[0]['cells'][$i][5]));
		    $compra->setSexo(utf8_encode($data->sheets[0]['cells'][$i][6]));
		    $compra->setTipo(utf8_encode($data->sheets[0]['cells'][$i][7]));
		    $compra->setProduto(utf8_encode($data->sheets[0]['cells'][$i][8]));
		    $compra->setModelo(utf8_encode($data->sheets[0]['cells'][$i][9]));
		    $compra->setCor(utf8_encode($data->sheets[0]['cells'][$i][10]));
		    $compra->setTamanho($data->sheets[0]['cells'][$i][11]);
		    $compra->setQuantidadeTotal((int) $data->sheets[0]['cells'][$i][12]);
		    $compra->setValorDolar((double) $data->sheets[0]['cells'][$i][14]);
		    $compra->setVenda((double) $data->sheets[0]['cells'][$i][18]);
		    $compra->setCliente($data->sheets[0]['cells'][$i][22]);
		    $compra->setTelefone($data->sheets[0]['cells'][$i][23]);
		    $compra->setEmail($data->sheets[0]['cells'][$i][24]);

		    if ($compra->Marca != null || $compra->Marca != "") {

			 if ($compra->ValorDolar < "0") {
			      $compra->ValorDolar = "0";
			 }

			 if ($compra->Venda < "0") {
			      $compra->Venda = "0";
			 }

			 $dao->cadastraListaCompra($compra, $fechamento->GetIdFechamento());
		    }
	       }

	       //Calculando a tabela toda
	       $ids = $dao->getIdsCompras();
	       for ($i = 0; $i < count($ids); $i++) {
		    $compra = $dao->getDadosCompra($ids[$i]);
		    $parametro = $daoPARAMETROS->getDadosParametro();

		    if ($compra->ValorDolar !== "0.00" && $compra->Venda !== "0.00" && $compra->Venda > "0" && $parametro->CotacaoDolar != "0.00" && $parametro->ImpostoUSA != "0.00" && $parametro->IOF != "0.00" && $parametro->DespesaViagem != "0.00") {			 
			 $compra_calculada = $dao->calculaValoresFechamento($compra, $parametro);
			 $dao->updValoresCompra($compra_calculada, $ids[$i]);
		    }
	       }

	       $_SESSION["notificacao_lista"] = "Lista importada com sucesso!";
	       ?>
	       <script>
	            location.href = "index.php";
	       </script>
	       <?php

	  } else {
	       $_SESSION["notificacao_lista"] = "Somente arquivos no formato .xls";
	       ?>
	       <script>
	            location.href = "index.php";
	       </script>
	       <?php

	  }
     }

