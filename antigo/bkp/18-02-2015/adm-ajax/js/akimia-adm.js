
/*Relatório defult*/
function getRelatorioClientesDefault() {
     var relatorio = $.post("src/relatorio-clientes-default.php");
     relatorio.done(function (retorno) {
	  $(".relatorios").html(retorno);
     });
}


$(document).ready(function () {
//     Scrol suave
//     $(".relatorios").niceScroll();
     $("#relatorio-default").niceScroll();


     $("#textoPesquisa").keyup(function () {
	  var pesquisa = $("#textoPesquisa").val();

	  if (pesquisa === "") {
	       getRelatorioClientesDefault();
	  }
	  else {
	       var relatorio_busca = $.post("src/relatorio-clientes-busca.php", {"search": pesquisa});
	       relatorio_busca.done(function (retorno) {
		    $(".relatorios").html(retorno);
	       });
	  }
     });
    
});