<?php

     require_once '../../src/classes/conexao.class.php';

     $busca = $_REQUEST["search"];
     
     $pagina = $_GET['page'];
     if ($pagina == null) {
	  $pagina = 1;
     }

     /* Abre conexao */
     $conexao = new Conexao();
     $conn = $conexao->getConexao();

     //Monta SQL de busca
     $sql = "SELECT * FROM clientes WHERE ";
     $sql .= "nome LIKE '%$busca%' OR ";
     $sql .= "apelido LIKE '%$busca%' OR ";
     $sql .= "email LIKE '%$busca%' OR ";
     $sql .= "whatsapp LIKE '%$busca%' OR ";
     $sql .= "celular LIKE '%$busca%' OR ";
     $sql .= "cep LIKE '%$busca%' OR ";
     $sql .= "endereco LIKE '%$busca%' OR ";
     $sql .= "numero_endereco LIKE '%$busca%' OR ";
     $sql .= "complemento LIKE '%$busca%' OR ";
     $sql .= "referencia LIKE '%$busca%' OR ";
     $sql .= "uf LIKE '%$busca%' OR ";
     $sql .= "cidade LIKE '%$busca%' OR ";
     $sql .= "bairro LIKE '%$busca%' OR ";
     $sqlLike = substr($sql, 0, -4);

     $retorno = $conn->query($sqlLike);

     //conta o total de itens
     $totalRegistros = $retorno->rowCount();

     /* Se tiverem registros encontrados, mostra a tabela */
     if ($totalRegistros > 0) {
	  //seta a quantidade de itens por página, neste caso, 50 itens
	  $padraoRegistros = 50;

	  //calcula o número de páginas arredondando o resultado para cima
	  $numPaginas = ceil($totalRegistros / $padraoRegistros);

	  //variavel para calcular o início da visualização com base na página atual
	  $inicio = ($padraoRegistros * $pagina) - $padraoRegistros;

	  //seleciona os itens por página
	  //Monta SQL de busca
	  $sql2 = "SELECT * FROM clientes WHERE ";
	  $sql2 .= "nome LIKE '%$busca%' OR ";
	  $sql2 .= "apelido LIKE '%$busca%' OR ";
	  $sql2 .= "email LIKE '%$busca%' OR ";
	  $sql2 .= "whatsapp LIKE '%$busca%' OR ";
	  $sql2 .= "celular LIKE '%$busca%' OR ";
	  $sql2 .= "cep LIKE '%$busca%' OR ";
	  $sql2 .= "endereco LIKE '%$busca%' OR ";
	  $sql2 .= "numero_endereco LIKE '%$busca%' OR ";
	  $sql2 .= "complemento LIKE '%$busca%' OR ";
	  $sql2 .= "referencia LIKE '%$busca%' OR ";
	  $sql2 .= "uf LIKE '%$busca%' OR ";
	  $sql2 .= "cidade LIKE '%$busca%' OR ";
	  $sql2 .= "bairro LIKE '%$busca%' OR ";

	  $sqlLike2 = substr($sql2, 0, -4);
	  $sqlLike2 .= "ORDER BY nome ASC LIMIT $inicio, $padraoRegistros";

	  $retorno = $conn->query($sqlLike2);

	  $tabela = "<table class='tabela-clientes'>
					<thead>
					     <tr>
						  <th>Nome</th>
						  <th>Cidade</th>
						  <th>Estado</th>
						  <th>E-mail</th>
					     </tr>
					</thead>
					<tbody>";

	  $abreModal = "<script>";
	  while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
//			 $tabela .= '<tr class="linha-user" id="' . $linhas[id_cliente] . '">
	       $link = "'index.php?user=$linhas[id_cliente]&page=$pagina&search=$busca'";
	       $tabela .= '<tr class="linha-user" onclick="location.href=' . $link . '">
				   <td>' . $linhas[nome] . '</td>
				   <td>' . $linhas[cidade] . '</td>
				   <td>' . $linhas[bairro] . '</td>
				   <td>' . $linhas[email] . '</td>
			      </tr>';
	  }

	  $tabela .= "</tbody>
			   </table>";

	  #Fecha conexão
	  unset($conn);

	  //Gambiarra de plural
	  if ($totalRegistros == 1) {
	       $textoPlural = "resultado encontrado";
	  } else {
	       $textoPlural = "resultados encontrados";
	  }

	  ?>
	       <script>
		    $(".mensagem").html("<?php echo "$totalRegistros $textoPlural para sua busca: $busca" ?>");
	       </script>
	  <?php
//	  echo '<span id=numeracao-page>Página ' . $pagina . ' de ' . $numPaginas . '</span>';
	  $tabela .= "</tbody></table>";
	  echo "<article id='relatorio-busca'>";
	  echo $tabela;
	  echo "</article>";

     }
//	       Senão tiverem registros encontrados, mostra sem resultados
     else {
	  ?>
	       <script>
		    $(".mensagem").html("Nenhum cliente encontrado para sua busca: <?php echo $busca ?>");
	       </script>
	  <?php
     }
     
     