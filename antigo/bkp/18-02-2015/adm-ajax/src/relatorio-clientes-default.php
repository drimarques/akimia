<?php
     require_once '../../src/classes/conexao.class.php';

     $pagina = $_GET['page'];
     if ($pagina == null) {
	  $pagina = 1;
     }

     /* Abre conexao */
     $conexao = new Conexao();
     $conn = $conexao->getConexao();

     //seleciona todos os itens da tabela
     $retorno = $conn->query("SELECT * FROM clientes ORDER BY nome ASC");

     //conta o total de itens
     $totalRegistros = $retorno->rowCount();

     if ($totalRegistros > 0) {
	  //seta a quantidade de itens por página, neste caso, 50 itens
	  $padraoRegistros = 50;

	  //calcula o número de páginas arredondando o resultado para cima
	  $numPaginas = ceil($totalRegistros / $padraoRegistros);


	  //variavel para calcular o início da visualização com base na página atual
	  $inicio = ($padraoRegistros * $pagina) - $padraoRegistros;


	  //seleciona os itens por página
	  $retorno = $conn->query("SELECT * FROM clientes ORDER BY nome ASC LIMIT $inicio, $padraoRegistros");
	  $tabela = "<table class='tabela-clientes'>
					<thead>
					     <tr>
						  <th>Contagem</th>
						  <th>Nome</th>
						  <th>Cidade</th>
						  <th>Estado</th>
						  <th>E-mail</th>
					     </tr>
					</thead>
					<tbody>";


	  $contador = 1;
	  while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
//			 $tabela .= '<tr class="linha-user" id="' . $linhas[id_cliente] . '">
	       $link = "'index.php?user=$linhas[id_cliente]&page=$pagina'";
//	       $tabela .= '<tr class="linha-user">
	       $tabela .= '<tr class="linha-user" id="'.$linhas[id_cliente].'">
		    		   <td>' . $contador . '</td>
				   <td>' . $linhas[nome] . '</td>
				   <td>' . $linhas[cidade] . '</td>
				   <td>' . $linhas[bairro] . '</td>
				   <td>' . $linhas[email] . '</td>
			      </tr>';
	       $contador++;
	  }

	  #Fecha conexão
	  unset($conn);

	  #Apresentando os dados em tela
	  $tabela .= "</tbody></table>";
	  echo "<article id='relatorio-default'>";
	  echo $tabela;
	  echo "</article>";

//	  $paginaSubtrair = $pagina - 1;
//	  $linkSubtrair = "location.href='index.php?page=$paginaSubtrair&search=$busca'";
//
//	  $paginaSomar = $pagina + 1;
//	  $linkSomar = "location.href='index.php?page=$paginaSomar&search=$busca'";
//
//	  /* Se a última página chegar */
//	  $disabledProxima = "";
//	  if ($paginaSomar > $numPaginas) {
//	       $disabledProxima = "disabled";
//	  }
//
//	  /* Se a última primeira chegar */
//	  $disabledAnterior = "";
//	  if ($paginaSubtrair == '0') {
//	       $disabledAnterior = "disabled";
//	  }
//
//	  $paginacao = '<div id="paginacao">
//			      <form method="post">
//				   <input type="hidden" name="pagina_anterior" id="pagina_anterior" value="'.$paginaSubtrair.'">
//					<input type="hidden" name="pagina_anterior" id="pagina_posterior" value="'.$paginaSomar.'">
//					<input type="button" class="btn anterior" value="Anterior">
//					<input type="button" class="btn proxima" value="Próxima">
//				</form>
//			</div>';
	  //Exibe a paginação
//	  echo $paginacao;
	  ?>
	  <script>
	       $(".mensagem").html("Exibindo <?php echo $totalRegistros ?> clientes cadastrados");
	  </script>
	  <?php
     } else {
	  ?>
	  <script>
	       $(".mensagem").html("Não há clientes cadastrados");
	  </script>
	  <?php
     }