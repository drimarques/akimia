function selecionaMenuCadastro(device) {
     if (device === "mobile") {
	  $('#menu-como-encomendar-mobile').removeClass('active');
	  $('#menu-a-akimia-mobile').removeClass('active');
	  $('#menu-fale-conosco-mobile').removeClass('active');
	  $('#menu-cadastro-mobile').addClass('active');

	  $("#cadastro").css({"display": "block"});
	  $("#como-encomendar").css({"display": "none"});
	  $("#a-akimia").css({"display": "none"});
	  $("#fale-conosco").css({"display": "none"});
     }

     else if (device === "desktop") {
	  $('#menu-como-encomendar-desktop').removeClass('active');
	  $('#menu-a-akimia-desktop').removeClass('active');
	  $('#menu-fale-conosco-desktop').removeClass('active');
	  $('#menu-cadastro-desktop').addClass('active');

//	  $("#como-encomendar").fadeOut("fast");
//	  $("#a-akimia").fadeOut("fast");
//	  $("#fale-conosco").fadeOut("fast");
//	  $("#cadastro").fadeIn(1000);

	  $("#como-encomendar").css({"display": "none"});
	  $("#a-akimia").css({"display": "none"});
	  $("#fale-conosco").css({"display": "none"});
	  $("#cadastro").fadeIn(1000);
     }

}

function selecionaMenuComoEncomendar(device) {
     if (device === "mobile") {
	  $('#menu-cadastro-mobile').removeClass('active');
	  $('#menu-a-akimia-mobile').removeClass('active');
	  $('#menu-fale-conosco-mobile').removeClass('active');
	  $('#menu-como-encomendar-mobile').addClass('active');

	  $("#como-encomendar").css({"display": "block"});
	  $("#cadastro").css({"display": "none"});
	  $("#a-akimia").css({"display": "none"});
	  $("#fale-conosco").css({"display": "none"});
     }
     else if (device === "desktop") {
	  $('#menu-cadastro-desktop').removeClass('active');
	  $('#menu-a-akimia-desktop').removeClass('active');
	  $('#menu-fale-conosco-desktop').removeClass('active');
	  $('#menu-como-encomendar-desktop').addClass('active');

//	  $("#cadastro").fadeOut("fast");
//	  $("#a-akimia").fadeOut("fast");
//	  $("#fale-conosco").fadeOut("fast");
//	  $("#como-encomendar").fadeIn(1000);

	  $("#cadastro").css({"display": "none"});
	  $("#a-akimia").css({"display": "none"});
	  $("#fale-conosco").css({"display": "none"});
	  $("#como-encomendar").fadeIn(1000);
     }

}

function selecionaMenuAAkimia(device) {
     if (device === "mobile") {
	  $('#menu-como-encomendar-mobile').removeClass('active');
	  $('#menu-cadastro-mobile').removeClass('active');
	  $('#menu-fale-conosco-mobile').removeClass('active');
	  $('#menu-a-akimia-mobile').addClass('active');

	  $("#a-akimia").css({"display": "block"});
	  $("#cadastro").css({"display": "none"});
	  $("#como-encomendar").css({"display": "none"});
	  $("#fale-conosco").css({"display": "none"});
     }
     else if (device === "desktop") {
	  $('#menu-como-encomendar-desktop').removeClass('active');
	  $('#menu-cadastro-desktop').removeClass('active');
	  $('#menu-fale-conosco-desktop').removeClass('active');
	  $('#menu-a-akimia-desktop').addClass('active');

//	  $("#como-encomendar").fadeOut("fast");
//	  $("#cadastro").fadeOut("fast");
//	  $("#fale-conosco").fadeOut("fast");
//	  $("#a-akimia").fadeIn(1000);

	  $("#como-encomendar").css({"display": "none"});
	  $("#cadastro").css({"display": "none"});
	  $("#fale-conosco").css({"display": "none"});
	  $("#a-akimia").fadeIn(1000);
     }
}

function selecionaMenuFaleConosco(device) {
     if (device === "mobile") {
	  $('#menu-como-encomendar-mobile').removeClass('active');
	  $('#menu-a-akimia-mobile').removeClass('active');
	  $('#menu-cadastro-mobile').removeClass('active');
	  $('#menu-fale-conosco-mobile').addClass('active');

	  $("#fale-conosco").css({"display": "block"});
	  $("#cadastro").css({"display": "none"});
	  $("#a-akimia").css({"display": "none"});
	  $("#como-encomendar").css({"display": "none"});
     }
     else if (device === "desktop") {
	  $('#menu-como-encomendar-desktop').removeClass('active');
	  $('#menu-a-akimia-desktop').removeClass('active');
	  $('#menu-cadastro-desktop').removeClass('active');
	  $('#menu-fale-conosco-desktop').addClass('active');

//	  $("#como-encomendar").fadeOut("fast");
//	  $("#cadastro").fadeOut("fast");
//	  $("#a-akimia").fadeOut("fast");
//	  $("#fale-conosco").fadeIn(1000);

	  $("#como-encomendar").css({"display": "none"});
	  $("#cadastro").css({"display": "none"});
	  $("#a-akimia").css({"display": "none"});
	  $("#fale-conosco").fadeIn(1000);
     }

}

function cachCamposFormCadastro(nome, apelido, email, whassapp, telefone, celular, CEP, endereco, numEndereco, complemento, referencia, UF, cidade, bairro) {
     document.formCadastro.nomeCadastro.value = nome;
     document.formCadastro.apelidoCadastro.value = apelido;
     document.formCadastro.emailCadastro.value = email;
     document.formCadastro.whatsAppCadastro.value = whassapp;
     document.formCadastro.telCadastro.value = telefone;
     document.formCadastro.celCadastro.value = celular;
     document.formCadastro.cepCadastro.value = CEP;
     document.formCadastro.enderecoCadastro.value = endereco;
     document.formCadastro.numEnderecoCadastro.value = numEndereco;
     document.formCadastro.complementoCadastro.value = complemento;
     document.formCadastro.referenciaCadastro.value = referencia;
     document.formCadastro.estadoCadastro.value = UF;
     document.formCadastro.cidadeCadastro.value = cidade;
     document.formCadastro.bairroCadastro.value = bairro;
}


/*Função que mantem o que foi digitado no form de contato, caso ocorram erros :P*/
function cachCamposFormContato(nome, email, assunto, mensagem) {
     document.formContato.nomeContato.value = nome;
     document.formContato.emailContato.value = email;
     document.formContato.assuntoContato.value = assunto;
     document.formContato.mensagemContato.value = mensagem;
}

$(document).ready(function () {
//     Quando clica no hamburger
     $('.navbar-toggle').click(function () {
	  $('.navbar-toggle').css({"background-image": "none", "background-color": "#e31b63"});
     });

     $('#cepCadastro').mask('99999-999');
     $('#telCadastro').mask('(99) 9999-9999');
     $('#celCadastro').mask('(99) 99999-9999');
     $('#whatsAppCadastro').mask('(+55) (99) 99999-9999');


     if ($(document).width() >= 1024) {
//     Scrol suave
	  $(".central").niceScroll();
     }

     //Todo a não dar clique e ficar sublinhado
     $('a').css({"text-decoration": "none"});


     $('.chama-fale-conosco').click(function () {
	  $('.central').animate({
	       scrollTop: $("#formContato").offset().top
	  }, 1500);

	  $("#nomeContato").focus();
     });
     
});



