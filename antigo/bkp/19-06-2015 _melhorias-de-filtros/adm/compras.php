<!--
<?php
     require_once '../src/classes/conexao.class.php';
     require_once '../src/classes/compra.class.php';
     require_once '../src/dao/comprasDAO.php';
?>
-->
<!DOCTYPE html>
<html lang="pt-br">
     <head>
	  <title>Compras - Akimia</title>
	  <meta charset="UTF-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	  <meta name="author" content="Adriano Marques">
	  <link rel="shortcut icon" href="../img/Logo.Akimia.transp.750x750.png">
	  <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	  <link rel="stylesheet" href="../bootstrap/bootstrap.min.css">
	  <link rel="stylesheet" href="../bootstrap/bootstrap-theme.min.css">
	  <link href="css/compras.css" rel="stylesheet" type="text/css">
	  <script src="../jquery/jquery-2.1.3.min.js"></script>	  

	  <!--Ajax de atualização automatica do relatorio-->
	  <script src="js/ajax.js"  type="text/javascript"></script>
	  <script type="text/javascript">

	       function notificacaoErro(texto) {
		    document.getElementById("notificacao_lista_compras").innerHTML = "Marque pelo menos 1 item para " + texto + ".";
		    $("#notificacao_lista_compras").fadeIn("fast").delay(1000).fadeOut(200, function () {

		    });
	       }

	       function notificacaoSucesso(texto) {
		    document.getElementById("notificacao_lista_compras").innerHTML = "Itens " + texto + ".";
		    $("#notificacao_lista_compras").fadeIn("fast").delay(1000).fadeOut(200, function () {
			 validaFiltros();
		    });
	       }

	       function temItensMarcados() {
		    var verificador = false;

		    $(".item_selecionado").each(function () {
			 verificador = true;
		    });

		    return verificador;
	       }

	       function limpaListaENotificacao() {
		    $("#notificacao_lista_compras").css({"display": "none"});
		    document.getElementById("lista_compras").innerHTML = "";
	       }

	       function marcaComprado() {
		    if (temItensMarcados()) {
			 $(".item_selecionado").each(function () {
			      var id_item = this.id;
			      var url = "compras/marca-comprado.php?id_item=" + id_item;

			      var comprado;
			      comprado = new montaXMLHTTP();
			      comprado.open("GET", url, true);
			      comprado.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			      comprado.onreadystatechange = function () {
				   if (comprado.readyState === 4) {

				   }
//
			      };
			      comprado.send(null);
			 });

			 notificacaoSucesso("comprados");
		    }
		    else {
			 notificacaoErro("comprar");
		    }

	       }

	       function marcaNaoComprado() {
		    if (temItensMarcados()) {
			 $(".item_selecionado").each(function () {
			      var id_item = this.id;
			      var url = "compras/marca-nao-comprado.php?id_item=" + id_item;

			      var comprado;
			      comprado = new montaXMLHTTP();
			      comprado.open("GET", url, true);
			      comprado.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			      comprado.onreadystatechange = function () {
				   if (comprado.readyState === 4) {

				   }
//
			      };
			      comprado.send(null);
			 });

			 notificacaoSucesso("não comprados");
		    }
		    else {
			 notificacaoErro("não comprar");
		    }
	       }

	       function marcaBuscaComprado(idItem, busca) {
		    var url = "compras/marca-comprado.php?id_item=" + idItem;

		    var compradoBusca;
		    compradoBusca = new montaXMLHTTP();
		    compradoBusca.open("GET", url, true);
		    compradoBusca.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		    compradoBusca.onreadystatechange = function () {
			 if (compradoBusca.readyState === 4) {
			      document.getElementById("item_clicado").innerHTML = compradoBusca.responseText;

			      var sexo = new montaXMLHTTP();
			      var url_sexo = "compras/get-sexo.php?id_item=" + idItem;

			      sexo.open("GET", url_sexo, true);
			      sexo.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			      sexo.onreadystatechange = function () {
				   if (sexo.readyState === 4) {
					animacao();
					listaComprasBusca(busca);
				   }

			      };
			      sexo.send(null);
			 }

		    };
		    compradoBusca.send(null);
	       }

	       function marcaBuscaNaoComprado(idItem, busca) {
		    var url = "compras/marca-nao-comprado.php?id_item=" + idItem;

		    var naoCompradoBusca;
		    naoCompradoBusca = new montaXMLHTTP();
		    naoCompradoBusca.open("GET", url, true);
		    naoCompradoBusca.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		    naoCompradoBusca.onreadystatechange = function () {
			 if (naoCompradoBusca.readyState === 4) {
			      document.getElementById("item_clicado").innerHTML = naoCompradoBusca.responseText;

			      var sexo = new montaXMLHTTP();
			      var url_sexo = "compras/get-sexo.php?id_item=" + idItem;

			      sexo.open("GET", url_sexo, true);
			      sexo.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			      sexo.onreadystatechange = function () {
				   if (sexo.readyState === 4) {
					animacao();
					listaComprasBusca(busca);
				   }

			      };
			      sexo.send(null);
			 }
		    };
		    naoCompradoBusca.send(null);
	       }

	       function marcaItem(item) {
		    $(item).toggleClass('item_selecionado');
	       }

	       function abreModal() {
		    $('#modalAddItem').modal('show');
	       }

	       function addItem() {
		    var marca = $("#marca_cadastro").val();
		    var sexo = $("#sexo_cadastro").val();
		    var tipo = $("#tipo_cadastro").val();
		    var produto = $("#produto_cadastro").val();
		    var modelo = $("#modelo_cadastro").val();
		    var cor = $("#cor_cadastro").val();
		    var tamanho = $("#tamanho_cadastro").val();
		    var quantidade = $("#quantidade_cadastro").val();


		    if (!validaCadastroItem(marca, sexo, tipo, produto, modelo, cor, tamanho, quantidade)) {
			 $.ajax({			      
			      url: "compras/adiciona-item.php",
			      data: { marca: marca, sexo: sexo, tipo: tipo, produto: produto, modelo: modelo, cor: cor, tamanho: tamanho, quantidade: quantidade },
			      type: 'POST',
			      context: $('#resultado'),
			      success: function () { //success: function (retorno) {
				   $("#notificacao_add_item").removeClass('alert-danger');
				   $("#notificacao_add_item").addClass('alert-success');
				   $("#notificacao_add_item").text("Item adicionado com sucesso.");
				   $("#notificacao_add_item").fadeIn("fast").delay(1000).fadeOut(200, function () {
					location.href = "compras.php";
				   });
			      }
			 });
			 
		    }
		    else {
			 $("#notificacao_add_item").removeClass('alert-success');
			 $("#notificacao_add_item").addClass('alert-danger');
			 $("#notificacao_add_item").text("Preencha todos os campos.");
			 $("#notificacao_add_item").fadeIn("fast");
		    }
	       }

	       function validaCadastroItem(marca, sexo, tipo, produto, modelo, cor, tamanho, quantidade) {
		    var teve_erro = false;

		    if (teve_erro === false) {
			 teve_erro = (marca === "") ? true : false;
			 (marca === "") ? $("#marca_cadastro").addClass('borda_erro') : $("#marca_cadastro").removeClass('borda_erro');
		    }

		    if (teve_erro === false) {
			 console.log(sexo);
			 teve_erro = (sexo === null) ? true : false;
			 (sexo === null) ? $("#sexo_cadastro").addClass('borda_erro') : $("#sexo_cadastro").removeClass('borda_erro');
		    }

		    if (teve_erro === false) {
			 teve_erro = (tipo === "") ? true : false;
			 (tipo === "") ? $("#tipo_cadastro").addClass('borda_erro') : $("#tipo_cadastro").removeClass('borda_erro');
		    }

		    if (teve_erro === false) {
			 teve_erro = (produto === "") ? true : false;
			 (produto === "") ? $("#produto_cadastro").addClass('borda_erro') : $("#produto_cadastro").removeClass('borda_erro');
		    }

		    if (teve_erro === false) {
			 teve_erro = (modelo === "") ? true : false;
			 (modelo === "") ? $("#modelo_cadastro").addClass('borda_erro') : $("#modelo_cadastro").removeClass('borda_erro');
		    }

		    if (teve_erro === false) {
			 teve_erro = (cor === "") ? true : false;
			 (cor === "") ? $("#cor_cadastro").addClass('borda_erro') : $("#cor_cadastro").removeClass('borda_erro');
		    }

		    if (teve_erro === false) {
			 teve_erro = (tamanho === "") ? true : false;
			 (tamanho === "") ? $("#tamanho_cadastro").addClass('borda_erro') : $("#tamanho_cadastro").removeClass('borda_erro');
		    }

		    if (teve_erro === false) {
			 teve_erro = (quantidade === "") ? true : false;
			 (quantidade === "") ? $("#quantidade_cadastro").addClass('borda_erro') : $("#quantidade_cadastro").removeClass('borda_erro');
		    }

		    return teve_erro;
	       }

	  </script>

     </head>
     <body>
	  <header>
	       <section id="menu-mobile">
		    <div id="close_menu_mobile">
			 <img src="img/fecha-menu-mobile.png" alt="Menu" id="fecha-menu-mobile">
			 <img src="img/close_menu_mobile.png" alt="Menu" id="hamburguer_close">			
		    </div>

		    <div id="div-filtros">
			 <table id="filtros">
			      <tbody>
				   <tr>				   				   
					<td><div id="btn_n_comprados"><img src="img/unlike.png" alt="Like">Não comprados</div></td>
				   </tr>
				   <tr>				   
					<td><div id="btn_comprados"><img src="img/like.png" alt="Like">Comprados</div></td>
				   </tr>
				   <tr>				   				   
					<td><div class="filtro_sexo" id="btn_masculino"><img src="img/masculino.png" alt="Like">Masculino</div></td>
				   </tr>
				   <tr>				   
					<td><div class="filtro_sexo" id="btn_feminino"><img src="img/feminino.png" alt="Like">Feminino</div></td>
				   </tr>
				   <tr>				   				   
					<td><div class="filtro_sexo" id="btn_unissex"><img src="img/indiferente.png" alt="Like">Unissex</div></td>
				   </tr>			      
				   <?php
					$dao = new comprasDAO();
					$dao->getMarcas();
				   ?>			      			      
			      </tbody>
			 </table>
		    </div>
	       </section>

	       <h1 id="menu-header">
		    <img onclick="location.href = 'compras.php'" src="img/Logo.Akimia.Mini.png" alt="Akimia">
	       </h1>

	       <img src="img/3pontos.png" alt="Menu" id="hamburguer_open">

	       <img src="img/buscar.png" alt="Buscar" id="img_buscar">	       	  
	       <div id="filtro_pesquisa">
		    <form method="post" name="form_busca" id="form_busca" autocomplete="off">
			 <input type="text" placeholder="Buscar" id="txtBusca" name="txtBusca" class="form-control" autofocus="autofocus">
			 <img src="img/close.png" alt="Fechar busca" id="close_busca">
		    </form>
	       </div>

	       <div class="btns_menu">
		    <img src="img/like.png" alt="Marca comprado" onclick="marcaComprado()">
	       </div>

	       <div class="btns_menu">
		    <img src="img/unlike.png" alt="Marca não comprado" onclick="marcaNaoComprado()">
	       </div>

	       <div class="btns_menu">
		    <img src="img/compras.png" alt="Marca comprado">
	       </div>

	       <div class="btns_menu">
		    <img src="img/add_item.png" alt="Adiciona item" onclick="abreModal()">
	       </div>

	  </header>

	  <article id="lista_compras"></article>

	  <div id='notificacao_lista_compras'></div>

	  <div id="modalAddItem" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	       <div class="modal-dialog modal-sm">
		    <div class="modal-content">
			 <div class="modal-header">
			      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      <div class="modal-title" id="myModalLabel"><b>Adicionar novo item</b></div>
			 </div>
			 <div class="modal-body">
			      <form class="form-horizontal" method="post" id="form_cadastro_item" name="form_cadastro_item" autocomplete="off">

				   <div class="form-group">
					<label for="marca_cadastro" class="col-sm-2 control-label">Marca</label>
					<div class="col-sm-10">
					     <input type="text" class="form-control" id="marca_cadastro" name="marca_cadastro" placeholder="Digite a marca" required="required"> 
					</div>
				   </div>
				   <div class="form-group">
					<label for="sexo_cadastro" class="col-sm-2 control-label">Sexo</label>
					<div class="col-sm-10">
					     <select class="form-control" name="sexo_cadastro" id="sexo_cadastro" required="required">
						  <option disabled="disabled" selected="selected">Selecione um sexo:</option>
						  <option value="Masculino">Masculino</option>
						  <option value="Feminino">Feminino</option>
						  <option value="-">Unissex</option>
					     </select>
					</div>
				   </div>
				   <div class="form-group">
					<label for="tipo_cadastro" class="col-sm-2 control-label">Tipo</label>
					<div class="col-sm-10">
					     <input type="text" class="form-control" id="tipo_cadastro" name="tipo_cadastro" placeholder="Digite o tipo" required="required">
					</div>
				   </div>
				   <div class="form-group">
					<label for="produto_cadastro" class="col-sm-2 control-label">Produto</label>
					<div class="col-sm-10">
					     <input type="text" class="form-control" id="produto_cadastro" name="produto_cadastro" placeholder="Digite o nome do produto" required="required">
					</div>
				   </div>
				   <div class="form-group">
					<label for="modelo_cadastro" class="col-sm-2 control-label">Modelo</label>
					<div class="col-sm-10">
					     <input type="text" class="form-control" id="modelo_cadastro" name="modelo_cadastro" placeholder="Digite o modelo do produto" required="required">
					</div>
				   </div>
				   <div class="form-group">
					<label for="cor_cadastro" class="col-sm-2 control-label">Cor</label>
					<div class="col-sm-10">
					     <input type="text" class="form-control" id="cor_cadastro" name="cor_cadastro" placeholder="Digite a cor do produto" required="required">
					</div>
				   </div>
				   <div class="form-group">
					<label for="tamanho_cadastro" class="col-sm-2 control-label">Tamanho</label>
					<div class="col-sm-10">
					     <input type="text" class="form-control" id="tamanho_cadastro" name="tamanho_cadastro" placeholder="Digite o tamanho" required="required">
					</div>
				   </div>
				   <div class="form-group">
					<label for="quantidade_cadastro" class="col-sm-2 control-label">Quantidade</label>
					<div class="col-sm-10">
					     <input type="number" class="form-control" id="quantidade_cadastro" name="quantidade_cadastro" min="1" placeholder="Digite a quantidade" required="required">
					</div>
				   </div>

				   <!--alertas-->
				   <div class="alert" id="notificacao_add_item" role="alert"></div>
				   
				   <div id="resultado" role="alert"></div>
			      </form>
			 </div>
			 <div class="modal-footer">			      
			      <button type="button" class="btn btn-default" onclick="addItem()">Adicionar</button>
			 </div>
		    </div>
	       </div>
	  </div>


	  <script src="../jquery/jquery-2.1.3.min.js" type="text/javascript"></script>
	  <script src="../bootstrap/bootstrap.min.js" type="text/javascript"></script>
	  <!--Biblioteca do scrol suave, juntamente com jQuery-->
	  <script src="../jquery/jquery.nicescroll.min.js"></script> 
	  <script src="js/akimia-adm.js" type="text/javascript"></script>
	  <script type="text/javascript">

	       function listaComprasBusca(busca) {
		    if (busca !== "") {
			 limpaListaENotificacao();
			 $("#filtro_comprados").fadeOut("fast");
			 $("#filtro_sexo").fadeOut("fast");

			 var url = "compras/busca.php?filtro_busca=" + busca;

			 var relatorioBusca;
			 relatorioBusca = new montaXMLHTTP();
			 relatorioBusca.open("GET", url, true);
			 relatorioBusca.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			 relatorioBusca.onreadystatechange = function () {
			      if (relatorioBusca.readyState === 4) {
				   document.getElementById("lista_compras").innerHTML = relatorioBusca.responseText;
				   if (relatorioBusca.responseText === "") {
					document.getElementById("notificacao_lista_compras").innerHTML = "Sem resultados encontrados para: \"" + busca + '\"';
					$("#notificacao_lista_compras").fadeIn("fast");
				   }
			      }
			 };
			 relatorioBusca.send(null);
		    }
	       }

	       function verificaFiltroMarca() {
		    var verificador = false;

		    $(".marca_selecionada").each(function () {
			 verificador = true;
		    });

		    return verificador;
	       }

	       function validaFiltros() {
		    limpaListaENotificacao();

		    var n_comprado = $("#btn_n_comprados").hasClass('filtro_selecionado').toString();
		    var comprado = $("#btn_comprados").hasClass('filtro_selecionado').toString();

		    var masculino = $("#btn_masculino").hasClass('filtro_selecionado').toString();
		    var feminino = $("#btn_feminino").hasClass('filtro_selecionado').toString();
		    var unissex = $("#btn_unissex").hasClass('filtro_selecionado').toString();

		    if (verificaFiltroMarca()) {
			 $(".marca_selecionada").each(function () {
			      var marca = this.innerHTML;
			      listaComprasVariasMarcas(n_comprado, comprado, masculino, feminino, unissex, marca);
			 });
		    }
		    else {
			 listaCompras(n_comprado, comprado, masculino, feminino, unissex, "null");
		    }
	       }

	       function listaCompras(n_comprado, comprado, masculino, feminino, unissex, marca) {
		    var url = "compras/gera-relatorio.php?n_comprado=" + n_comprado + "&comprado=" + comprado + "&masculino=" + masculino + "&feminino=" + feminino + "&unissex=" + unissex + "&marca=" + marca;

		    var relatorio;
		    relatorio = new montaXMLHTTP();
		    relatorio.open("GET", url, true);
		    relatorio.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		    relatorio.onreadystatechange = function () {
			 if (relatorio.readyState === 4) {
			      if (relatorio.responseText === "") {
				   document.getElementById("notificacao_lista_compras").innerHTML = "Não encontramos nenhum produto.";
				   $("#notificacao_lista_compras").fadeIn("fast");

			      }
			      else if (relatorio.responseText !== "") {
				   document.getElementById("lista_compras").innerHTML = relatorio.responseText;
			      }

			 }
		    };

		    relatorio.send(null);
	       }

	       function listaComprasVariasMarcas(n_comprado, comprado, masculino, feminino, unissex, marca) {
		    var url = "compras/gera-relatorio.php?n_comprado=" + n_comprado + "&comprado=" + comprado + "&masculino=" + masculino + "&feminino=" + feminino + "&unissex=" + unissex + "&marca=" + marca;					

		    var relatorio;
		    relatorio = new montaXMLHTTP();
		    relatorio.open("GET", url, true);
		    relatorio.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		    relatorio.onreadystatechange = function () {
			 if (relatorio.readyState === 4) {
			      if (relatorio.responseText === "") {
			      }
			      else if (relatorio.responseText !== "") {
				   document.getElementById("lista_compras").innerHTML += relatorio.responseText;
			      }

			 }
		    };

		    relatorio.send(null);
	       }

	  </script>

     </body>
</html>