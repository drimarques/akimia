$(document).ready(function () {
//     Scrol suave     
     //$("html").niceScroll();
     
     $("#hamburguer_open").click(function () {
	  	  
	  $("#notificacao_lista_compras").fadeOut("fast");
	  
	  $("#menu-mobile").css({"display": "block"});
	  $("#menu-mobile").animate({
	       width: "88%"
	  },
	  500,
	     function () {

	     });
     });

     $("#fecha-menu-mobile").click(function () {
	  $("#menu-mobile").animate({
	       width: "0"
	  },
	  500,
	     function () {
		  $("#hamburguer_open").css({"display": "block"});
		  $("#menu-mobile").css({"display": "none"});		  
	     });
     });
     
     $("#hamburguer_close").click(function () {
	  $("#menu-mobile").animate({
	       width: "0"
	  },
	  500,
	     function () {
		  $("#hamburguer_open").css({"display": "block"});
		  $("#menu-mobile").css({"display": "none"});
		  validaFiltros();
	     });
     });


     $("#img_buscar").click(function () {
	  $("#img_buscar").css({"display": "none"});
	  $("#hamburguer_open").css({"display": "none"});
	  $(".btns_menu").css({"display": "none"});
	  

	  $("#filtro_pesquisa").css({"display": "block"});	  
	  $("#filtro_pesquisa").animate({
	       width: "79%"
	  }, 500,
	     function () {
		  $("#close_busca").css({"display": "block"});
		  $("#txtBusca").focus();
	     });
     });

     $("#close_busca").click(function () {
	  $("#close_busca").css({"display": "none"});	  	  
	  
	  $("#filtro_pesquisa").animate({
	       width: "0"
	  }, 500,
	     function () {
		  $("#filtro_pesquisa").css({"display": "none"});
		  $("#img_buscar").css({"display": "block"});
		  $("#hamburguer_open").css({"display": "block"});
		  $(".btns_menu").css({"display": "block"});
	     });
     });

     $("#txtBusca").keyup(function () {
	  localStorage.clear();

	  var busca = document.getElementById("txtBusca");

	  if (busca.value.toString() !== "") {
	       listaComprasBusca(busca.value.toString());
	  }
	  else {
	       limpaListaENotificacao();	       
	       document.getElementById("notificacao_lista_compras").innerHTML = "Digite ou selecione um filtro, vamos começar :D";
	       $("#notificacao_lista_compras").fadeIn("fast");
	  }

     });

     $("#txtBusca").focusin(function () {
//	  limpaListaENotificacao();
//	  document.getElementById("notificacao_lista_compras").innerHTML = "Digite ou selecione um filtro, vamos começar :D";
//	  $("#notificacao_lista_compras").fadeIn("fast");
     });

     //Botoes comprados n comprados comprados parc
     $("#btn_n_comprados").click(function () {
	  $("#btn_n_comprados").toggleClass('filtro_selecionado');
	  $(".filtro_sexo").fadeIn();
	  $(".filtro_marca").fadeIn();

	  if (($("#btn_n_comprados").hasClass('filtro_selecionado').toString()) === "false" && ($("#btn_comprados").hasClass('filtro_selecionado').toString() === "false")) {
	       $(".filtro_marca").fadeOut();
	       $(".filtro_sexo").fadeOut();	       
	  }	  	  
     });

     $("#btn_comprados").click(function () {
	  $("#btn_comprados").toggleClass('filtro_selecionado');
	  $(".filtro_sexo").fadeIn();
	  $(".filtro_marca").fadeIn();

	  if (($("#btn_n_comprados").hasClass('filtro_selecionado').toString()) === "false" && ($("#btn_comprados").hasClass('filtro_selecionado').toString() === "false")) {
	       $(".filtro_marca").fadeOut();
	       $(".filtro_sexo").fadeOut();	      
	  }

     });

     //Botoes de sexo
     $("#btn_masculino").click(function () {
	  $("#btn_masculino").toggleClass('filtro_selecionado');
     });

     $("#btn_feminino").click(function () {
	  $("#btn_feminino").toggleClass('filtro_selecionado');
     });

     $("#btn_unissex").click(function () {
	  $("#btn_unissex").toggleClass('filtro_selecionado');
     });

     $(".filtro_marca").click(function () {
	  $(this).toggleClass('marca_selecionada');	  
     });


//*************************************************Akimia ADM
     $("#settings").click(function () {
	  $("#settings_expand").fadeToggle("fast");
     });


     //Fazendo abrir a caixa de selecao de arquivo
     $(".link_importar").click(function () {
	  var botao = document.getElementById("arquivo_excel");
	  botao.click();
     });


     //Fazendo auto submit apos selecionar um arquivo no inputtype file
     document.getElementById("arquivo_excel").onchange = function () {
	  var botao = document.getElementById("btnEnviarExcel");
	  botao.click();
     };

     
});