<?php

     require_once '../../src/classes/conexao.class.php';
     require_once '../../src/classes/compra.class.php';
     require_once '../../src/dao/comprasDAO.php';

     $dao = new comprasDAO();

     $n_comprado = $_GET["n_comprado"];
     $comprado = $_GET["comprado"];
     $masculino = $_GET["masculino"];
     $feminino = $_GET["feminino"];
     $unissex = $_GET["unissex"];
     $filtroMarca = $_GET["marca"];

     $query = "SELECT * FROM lista_compras WHERE";
     $marcas = "SELECT distinct marca FROM lista_compras WHERE";

     $filtraOsDois = false;

     //Se nenhum tiver filtrando
     if ($n_comprado == "false" && $comprado == "false") {
	  return "";
     }

     //Se tiver filtrando os dois
     else if ($n_comprado == "true" && $comprado == "true") {
	  $filtraOsDois = true;

	  //3 sexos selecionados
	  if (($masculino == "true") && ($feminino == "true") && ($unissex == "true")) {
	       $query .= " (sexo = 'Masculino' OR sexo = 'Feminino' OR sexo = '-')";
	       $marcas .= " (sexo = 'Masculino' OR sexo = 'Feminino' OR sexo = '-')";
	  } else {
	       if ($masculino == "true") {
		    $query .= " (sexo = 'Masculino'";
		    $marcas .= " (sexo = 'Masculino'";

		    if ($feminino == "true") {
			 $query .= " OR sexo = 'Feminino'";
			 $marcas .= " OR sexo = 'Feminino'";
		    }

		    if ($unissex == "true") {
			 $query .= " OR sexo = '-'";
			 $marcas .= " OR sexo = '-'";
		    }

		    $query .= ")";
		    $marcas .= ")";
	       } else if ($feminino == "true") {
		    $query .= " (sexo = 'Feminino'";
		    $marcas .= " (sexo = 'Feminino'";

		    if ($unissex == "true") {
			 $query .= " OR sexo = '-'";
			 $marcas .= " OR sexo = '-'";
		    }

		    $query .= ")";
		    $marcas .= ")";
	       } else if ($unissex == "true") {
		    $query .= " (sexo = '-')";
		    $marcas .= " (sexo = '-')";
	       }
	  }
     }

     //SE FOR UM FILTRO NORMAL
     else {	  
	  if ($n_comprado == "true") {
	       $query .= " (comprado = 0";
	       $marcas .= " (comprado = 0";

	       if ($comprado == "true") {
		    $query .= " OR comprado = 1)";
		    $marcas .= " OR comprado = 1)";
	       } else {
		    $query .= ")";
		    $marcas .= ")";
	       }
	  } else {
	       $query .= " (comprado = 1)";
	       $marcas .= " (comprado = 1)";
	  }


	  //3 sexos selecionados
	  if (($masculino == "true") && ($feminino == "true") && ($unissex == "true")) {
	       $query .= " AND (sexo = 'Masculino' OR sexo = 'Feminino' OR sexo = '-')";
	       $marcas .= " AND (sexo = 'Masculino' OR sexo = 'Feminino' OR sexo = '-')";
	  } else {
	       if ($masculino == "true") {
		    $query .= " AND (sexo = 'Masculino'";
		    $marcas .= " AND (sexo = 'Masculino'";

		    if ($feminino == "true") {
			 $query .= " OR sexo = 'Feminino'";
			 $marcas .= " OR sexo = 'Feminino'";
		    }

		    if ($unissex == "true") {
			 $query .= " OR sexo = '-'";
			 $marcas .= " OR sexo = '-'";
		    }

		    $query .= ")";
		    $marcas .= ")";
	       } else if ($feminino == "true") {
		    $query .= " AND (sexo = 'Feminino'";
		    $marcas .= " AND (sexo = 'Feminino'";

		    if ($unissex == "true") {
			 $query .= " OR sexo = '-'";
			 $marcas .= " OR sexo = '-'";
		    }

		    $query .= ")";
		    $marcas .= ")";
	       } else if ($unissex == "true") {
		    $query .= " AND (sexo = '-')";
		    $marcas .= " AND (sexo = '-')";
	       }
	  }
     }


     if ($filtroMarca != "null") {
	  if ($filtraOsDois) {
	       
	       $temSexo = strripos($query, "sexo");
	       if ($temSexo == true) {
		    $query .= " AND marca = '$filtroMarca'";
	       } 
	       else {
		    $query .= " marca = '$filtroMarca'";
	       }
	       	       
	       $dao->geraRelatorioPorMarcaSeparacao($filtroMarca, $query);	       
	  } 
	  else {	       	       
	       $query .= " AND marca = '$filtroMarca'";
	       $dao->geraRelatorioPorMarca($filtroMarca, $query);	       
	       
	  }
	  
     } 
     
     else if ($filtraOsDois == true) {
	  $dao->geraRelatorioSeparacao($marcas, $query);
     } 
     
     else {
	  $marcas .= " ORDER BY marca ASC, COMPRADO ASC";
	  $dao->geraRelatorio($marcas, $query);
     }               


