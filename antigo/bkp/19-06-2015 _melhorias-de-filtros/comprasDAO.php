<?php

     class comprasDAO {

	  function getImagemComprado($comprado, $id_item) {
	       if ($comprado == '1') {
		    return "<img src='img/like.png' alt='Marcar como não comprado' onclick=marcaNaoComprado(\"$id_item\")>";
	       } else {
		    return "<img src='img/unlike.png' alt='Marcar como comprado' onclick=marcaComprado(\"$id_item\")>";
	       }
	  }

	  function getImagemCompradoBusca($comprado, $id_item, $busca) {
	       if ($comprado == '1') {
		    return "<img src='img/like.png' alt='Marcar como não comprado' onclick=\"marcaBuscaNaoComprado('$id_item', '$busca')\">";
	       } else {
		    return "<img src='img/unlike.png' alt='Marcar como não comprado' onclick=\"marcaBuscaComprado('$id_item', '$busca')\">";
	       }
	  }

	  function cadastraParametros(Compra $compra) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();

	       $conn->query("DELETE FROM parametros");
	       $conn->query("INSERT INTO parametros (cotacaoDolar, impostoUSA, IOF) VALUES ('{$compra->Dolar}', '{$compra->ImpostoUSA}', '{$compra->IOF}')");

	       unset($conn);
	  }

	  function cadastraListaCompra(Compra $compra) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();

	       $conn->query("INSERT INTO lista_compras (tipo, sexo, produto, marca, modelo, cor, quantidade, tamanho)
	       VALUES ('{$compra->Tipo}', '{$compra->Sexo}', '{$compra->Produto}', '{$compra->Marca}', '{$compra->Modelo}', '{$compra->Cor}', '{$compra->Quantidade}', '{$compra->Tamanho}')");

	       unset($conn);
	  }

	  function limpaListaCompras() {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();

	       $conn->query("DELETE FROM lista_compras");
	       unset($conn);
	  }

	  function listaCompras() {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $retorno = $conn->query("SELECT * FROM lista_compras ORDER BY marca, produto asc");

	       $tabela = "<table class='table-responsive table-hover tabelas2' id='table-lista-compras'>";
//	       $tabela .= '<thead>
//			      <tr>
//				 <th>Tipo</th>
//				 <th>Sexo</th>
//				 <th>Produto</th>
//				 <th>Marca</th>
//				 <th>Modelo</th>
//				 <th>Cor</th>
//				 <th>Quantidade</th>
//				 <th>Tamanho</th>
//				 <th>Valor USA</th>
//				 <th>Valor BR</th>
//				 <th>Markup</th>
//				 <th>Venda BR</th>
//				 <th>Lucro BR</th>			      
//			      </tr>
//			   </thead>';

	       $tabela .= '<thead>
			      <tr>
				   <th>Marca</th>
				   <th>Sexo</th>
				   <th>Tipo</th>				   
				   <th>Produto</th>				 
				   <th>Modelo</th>
				   <th>Cor</th>
				   <th>Tamanho</th>
				   <th>Quantidade</th>				   			      
			      </tr>
			   </thead>';

	       while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
//		    $tabela .= '<tr>
//				   <td>' . $linhas[tipo] . '</td>
//				   <td>' . $linhas[sexo] . '</td>
//				   <td>' . $linhas[produto] . '</td>
//				   <td>' . $linhas[marca] . '</td>
//				   <td>' . $linhas[modelo] . '</td>
//				   <td>' . $linhas[cor] . '</td>
//				   <td>' . $linhas[quantidade] . '</td>
//				   <td>' . $linhas[tamanho] . '</td>
//				   <td>$ '.number_format($linhas[valorUSA], 2, ',', '.').'</td>					
//				   <td>R$ '.number_format($linhas[valorBR], 2, ',', '.').'</td>
//				   <td>'.number_format($linhas[markup], 2, ',', '.').' %</td>
//				   <td>R$ '.number_format($linhas[vendaBR], 2, ',', '.').'</td>
//				   <td>R$ '.number_format($linhas[lucroBR], 2, ',', '.').'</td>
//			      </tr>';

		    $tabela .= '<tr>
				   <td>' . $linhas[marca] . '</td>
				   <td>' . $linhas[sexo] . '</td>
				   <td>' . $linhas[tipo] . '</td>
				   <td>' . $linhas[produto] . '</td>				   
				   <td>' . $linhas[modelo] . '</td>
				   <td>' . $linhas[cor] . '</td>				   
				   <td>' . $linhas[tamanho] . '</td>
				   <td>' . $linhas[quantidade] . '</td>
			        </tr>';
	       }

	       $tabela .= "</table>";
	       unset($conn);

	       echo $tabela;
	  }

	  function listaParametros() {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $retorno = $conn->query("SELECT * FROM parametros");

	       $tabela = '<table class="table-responsive table-hover tabelas2" id="table-parametros">';
	       $tabela .= "<thead>
			      <tr>
				   <th>Cotação Dólar</th>
				   <th>Imposto USA</th>
				   <th>IOF</th>		      
			      </tr>
			 </thead>
			 <tbody>";

	       while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
		    $tabela .= '<tr>				   
				   <td>R$ ' . number_format($linhas[cotacaoDolar], 2, ',', '.') . '</td>
				   <td>' . number_format($linhas[impostoUSA], 2, ',', '.') . ' %</td>
				   <td>' . number_format($linhas[IOF], 2, ',', '.') . ' %</td>
			      </tr>';
	       }

	       $tabela .= "</tbody>
			 </table>";

	       unset($conn);

	       echo $tabela;
	  }
	  	  
	  function geraRelatorioSeparacao($query_marcas, $query){
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       
	       //Fazendo para não comprados
	       $temSexo = strripos($query_marcas, "sexo");
	       if($temSexo == true){
		    $query_marcas2 = "$query_marcas AND comprado = 0 ORDER BY marca ASC";
	       }
	       else{
		    $query_marcas2 = "$query_marcas comprado = 0 ORDER BY marca ASC";
	       }
	       	       	       
	       $marcas = $conn->query($query_marcas2);	       	       
	       //Verifica se existe resultados
	       if ($marcas->rowCount() > 0) {
		    $lista = "";

		    //Percorrendo as marcas e montando as tabelas
		    while ($marca = $marcas->fetch(PDO::FETCH_ASSOC)) {			 
			 $lista.= "<div class='marca'>$marca[marca]</div>";
			 
			 //Remove a marca e adiciona uma nova marca
			 $marcaAnterior = $marca[marca];
			 $queryNova = str_replace(" AND marca = '$marcaAnterior'", " ", $query);
			 
			 $temSexo = strripos($queryNova, "sexo");
			 if($temSexo == true){
			      $queryNova2 = "$queryNova AND marca = '$marca[marca]'";
			 }
			 else{
			      $queryNova2 = "$queryNova marca = '$marca[marca]'";	
			 }			 			 
			 			 			 	       
			 $queryNova3 = "$queryNova2 AND COMPRADO = 0";			 			 
			 $retorno = $conn->query($queryNova3);

			 while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
			      			      
			      $lista .= "<div class='produto_lista' id='$linhas[id_item]' onclick=\"marcaItem(this)\">
					     <div class='titulo_produto'>$linhas[produto] $linhas[modelo] - $linhas[cor]</div>
					     <span class='descricao_produto'>Tamanho: $linhas[tamanho] - Quantidade: $linhas[quantidade]</span>
					</div>";
			 }
		    }

		    echo "<div id='separacao-nao-comprados' class='separacao'>Não comprados</div>";
		    echo $lista;

		    unset($conn);
	       } 
	       else {
		    echo "";
	       }
	       
	       
	       
	       //Fazendo para comprados
	       $temSexo2 = strripos($query_marcas, "sexo");
	       	       	       
	       if($temSexo2 == true){
		    $query_marcas2 = "$query_marcas AND comprado = 1 ORDER BY marca ASC";
	       }
	       else{
		    $query_marcas2 = "$query_marcas comprado = 1 ORDER BY marca ASC";
	       }
	       	       	       
	       $conn2 = $conexao->getConexao();
	       $marcas2 = $conn2->query($query_marcas2);
	       	       	      
	       //Verifica se existe resultados
	       if ($marcas2->rowCount() > 0) {
		    $lista = "";
		    
		    //Percorrendo as marcas e montando as tabelas
		    while ($marca = $marcas2->fetch(PDO::FETCH_ASSOC)) {			 
			 $lista.= "<div class='marca'>$marca[marca]</div>";
			 
			 //Remove a marca e adiciona uma nova marca
			 $marcaAnterior = $marca[marca];
			 $queryNova = str_replace(" AND marca = '$marcaAnterior'", " ", $query);
			 
			 $temSexo2 = strripos($queryNova, "sexo");
			 if($temSexo2 == true){
			      $queryNova2 = "$queryNova AND marca = '$marca[marca]'";
			 }
			 else{
			      $queryNova2 = "$queryNova marca = '$marca[marca]'";	
			 }			 			 
			 			 			 	       
			 $queryNova3 = "$queryNova2 AND COMPRADO = 1";			 			 
			 $retorno = $conn2->query($queryNova3);

			 while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
			      			      
			      $lista .= "<div class='produto_lista' id='$linhas[id_item]' onclick=\"marcaItem(this)\">
					     <div class='titulo_produto'><s>$linhas[produto] $linhas[modelo] - $linhas[cor]</s></div>
					     <span class='descricao_produto'><s>Tamanho: $linhas[tamanho] - Quantidade: $linhas[quantidade]</s></span>
					</div>";
			 }
		    }

		    echo "<div class='separacao'>Comprados</div>";
		    echo $lista;

		    unset($conn);
	       } 
	       else {
		    echo "";
	       }
	  }	    	  
	  
	  function geraRelatorio($query_marcas, $query) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $marcas = $conn->query($query_marcas);

	       //Verifica se existe resultados
	       if ($marcas->rowCount() > 0) {
		    $lista = "";

		    //Percorrendo as marcas e montando as tabelas
		    while ($marca = $marcas->fetch(PDO::FETCH_ASSOC)) {

			 $lista.= "<div class='marca'>$marca[marca]</div>";
//			 $tabela .= "<tr><td colspan='9'><hr></td></tr>";
			 //Remove a marca e adiciona uma nova marca
			 $marcaAnterior = $marca[marca];
			 $queryNova = str_replace(" AND marca = '$marcaAnterior'", " ", $query);
			 $queryNova .= " AND marca = '$marca[marca]'";
			 $retorno = $conn->query($queryNova);

			 while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {

			      $titulo_produto = "";
			      $descricao_produto = "";
			      			      			      
			      //Verifica se está comprado
			      if($linhas['comprado'] == '1'){
				   $titulo_produto = "<div class='titulo_produto'><s>$linhas[produto] $linhas[modelo] - $linhas[cor]</s></div>";
				   $descricao_produto = "<span class='descricao_produto'><s>Tamanho: $linhas[tamanho] - Quantidade: $linhas[quantidade]</s></span>";
			      }
			      else{
				   $titulo_produto = "<div class='titulo_produto'>$linhas[produto] $linhas[modelo] - $linhas[cor]</div>";
				   $descricao_produto = "<span class='descricao_produto'>Tamanho: $linhas[tamanho] - Quantidade: $linhas[quantidade]</span>";
			      }
			      
			      $lista .= "<div class='produto_lista' id='$linhas[id_item]' onclick=\"marcaItem(this)\">
					     $titulo_produto
					     $descricao_produto
					</div>";
			 }
		    }


		    echo $lista;

		    unset($conn);
	       } else {
		    echo "";
	       }
	  }

	  function geraRelatorioPorMarca($marca, $query) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();

	       $lista = "";
	       $lista.= "<div class='marca'>$marca</div>";
	       
	       $retorno = $conn->query($query);
	       if ($retorno->rowCount() > 0) {
		    while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {

			 $titulo_produto = "";
			 $descricao_produto = "";

			 //Verifica se está comprado
			 if($linhas['comprado'] == '1'){
			      $titulo_produto = "<div class='titulo_produto'><s>$linhas[produto] $linhas[modelo] - $linhas[cor]</s></div>";
			      $descricao_produto = "<span class='descricao_produto'><s>Tamanho: $linhas[tamanho] - Quantidade: $linhas[quantidade]</s></span>";
			 }
			 else{
			      $titulo_produto = "<div class='titulo_produto'>$linhas[produto] $linhas[modelo] - $linhas[cor]</div>";
			      $descricao_produto = "<span class='descricao_produto'>Tamanho: $linhas[tamanho] - Quantidade: $linhas[quantidade]</span>";
			 }

			 $lista .= "<div class='produto_lista' id='$linhas[id_item]' onclick=\"marcaItem(this)\">
					$titulo_produto
					$descricao_produto
				   </div>";
		    }
		    
		    
		    echo $lista;
		    
	       } 
	       else {
		    "";
	       }

	       unset($conn);
	  }
	  
	  function geraRelatorioPorMarcaSeparacao($marca, $query){	       
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();	       	       	       	       
	       
	       $lista = "<div class='marca'>$marca</div>";	       
	       
	       $queryNaoComprados = "$query AND comprado = 0";	      	       
	       
	       $retornoNaoComprados = $conn->query($queryNaoComprados);
	       if ($retornoNaoComprados->rowCount() > 0) {
		    while ($linhas = $retornoNaoComprados->fetch(PDO::FETCH_ASSOC)) {
			 $lista .= "<div class='produto_lista' id='$linhas[id_item]' onclick=\"marcaItem(this)\">
					<div class='titulo_produto'>$linhas[produto] $linhas[modelo] - $linhas[cor]</div>
					<span class='descricao_produto'>Tamanho: $linhas[tamanho] - Quantidade: $linhas[quantidade]</span>
				   </div>";
		    }
		    		    		    		    
	       } 
	       else {
		    echo "";
	       }

	       $conn2 = $conexao->getConexao();
	       $queryComprados = "$query AND comprado = 1";	       
	       	       	       
	       $retornoComprados = $conn2->query($queryComprados);
	       if ($retornoComprados->rowCount() > 0) {
		    while ($linhas = $retornoComprados->fetch(PDO::FETCH_ASSOC)) {
			 			
			 $lista .= "<div class='produto_lista' id='$linhas[id_item]' onclick=\"marcaItem(this)\">
					<div class='titulo_produto'><s>$linhas[produto] $linhas[modelo] - $linhas[cor]</s></div>
					<span class='descricao_produto'><s>Tamanho: $linhas[tamanho] - Quantidade: $linhas[quantidade]</s></span>
				   </div>";
		    }
	       } 
	       else {
		    "";
	       }
	       
	       echo $lista;
	       	       
	  }

	  function listaComprasBusca($busca) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();

	       //Monta SQL de busca
	       $sql = "SELECT * FROM lista_compras WHERE ";
	       $sql .= "tipo LIKE '%$busca%' OR ";
	       $sql .= "sexo LIKE '%$busca%' OR ";
	       $sql .= "produto LIKE '%$busca%' OR ";
	       $sql .= "marca LIKE '%$busca%' OR ";
	       $sql .= "modelo LIKE '%$busca%' OR ";
	       $sql .= "cor LIKE '%$busca%' OR ";
	       $sqlLike = substr($sql, 0, -4);

	       $sqlLike2 = "$sqlLike ORDER BY marca ASC";
	       $retorno = $conn->query($sqlLike2);

	       $lista = "";	       
	       $totalRegistros = $retorno->rowCount();
	       /* Se tiverem registros encontrados, mostra a tabela */
	       if ($totalRegistros > 0) {		   
		    while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
			 $titulo_produto = "";
			 $descricao_produto = "";

			 //Verifica se está comprado
			 if($linhas['comprado'] == '1'){
			      $titulo_produto = "<div class='titulo_produto'><s>$linhas[marca]<br>$linhas[produto] $linhas[modelo] - $linhas[cor]</s></div>";
			      $descricao_produto = "<span class='descricao_produto'><s>Tamanho: $linhas[tamanho] - Quantidade: $linhas[quantidade]</s></span>";
			 }
			 else{
			      $titulo_produto = "<div class='titulo_produto'>$linhas[marca]<br>$linhas[produto] $linhas[modelo] - $linhas[cor]</div>";
			      $descricao_produto = "<span class='descricao_produto'>Tamanho: $linhas[tamanho] - Quantidade: $linhas[quantidade]</span>";
			 }

			 $lista .= "<div class='produto_lista' id='$linhas[id_item]' onclick=\"marcaItem(this)\">
					$titulo_produto
					$descricao_produto
				   </div>";
		    }
		    
		    echo $lista;
		    
	       } else {
		    echo "";
	       }
	  }

	  function marcaComprado($id_item) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();

	       $conn->query("UPDATE lista_compras SET comprado = 1 WHERE id_item = '$id_item'");

	       unset($conn);
	  }

	  function marcaNaoComprado($id_item) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();

	       $conn->query("UPDATE lista_compras SET comprado = 0 WHERE id_item = '$id_item'");

	       unset($conn);
	  }

	  function getDadosCompra($id_item) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();

	       $retorno = $conn->query("SELECT * FROM lista_compras WHERE id_item = '$id_item'");
	       $linhas = $retorno->fetch(PDO::FETCH_ASSOC);

	       $compra = new Compra();

	       $compra->setTipo($linhas[tipo]);
	       $compra->setSexo($linhas[sexo]);
	       $compra->setProduto($linhas[produto]);
	       $compra->setMarca($linhas[marca]);
	       $compra->setModelo($linhas[modelo]);
	       $compra->setCor($linhas[cor]);
	       $compra->setQuantidade($linhas[quantidade]);
	       $compra->setTamanho($linhas[tamanho]);

	       unset($conn);
	       return $compra;
	  }

	  function getMarcas() {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $retorno = $conn->query("SELECT distinct marca FROM lista_compras order by marca asc");
	       while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
		    echo "<tr>
			      <td><div class='filtro_marca'>$linhas[marca]</div></td>
			 </tr>";
	       }
	  }

     }
     