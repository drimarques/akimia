<?php

     require_once '../src/classes/conexao.class.php';
     require_once '../src/classes/cliente.class.php';

     /* Abre conexao */
     $conexao = new Conexao();
     $conn = $conexao->getConexao();

     $contador = 1;
     while ($contador < 40) {
	  /* Executa query */
	  $conn->query("INSERT INTO clientes (nome, email, cep, endereco, numero_endereco, uf, cidade, bairro, permite_notificacoes) VALUES ('Maria', 'terra@eu.com.br', '00000-000', 'Rua teste', '100', 'MG', 'Belo Horizonte', 'Chuck Norris', '1')");
	  $contador++;
     }
     
     echo "Acabou!!!";
     
     #Fecha conexão
     unset($conn);
     