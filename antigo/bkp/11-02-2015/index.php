<!--
<?php
     require_once '../src/classes/conexao.class.php';
     require_once '../src/classes/cliente.class.php';
     require_once 'src/classes/interface-adm.class.php';
     $relatorio = new interfaceAdm();

     $pagina = $_GET['page'];
     if ($pagina == null) {
	  $pagina = 1;
     }
?>
-->
<!DOCTYPE html>
<html>
     <head>
	  <title>Administrador - Akimia</title>
	  <meta charset="UTF-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	  <meta name="author" content="Adriano Marques">
	  <link rel="shortcut icon" href="../img/Logo.Akimia.transp.750x750.png">
	  <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	  <link rel="stylesheet" href="../bootstrap/bootstrap.min.css">
	  <link rel="stylesheet" href="../bootstrap/bootstrap-theme.min.css">
	  <link href="css/estilo.css" rel="stylesheet" type="text/css">
	  <script src="../jquery/jquery-2.1.3.min.js"></script>
	  <!--<script src="js/akimia-adm.js" type="text/javascript"></script>-->
     </head>
     <body>

	  <header>
	       <img onclick="location.href = 'index.php'" src="../img/Logo.Akimia.transp.750x750.png" alt="Logo Akimia">

	       <form class="form-horizontal" method="post" id="formPesquisa" name="formPesquisa" autocomplete="off">
		    <div class="form-group">
			 <div class="col-sm-10">
			      <input type="text" class="form-control" id="textoPesquisa" name="textoPesquisa" placeholder="Buscar...">
			      <button type="submit" class="btn btn-default" name="btnPesquisarCliente" id="btnPesquisarCliente"><img src="img/search.png" alt="Imagem search"></button>
			 </div>
		    </div>
	       </form>

	       <!--Botão que realiza a pesquisa-->
	       <?php
		    require_once 'src/botao-pesquisar-cliente.click.php';
	       ?>

	       <section id="usuarioLogado">
		    emailusuariologado@opa.com
	       </section>
	  </header>

	  <?php
	       /* Verifica se existe uma busca */
	       $busca = $_GET['search'];
	       
	       /*Senão tiver nenhuma busca chama o relatório padrão*/
	       if ($busca == null) {
		    $relatorio->getTabelaClientesDefault($pagina);
	       } 
	       /*Se tiver busca chama o relatório de busca*/
	       else {
		    $relatorio->getTabelaClientesBusca($pagina, $busca);
	       }
	  ?>

	  <div class="clear"></div>	       

	  <!--Rodapé-->
	  <footer>
	       <div id="copyright">Copyright &#169; 2012 - <?php echo date('Y') ?> - Akimia Produtos Importados</div>
	       <div id="dev">Desenvolvido por <a href="http://www.facebook.com.br/adrianomarques9" target="_blank">Adriano Marques</a>.</div>
	  </footer>

	  <script src="../jquery/jquery-2.1.3.min.js" type="text/javascript"></script>
	  <script src="../bootstrap/bootstrap.min.js" type="text/javascript"></script>
	  <!--Biblioteca do scrol suave, juntamente com jQuery-->
	  <script src="../jquery/jquery.nicescroll.min.js"></script> 
	  <script src="js/akimia-adm.js" type="text/javascript"></script>
     </body>
</html>
