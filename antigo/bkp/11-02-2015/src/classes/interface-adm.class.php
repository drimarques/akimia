<?php

     class interfaceAdm {
	  #Tabela de clientes cadastrados default

	  function getTabelaClientesDefault($pagina) { 
	       /* Abre conexao */
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $cliente = new Cliente();

	       //seleciona todos os itens da tabela
	       $retorno = $conn->query("SELECT * FROM clientes ORDER BY nome ASC");

	       //conta o total de itens
	       $totalRegistros = $retorno->rowCount();

	       if ($totalRegistros > 0) {
		    //seta a quantidade de itens por página, neste caso, 50 itens
		    $padraoRegistros = 50;

		    //calcula o número de páginas arredondando o resultado para cima
		    $numPaginas = ceil($totalRegistros / $padraoRegistros);

		    //variavel para calcular o início da visualização com base na página atual
		    $inicio = ($padraoRegistros * $pagina) - $padraoRegistros;

		    //seleciona os itens por página
		    $retorno = $conn->query("SELECT * FROM clientes ORDER BY nome ASC LIMIT $inicio, $padraoRegistros");

		    $tabela = "<table class='tabela-clientes'>
					<thead>
					     <tr>
						  <th>Nome</th>
						  <th>Cidade</th>
						  <th>Estado</th>
						  <th>E-mail</th>
					     </tr>
					</thead>
					<tbody>";

		    $abreModal = "<script>";
		    while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
//			 $link = "'index.php?user=$linhas[id_cliente]&page=$pagina&search=$busca'";
//			 $tabela .= '<tr class="linha-user" onclick="location.href=' . $link . '">
			 $tabela .= '<tr class="linha-user" id="' . $linhas[id_cliente] . '">
				   <td>' . $linhas[nome] . '</td>
				   <td>' . $linhas[cidade] . '</td>
				   <td>' . $linhas[bairro] . '</td>
				   <td>' . $linhas[email] . '</td>
			      </tr>';

			 /* Montagem do modal */
			 $cliente->getDadosCliente($linhas[id_cliente]);

			 $modal = "";
			 $modal = '<div class="modal fade" id="' . md5($linhas[id_cliente]) . '" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
					     <div class="modal-content">
						  <div class="modal-header">
						       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						       <h4 class="modal-title" id="myModalLabel">Dados do cliente: ' . $cliente->GetNome() . '</h4>
						  </div>
						  <div class="modal-body">
						       <form class="form-horizontal form-usuario" method="post">
							    <div class="form-group">
								 <label for="" class="col-sm-2 control-label">Nome completo</label>
								 <div class="col-sm-10">
								      <input type="text" class="form-control" id="" name="" placeholder="Digite seu nome" maxlength="50" value="' . $cliente->GetNome() . '" readonly="readonly"> 
								 </div>
							    </div>
							    <div class="form-group">
								 <label for="" class="col-sm-2 control-label">Como gosta de ser chamado?</label>
								 <div class="col-sm-10">
								      <input type="text" class="form-control apelido-form" id="" name="" placeholder="Digite aqui" maxlength="20" value="' . $cliente->GetApelido() . '" readonly="readonly">
								 </div>
							    </div>
							    <div class="form-group">
								 <label for="" class="col-sm-2 control-label">E-mail</label>
								 <div class="col-sm-10">
								      <input type="email" class="form-control" id="" name="" placeholder="Digite seu e-mail" value="' . $cliente->GetEmail() . '" readonly="readonly">
								 </div>
							    </div>
							    <div class="form-group">
								 <label for="" class="col-sm-2 control-label">WhatsApp</label>
								 <div class="col-sm-10">
								      <input type="text" class="form-control" id="" name="" placeholder="Apenas números" value="' . $cliente->GetWhatsApp() . '" readonly="readonly">
								 </div>
							    </div>
							    <div class="form-group">
								 <label for="" class="col-sm-2 control-label">Telefone fixo</label>
								 <div class="col-sm-10">
								      <input type="text" class="form-control" id="" name="" placeholder="Apenas números" value="' . $cliente->GetTelefone() . '" readonly="readonly">
								 </div>
							    </div>
							    <div class="form-group">
								 <label for="" class="col-sm-2 control-label">Celular</label>
								 <div class="col-sm-10">
								      <input type="text" class="form-control" id="" name="" placeholder="Apenas números" value="' . $cliente->GetCelular() . '" readonly="readonly">
								 </div>
							    </div>
							    <div class="form-group">
								 <label for="" class="col-sm-2 control-label">CEP</label>
								 <div class="col-sm-10">
								      <input type="text" class="form-control" id="" name="" placeholder="Somente números" value="' . $cliente->GetCEP() . '" readonly="readonly">			 
								 </div>
							    </div>
							    <div class="form-group">
								 <label for="" class="col-sm-2 control-label">Endereço</label>
								 <div class="col-sm-10">
								      <input type="text" class="form-control" id="" name="" placeholder="Digite seu endereço" readonly="readonly" value="' . $cliente->GetEndereco() . '">
								 </div>
							    </div>
							    <div class="form-group">
								 <label for="numEnderecoCadastro" class="col-sm-2 control-label">Número</label>
								 <div class="col-sm-10">
								      <input type="number" class="form-control" id="" name="" min="1" placeholder="Digite o número" pattern="[0-9]+$" readonly="readonly" value="' . $cliente->GetNumeroEndereco() . '">
								 </div>
							    </div>
							    <div class="form-group">
								 <label for="" class="col-sm-2 control-label">Complemento</label>
								 <div class="col-sm-10">
								      <input type="text" class="form-control" id="" name="" placeholder="Complemento" maxlength="100" readonly="readonly" value="' . $cliente->GetComplemento() . '">
								 </div>
							    </div>
							    <div class="form-group">
								 <label for="" class="col-sm-2 control-label">Referência</label>
								 <div class="col-sm-10">
								      <input type="text" class="form-control" id="" name="" placeholder="Referência" maxlength="100" readonly="readonly" value="' . $cliente->GetReferencia() . '">
								 </div>
							    </div>
							    <div class="form-group">
								 <label for="" class="col-sm-2 control-label">Estado</label>
								 <div class="col-sm-10">
								      <input type="text" class="form-control" id="" name="" placeholder="Digite seu estado" readonly="readonly" value="' . $cliente->GetUF() . '">
								 </div>
							    </div>
							    <div class="form-group">
								 <label for="" class="col-sm-2 control-label">Cidade</label>
								 <div class="col-sm-10">
								      <input type="text" class="form-control" id="" name="" placeholder="Digite sua cidade" readonly="readonly" value="' . $cliente->GetCidade() . '">
								 </div>
							    </div>
							    <div class="form-group">
								 <label for="" class="col-sm-2 control-label">Bairro</label>
								 <div class="col-sm-10">
								      <input type="text" class="form-control" id="" name="" placeholder="Digite seu bairro" readonly="readonly" value="' . $cliente->GetBairro() . '">
								 </div>
							    </div>
						       </form>
						  </div>
					     </div>
					</div>
				   </div>';

			 echo $modal;
			 /*Função de abertura de modal*/
			 $abreModal .= '$("#' . $linhas[id_cliente] . '").click(function () {
					     $("#' . md5($linhas[id_cliente]) . '").modal("show");
					});';
		    }

		    #Fecha conexão
		    unset($conn);
		    
		    #Apresentando os dados em tela
		    $tabela .= "</tbody></table>";
		    echo '<div class="total-clientes">Exibindo '.$totalRegistros.' clientes cadastrados</div>';
		    echo "<article class='relatorios'>";
		    echo $tabela;
		    echo "</article>";
		    $abreModal .= "</script>";
		    echo $abreModal;

		    //Monta paginação
//		    $paginacao = "<nav class='nav-pagination'>
//			      <ul class='pagination'>";
//		    for ($i = 1; $i < $numPaginas + 1; $i++) {
//			 $paginacao .= "<li><a href='index.php?page=$i&search=$busca'>$i</a></li>";
//		    }
//		    $paginacao .= "</ul>
//			    </nav>";

		    $paginaSubtrair = $pagina - 1;
		    $linkSubtrair = "location.href='index.php?page=$paginaSubtrair'";

		    $paginaSomar = $pagina + 1;
		    $linkSomar = "location.href='index.php?page=$paginaSomar'";

		    /* Se a última página chegar */
		    $disabledProxima = "";
		    if ($paginaSomar > $numPaginas) {
			 $disabledProxima = "disabled";
		    }

		    /* Se a última primeira chegar */
		    $disabledAnterior = "";
		    if ($paginaSubtrair == '0') {
			 $disabledAnterior = "disabled";
		    }

		    $paginacao = '<div id="paginacao">
					<button onclick=' . $linkSubtrair . ' class="btn anteior" ' . $disabledAnterior . '>Anterior</button>
					<button onclick=' . $linkSomar . ' class="btn proxima" ' . $disabledProxima . '>Próxima</button>
			          </div>';

		    //Exibe a paginação
		    echo $paginacao;
	       }
	       //Senão existirem clientes cadastrados
	       else{
		    echo "<div class='sem-clientes-cadastrados'>Não há clientes cadastrados</div>";
	       }
	  }

//	  Busca clientes por palavras
	  function getTabelaClientesBusca($pagina, $busca) {
	       /* Abre conexao */
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $cliente = new Cliente();

	       //Monta SQL de busca
	       $sql = "SELECT * FROM clientes WHERE ";
	       $sql .= "nome LIKE '%$busca%' OR ";
	       $sql .= "apelido LIKE '%$busca%' OR ";
	       $sql .= "email LIKE '%$busca%' OR ";
	       $sql .= "whatsapp LIKE '%$busca%' OR ";
	       $sql .= "celular LIKE '%$busca%' OR ";
	       $sql .= "cep LIKE '%$busca%' OR ";
	       $sql .= "endereco LIKE '%$busca%' OR ";
	       $sql .= "numero_endereco LIKE '%$busca%' OR ";
	       $sql .= "complemento LIKE '%$busca%' OR ";
	       $sql .= "referencia LIKE '%$busca%' OR ";
	       $sql .= "uf LIKE '%$busca%' OR ";
	       $sql .= "cidade LIKE '%$busca%' OR ";
	       $sql .= "bairro LIKE '%$busca%' OR ";
	       $sqlLike = substr($sql, 0, -4);

	       $retorno = $conn->query($sqlLike);

	       //conta o total de itens
	       $totalRegistros = $retorno->rowCount();

	       /* Se tiverem registros encontrados, mostra a tabela */
	       if ($totalRegistros > 0) {
		    //seta a quantidade de itens por página, neste caso, 50 itens
		    $padraoRegistros = 50;

		    //calcula o número de páginas arredondando o resultado para cima
		    $numPaginas = ceil($totalRegistros / $padraoRegistros);

		    //variavel para calcular o início da visualização com base na página atual
		    $inicio = ($padraoRegistros * $pagina) - $padraoRegistros;

		    //seleciona os itens por página
		    //Monta SQL de busca
		    $sql2 = "SELECT * FROM clientes WHERE ";
		    $sql2 .= "nome LIKE '%$busca%' OR ";
		    $sql2 .= "apelido LIKE '%$busca%' OR ";
		    $sql2 .= "email LIKE '%$busca%' OR ";
		    $sql2 .= "whatsapp LIKE '%$busca%' OR ";
		    $sql2 .= "celular LIKE '%$busca%' OR ";
		    $sql2 .= "cep LIKE '%$busca%' OR ";
		    $sql2 .= "endereco LIKE '%$busca%' OR ";
		    $sql2 .= "numero_endereco LIKE '%$busca%' OR ";
		    $sql2 .= "complemento LIKE '%$busca%' OR ";
		    $sql2 .= "referencia LIKE '%$busca%' OR ";
		    $sql2 .= "uf LIKE '%$busca%' OR ";
		    $sql2 .= "cidade LIKE '%$busca%' OR ";
		    $sql2 .= "bairro LIKE '%$busca%' OR ";

		    $sqlLike2 = substr($sql2, 0, -4);
		    $sqlLike2 .= "ORDER BY nome ASC LIMIT $inicio, $padraoRegistros";

		    $retorno = $conn->query($sqlLike2);

		    $tabela = "<table class='tabela-clientes'>
					<thead>
					     <tr>
						  <th>Nome</th>
						  <th>Cidade</th>
						  <th>Estado</th>
						  <th>E-mail</th>
					     </tr>
					</thead>
					<tbody>";

		    $abreModal = "<script>";
		    while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
//			 $link = "'index.php?user=$linhas[id_cliente]&page=$pagina&search=$busca'";
//			 $tabela .= '<tr class="linha-user" onclick="location.href=' . $link . '">
			 $tabela .= '<tr class="linha-user" id="' . $linhas[id_cliente] . '">
				   <td>' . $linhas[nome] . '</td>
				   <td>' . $linhas[cidade] . '</td>
				   <td>' . $linhas[bairro] . '</td>
				   <td>' . $linhas[email] . '</td>
			      </tr>';

			 /* Montagem do modal */
			 $cliente->getDadosCliente($linhas[id_cliente]);

			 $modal = "";
			 $modal = '<div class="modal fade" id="' . md5($linhas[id_cliente]) . '" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
					     <div class="modal-content">
						  <div class="modal-header">
						       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						       <h4 class="modal-title" id="myModalLabel">Dados do cliente: ' . $cliente->GetNome() . '</h4>
						  </div>
						  <div class="modal-body">
						       <form class="form-horizontal form-usuario" method="post">
							    <div class="form-group">
								 <label for="" class="col-sm-2 control-label">Nome completo</label>
								 <div class="col-sm-10">
								      <input type="text" class="form-control" id="" name="" placeholder="Digite seu nome" maxlength="50" value="' . $cliente->GetNome() . '" readonly="readonly"> 
								 </div>
							    </div>
							    <div class="form-group">
								 <label for="" class="col-sm-2 control-label">Como gosta de ser chamado?</label>
								 <div class="col-sm-10">
								      <input type="text" class="form-control apelido-form" id="" name="" placeholder="Digite aqui" maxlength="20" value="' . $cliente->GetApelido() . '" readonly="readonly">
								 </div>
							    </div>
							    <div class="form-group">
								 <label for="" class="col-sm-2 control-label">E-mail</label>
								 <div class="col-sm-10">
								      <input type="email" class="form-control" id="" name="" placeholder="Digite seu e-mail" value="' . $cliente->GetEmail() . '" readonly="readonly">
								 </div>
							    </div>
							    <div class="form-group">
								 <label for="" class="col-sm-2 control-label">WhatsApp</label>
								 <div class="col-sm-10">
								      <input type="text" class="form-control" id="" name="" placeholder="Apenas números" value="' . $cliente->GetWhatsApp() . '" readonly="readonly">
								 </div>
							    </div>
							    <div class="form-group">
								 <label for="" class="col-sm-2 control-label">Telefone fixo</label>
								 <div class="col-sm-10">
								      <input type="text" class="form-control" id="" name="" placeholder="Apenas números" value="' . $cliente->GetTelefone() . '" readonly="readonly">
								 </div>
							    </div>
							    <div class="form-group">
								 <label for="" class="col-sm-2 control-label">Celular</label>
								 <div class="col-sm-10">
								      <input type="text" class="form-control" id="" name="" placeholder="Apenas números" value="' . $cliente->GetCelular() . '" readonly="readonly">
								 </div>
							    </div>
							    <div class="form-group">
								 <label for="" class="col-sm-2 control-label">CEP</label>
								 <div class="col-sm-10">
								      <input type="text" class="form-control" id="" name="" placeholder="Somente números" value="' . $cliente->GetCEP() . '" readonly="readonly">			 
								 </div>
							    </div>
							    <div class="form-group">
								 <label for="" class="col-sm-2 control-label">Endereço</label>
								 <div class="col-sm-10">
								      <input type="text" class="form-control" id="" name="" placeholder="Digite seu endereço" readonly="readonly" value="' . $cliente->GetEndereco() . '">
								 </div>
							    </div>
							    <div class="form-group">
								 <label for="numEnderecoCadastro" class="col-sm-2 control-label">Número</label>
								 <div class="col-sm-10">
								      <input type="number" class="form-control" id="" name="" min="1" placeholder="Digite o número" pattern="[0-9]+$" readonly="readonly" value="' . $cliente->GetNumeroEndereco() . '">
								 </div>
							    </div>
							    <div class="form-group">
								 <label for="" class="col-sm-2 control-label">Complemento</label>
								 <div class="col-sm-10">
								      <input type="text" class="form-control" id="" name="" placeholder="Complemento" maxlength="100" readonly="readonly" value="' . $cliente->GetComplemento() . '">
								 </div>
							    </div>
							    <div class="form-group">
								 <label for="" class="col-sm-2 control-label">Referência</label>
								 <div class="col-sm-10">
								      <input type="text" class="form-control" id="" name="" placeholder="Referência" maxlength="100" readonly="readonly" value="' . $cliente->GetReferencia() . '">
								 </div>
							    </div>
							    <div class="form-group">
								 <label for="" class="col-sm-2 control-label">Estado</label>
								 <div class="col-sm-10">
								      <input type="text" class="form-control" id="" name="" placeholder="Digite seu estado" readonly="readonly" value="' . $cliente->GetUF() . '">
								 </div>
							    </div>
							    <div class="form-group">
								 <label for="" class="col-sm-2 control-label">Cidade</label>
								 <div class="col-sm-10">
								      <input type="text" class="form-control" id="" name="" placeholder="Digite sua cidade" readonly="readonly" value="' . $cliente->GetCidade() . '">
								 </div>
							    </div>
							    <div class="form-group">
								 <label for="" class="col-sm-2 control-label">Bairro</label>
								 <div class="col-sm-10">
								      <input type="text" class="form-control" id="" name="" placeholder="Digite seu bairro" readonly="readonly" value="' . $cliente->GetBairro() . '">
								 </div>
							    </div>
						       </form>
						  </div>
					     </div>
					</div>
				   </div>';
			 
			 echo $modal;
			 /*Função de abertura de modal*/
			 $abreModal .= '$("#' . $linhas[id_cliente] . '").click(function () {
					     $("#' . md5($linhas[id_cliente]) . '").modal("show");
					});';
		    }

		    $tabela .= "</tbody>
			   </table>";

		    #Fecha conexão
		    unset($conn);

		    //Gambiarra de plural
		    if ($totalRegistros == 1) {
			 $textoPlural = "resultado encontrado";
		    } else {
			 $textoPlural = "resultados encontrados";
		    }

		    /* Apresentando os dados */
		    echo '<div class="com-resultados-busca-clientes">' . $totalRegistros . "\n" . $textoPlural . ' para sua busca: "' . $busca . '"</div>';
		    echo '<span id=numeracao-page>Página ' . $pagina . ' de ' . $numPaginas . '</span>';
		    echo "<article class='relatorios'>";
		    echo $tabela;
		    echo "</article>";
		    /*Funções de abertura de modal*/
		    $abreModal .= "</script>";
		    echo $abreModal;

		    //Monta paginação
//		    $paginacao = "<nav class='nav-pagination'>
//			      <ul class='pagination'>";
//		    for ($i = 1; $i < $numPaginas + 1; $i++) {
//			 $paginacao .= "<li><a href='index.php?page=$i&search=$busca'>$i</a></li>";
//		    }
//		    $paginacao .= "</ul>
//			    </nav>";


		    $paginaSubtrair = $pagina - 1;
		    $linkSubtrair = "location.href='index.php?page=$paginaSubtrair&search=$busca'";

		    $paginaSomar = $pagina + 1;
		    $linkSomar = "location.href='index.php?page=$paginaSomar&search=$busca'";

		    /* Se a última página chegar */
		    $disabledProxima = "";
		    if ($paginaSomar > $numPaginas) {
			 $disabledProxima = "disabled";
		    }

		    /* Se a última primeira chegar */
		    $disabledAnterior = "";
		    if ($paginaSubtrair == '0') {
			 $disabledAnterior = "disabled";
		    }

		    $paginacao = '<div id="paginacao">
					<button onclick=' . $linkSubtrair . ' class="btn anteior" ' . $disabledAnterior . '>Anterior</button>
					<button onclick=' . $linkSomar . ' class="btn proxima" ' . $disabledProxima . '>Próxima</button>
			          </div>';

		    //Exibe a paginação
		    echo $paginacao;
	       }
//	       Senão tiverem registros encontrados, mostra sem resultados
	       else {
		    echo '<div class="sem-resultados-busca-clientes">Nenhum cliente encontrado para sua busca: "' . $busca . '"</div>';
		    echo "</article>";
	       }
	  }

     }
     