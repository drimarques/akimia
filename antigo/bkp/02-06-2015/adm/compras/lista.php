<?php
require_once '../../src/classes/conexao.class.php';
require_once '../../src/classes/compra.class.php';
require_once '../../src/dao/comprasDAO.php';

$dao = new comprasDAO();

$comprado = $_GET["filtro_compras"];
$sexo = $_GET["sexo"];
$busca = $_GET["filtro_busca"];

if($busca != ""){
     $dao->listaComprasBusca($busca);
}
else{
     switch ($comprado){
	  case "null":
	       $dao->listaCompleta();
	       break;
	  case "comprados":
	       $dao->listaComprados($sexo);
	       break;
	  case "n_comprados":
	       $dao->listaNaoComprados($sexo);
	       break;
	  case "comprados_parc":
	       $dao->listaCompradosParc($sexo);
	       break;
     }
}

