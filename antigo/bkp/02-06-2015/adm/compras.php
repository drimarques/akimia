<!DOCTYPE html>
<html lang="pt-br">
     <head>
	  <title>Compras - Akimia</title>
	  <meta charset="UTF-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	  <meta name="author" content="Adriano Marques">
	  <link rel="shortcut icon" href="../img/Logo.Akimia.transp.750x750.png">
	  <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	  <link rel="stylesheet" href="../bootstrap/bootstrap.min.css">
	  <link rel="stylesheet" href="../bootstrap/bootstrap-theme.min.css">
	  <link href="css/compras.css" rel="stylesheet" type="text/css">
	  <script src="../jquery/jquery-2.1.3.min.js"></script>	  

	  <!--Ajax de atualização automatica do relatorio-->
	  <script src="js/ajax.js" type="text/javascript"></script>
	  <script type="text/javascript">

	       function limpaListaENotificacao(){
		    $("#notificacao_lista_compras").css({"display": "none"});
		    document.getElementById("lista_compras").innerHTML = "";
	       }
	       

	       function animacao() {
		    $("#item_clicado").finish();

		    $("#item_clicado").animate({
			 padding: "20px 0 0 0",
			 paddingBottom: "40px"
		    },
		    400,
			    //Animação completa
				    function () {
					 $("#item_clicado").delay(800);
					 $("#item_clicado").animate({
					      padding: "0",
					      paddingBottom: "0"
					 },
					 400);
				    });			    
		       }

	       var comprado;
	       comprado = new montaXMLHTTP();
	       function marcaComprado(idItem) {
		    var url = "compras/marca-comprado.php?id_item=" + idItem;

		    comprado.open("GET", url, true);
		    comprado.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		    comprado.onreadystatechange = function () {
			 if (comprado.readyState === 4) {
			      document.getElementById("item_clicado").innerHTML = comprado.responseText;
//			      $("#item_clicado").fadeIn("fast").delay(800).fadeOut(200, function (){
//				   listaCompras('n_comprados');
//			      });


			      var sexo = new montaXMLHTTP();
			      var url_sexo = "compras/get-sexo.php?id_item=" + idItem;

			      sexo.open("GET", url_sexo, true);
			      sexo.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			      sexo.onreadystatechange = function () {
				   if (sexo.readyState === 4) {
					animacao();
					listaCompras(sexo.responseText);
				   }

			      };
			      sexo.send(null);
			 }

		    };
		    comprado.send(null);
	       }

	       var naoComprado;
	       naoComprado = new montaXMLHTTP();
	       function marcaNaoComprado(idItem) {
		    var url = "compras/marca-nao-comprado.php?id_item=" + idItem;

		    naoComprado.open("GET", url, true);
		    naoComprado.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		    naoComprado.onreadystatechange = function () {
			 if (naoComprado.readyState === 4) {
			      document.getElementById("item_clicado").innerHTML = naoComprado.responseText;
//			      $("#item_clicado").fadeIn("fast").delay(800).fadeOut(200, function (){
//				   listaCompras('n_comprados');
//			      });

			      var sexo = new montaXMLHTTP();
			      var url_sexo = "compras/get-sexo.php?id_item=" + idItem;

			      sexo.open("GET", url_sexo, true);
			      sexo.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			      sexo.onreadystatechange = function () {
				   if (sexo.readyState === 4) {
					animacao();
					listaCompras(sexo.responseText);
				   }

			      };
			      sexo.send(null);
			 }

		    };
		    naoComprado.send(null);
	       }


	       var compradoBusca;
	       compradoBusca = new montaXMLHTTP();
	       function marcaBuscaComprado(idItem, busca) {
		    var url = "compras/marca-comprado.php?id_item=" + idItem;

		    compradoBusca.open("GET", url, true);
		    compradoBusca.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		    compradoBusca.onreadystatechange = function () {
			 if (compradoBusca.readyState === 4) {
			      document.getElementById("item_clicado").innerHTML = compradoBusca.responseText;

			      var sexo = new montaXMLHTTP();
			      var url_sexo = "compras/get-sexo.php?id_item=" + idItem;

			      sexo.open("GET", url_sexo, true);
			      sexo.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			      sexo.onreadystatechange = function () {
				   if (sexo.readyState === 4) {
					animacao();
					listaComprasBusca(busca);
				   }

			      };
			      sexo.send(null);
			 }

		    };
		    compradoBusca.send(null);
		    limpaListaENotificacao();
		    listaComprasBusca(busca);
	       }

	       var naoCompradoBusca;
	       naoCompradoBusca = new montaXMLHTTP();
	       function marcaBuscaNaoComprado(idItem, busca) {
		    var url = "compras/marca-nao-comprado.php?id_item=" + idItem;

		    naoCompradoBusca.open("GET", url, true);
		    naoCompradoBusca.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		    naoCompradoBusca.onreadystatechange = function () {
			 if (naoCompradoBusca.readyState === 4) {
			      document.getElementById("item_clicado").innerHTML = naoCompradoBusca.responseText;
				   
			      var sexo = new montaXMLHTTP();
			      var url_sexo = "compras/get-sexo.php?id_item=" + idItem;

			      sexo.open("GET", url_sexo, true);
			      sexo.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			      sexo.onreadystatechange = function () {
				   if (sexo.readyState === 4) {
					animacao();
					listaComprasBusca(busca);
				   }

			      };
			      sexo.send(null);
			 }
		    };		    
		    naoCompradoBusca.send(null);		    
	       }

	  </script>

	  <script>
	       $(document).ready(function () {
		    $("#txtBusca").keyup(function () {
			 var busca = document.getElementById("txtBusca");
			 limpaListaENotificacao();
			 listaComprasBusca(busca.value);
		    });
	       });
	  </script>

     </head>
     <body>
	  <header>	       
	       <h1><img onclick="location.href = 'compras.php'" src="../img/Logo.Akimia.transp.750x750.png" alt="Akimia">Compras</h1>
	  </header>

	  <div class="filtros" id="filtro_pesquisa">
	       <form method="post" name="form_busca" id="form_busca">
		    <input type="text" placeholder="Buscar" id="txtBusca" name="txtBusca" class="form-control">
	       </form>
	  </div>

	  <div class="filtros" id="filtro_comprados">
	       <div class="btns_filtros" id="btn_n_comprados"><img src="img/unlike.png" alt="Like">Não comprados</div>
	       <div class="btns_filtros" id="btn_comprados"><img src="img/like.png" alt="Like">Comprados</div>
	       <div class="btns_filtros" id="btn_comprados_parc"><img src="img/compras.png" alt="Like">Comprados parcialmente</div>
	  </div>

	  <div class="filtros" id="filtro_sexo">
	       <div onclick="listaCompras('masculino')" class="btns_filtros" id="btn_masculino"><img src="img/masculino.png" alt="Like">Masculino</div>
	       <div onclick="listaCompras('feminino')" class="btns_filtros" id="btn_feminino"><img src="img/feminino.png" alt="Like">Feminino</div>
	       <div onclick="listaCompras('-')" class="btns_filtros" id="btn_indiferente"><img src="img/indiferente.png" alt="Like">Unissex</div>
	  </div>

	  <article id="lista_compras">

	  </article>

	  <div id='notificacao_lista_compras'></div>

	  <div id="item_clicado"></div>	  

	  <script src="../jquery/jquery-2.1.3.min.js" type="text/javascript"></script>
	  <script src="../bootstrap/bootstrap.min.js" type="text/javascript"></script>
	  <!--Biblioteca do scrol suave, juntamente com jQuery-->
	  <script src="../jquery/jquery.nicescroll.min.js"></script> 
	  <script src="js/akimia-adm.js" type="text/javascript"></script>
	  <script type="text/javascript">
		    var relatorio;
		    relatorio = new montaXMLHTTP();
		    function listaCompras(sexo) {
			 limpaListaENotificacao();

			 var n_comprados = $("#btn_n_comprados").hasClass('filtro_selecionado').toString();
			 var comprados = $("#btn_comprados").hasClass('filtro_selecionado').toString();
			 var comprados_parc = $("#btn_comprados_parc").hasClass('filtro_selecionado').toString();

			 if (n_comprados === "true") {
			      var url = "compras/lista.php?filtro_compras=n_comprados" + "&sexo=" + sexo;
			      var filtro = "n_comprados";
			 }
			 else if (comprados === "true") {
			      var url = "compras/lista.php?filtro_compras=comprados" + "&sexo=" + sexo;
			      var filtro = "comprados";
			 }
			 else if (comprados_parc === "true") {
			      var url = "compras/lista.php?filtro_compras=comprados_parc" + "&sexo=" + sexo;
			      var filtro = "comprados_parc";
			 }

			 relatorio.open("GET", url, true);
			 relatorio.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			 relatorio.onreadystatechange = function () {
			      if (relatorio.readyState === 4) {
				   document.getElementById("lista_compras").innerHTML = relatorio.responseText;

				   //Se ão tiver resultados
				   if (relatorio.responseText === "") {
					switch (filtro) {
					     case "comprados" :
						  if (sexo === "-") {
						       document.getElementById("notificacao_lista_compras").innerHTML = "Não há produtos unissex comprados.";
						       $("#notificacao_lista_compras").fadeIn("fast");
						  }
						  else {
						       document.getElementById("notificacao_lista_compras").innerHTML = "Não há produtos " + sexo.toLowerCase() + "s comprados.";
						       $("#notificacao_lista_compras").fadeIn("fast");
						  }

						  break;
					     case "n_comprados" :
						  if (sexo === "-") {
						       document.getElementById("notificacao_lista_compras").innerHTML = "Todos produtos unissex foram comprados.";
						       $("#notificacao_lista_compras").fadeIn("fast");
						  }
						  else {
						       document.getElementById("notificacao_lista_compras").innerHTML = "Todos produtos " + sexo.toLowerCase() + "s foram comprados.";
						       $("#notificacao_lista_compras").fadeIn("fast");
						  }
						  break;
					}
				   }

			      }
			 };
			 relatorio.send(null);
		    }

		    var relatorioBusca;
		    relatorioBusca = new montaXMLHTTP();
		    function listaComprasBusca(busca) {
			 if (busca !== "") {
			      $("#filtro_comprados").fadeOut("fast");
			      $("#filtro_sexo").fadeOut("fast");
			      
			      var url = "compras/lista.php?filtro_busca=" + busca;
			      
			      relatorioBusca.open("GET", url, true);
			      relatorioBusca.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			      relatorioBusca.onreadystatechange = function () {
				   if (relatorioBusca.readyState === 4) {
					document.getElementById("lista_compras").innerHTML = relatorioBusca.responseText;					
					if(relatorioBusca.responseText === ""){
					     document.getElementById("notificacao_lista_compras").innerHTML = "Sem resultados encontrados para: \"" + busca + '\"';
					     $("#notificacao_lista_compras").fadeIn("fast");
					}
				   }
			      };
			      relatorioBusca.send(null);
			 }
			 else {
			      limpaListaENotificacao();
			      $("#filtro_comprados").fadeIn("fast");
			 }
		    }
	  </script>
     </body>
</html>


