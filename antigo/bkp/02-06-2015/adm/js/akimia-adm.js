$(document).ready(function () {
//     Scrol suave     
     //$("html").niceScroll();

     //Botoes comprados n comprados comprados parc
     $("#btn_n_comprados").click(function () {
	  $("#btn_comprados").removeClass('filtro_selecionado');
	  $("#btn_comprados_parc").removeClass('filtro_selecionado');
	  $("#btn_n_comprados").addClass('filtro_selecionado');

	  $("#btn_feminino").removeClass('filtro_selecionado');
	  $("#btn_masculino").removeClass('filtro_selecionado');
	  $("#btn_indiferente").removeClass('filtro_selecionado');

	  document.getElementById("lista_compras").innerHTML = "";
	  $("#notificacao_lista_compras").css({"display": "none"});

	  $("#filtro_sexo").fadeOut();
	  $("#filtro_sexo").fadeIn();
     });

     $("#btn_comprados").click(function () {
	  $("#btn_n_comprados").removeClass('filtro_selecionado');
	  $("#btn_comprados_parc").removeClass('filtro_selecionado');
	  $("#btn_comprados").addClass('filtro_selecionado');

	  $("#btn_feminino").removeClass('filtro_selecionado');
	  $("#btn_masculino").removeClass('filtro_selecionado');
	  $("#btn_indiferente").removeClass('filtro_selecionado');

	  document.getElementById("lista_compras").innerHTML = "";
	  $("#notificacao_lista_compras").css({"display": "none"});

	  $("#filtro_sexo").fadeOut();
	  $("#filtro_sexo").fadeIn();
     });

     $("#btn_comprados_parc").click(function () {
	  $("#btn_n_comprados").removeClass('filtro_selecionado');
	  $("#btn_comprados").removeClass('filtro_selecionado');
	  $("#btn_comprados_parc").addClass('filtro_selecionado');

	  $("#btn_feminino").removeClass('filtro_selecionado');
	  $("#btn_masculino").removeClass('filtro_selecionado');
	  $("#btn_indiferente").removeClass('filtro_selecionado');

	  document.getElementById("lista_compras").innerHTML = "";
	  $("#notificacao_lista_compras").css({"display": "none"});

	  $("#filtro_sexo").fadeOut();
	  $("#filtro_sexo").fadeIn();
     });

     //Botoes de sexo
     $("#btn_masculino").click(function () {
	  $("#btn_feminino").removeClass('filtro_selecionado');
	  $("#btn_indiferente").removeClass('filtro_selecionado');
	  $("#btn_masculino").addClass('filtro_selecionado');
     });

     $("#btn_feminino").click(function () {
	  $("#btn_masculino").removeClass('filtro_selecionado');
	  $("#btn_indiferente").removeClass('filtro_selecionado');
	  $("#btn_feminino").addClass('filtro_selecionado');
     });

     $("#btn_indiferente").click(function () {
	  $("#btn_masculino").removeClass('filtro_selecionado');
	  $("#btn_feminino").removeClass('filtro_selecionado');
	  $("#btn_indiferente").addClass('filtro_selecionado');
     });



     $("#settings").click(function () {
	  $("#settings_expand").fadeToggle("fast");
     });


     //Fazendo abrir a caixa de selecao de arquivo
     $(".link_importar").click(function () {
	  var botao = document.getElementById("arquivo_excel");
	  botao.click();
     });


     //Fazendo auto submit apos selecionar um arquivo no inputtype file
     document.getElementById("arquivo_excel").onchange = function () {
	  var botao = document.getElementById("btnEnviarExcel");
	  botao.click();
     };

     if ($(document).width() >= 480 && $(document).width() < 1023) {
	  location.href = "compras.php";
     }
     ;
});