<?php

     class interfaceAdm {
//	  Tabela de clientes cadastrados default
	  function getTodosClientes($pagina) { 
	       /* Abre conexao */
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();

	       //seleciona todos os itens da tabela
	       $retorno = $conn->query("SELECT * FROM clientes ORDER BY nome ASC");

	       //conta o total de itens
	       $totalRegistros = $retorno->rowCount();

	       if ($totalRegistros > 0) {
		    //seta a quantidade de itens por página, neste caso, 50 itens
		    $padraoRegistros = 50;

		    //calcula o número de páginas arredondando o resultado para cima
		    $numPaginas = ceil($totalRegistros / $padraoRegistros);

		    //variavel para calcular o início da visualização com base na página atual
		    $inicio = ($padraoRegistros * $pagina) - $padraoRegistros;

		    //seleciona os itens por página
		    $retorno = $conn->query("SELECT * FROM clientes ORDER BY nome ASC LIMIT $inicio, $padraoRegistros");

		    $tabela = "<table class='table-responsive table-hover tabelas'>
					<thead>
					     <tr>
						  <th>Nome</th>
						  <th>Cidade</th>
						  <th>Estado</th>
						  <th>E-mail</th>
					     </tr>
					</thead>
					<tbody>";

		    while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
//			 $tabela .= '<tr class="linha-user" id="' . $linhas[id_cliente] . '">
			 $link = "'index.php?user=$linhas[id_cliente]&page=$pagina'";
			 $tabela .= '<tr class="linha-user" onclick="location.href=' . $link . '">
				   <td>' . $linhas[nome] . '</td>
				   <td>' . $linhas[cidade] . '</td>
				   <td>' . $linhas[bairro] . '</td>
				   <td>' . $linhas[email] . '</td>
			      </tr>';
		    }

		    #Fecha conexão
		    unset($conn);
		    
		    #Apresentando os dados em tela
		    $tabela .= "</tbody></table>";
		   		    
		    echo '<div class="notifica_resultados">Exibindo '.$totalRegistros.' clientes cadastrados</div>';
		    echo '<div class="numeracao_paginas">Página '.$pagina.' de ' .$numPaginas. '</div>';
		    echo $tabela;		    

		    //Monta paginação
//		    $paginacao = "<nav class='nav-pagination'>
//			      <ul class='pagination'>";
//		    for ($i = 1; $i < $numPaginas + 1; $i++) {
//			 $paginacao .= "<li><a href='index.php?page=$i&search=$busca'>$i</a></li>";
//		    }
//		    $paginacao .= "</ul>
//			    </nav>";

		    $paginaSubtrair = $pagina - 1;
		    $linkSubtrair = "location.href='index.php?page=$paginaSubtrair'";

		    $paginaSomar = $pagina + 1;
		    $linkSomar = "location.href='index.php?page=$paginaSomar'";

		    /* Se a última página chegar */
		    $disabledProxima = "";
		    if ($paginaSomar > $numPaginas) {
			 $disabledProxima = "disabled";
		    }

		    /* Se a última primeira chegar */
		    $disabledAnterior = "";
		    if ($paginaSubtrair == '0') {
			 $disabledAnterior = "disabled";
		    }

		    if($numPaginas > 1){
			 $paginacao = '<div class="paginacao">
					     <button onclick=' . $linkSubtrair . ' class="btnPaginacao" ' . $disabledAnterior . '>Anterior</button>
					     <button onclick=' . $linkSomar . ' class="btnPaginacao" ' . $disabledProxima . '>Próxima</button>
				       </div>';

			 //Exibe a paginação
			 echo $paginacao;
		    }
	       }
	  }

//	  Busca clientes por palavras
	  function buscaClientes($pagina, $busca) {	       
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();

	       //Monta SQL de busca
	       $sql = "SELECT * FROM clientes WHERE ";
	       $sql .= "nome LIKE '%$busca%' OR ";
	       $sql .= "apelido LIKE '%$busca%' OR ";
	       $sql .= "email LIKE '%$busca%' OR ";
	       $sql .= "whatsapp LIKE '%$busca%' OR ";
	       $sql .= "celular LIKE '%$busca%' OR ";
	       $sql .= "cep LIKE '%$busca%' OR ";
	       $sql .= "endereco LIKE '%$busca%' OR ";
	       $sql .= "numero_endereco LIKE '%$busca%' OR ";
	       $sql .= "complemento LIKE '%$busca%' OR ";
	       $sql .= "referencia LIKE '%$busca%' OR ";
	       $sql .= "uf LIKE '%$busca%' OR ";
	       $sql .= "cidade LIKE '%$busca%' OR ";
	       $sql .= "bairro LIKE '%$busca%' OR ";
	       $sqlLike = substr($sql, 0, -4);

	       $retorno = $conn->query($sqlLike);

	       //conta o total de itens
	       $totalRegistros = $retorno->rowCount();

	       /* Se tiverem registros encontrados, mostra a tabela */
	       if ($totalRegistros > 0) {
		    //seta a quantidade de itens por página, neste caso, 50 itens
		    $padraoRegistros = 50;

		    //calcula o número de páginas arredondando o resultado para cima
		    $numPaginas = ceil($totalRegistros / $padraoRegistros);

		    //variavel para calcular o início da visualização com base na página atual
		    $inicio = ($padraoRegistros * $pagina) - $padraoRegistros;

		    //seleciona os itens por página
		    //Monta SQL de busca
		    $sql2 = "SELECT * FROM clientes WHERE ";
		    $sql2 .= "nome LIKE '%$busca%' OR ";
		    $sql2 .= "apelido LIKE '%$busca%' OR ";
		    $sql2 .= "email LIKE '%$busca%' OR ";
		    $sql2 .= "whatsapp LIKE '%$busca%' OR ";
		    $sql2 .= "celular LIKE '%$busca%' OR ";
		    $sql2 .= "cep LIKE '%$busca%' OR ";
		    $sql2 .= "endereco LIKE '%$busca%' OR ";
		    $sql2 .= "numero_endereco LIKE '%$busca%' OR ";
		    $sql2 .= "complemento LIKE '%$busca%' OR ";
		    $sql2 .= "referencia LIKE '%$busca%' OR ";
		    $sql2 .= "uf LIKE '%$busca%' OR ";
		    $sql2 .= "cidade LIKE '%$busca%' OR ";
		    $sql2 .= "bairro LIKE '%$busca%' OR ";

		    $sqlLike2 = substr($sql2, 0, -4);
		    $sqlLike2 .= "ORDER BY nome ASC LIMIT $inicio, $padraoRegistros";

		    $retorno = $conn->query($sqlLike2);

		    $tabela = "<table class='table-responsive table-hover tabelas'>
					<thead>
					     <tr>
						  <th>Nome</th>
						  <th>Cidade</th>
						  <th>Estado</th>
						  <th>E-mail</th>
					     </tr>
					</thead>
					<tbody>";

		    while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
//			 $tabela .= '<tr class="linha-user" id="' . $linhas[id_cliente] . '">
			 $link = "'index.php?user=$linhas[id_cliente]&page=$pagina&search=$busca'";
			 $tabela .= '<tr class="linha-user" onclick="location.href=' . $link . '">
				   <td>' . $linhas[nome] . '</td>
				   <td>' . $linhas[cidade] . '</td>
				   <td>' . $linhas[bairro] . '</td>
				   <td>' . $linhas[email] . '</td>
			      </tr>';
		    }

		    $tabela .= "</tbody>
			   </table>";

		    #Fecha conexão
		    unset($conn);

		    //Gambiarra de plural
		    if ($totalRegistros == 1) {
			 $textoPlural = "resultado encontrado";
		    } else {
			 $textoPlural = "resultados encontrados";
		    }

		    /* Apresentando os dados */
		    echo '<div class="notifica_resultados">' . $totalRegistros . "\n" . $textoPlural . ' para sua busca: "' . $busca . '"</div>';
		    echo '<div class="numeracao_paginas">Página '.$pagina.' de ' .$numPaginas. '</div>';		    
		    echo $tabela;		    

		    //Monta paginação
//		    $paginacao = "<nav class='nav-pagination'>
//			      <ul class='pagination'>";
//		    for ($i = 1; $i < $numPaginas + 1; $i++) {
//			 $paginacao .= "<li><a href='index.php?page=$i&search=$busca'>$i</a></li>";
//		    }
//		    $paginacao .= "</ul>
//			    </nav>";


		    $paginaSubtrair = $pagina - 1;
		    $linkSubtrair = "location.href='index.php?page=$paginaSubtrair&search=$busca'";

		    $paginaSomar = $pagina + 1;
		    $linkSomar = "location.href='index.php?page=$paginaSomar&search=$busca'";

		    /* Se a última página chegar */
		    $disabledProxima = "";
		    if ($paginaSomar > $numPaginas) {
			 $disabledProxima = "disabled";
		    }

		    /* Se a última primeira chegar */
		    $disabledAnterior = "";
		    if ($paginaSubtrair == '0') {
			 $disabledAnterior = "disabled";
		    }

		    if($numPaginas > 1){
			 $paginacao = '<div class="paginacao">
					<button onclick=' . $linkSubtrair . ' class="btnPaginacao" ' . $disabledAnterior . '>Anterior</button>
					<button onclick=' . $linkSomar . ' class="btnPaginacao" ' . $disabledProxima . '>Próxima</button>
			          </div>';

			 //Exibe a paginação
			 echo $paginacao;
		    }		    
	       }
	       else{
		    echo '<div class="notifica_resultados">Nenhum resultado para sua busca: "'.$busca.'"</div>';
	       }
	  }

//	  Nome e e-mail dos clientes pro AutoComplete
	  function getClientes(){
	       /* Abre conexao */
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();

	       //seleciona todos os itens da tabela
	       $retorno = $conn->query("SELECT distinct nome, email FROM clientes ORDER BY nome ASC");
	       while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
		    echo "<option value='$linhas[nome]' label='$linhas[email]'>";
	       }
	  }
	  
     }
     