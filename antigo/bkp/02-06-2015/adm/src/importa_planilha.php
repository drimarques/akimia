<?php

     if (isset($_POST['btnEnviarExcel'])) {	  
	  $dao = new comprasDAO();
	  $compra = new Compra();
	  
	  
	  $data = new Spreadsheet_Excel_Reader();
	  $data->setOutputEncoding('UTF-8');
	  
	  //Se o arquivo não for vazio e for um arquivo excel
	  if (!empty($_FILES['arquivo_excel']) && $_FILES['arquivo_excel']['type'] == "application/vnd.ms-excel") {
	       $data->read($_FILES['arquivo_excel']['tmp_name']);

	       //Obtem cotação do dolar, célula A1... [linha][coluna]
	       if (utf8_encode($data->sheets[0]['cells'][1][1]) == "Cotação Dolar"){
		    $cotacaoDolar = (double)$data->sheets[0]['cells'][2][1];
		    $compra->setDolar($cotacaoDolar);
	       }
	       
	       //Obtem impostoUSA
	       if (utf8_encode($data->sheets[0]['cells'][1][2]) == "Imposto USA"){
		    $impostoUSA = (double)$data->sheets[0]['cells'][2][2];
		    $compra->setImpostoUSA($impostoUSA);
	       }
	       
	       //Obtem IOF
	       if (utf8_encode($data->sheets[0]['cells'][1][3]) == "IOF"){
		    $IOF = (double)$data->sheets[0]['cells'][2][3];
		    $compra->setIOF($IOF);
	       }
	       	       	       
	       //Cadastra parametros
	       $dao->cadastraParametros($compra);

	       
	       $qtdColunas = $data->sheets[0]['numCols'];
	       $qtdLinhas = $data->sheets[0]['numRows'];
	       
	       //Limpa lista
	       $dao->limpaListaCompras();
	       
//	       //Percorrendo os valores a partir da 5 linha
	       for ($i = 5; $i <= $qtdLinhas; $i++) {		    
		    $compra = new Compra();
		    
//		    $compra->setTipo(utf8_encode($data->sheets[0]['cells'][$i][1]));
//		    $compra->setSexo(utf8_encode($data->sheets[0]['cells'][$i][2]));
//		    $compra->setProduto(utf8_encode($data->sheets[0]['cells'][$i][3]));
//		    $compra->setMarca(utf8_encode($data->sheets[0]['cells'][$i][4]));
//		    $compra->setModelo(utf8_encode($data->sheets[0]['cells'][$i][5]));
//		    $compra->setCor(utf8_encode($data->sheets[0]['cells'][$i][6]));
//		    $compra->setQuantidade((int)$data->sheets[0]['cells'][$i][7]);
//		    $compra->setTamanho($data->sheets[0]['cells'][$i][8]);
//		    $compra->setValorUSA((double)$data->sheets[0]['cells'][$i][9]);
//		    $compra->setValorBR((double)$data->sheets[0]['cells'][$i][10]);
//		    $compra->setMarkup($data->sheets[0]['cells'][$i][11]);
//		    $compra->setVendaBR((double)$data->sheets[0]['cells'][$i][12]);
//		    $compra->setLucroBR((double)$data->sheets[0]['cells'][$i][13]);
		    
		    $compra->setMarca(utf8_encode($data->sheets[0]['cells'][$i][1]));
		    $compra->setSexo(utf8_encode($data->sheets[0]['cells'][$i][2]));
		    $compra->setTipo(utf8_encode($data->sheets[0]['cells'][$i][3]));		    
		    $compra->setProduto(utf8_encode($data->sheets[0]['cells'][$i][4]));
		    $compra->setModelo(utf8_encode($data->sheets[0]['cells'][$i][5]));
		    $compra->setCor(utf8_encode($data->sheets[0]['cells'][$i][6]));
		    $compra->setTamanho($data->sheets[0]['cells'][$i][7]);
		    $compra->setQuantidade((int)$data->sheets[0]['cells'][$i][8]);
		    
		    
		    $dao->cadastraListaCompra($compra);
	       }
	       
	       ?>
	       <script>
		    //Planilha importada com sucesso		    
		    $("#sucesso_planilha").fadeIn("slow").delay(1000).fadeOut(1000, function(){
			 location.href = "lista-compras.php";
		    });		    
	       </script>
	       <?php
	  }
	  //Não é .xls
	  else{
	       ?>
	       <script>		    
		    $("#formato_invalido").fadeIn("slow");			 		    		    
	       </script>
	       <?php
	  }
     }

