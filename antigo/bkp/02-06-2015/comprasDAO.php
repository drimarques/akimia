<?php

     class comprasDAO {
	  
	  function getImagemComprado($comprado, $id_item){
	       if($comprado == '1'){
		    return "<img src='img/unlike.png' alt='Marcar como não comprado' onclick=marcaNaoComprado(\"$id_item\")>";
	       }
	       else{		    		    
		    return "<img src='img/like.png' alt='Marcar como comprado' onclick=marcaComprado(\"$id_item\")>";
	       }
	  }
	  
	  function getImagemCompradoBusca($comprado, $id_item, $busca){
	       if($comprado == '1'){
		    return "<img src='img/like.png' alt='Marcar como não comprado' onclick=\"marcaBuscaNaoComprado('$id_item', '$busca')\">";
	       }
	       else{		    		    
		    return "<img src='img/unlike.png' alt='Marcar como não comprado' onclick=\"marcaBuscaComprado('$id_item', '$busca')\">";
	       }
	  }
	  	  
	  function cadastraParametros(Compra $compra){	       
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       	       
	       $conn->query("DELETE FROM parametros");
	       $conn->query("INSERT INTO parametros (cotacaoDolar, impostoUSA, IOF) VALUES ('{$compra->Dolar}', '{$compra->ImpostoUSA}', '{$compra->IOF}')");
	       
	       unset($conn);
	  }
	  
	  function cadastraListaCompra(Compra $compra){
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       
	       $conn->query("INSERT INTO lista_compras (tipo, sexo, produto, marca, modelo, cor, quantidade, tamanho)
	       VALUES ('{$compra->Tipo}', '{$compra->Sexo}', '{$compra->Produto}', '{$compra->Marca}', '{$compra->Modelo}', '{$compra->Cor}', '{$compra->Quantidade}', '{$compra->Tamanho}')");
	       
	       unset($conn);
	  }
	  
	  function limpaListaCompras(){
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       
	       $conn->query("DELETE FROM lista_compras");
	       unset($conn);
	  }
	  
	  function listaCompras(){	       	       
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();	       
	       $retorno = $conn->query("SELECT * FROM lista_compras ORDER BY marca, produto asc");
	       
	       $tabela = "<table class='table-responsive table-hover tabelas2' id='table-lista-compras'>";
//	       $tabela .= '<thead>
//			      <tr>
//				 <th>Tipo</th>
//				 <th>Sexo</th>
//				 <th>Produto</th>
//				 <th>Marca</th>
//				 <th>Modelo</th>
//				 <th>Cor</th>
//				 <th>Quantidade</th>
//				 <th>Tamanho</th>
//				 <th>Valor USA</th>
//				 <th>Valor BR</th>
//				 <th>Markup</th>
//				 <th>Venda BR</th>
//				 <th>Lucro BR</th>			      
//			      </tr>
//			   </thead>';
	       
	       	       $tabela .= '<thead>
			      <tr>
				   <th>Marca</th>
				   <th>Sexo</th>
				   <th>Tipo</th>				   
				   <th>Produto</th>				 
				   <th>Modelo</th>
				   <th>Cor</th>
				   <th>Tamanho</th>
				   <th>Quantidade</th>				   			      
			      </tr>
			   </thead>';
	       
	       while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
//		    $tabela .= '<tr>
//				   <td>' . $linhas[tipo] . '</td>
//				   <td>' . $linhas[sexo] . '</td>
//				   <td>' . $linhas[produto] . '</td>
//				   <td>' . $linhas[marca] . '</td>
//				   <td>' . $linhas[modelo] . '</td>
//				   <td>' . $linhas[cor] . '</td>
//				   <td>' . $linhas[quantidade] . '</td>
//				   <td>' . $linhas[tamanho] . '</td>
//				   <td>$ '.number_format($linhas[valorUSA], 2, ',', '.').'</td>					
//				   <td>R$ '.number_format($linhas[valorBR], 2, ',', '.').'</td>
//				   <td>'.number_format($linhas[markup], 2, ',', '.').' %</td>
//				   <td>R$ '.number_format($linhas[vendaBR], 2, ',', '.').'</td>
//				   <td>R$ '.number_format($linhas[lucroBR], 2, ',', '.').'</td>
//			      </tr>';
		    
		     $tabela .= '<tr>
				   <td>' . $linhas[marca] . '</td>
				   <td>' . $linhas[sexo] . '</td>
				   <td>' . $linhas[tipo] . '</td>
				   <td>' . $linhas[produto] . '</td>				   
				   <td>' . $linhas[modelo] . '</td>
				   <td>' . $linhas[cor] . '</td>				   
				   <td>' . $linhas[tamanho] . '</td>
				   <td>' . $linhas[quantidade] . '</td>
			        </tr>';
	       }
	       
	       $tabela .= "</table>";
	       unset($conn);
	       
	       echo $tabela;
	  }
	  
	  function listaParametros(){	       	       
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();	       
	       $retorno = $conn->query("SELECT * FROM parametros");
	       
	       $tabela = '<table class="table-responsive table-hover tabelas2" id="table-parametros">';
	       $tabela .= "<thead>
			      <tr>
				   <th>Cotação Dólar</th>
				   <th>Imposto USA</th>
				   <th>IOF</th>		      
			      </tr>
			 </thead>
			 <tbody>";
	       
	       while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
		    $tabela .= '<tr>				   
				   <td>R$ '.number_format($linhas[cotacaoDolar], 2, ',', '.').'</td>
				   <td>'.number_format($linhas[impostoUSA], 2, ',', '.').' %</td>
				   <td>'.number_format($linhas[IOF], 2, ',', '.').' %</td>
			      </tr>';
	       }
	       
	       $tabela .= "</tbody>
			 </table>";
	       
	       unset($conn);
	       
	       echo $tabela;
	  }
	  	  	  	  
	  function listaComprados($sexo){
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();	       
	       $marcas = $conn->query("SELECT distinct marca FROM lista_compras WHERE sexo = '$sexo' and comprado = '1' ORDER BY marca ASC");
	       	       	       	       
	       //Verifica se existe algo comprado	       	       
	       if($marcas->rowCount() > 0) {
		    $tabela = "<table>";
		    $tabela .= '<thead>
			      <tr>
				   <th>Sexo</th>
				   <th>Tipo</th>				   
				   <th>Produto</th>				 
				   <th>Modelo</th>
				   <th>Cor</th>
				   <th>Tamanho</th>
				   <th>Quantidade</th>
				   <th colspan="2">Comprado?</th>
			      </tr>
			   </thead>
			   <tbody>';

		    //Percorrendo as marcas e montando as tabelas
		    while ($marca = $marcas->fetch(PDO::FETCH_ASSOC)) {			 
//			 <td colspan='9'><div class='marca' onclick=\"location.href='compras.php?marca=$marca[marca]'\">$marca[marca]</div></td>
			 $tabela.= "<tr>
					<td colspan='9'><div class='marca'>$marca[marca]</div></td>
				   </tr>";
			 $tabela .= "<tr><td colspan='9'><hr></td></tr>";
			 $retorno = $conn->query("SELECT * FROM lista_compras WHERE marca = '$marca[marca]' AND comprado = '1' and sexo = '$sexo'");
			 while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {

				   $dao = new comprasDAO();
				   $imagem = $dao->getImagemComprado($linhas[comprado], $linhas[id_item]);			      			 

				   $tabela .= '<tr>				   
						  <td>'.$linhas[sexo].'</td>
						  <td>'.$linhas[tipo].'</td>
						  <td>'.$linhas[produto].'</td>
						  <td>'.$linhas[modelo].'</td>
						  <td>'.$linhas[cor].'</td>
						  <td>'.$linhas[tamanho].'</td>
						  <td>'.$linhas[quantidade].'</td>
						  <td>'.$imagem.'</td>
					       </tr>';
				   $tabela .= "<tr><td colspan='9'><hr></td></tr>";
			 }
		    }

		    $tabela .= "</tbody>
			     </table>";
		    echo $tabela;
	       }
	       else{
		    echo "";
	       }
	       	       	       	       	       
	       unset($conn);
	  }
	  
	  function listaNaoComprados($sexo){
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();	       
	       $marcas = $conn->query("SELECT distinct marca FROM lista_compras WHERE sexo = '$sexo' and comprado = '0' ORDER BY marca ASC");
	       
	       //Verifica se existe algo não comprado
	       if($marcas->rowCount() > 0) {
	       
		    $tabela = "<table>";
		    $tabela .= '<thead>
			      <tr>				   
				   <th>Sexo</th>
				   <th>Tipo</th>				   
				   <th>Produto</th>				 
				   <th>Modelo</th>
				   <th>Cor</th>
				   <th>Tamanho</th>
				   <th>Quantidade</th>
				   <th colspan="2">Comprado?</th>
			      </tr>
			   </thead>
			   <tbody>';

		    //Percorrendo as marcas e montando as tabelas
		    while ($marca = $marcas->fetch(PDO::FETCH_ASSOC)) {			 
//			 <td colspan='9'><div class='marca' onclick=\"location.href='compras.php?marca=$marca[marca]'\">$marca[marca]</div></td>
			 $tabela.= "<tr>
					<td colspan='9'><div class='marca'>$marca[marca]</div></td>
				   </tr>";
			 $tabela .= "<tr><td colspan='9'><hr></td></tr>";
			 $retorno = $conn->query("SELECT * FROM lista_compras WHERE marca = '$marca[marca]' AND comprado = '0' and sexo = '$sexo'");
			 while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {

				   $dao = new comprasDAO();
				   $imagem = $dao->getImagemComprado($linhas[comprado], $linhas[id_item]);			      			 

				   $tabela .= '<tr>				   
						  <td>'.$linhas[sexo].'</td>
						  <td>'.$linhas[tipo].'</td>
						  <td>'.$linhas[produto].'</td>
						  <td>'.$linhas[modelo].'</td>
						  <td>'.$linhas[cor].'</td>
						  <td>'.$linhas[tamanho].'</td>
						  <td>'.$linhas[quantidade].'</td>
						  <td>'.$imagem.'</td>
					       </tr>';
				   $tabela .= "<tr><td colspan='9'><hr></td></tr>";
			 }
		    }

		    $tabela .= "</tbody>
			     </table>";
		    echo $tabela;

		    unset($conn);
	       }
	       else{
		    echo "";
	       }
	  }
	  
	  function listaCompradosParc(){
	       echo "AINDA EM DESENVOLVIMENTO...";
	  }
	  
	  function listaComprasBusca($busca){
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();	       
	       
	       //Monta SQL de busca
	       $sql = "SELECT * FROM lista_compras WHERE ";
	       $sql .= "tipo LIKE '%$busca%' OR ";
	       $sql .= "sexo LIKE '%$busca%' OR ";
	       $sql .= "produto LIKE '%$busca%' OR ";
	       $sql .= "marca LIKE '%$busca%' OR ";
	       $sql .= "modelo LIKE '%$busca%' OR ";
	       $sql .= "cor LIKE '%$busca%' OR ";	       
	       $sqlLike = substr($sql, 0, -4);
	       	       
	       $retorno = $conn->query($sqlLike);
	       
	       $totalRegistros = $retorno->rowCount();

	       /* Se tiverem registros encontrados, mostra a tabela */
	       if ($totalRegistros > 0) {
		    $tabela = "<table>";
		    $tabela .= '<thead>
				   <tr>
					<th>Marca</th>			      
					<th>Sexo</th>
					<th>Tipo</th>				   
					<th>Produto</th>				 
					<th>Modelo</th>
					<th>Cor</th>
					<th>Tamanho</th>
					<th>Quantidade</th>
					<th colspan="2">Comprado?</th>
				   </tr>
				</thead>
				<tbody>';
		    
		    while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
			 $dao = new comprasDAO();
			 $imagem = $dao->getImagemCompradoBusca($linhas[comprado], $linhas[id_item], $busca);			 

			 $tabela .= '<tr>
					<td>'.$linhas[marca].'</td>
					<td>'.$linhas[sexo].'</td>
					<td>'.$linhas[tipo].'</td>
					<td>'.$linhas[produto].'</td>
					<td>'.$linhas[modelo].'</td>
					<td>'.$linhas[cor].'</td>
					<td>'.$linhas[tamanho].'</td>
					<td>'.$linhas[quantidade].'</td>
					<td>'.$imagem.'</td>
				     </tr>';
			 $tabela .= "<tr><td colspan='9'><hr></td></tr>";
			 }
		    		    
		    $tabela .= "</tbody>
			     </table>";
		    echo $tabela;
	       }
	       else{
		    echo "";
	       }
	  }
	  
	  function marcaComprado($id_item){
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       	       
	       $conn->query("UPDATE lista_compras SET comprado = 1 WHERE id_item = '$id_item'");	       
	       
	       unset($conn);
	  }
	  
	  function marcaNaoComprado($id_item){
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       	       
	       $conn->query("UPDATE lista_compras SET comprado = 0 WHERE id_item = '$id_item'");	       
	       
	       unset($conn);
	  }
	  
	  function getDadosCompra($id_item){	       	       
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       	       
	       $retorno = $conn->query("SELECT * FROM lista_compras WHERE id_item = '$id_item'");	       
	       $linhas = $retorno->fetch(PDO::FETCH_ASSOC);
	       
	       $compra = new Compra();
	       
	       $compra->setTipo($linhas[tipo]);
	       $compra->setSexo($linhas[sexo]);
	       $compra->setProduto($linhas[produto]);
	       $compra->setMarca($linhas[marca]);
	       $compra->setModelo($linhas[modelo]);
	       $compra->setCor($linhas[cor]);
	       $compra->setQuantidade($linhas[quantidade]);
	       $compra->setTamanho($linhas[tamanho]);	       
	       	       
	       unset($conn);
	       return $compra;
	  }
     }
     