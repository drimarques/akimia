function verificaFiltroPrimeiroNivel() {
     if (($("#btn_n_comprados").hasClass('filtro_selecionado').toString()) === "false" && ($("#btn_comprados").hasClass('filtro_selecionado').toString() === "false")) {
	  //Some com os titulos
	  $("#abre_sexo").fadeOut();
	  $("#abre_marcas").fadeOut();

	  //Remove todos os filtros	       
	  $(".filtro_sexo").removeClass('sexo_selecionado');
	  $(".filtro_sexo").fadeOut();
	  $(".filtro_marca").removeClass('marca_selecionada');
	  $(".filtro_marca").fadeOut();
     }
}

function verificaFiltroSexo() {
     if (($("#btn_masculino").hasClass('sexo_selecionado').toString()) === "false" && ($("#btn_feminino").hasClass('sexo_selecionado').toString()) === "false" && ($("#btn_unissex").hasClass('sexo_selecionado').toString()) === "false") {
	  $("#abre_marcas").fadeOut();
	  $(".filtro_marca").removeClass('marca_selecionada');
	  $("#espaco_marcas").html(null);
	  $("#abre_tipo").fadeOut();
	  $(".filtro_tipo").removeClass('tipo_selecionado');
	  $("#espaco_tipos").html(null);
     }
     else {
	  $("#abre_marcas").fadeIn();
     }
}

function verificaFiltroMarcas() {
     var verificador = false;
     $(".marca_selecionada").each(function () {
	  verificador = true;
     });

     if (verificador === true) {
	  $("#abre_tipo").fadeIn();
	  $("#abre_tipo").focus();
     }
     else {
	  $("#abre_tipo").fadeOut();
	  $(".filtro_tipo").removeClass('tipo_selecionado');
	  $("#espaco_tipos").html(null);
     }
}




