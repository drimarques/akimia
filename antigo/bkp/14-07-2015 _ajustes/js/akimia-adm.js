


$(document).ready(function () {
     $("#settings").click(function () {
	  $("#settings_expand").fadeToggle("fast");
     });


     //Fazendo abrir a caixa de selecao de arquivo
     $(".link_importar").click(function () {
	  var botao = document.getElementById("arquivo_excel");
	  botao.click();
     });


     //Fazendo auto submit apos selecionar um arquivo no inputtype file
     document.getElementById("arquivo_excel").onchange = function () {
	  var botao = document.getElementById("btnEnviarExcel");
	  botao.click();
     };
});