<?php

     class parametrosDAO {

	  function listaParametros() {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $retorno = $conn->query("SELECT * FROM parametros_calculo");

	       $tabela = '<table class="table-responsive table-hover tabelas2" id="table-parametros">';
	       $tabela .= "<thead>
			      <tr>
				   <th>Cotação Dólar</th>
				   <th>Imposto USA</th>
				   <th>IOF</th>		      
			      </tr>
			 </thead>
			 <tbody>";

	       while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
		    $tabela .= '<tr>				   
				   <td>R$ ' . number_format($linhas[cotacaoDolar], 2, ',', '.') . '</td>
				   <td>' . number_format($linhas[impostoUSA], 2, ',', '.') . ' %</td>
				   <td>' . number_format($linhas[IOF], 2, ',', '.') . ' %</td>
			      </tr>';
	       }

	       $tabela .= "</tbody>
			 </table>";

	       unset($conn);

	       echo $tabela;
	  }

	  function listaParametrosFechamento() {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $retorno = $conn->query("SELECT * FROM parametros_calculo");

	       $tabela = '<table class="table-responsive table-hover tabelas2" id="table-parametros-fechamento">';
	       $tabela .= "<thead>
			      <tr>
				   <th>Cotação Dólar</th>
				   <th>Imposto USA</th>
				   <th>IOF</th>
				   <th>Despesa de Importação</th>
			      </tr>
			 </thead>
			 <tbody>";

	       if ($retorno->rowCount() > 0) {
		    while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
			 $tabela .= '<tr>				   
				   <td><input type="text" class="dolar form-control" value=' . $linhas[cotacaoDolar] . ' id="cotacao-dolar"></td>
				   <td><input type="text" class="porcentagem form-control" value=' . $linhas[impostoUSA] . ' id="imposto-usa"></td>
				   <td><input type="text" class="porcentagem form-control" value=' . $linhas[IOF] . ' id="IOF"></td>
				   <td><input type="text" class="real form-control" value=' . $linhas[despesa_viagem] . ' id="despesa_imp"></td>
			      </tr>';
		    }
	       } else {
		    $tabela .= '<tr>				   
				   <td><input type="text" class="dolar form-control" id="cotacao-dolar"></td>
				   <td><input type="text" class="porcentagem form-control" id="imposto-usa"></td>
				   <td><input type="text" class="porcentagem form-control" id="IOF"></td>
				   <td><input type="text" class="real form-control" id="despesa_imp"></td>
			      </tr>';
	       }


	       $tabela .= "</tbody>
			 </table>";

	       unset($conn);

	       echo $tabela;
	  }

	  function temParametros() {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $retorno = $conn->query("SELECT * FROM parametros_calculo");

	       if ($retorno->rowCount() > 0) {
		    unset($conn);
		    return true;
	       } else {
		    unset($conn);
		    return false;
	       }
	  }

	  function upd_cotacaoDolar($valor) {
	       echo $valor;
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $conn->query("UPDATE parametros_calculo SET cotacaoDolar = '$valor'");
	       unset($conn);
	  }

	  function ins_cotacaoDolar($valor) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $conn->query("INSERT INTO parametros_calculo (cotacaoDolar) VALUES ('$valor')");
	       unset($conn);
	  }

	  function upd_impostoUsa($valor) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $conn->query("UPDATE parametros_calculo SET impostoUSA = '$valor'");
	       unset($conn);
	  }

	  function ins_impostoUsa($valor) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $conn->query("INSERT INTO parametros_calculo (impostoUSA) VALUES ('$valor')");
	       unset($conn);
	  }

	  function upd_IOF($valor) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $conn->query("UPDATE parametros_calculo SET IOF = '$valor'");
	       unset($conn);
	  }

	  function ins_IOF($valor) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $conn->query("INSERT INTO parametros_calculo (IOF) VALUES ('$valor')");
	       unset($conn);
	  }

	  function ins_despesaImportacao($valor) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $conn->query("INSERT INTO parametros_calculo (despesa_viagem) VALUES ('$valor')");
	       unset($conn);
	  }

	  function upd_despesaImportacao($valor) {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();
	       $conn->query("UPDATE parametros_calculo SET despesa_viagem = '$valor'");
	       unset($conn);
	  }
	  
	  function getDadosParametro () {
	       $conexao = new Conexao();
	       $conn = $conexao->getConexao();

	       $retorno = $conn->query("SELECT * FROM parametros_calculo");
	       $linhas = $retorno->fetch(PDO::FETCH_ASSOC);

	       $parametro = new ParametrosCalculo();
	       $parametro->SetIdParametro($linhas[idParametro]);
	       $parametro->SetCotacaoDolar($linhas[cotacaoDolar]);
	       $parametro->SetImpostoUSA($linhas[impostoUSA]);
	       $parametro->SetIOF($linhas[IOF]);
	       $parametro->SetDespesaViagem($linhas[despesa_viagem]);	       

	       unset($conn);
	       return $parametro;	       
	  }

     }
     