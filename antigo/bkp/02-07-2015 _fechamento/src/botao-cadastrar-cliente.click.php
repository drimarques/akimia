<?php
     if (isset($_POST['btnCadastrarCliente'])) {
	  $cliente = new Cliente();

	  $nome = filter_input(INPUT_POST, 'nomeCadastro');
	  $apelido = filter_input(INPUT_POST, 'apelidoCadastro');
	  $email = filter_input(INPUT_POST, 'emailCadastro');
	  $whatsapp = filter_input(INPUT_POST, 'whatsAppCadastro');
	  $telefone = filter_input(INPUT_POST, 'telCadastro');
	  $celular = filter_input(INPUT_POST, 'celCadastro');
	  $CEP = filter_input(INPUT_POST, 'cepCadastro');
	  $endereco = filter_input(INPUT_POST, 'enderecoCadastro');
	  $numEndereco = filter_input(INPUT_POST, 'numEnderecoCadastro');
	  $complemento = filter_input(INPUT_POST, 'complementoCadastro');
	  $referencia = filter_input(INPUT_POST, 'referenciaCadastro');
	  $UF = filter_input(INPUT_POST, 'estadoCadastro');
	  $cidade = filter_input(INPUT_POST, 'cidadeCadastro');
	  $bairro = filter_input(INPUT_POST, 'bairroCadastro');

	  $erroCadastroCliente = false;

	  #Verifica se já existe um cliente cadastrado com esse e-mail
	  # A função getEmailExistente($email) retorna TRUE se já existir um e-mail cadastrado
	  if ($cliente->getEmailCadastrado($email)) {
	       $erroCadastroCliente = true;
	       echo "<div class='alert alert-danger' role='alert'>Este e-mail já foi cadastrado.</div>";
	       ?>
	       <script>
	            cachCamposFormCadastro("<?php echo $nome ?>", "<?php echo $apelido ?>", "<?php echo $email ?>", "<?php echo $whassapp ?>", "<?php echo $telefone ?>", "<?php echo $celular ?>", "<?php echo $CEP ?>", "<?php echo $endereco ?>", "<?php echo $numEndereco ?>", "<?php echo $complemento ?>", "<?php echo $referencia ?>", "<?php echo $UF ?>", "<?php echo $cidade ?>", "<?php echo $bairro ?>");
	            $('#emailCadastro').addClass('destaqueCampo');
	       </script>
	       <?php
	  }
	  //Senão existe faz todas as validações pra um novo cadastro
	  else {
	       //	  echo $nome		.'<br>';
	       //	  echo $apelido .'<br>';
	       //	  echo $email	.'<br>';
	       //	  echo $whassapp	.'<br>';
	       //	  echo $telefone	.'<br>';
	       //	  echo $celular	.'<br>';
	       //	  echo $CEP		.'<br>';
	       //	  echo $endereco	.'<br>';
	       //	  echo $numEndereco	.'<br>';
	       //	  echo $complemento	.'<br>';
	       //	  echo $referencia	.'<br>';
	       //	  echo $UF		.'<br>';
	       //	  echo $cidade	.'<br>';
	       //	  echo $bairro	.'<br>';
	       #Tratamento de Erros
	       #Campo Nome
	       //Se nome não é nulo e não é um número
	       if ((!empty($nome)) && (!is_numeric($nome))) {
		    $cliente->SetNome($nome);
	       } else {
		    $erroCadastroCliente = true;
		    echo "<div class='alert alert-danger' role='alert'>Nome inválido.</div>";
		    ?>
		    <script>
		         cachCamposFormCadastro("<?php echo $nome ?>", "<?php echo $apelido ?>", "<?php echo $email ?>", "<?php echo $whassapp ?>", "<?php echo $telefone ?>", "<?php echo $celular ?>", "<?php echo $CEP ?>", "<?php echo $endereco ?>", "<?php echo $numEndereco ?>", "<?php echo $complemento ?>", "<?php echo $referencia ?>", "<?php echo $UF ?>", "<?php echo $cidade ?>", "<?php echo $bairro ?>");
		         $('#nomeCadastro').addClass('destaqueCampo');
		    </script>
		    <?php
	       }

	       #Campo e-mail
	       /* Se e-mail nao for nulo */
	       if (!empty($email)) {
		    /* Se não é um formato de e-mail */
		    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			 $erroCadastroCliente = true;
			 echo "<div class='alert alert-danger' role='alert'>E-mail inválido.</div>";
			 ?>
			 <script>
			      cachCamposFormCadastro("<?php echo $nome ?>", "<?php echo $apelido ?>", "<?php echo $email ?>", "<?php echo $whassapp ?>", "<?php echo $telefone ?>", "<?php echo $celular ?>", "<?php echo $CEP ?>", "<?php echo $endereco ?>", "<?php echo $numEndereco ?>", "<?php echo $complemento ?>", "<?php echo $referencia ?>", "<?php echo $UF ?>", "<?php echo $cidade ?>", "<?php echo $bairro ?>");
			      $('#emailCadastro').addClass('destaqueCampo');
			 </script>
			 <?php
		    } else {
			 $cliente->SetEmail($email);
		    }
	       }
	       //Se e-mail for nulo
	       else {
		    $erroCadastroCliente = true;
		    echo "<div class='alert alert-danger' role='alert'>Por favor, informe um e-mail.</div>";
		    ?>
		    <script>
		         cachCamposFormCadastro("<?php echo $nome ?>", "<?php echo $apelido ?>", "<?php echo $email ?>", "<?php echo $whassapp ?>", "<?php echo $telefone ?>", "<?php echo $celular ?>", "<?php echo $CEP ?>", "<?php echo $endereco ?>", "<?php echo $numEndereco ?>", "<?php echo $complemento ?>", "<?php echo $referencia ?>", "<?php echo $UF ?>", "<?php echo $cidade ?>", "<?php echo $bairro ?>");
		         $('#emailCadastro').addClass('destaqueCampo');
		    </script>
		    <?php
	       }

	       #Campo CEP
	       //Se CEP não é nulo, tem que ter tudo 
	       if (!empty($CEP)) {
		    $cliente->SetCEP($CEP);

		    #Campo Endereço
		    //Se Endereço não é nulo
		    if (!empty($endereco)) {
			 $cliente->SetEndereco($endereco);
		    } else {
			 $erroCadastroCliente = true;
			 echo "<div class='alert alert-danger' role='alert'>Por favor, informe o seu endereço</div>";
			 ?>
			 <script>
			      cachCamposFormCadastro("<?php echo $nome ?>", "<?php echo $apelido ?>", "<?php echo $email ?>", "<?php echo $whassapp ?>", "<?php echo $telefone ?>", "<?php echo $celular ?>", "<?php echo $CEP ?>", "<?php echo $endereco ?>", "<?php echo $numEndereco ?>", "<?php echo $complemento ?>", "<?php echo $referencia ?>", "<?php echo $UF ?>", "<?php echo $cidade ?>", "<?php echo $bairro ?>");
			      $('#enderecoCadastro').addClass('destaqueCampo');
			 </script>
			 <?php
		    }


		    #Campo Número
		    //Se Número não é nulo e é um número
		    if (!empty($numEndereco) && is_numeric($numEndereco)) {
			 $cliente->SetNumeroEndereco($numEndereco);
		    } else {
			 $erroCadastroCliente = true;
			 echo "<div class='alert alert-danger' role='alert'>Por favor, informe o número de sua residência.</div>";
			 ?>
			 <script>
			      cachCamposFormCadastro("<?php echo $nome ?>", "<?php echo $apelido ?>", "<?php echo $email ?>", "<?php echo $whassapp ?>", "<?php echo $telefone ?>", "<?php echo $celular ?>", "<?php echo $CEP ?>", "<?php echo $endereco ?>", "<?php echo $numEndereco ?>", "<?php echo $complemento ?>", "<?php echo $referencia ?>", "<?php echo $UF ?>", "<?php echo $cidade ?>", "<?php echo $bairro ?>");
			      $('#numEnderecoCadastro').addClass('destaqueCampo');
			      $('#numEnderecoCadastro').prop('readonly', false);
			 </script>
			 <?php
		    }

		    #Campo Estado
		    //Se Estado não é nulo
		    if (!empty($UF)) {
			 $cliente->SetUF($UF);
		    } else {
			 $erroCadastroCliente = true;
			 echo "<div class='alert alert-danger' role='alert'>Por favor, informe o seu estado.</div>";
			 ?>
			 <script>
			      cachCamposFormCadastro("<?php echo $nome ?>", "<?php echo $apelido ?>", "<?php echo $email ?>", "<?php echo $whassapp ?>", "<?php echo $telefone ?>", "<?php echo $celular ?>", "<?php echo $CEP ?>", "<?php echo $endereco ?>", "<?php echo $numEndereco ?>", "<?php echo $complemento ?>", "<?php echo $referencia ?>", "<?php echo $UF ?>", "<?php echo $cidade ?>", "<?php echo $bairro ?>");
			      $('#estadoCadastro').addClass('destaqueCampo');
			 </script>
			 <?php
		    }


		    #Campo Cidade
		    //Se Cidade não é nulo
		    if (!empty($cidade)) {
			 $cliente->SetCidade($cidade);
		    } else {
			 $erroCadastroCliente = true;
			 echo "<div class='alert alert-danger' role='alert'>Por favor, informe o sua cidade.</div>";
			 ?>
			 <script>
			      cachCamposFormCadastro("<?php echo $nome ?>", "<?php echo $apelido ?>", "<?php echo $email ?>", "<?php echo $whassapp ?>", "<?php echo $telefone ?>", "<?php echo $celular ?>", "<?php echo $CEP ?>", "<?php echo $endereco ?>", "<?php echo $numEndereco ?>", "<?php echo $complemento ?>", "<?php echo $referencia ?>", "<?php echo $UF ?>", "<?php echo $cidade ?>", "<?php echo $bairro ?>");
			      $('#cidadeCadastro').addClass('destaqueCampo');
			 </script>
			 <?php
		    }

		    #Campo Cidade
		    //Se Cidade não é nulo e é um número
		    if (!empty($bairro)) {
			 $cliente->SetBairro($bairro);
		    } else {
			 $erroCadastroCliente = true;
			 echo "<div class='alert alert-danger' role='alert'>Por favor, informe o seu bairro.</div>";
			 ?>
			 <script>
			      cachCamposFormCadastro("<?php echo $nome ?>", "<?php echo $apelido ?>", "<?php echo $email ?>", "<?php echo $whassapp ?>", "<?php echo $telefone ?>", "<?php echo $celular ?>", "<?php echo $CEP ?>", "<?php echo $endereco ?>", "<?php echo $numEndereco ?>", "<?php echo $complemento ?>", "<?php echo $referencia ?>", "<?php echo $UF ?>", "<?php echo $cidade ?>", "<?php echo $bairro ?>");
			      $('#bairroCadastro').addClass('destaqueCampo');
			 </script>
			 <?php
		    }


		    //Após verificar todos os campos, fazemos uma nova validação no CEP
		    $pagina = file_get_contents('http://www.clareslab.com.br/ws/cep/json/' . $cliente->GetCEP() . '/');
		    $json = json_decode(utf8_encode($pagina), true);

		    #Verifica se é um CEP válido
		    if ($pagina == '0') {
			 $erroCadastroCliente = true;
			 echo "<div class='alert alert-danger' role='alert'>CEP Inválido.</div>";
			 ?>
			 <script>
			      cachCamposFormCadastro("<?php echo $nome ?>", "<?php echo $apelido ?>", "<?php echo $email ?>", "<?php echo $whassapp ?>", "<?php echo $telefone ?>", "<?php echo $celular ?>", "<?php echo $CEP ?>", "<?php echo $endereco ?>", "<?php echo $numEndereco ?>", "<?php echo $complemento ?>", "<?php echo $referencia ?>", "<?php echo $UF ?>", "<?php echo $cidade ?>", "<?php echo $bairro ?>");
			      $('#cepCadastro').addClass('destaqueCampo');
			      $('#enderecoCadastro').val("");
			      $('#bairroCadastro').val("");
			      $('#cidadeCadastro').val("");
			      $('#estadoCadastro').val("");
			      $('#numEnderecoCadastro').val("");
			      $('#numEnderecoCadastro').prop('readonly', true);
			      $('#complementoCadastro').prop('readonly', true);
			      $('#referenciaCadastro').prop('readonly', true);
			 </script>
			 <?php
		    }
		    #Se for um CEP válido, verifica se todos os dados estão corretos. Se algum campo não está correto dá erro
		    else {
			 if (!($json['cep'] == $cliente->GetCEP()) && ($json['cidade'] == $cliente->GetCidade()) && ($json['bairro'] == $cliente->GetBairro()) && ($json['uf'] == $cliente->GetUF()) && ($json['endereco'] == $cliente->GetEndereco())) {
			      $erroCadastroCliente = true;
			      echo "<div class='alert alert-danger' role='alert'>CEP Inválido.</div>";
			      ?>
			      <script>
			           cachCamposFormCadastro("<?php echo $nome ?>", "<?php echo $apelido ?>", "<?php echo $email ?>", "<?php echo $whassapp ?>", "<?php echo $telefone ?>", "<?php echo $celular ?>", "<?php echo $CEP ?>", "<?php echo $endereco ?>", "<?php echo $numEndereco ?>", "<?php echo $complemento ?>", "<?php echo $referencia ?>", "<?php echo $UF ?>", "<?php echo $cidade ?>", "<?php echo $bairro ?>");
			           $('#cepCadastro').addClass('destaqueCampo');
			           $('#enderecoCadastro').val("");
			           $('#bairroCadastro').val("");
			           $('#cidadeCadastro').val("");
			           $('#estadoCadastro').val("");
			           $('#numEnderecoCadastro').val("");
			           $('#numEnderecoCadastro').prop('readonly', true);
			           $('#complementoCadastro').prop('readonly', true);
			           $('#referenciaCadastro').prop('readonly', true);
			      </script>
			      <?php
			 }
		    }
	       } 
//	       else {
//		    $erroCadastroCliente = true;
//		    echo "<div class='alert alert-danger' role='alert'>Por favor, informe um CEP.</div>";
//		    ?>
		    <script>
//		         cachCamposFormCadastro("<?php echo $nome ?>", "<?php echo $apelido ?>", "<?php echo $email ?>", "<?php echo $whassapp ?>", "<?php echo $telefone ?>", "<?php echo $celular ?>", "<?php echo $CEP ?>", "<?php echo $endereco ?>", "<?php echo $numEndereco ?>", "<?php echo $complemento ?>", "<?php echo $referencia ?>", "<?php echo $UF ?>", "<?php echo $cidade ?>", "<?php echo $bairro ?>");
//		         $('#cepCadastro').addClass('destaqueCampo');
		    </script>
		    <?php
//	       }



	       /* Setando os outros campos que não são obrigatórios */
	       $cliente->SetApelido($apelido);
	       $cliente->SetWhatsApp($whatsapp);
	       $cliente->SetTelefone($telefone);
	       $cliente->SetCelular($celular);
	       $cliente->SetComplemento($complemento);
	       $cliente->SetReferencia($referencia);

	       #Senão ocorrerem erros, cadastramos o usuário
	       if ($erroCadastroCliente == false) {
		    $cliente->cadastraCliente();
		    echo "<div class='alert alert-success' role='alert'>Cadastro realizado com sucesso.</div>";
	       }
	  }
	  ?>
	  <script>
	       //	       location.href = "index.php#bairroCadastro";
	       //	       selecionaMenuCadastro('mobile');
	       $("#btnCadastrarCliente").focus();
	  </script>
	  <?php
     }

