<?php

     if (isset($_POST['btnEnviarFaleConosco'])) {
	  $usuario	 = new Contato();
	  $enviarEmail	 = new EnviarEmail();

	  $nome		 = filter_input(INPUT_POST, 'nomeContato');
	  $email	 = filter_input(INPUT_POST, 'emailContato');
	  $assunto	 = filter_input(INPUT_POST, 'assuntoContato');
	  $mensagem	 = filter_input(INPUT_POST, 'mensagemContato');
	  
	  $erroEnvioEmail = false;

	  #Tratamento do campo nome
	  /* Não pode ser nulo e não pode ser um número */
	  if ((!empty($nome)) && (!is_numeric($nome))) {
	       $usuario->setNome($nome);
	  } 
	  else {
	       $erroEnvioEmail = true;
	       echo "<div class='alert alert-danger' role='alert'>Nome inválido.</div>";
	       ?>
	       <script>
		    cachCamposFormContato("<?php echo $nome ?>", "<?php echo $email ?>", "<?php echo $assunto ?>", "<?php echo $mensagem ?>");
		    $('#nomeContato').addClass('destaqueCampo');
	       </script>
	       <?php
	  }

	  #Campo e-mail
	  /* Se e-mail nao for nulo */
	  if (!empty($email)) {
	       /* Se não é um formato de e-mail */
	       if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		    $erroEnvioEmail = true;
		    echo "<div class='alert alert-danger' role='alert'>E-mail inválido.</div>";
		    ?>
		    <script>
			 cachCamposFormContato("<?php echo $nome ?>", "<?php echo $email ?>", "<?php echo $assunto ?>", "<?php echo $mensagem ?>");
			 $('#emailContato').addClass('destaqueCampo');
		    </script>
		    <?php
	       } 
	       else {
		    $usuario->setEmail($email);
	       }
	  }

	  #Campo assunto
	  /* Se assunto nao for nulo */
	  if (!empty($assunto)) {
	       $usuario->setAssunto($assunto);
	  }
	  else{
	       $erroEnvioEmail = true;
	       echo "<div class='alert alert-danger' role='alert'>Por favor, selecione um assunto.</div>";
	       ?>
	       <script>
		    cachCamposFormContato("<?php echo $nome ?>", "<?php echo $email ?>", "<?php echo $assunto ?>", "<?php echo $mensagem ?>");
		    $('#assuntoContato').addClass('destaqueCampo');
	       </script>
	       <?php
	  }
	  
	  #Campo mensagem
	  /* Se mensagem nao for nula */
	  if (!empty($mensagem)){
	       #Verifica se a mensagem tem menos de 5 caracteres
	       if(strlen($mensagem) < 5){
		    $erroEnvioEmail = true;
		    echo "<div class='alert alert-danger' role='alert'>Sua mensagem deve ter no mínimo 5 caracteres.</div>";
		    ?>
		    <script>
			 cachCamposFormContato("<?php echo $nome ?>", "<?php echo $email ?>", "<?php echo $assunto ?>", "<?php echo $mensagem ?>");
			 $('#mensagemContato').addClass('destaqueCampo');
		    </script>
		    <?php
	       }
	       else{
		    $usuario->setMensagem($mensagem);
	       }
	  }
	  //Se mensagem for nula
	  else{
	       $erroEnvioEmail = true;
	       echo "<div class='alert alert-danger' role='alert'>Por favor, digite uma mensagem.</div>";
	       ?>
	       <script>
		    cachCamposFormContato("<?php echo $nome ?>", "<?php echo $email ?>", "<?php echo $assunto ?>", "<?php echo $mensagem ?>");
		    $('#mensagemContato').addClass('destaqueCampo');
	       </script>
	       <?php
	  }
	  
	  /*Senão tiverem ocorrido erros, envia o e-mail*/
	  if($erroEnvioEmail == false){
	       $enviarEmail->enviaEmailMailer($usuario->getNome(), $usuario->getEmail(), $usuario->getAssunto(), $usuario->getMensagem());
	       echo "<div class='alert alert-success' role='alert'>E-mail enviado com sucesso!</div>";
	  }
	  
	 
//	  echo "Nome: {$usuario->Nome} <br>";
//	  echo "E-mail: {$usuario->Email} <br>";
//	  echo "Assunto: {$usuario->Assunto} <br>";
//	  echo "Mensagem: {$usuario->Mensagem} <br>";
	  ?>
	  <script>
	       selecionaMenuFaleConosco("mobile");
	       $( "#btnEnviarFaleConosco").focus();
	  </script>
	  <?php
     }