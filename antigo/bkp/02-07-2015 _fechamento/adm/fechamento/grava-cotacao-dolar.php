<?php
     require_once '../../src/classes/conexao.class.php';
     require_once '../../src/classes/compra.class.php';
     require_once '../../src/dao/comprasDAO.php';
     
     require_once '../../src/classes/parametros_calculo.class.php';
     require_once '../../src/dao/parametrosDAO.php';
     
     $valor = $_POST['valor'];     
     $valor = str_replace("$ ", " ", $valor);
     $valor = str_replace(",", "", $valor);
     
     $parametrosDAO = new parametrosDAO();
     if($parametrosDAO->temParametros()){
	  $parametrosDAO->upd_cotacaoDolar($valor);
     }
     else{
	  $parametrosDAO->ins_cotacaoDolar($valor);	  
     }     
     
