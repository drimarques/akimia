<!--
<?php
     require_once '../src/classes/conexao.class.php';               
     require_once '../src/classes/compra.class.php';
     require_once '../src/dao/comprasDAO.php';
     
     $dao = new comprasDAO();     
?>
-->
<!DOCTYPE html>
<html lang="pt-br">
     <head>
	  <title>Lista de compras - Akimia</title>
	  <meta charset="UTF-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	  <meta name="author" content="Adriano Marques">
	  <link rel="shortcut icon" href="../img/Logo.Akimia.transp.750x750.png">
	  <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	  <link rel="stylesheet" href="../bootstrap/bootstrap.min.css">
	  <link rel="stylesheet" href="../bootstrap/bootstrap-theme.min.css">
	  <link href="css/estilo.css" rel="stylesheet" type="text/css">
	  <script src="../jquery/jquery-2.1.3.min.js"></script>	  
     </head>
     <body>	  
	  <header>	       
	       <h1><img onclick="location.href = 'index.php'" src="../img/Logo.Akimia.transp.750x750.png" alt="Akimia">Lista de Compras</h1>
	  </header>
	  
	  <?php
	       $dao->listaParametros();
	       $dao->listaCompras();
	  ?>
	  

	  <script src="../jquery/jquery-2.1.3.min.js" type="text/javascript"></script>
	  <script src="../bootstrap/bootstrap.min.js" type="text/javascript"></script>
	  <!--Biblioteca do scrol suave, juntamente com jQuery-->
	  <script src="../jquery/jquery.nicescroll.min.js"></script> 
	  <script src="js/akimia-adm.js" type="text/javascript"></script>	 
     </body>
</html>


