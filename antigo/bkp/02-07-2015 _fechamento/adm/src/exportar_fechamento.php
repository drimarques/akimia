<?php
     date_default_timezone_set('America/Sao_Paulo');
     $data_hora = date("d-m-Y_H-i-s");
     
     require_once '../php_excel/Classes/PHPExcel.php';
     require_once '../../src/classes/conexao.class.php';
     

// Instanciamos a classe
     $objPHPExcel = new PHPExcel();

// Definimos o estilo da fonte
     $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
     
     $objPHPExcel->getActiveSheet()->getStyle('A4')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('B4')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('C4')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('D4')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('E4')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('F4')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('G4')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('H4')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('I4')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('J4')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('K4')->getFont()->setBold(true);

// Criamos as colunas
     $objPHPExcel->setActiveSheetIndex(0)
     ->setCellValue('A1', "Cotação Dolar")
     ->setCellValue("B1", "Imposto USA")
     ->setCellValue("C1", "IOF")
     ->setCellValue("D1", "Despesa com Importação")
     
     ->setCellValue('A4', "Marca")
     ->setCellValue("B4", "Sexo")
     ->setCellValue("C4", "Tipo")
     ->setCellValue("D4", "Produto")
     ->setCellValue("E4", "Modelo")
     ->setCellValue("F4", "Cor")
     ->setCellValue("G4", "Tamanho")
     ->setCellValue("H4", "Quantidade")
     ->setCellValue("I4", "Valor - Dolar(USD)")
     ->setCellValue("J4", "IOF")
     ->setCellValue("K4", "Venda");

// Podemos configurar diferentes larguras paras as colunas como padrão
     $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
     $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(18);


     $conexao = new Conexao();
     $conn = $conexao->getConexao();
          
     $parametros = $conn->query("SELECT * FROM parametros");
     $valores = $parametros->fetch(PDO::FETCH_ASSOC);
     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, $valores[cotacaoDolar]);
     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, $valores[impostoUSA]);
     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 2, $valores[IOF]);
          
     $retorno = $conn->query("SELECT * FROM lista_compras ORDER BY marca ASC");

     $contador = 5;
     while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
	  // Também podemos escolher a posição exata aonde o dado será inserido (coluna, linha, dado);	  
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $contador, $linhas[marca]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $contador, $linhas[sexo]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $contador, $linhas[tipo]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $contador, $linhas[produto]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $contador, $linhas[modelo]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $contador, $linhas[cor]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $contador, $linhas[tamanho]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $contador, $linhas[quantidade_total]);
	  $contador++;
     }
     

     unset($conn);                         

// Podemos renomear o nome das planilha atual, lembrando que um único arquivo pode ter várias planilhas
     $objPHPExcel->getActiveSheet()->setTitle('Lista de Compras - Akimia');

// Cabeçalho do arquivo para ele baixar
     header('Content-Type: application/vnd.ms-excel');
     header('Content-Disposition: attachment;filename="exportar_'.$data_hora.'.xls"');
     header('Cache-Control: max-age=0');
// Se for o IE9, isso talvez seja necessário
     header('Cache-Control: max-age=1');

// Acessamos o 'Writer' para poder salvar o arquivo
     $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

// Salva diretamente no output, poderíamos mudar arqui para um nome de arquivo em um diretório ,caso não quisessemos jogar na tela
     $objWriter->save('php://output');

     exit;
     
     
     