function abreModalQtdComprada() {
     var contador = 0;

     $(".item_naocomprado_selecionado").each(function () {
	  contador++;
     });

     if (contador === 0) {
	  $("#notificacao_lista_compras").finish();
	  $("#notificacao_lista_compras").text("Selecione um item não comprado.");
	  $("#notificacao_lista_compras").fadeIn("fast").delay(1000).fadeOut(200, function () {
	  });
     }
     else if (contador > 1) {
	  $("#notificacao_lista_compras").finish();
	  $("#notificacao_lista_compras").text("Selecione somente um item não comprado.");
	  $("#notificacao_lista_compras").fadeIn("fast").delay(1000).fadeOut(200, function () {
	  });
     }
     else {
	  $(".item_naocomprado_selecionado").each(function () {
	       var id_item = this.id;
	       $.ajax({
		    url: "compras/informa-qtd-comprada.php",
		    data: {id_item: id_item},
		    type: 'POST',
		    context: $('#info_item'),
		    success: function (retorno) { //success: function () {
			 $("#info_item").html(retorno);
			 $('#modalQuantidade').modal('show');
		    }
	       });
	  });
     }
}

function validaQuantidade() {
     var quantidade_comprada = $("#quantidade_comprada").val();     

     if ($.isNumeric(quantidade_comprada) && quantidade_comprada > 0) {	  
	  $(".item_naocomprado_selecionado").each(function () {
	       var id_item = this.id;
	       $.ajax({
		    url: "compras/upd-qtd-comprada.php",
		    data: {id_item: id_item, quantidade_comprada: quantidade_comprada},
		    type: 'POST',		    
		    success: function (retorno) { //success: function () {			 
			 if(retorno === "success"){
			      verificaLocalStorage();
			      
			      $("#notificacao_quantidade_comprada").removeClass('alert-danger');
			      $("#notificacao_quantidade_comprada").addClass('alert-success');
			      $("#notificacao_quantidade_comprada").text("Compra realizada com sucesso!");
			      $("#notificacao_quantidade_comprada").fadeIn("fast").delay(1000).fadeOut(200, function () {
//				   location.href='compras.php';
				   $("#quantidade_comprada").val(null);
				   $('#modalQuantidade').modal('toggle');
				   
			      });
			 }
			 else if(retorno === "error"){			      
			      $("#notificacao_quantidade_comprada").removeClass('alert-success');
			      $("#notificacao_quantidade_comprada").addClass('alert-danger');
			      $("#notificacao_quantidade_comprada").text("Ocorreu um erro inesperado.");
			      $("#notificacao_quantidade_comprada").fadeIn("fast");
			 }
		    }
	       });	       
	  });
     }
     else {	  	  
	  $("#quantidade_comprada").css({"border": "solid 1px red"});
	  $("#notificacao_quantidade_comprada").removeClass('alert-success');
	  $("#notificacao_quantidade_comprada").addClass('alert-danger');
	  $("#notificacao_quantidade_comprada").text("Por favor digite um número.");
	  $("#notificacao_quantidade_comprada").fadeIn("fast");	  
     }
}