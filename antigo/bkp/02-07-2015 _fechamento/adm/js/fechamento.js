function atualizaTabelaPorIdItem(id_item) {
     idValorRS = id_item + "valorbr";     
     $.ajax({
	  url: "getters-fechamento/get-valorRS.php",
	  data: {id_item: id_item},
	  type: 'POST',
	  context: $("#" + idValorRS),
	  success: function (retorno) {	       
	       $("#" + idValorRS).html(retorno);
	  }
     });

     idMarkup = id_item + "markup";
     $.ajax({
	  url: "getters-fechamento/get-markup.php",
	  data: {id_item: id_item},
	  type: 'POST',
	  context: $("#" + idMarkup),
	  success: function (retorno) {	       
	       $("#" + idMarkup).html(retorno);
	  }
     });
     
     idLucro = id_item + "lucro";
     $.ajax({
	  url: "getters-fechamento/get-lucro.php",
	  data: {id_item: id_item},
	  type: 'POST',
	  context: $("#" + idLucro),
	  success: function (retorno) {	       
	       $("#" + idLucro).html(retorno);
	  }
     });
     
     idCustoTotal = id_item + "custo_total";
     $.ajax({
	  url: "getters-fechamento/get-custoTotal.php",
	  data: {id_item: id_item},
	  type: 'POST',
	  context: $("#" + idCustoTotal),
	  success: function (retorno) {	       
	       $("#" + idCustoTotal).html(retorno);
	  }
     });
     
     idVendaTotal = id_item + "venda_total";
     $.ajax({
	  url: "getters-fechamento/get-vendaTotal.php",
	  data: {id_item: id_item},
	  type: 'POST',
	  context: $("#" + idVendaTotal),
	  success: function (retorno) {	       
	       $("#" + idVendaTotal).html(retorno);
	  }
     });
     
     idLucroTotal = id_item + "lucro_total";
     $.ajax({
	  url: "getters-fechamento/get-lucroTotal.php",
	  data: {id_item: id_item},
	  type: 'POST',
	  context: $("#" + idLucroTotal),
	  success: function (retorno) {	       
	       $("#" + idLucroTotal).html(retorno);
	  }
     });

}










function upd_valorDolarProduto(elemento) {
     var id_item = elemento.id;
     var valor_dolar = elemento.value;

     $.ajax({
	  url: "fechamento/grava-valor-dolar-compra.php",
	  data: {valor: valor_dolar, id_item: id_item},
	  type: 'POST',
//	  context: $('#teste'),
	  success: function (retorno) { //success: function () {		    
//	       $("#teste").html(relatorio);
	       atualizaTabelaPorIdItem(id_item);
	  }
     });

}

function upd_IOFProduto(elemento) {
     var iof = elemento.value;
     var id_item = elemento.id;

     $.ajax({
	  url: "fechamento/grava-iof-produto.php",
	  data: {iof: iof, id_item: id_item},
	  type: 'POST',
//	  context: $('#teste'),
	  success: function (retorno) {
//	       $("#teste").html(relatorio);
	       atualizaTabelaPorIdItem(id_item);
	  }
     });
}

function upd_valorVendaProduto(elemento) {
     var id_item = elemento.id;
     var valor_venda = elemento.value;

     $.ajax({
	  url: "fechamento/grava-valor-venda-compra.php",
	  data: {valor: valor_venda, id_item: id_item},
	  type: 'POST',
//	  context: $('#teste'),
	  success: function (retorno) {
//	       $("#teste").html(retorno);
	       atualizaTabelaPorIdItem(id_item);
	  }
     });

}

$(document).ready(function () {
     $(".dolar").maskMoney({showSymbol: false, symbol: "$ ", decimal: ".", thousands: ","});
     $(".real").maskMoney({showSymbol: false, symbol: "R$ ", decimal: ",", thousands: "."});
     $(".porcentagem").maskMoney({showSymbol: true, symbol: "% ", decimal: ",", thousands: "."});

     $("#cotacao-dolar").focusout(function () {
	  var cotacao_dolar = $("#cotacao-dolar").val();
	  $.ajax({
	       url: "fechamento/grava-cotacao-dolar.php",
	       data: {valor: cotacao_dolar},
	       type: 'POST',
	       success: function (retorno) { //success: function () {		    
		    //Apos alterar qualquer valor dos parametros, eu preciso atualizar todos os itens da lista de compras com base no valor novo
	       }
	  });
     });

     $("#imposto-usa").focusout(function () {
	  var imposto_usa = $("#imposto-usa").val();
	  $.ajax({
	       url: "fechamento/grava-imposto-usa.php",
	       data: {imposto_usa: imposto_usa},
	       type: 'POST',
//	       context: $('#teste'),
	       success: function (retorno) { //success: function () {		    

	       }
	  });
     });

     $("#IOF").focusout(function () {
	  var iof = $("#IOF").val();
	  $.ajax({
	       url: "fechamento/grava-iof.php",
	       data: {IOF: iof},
	       type: 'POST',
//	       context: $('#teste'),
	       success: function (retorno) { //success: function () {		    
		    
	       }
	  });
     });

     $("#despesa_imp").focusout(function () {
	  var despesa_importacao = $("#despesa_imp").val();
	  console.log(despesa_importacao);
	  $.ajax({
	       url: "fechamento/grava-despesa-importacao.php",
	       data: {despesa_importacao: despesa_importacao},
	       type: 'POST',
//	       context: $('#teste'),
	       success: function (retorno) {
		    
	       }
	  });
     });

});

/*
 * Preciso pensar em... quando fazer qualquer alteração em algum campo
 * Atualizar o valor no banco;
 * Pegar todos valores atualizados;
 * Fazer os calculos;
 * Atualizar no banco;
 * Atualizar na tela;
 */