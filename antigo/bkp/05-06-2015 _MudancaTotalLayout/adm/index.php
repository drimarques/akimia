<!--
<?php
     require_once '../src/classes/conexao.class.php';
     require_once '../src/classes/cliente.class.php';
     require_once 'src/classes/interface-adm.class.php';
     require_once 'excel-reader/reader.php';
     require_once '../src/classes/compra.class.php';
     require_once '../src/dao/comprasDAO.php';
     
     $interface = new interfaceAdm();
     $id_cliente = $_GET['user'];

     $pagina = $_GET['page'];
     if ($pagina == null) {
	  $pagina = 1;
     }
?>
-->
<!DOCTYPE html>
<html lang="pt-br">
     <head>
	  <title>Administrador - Akimia</title>
	  <meta charset="UTF-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	  <meta name="author" content="Adriano Marques">
	  <link rel="shortcut icon" href="../img/Logo.Akimia.transp.750x750.png">
	  <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	  <link rel="stylesheet" href="../bootstrap/bootstrap.min.css">
	  <link rel="stylesheet" href="../bootstrap/bootstrap-theme.min.css">
	  <link href="css/estilo.css" rel="stylesheet" type="text/css">
	  <script src="../jquery/jquery-2.1.3.min.js"></script>
	  <!--<script src="js/akimia-adm.js" type="text/javascript"></script>-->
     </head>
     <body>	  	  	  
	  <header>
	       <img onclick="location.href = 'index.php'" src="../img/Logo.Akimia.transp.750x750.png" alt="Logo Akimia">

	       <form class="form-horizontal" method="post" id="formPesquisa" name="formPesquisa" autocomplete="off">
		    <div class="form-group">
			 <div class="col-sm-10">
			      <input type="text" class="form-control" id="textoPesquisa" name="textoPesquisa" placeholder="Buscar..." list="lista_clientes" >
			      <datalist id="lista_clientes">
				   <?php
					$interface->getClientes()
				   ?>     
			      </datalist>
			      <button type="submit" class="btn btn-default" name="btnPesquisarCliente" id="btnPesquisarCliente"><img src="img/search.png" alt="Imagem search"></button>
			 </div>
		    </div>
	       </form>	       	       
	       <!--Botão que realiza a pesquisa-->
	       <?php
		    require_once 'src/botao-pesquisar-cliente.click.php';
	       ?>

	       
	       <section id="settings">
		    <img src="img/3pontos.png" alt="Opções" >
	       </section>
	       <section id="settings_expand">		    
		    <table>
			 <tr>
			      <td class="link_importar"><img src="img/excel.png" alt="Importar"></td>
			      <td><a href="#"><img src="img/down.png" alt="Exportar"></a></td>
			 </tr>
			 <tr>
			      <td class="link_importar">
				   Importar
				   <form method="post" hidden="hidden" name="form_arquivo_excel" enctype="multipart/form-data">
					<input type="file" id="arquivo_excel" name="arquivo_excel" accept=".xls, application/vnd.ms-excel">
					<input type="submit" name="btnEnviarExcel" id="btnEnviarExcel">
				   </form>				   
			      </td>
			      <td><a href="#">Exportar</a></td>
			 </tr>
			 <tr>
			      <td><a href="lista-compras.php"><img src="img/compras.png" alt="Lista de compras"></a></td>
			 </tr>
			 <tr>
			      <td><a href="lista-compras.php">Ver lista</a></td>
			 </tr>
		    </table>
	       </section>
	       <section id="usuarioLogado">
		    emailusuariologado@opa.com
	       </section>
	  </header>
	  
	  <div id='sucesso_planilha' class="alert alert-success notificacao"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Planilha importada com sucesso :D</div>
	  <div id='formato_invalido' class="alert alert-danger notificacao"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Formato de arquivo inválido, por favor importe arquivos .xls</div>
	  
	  <!--Importação da planilha-->
	  <?php
	       require_once 'src/importa_planilha.php';
	  ?>	  		  
	  
	  <?php
//	       Verifica se tem clientes cadastrados
	       $cliente = new Cliente();
	       if($cliente->existeClientes()){		    
		    $busca = $_GET['search'];
		    if ($busca == null) {
			 $interface->getTodosClientes($pagina);
		    } else {
			 $interface->buscaClientes($pagina, $busca);
		    }
	       }
	       //Não existem clientes
	       else{
		    echo "<div class='notifica_resultados'>Não há clientes cadastrados</div>";
	       }
	  ?>

	  <div class="clear"></div>	       

	  <!--Rodapé-->
<!--	  <footer>
	       <div id="copyright">Copyright &COPY; 2012 - <?php echo date('Y') ?> - Akimia Produtos Importados</div>
	       <div id="dev">Desenvolvido por <a href="http://www.facebook.com.br/adrianomarques9" target="_blank">Adriano Marques</a>.</div>
	  </footer>-->

	  <script src="../jquery/jquery-2.1.3.min.js" type="text/javascript"></script>
	  <script src="../bootstrap/bootstrap.min.js" type="text/javascript"></script>
	  <!--Biblioteca do scrol suave, juntamente com jQuery-->
	  <script src="../jquery/jquery.nicescroll.min.js"></script> 
	  <script src="js/akimia-adm.js" type="text/javascript"></script>

	  <!--Modal usuário-->
	  <?php
	       /* Verifica se tem um usuário clicado */
	       if ($id_cliente != null) {
		    $cliente = new Cliente();
		    $cliente->getDadosCliente($id_cliente);
		    ?>

	  	  <div class="modal fade" id="myModalUsuario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  	       <div class="modal-dialog">
	  		    <div class="modal-content">
	  			 <div class="modal-header">
	  			      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  			      <h4 class="modal-title" id="myModalLabel">Dados do cliente: <?php echo $cliente->GetNome() ?></h4>
	  			 </div>
	  			 <div class="modal-body">
	  			      <form class="form-horizontal form-usuario" method="post">
	  				   <div class="form-group">
	  					<label for="" class="col-sm-2 control-label">Nome completo</label>
	  					<div class="col-sm-10">
	  					     <input type="text" class="form-control" id="" name="" placeholder="" maxlength="50" value="<?php echo $cliente->GetNome(); ?>" > 
	  					</div>
	  				   </div>
	  				   <div class="form-group">
	  					<label for="" class="col-sm-2 control-label">Como gosta de ser chamado?</label>
	  					<div class="col-sm-10">
	  					     <input type="text" class="form-control apelido-form" id="" name="" placeholder="" maxlength="20" value="<?php echo $cliente->GetApelido(); ?>" >
	  					</div>
	  				   </div>
	  				   <div class="form-group">
	  					<label for="" class="col-sm-2 control-label">E-mail</label>
	  					<div class="col-sm-10">
	  					     <input type="email" class="form-control" id="" name="" placeholder="" value="<?php echo $cliente->GetEmail() ?>" >
	  					</div>
	  				   </div>
	  				   <div class="form-group">
	  					<label for="" class="col-sm-2 control-label">WhatsApp</label>
	  					<div class="col-sm-10">
	  					     <input type="text" class="form-control" id="" name="" placeholder="" value="<?php echo $cliente->GetWhatsApp() ?>" >
	  					</div>
	  				   </div>
	  				   <div class="form-group">
	  					<label for="" class="col-sm-2 control-label">Telefone fixo</label>
	  					<div class="col-sm-10">
	  					     <input type="text" class="form-control" id="" name="" placeholder="" value="<?php echo $cliente->GetTelefone() ?>" >
	  					</div>
	  				   </div>
	  				   <div class="form-group">
	  					<label for="" class="col-sm-2 control-label">Celular</label>
	  					<div class="col-sm-10">
	  					     <input type="text" class="form-control" id="" name="" placeholder="" value="<?php echo $cliente->GetCelular() ?>" >
	  					</div>
	  				   </div>
	  				   <div class="form-group">
	  					<label for="" class="col-sm-2 control-label">CEP</label>
	  					<div class="col-sm-10">
	  					     <input type="text" class="form-control" id="" name="" placeholder="" value="<?php echo $cliente->GetCEP() ?>" >
	  					</div>
	  				   </div>
	  				   <div class="form-group">
	  					<label for="" class="col-sm-2 control-label">Endereço</label>
	  					<div class="col-sm-10">
	  					     <input type="text" class="form-control" id="" name="" placeholder=""  value="<?php echo $cliente->GetEndereco() ?>">
	  					</div>
	  				   </div>
	  				   <div class="form-group">
	  					<label for="numEnderecoCadastro" class="col-sm-2 control-label">Número</label>
	  					<div class="col-sm-10">
	  					     <input type="number" class="form-control" id="" name="" min="1" placeholder="" pattern="[0-9]+$"  value="<?php echo $cliente->GetNumeroEndereco() ?>">
	  					</div>
	  				   </div>
	  				   <div class="form-group">
	  					<label for="" class="col-sm-2 control-label">Complemento</label>
	  					<div class="col-sm-10">
	  					     <input type="text" class="form-control" id="" name="" placeholder="" maxlength="100"  value="<?php echo $cliente->GetComplemento() ?>">
	  					</div>
	  				   </div>
	  				   <div class="form-group">
	  					<label for="" class="col-sm-2 control-label">Referência</label>
	  					<div class="col-sm-10">
	  					     <input type="text" class="form-control" id="" name="" placeholder="" maxlength="100"  value="<?php echo $cliente->GetReferencia() ?>">
	  					</div>
	  				   </div>
	  				   <div class="form-group">
	  					<label for="" class="col-sm-2 control-label">Estado</label>
	  					<div class="col-sm-10">
	  					     <input type="text" class="form-control" id="" name="" placeholder=""  value="<?php echo $cliente->GetUF() ?>">
	  					</div>
	  				   </div>
	  				   <div class="form-group">
	  					<label for="" class="col-sm-2 control-label">Cidade</label>
	  					<div class="col-sm-10">
	  					     <input type="text" class="form-control" id="" name="" placeholder=""  value="<?php echo $cliente->GetCidade() ?>">
	  					</div>
	  				   </div>
	  				   <div class="form-group">
	  					<label for="" class="col-sm-2 control-label">Bairro</label>
	  					<div class="col-sm-10">
	  					     <input type="text" class="form-control" id="" name="" placeholder=""  value="<?php echo $cliente->GetBairro() ?>">
	  					</div>
	  				   </div>
	  			      </form>
	  			 </div>
	  		    </div>
	  	       </div>
	  	  </div>
	  	  <script>
	  	    $('#myModalUsuario').modal('show');
	  	    //Se o "X" for clicado, fecha o form 
	  	    $(".close").click(function () {
	  		 location.href = "index.php?page=<?php echo $pagina ?>&search=<?php echo $busca ?>";
	  	    });
	  	  </script>
		    <?php
	       }
	  ?>
     </body>
</html>
