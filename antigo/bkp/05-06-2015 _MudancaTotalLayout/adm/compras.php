<!DOCTYPE html>
<html lang="pt-br">
     <head>
	  <title>Compras - Akimia</title>
	  <meta charset="UTF-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	  <meta name="author" content="Adriano Marques">
	  <link rel="shortcut icon" href="../img/Logo.Akimia.transp.750x750.png">
	  <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	  <link rel="stylesheet" href="../bootstrap/bootstrap.min.css">
	  <link rel="stylesheet" href="../bootstrap/bootstrap-theme.min.css">
	  <link href="css/compras.css" rel="stylesheet" type="text/css">
	  <script src="../jquery/jquery-2.1.3.min.js"></script>	  

	  <!--Ajax de atualização automatica do relatorio-->
	  <script src="js/ajax.js" type="text/javascript"></script>
	  <script type="text/javascript">

	       function limpaListaENotificacao(){
		    $("#notificacao_lista_compras").css({"display": "none"});
		    document.getElementById("lista_compras").innerHTML = "";
	       }	       

	       function animacao() {
		    $("#item_clicado").finish();

		    $("#item_clicado").animate({
			 padding: "20px 0 0 0",
			 paddingBottom: "40px"
		    },
		    400,
		    //Animação completa
		    function () {
			 $("#item_clicado").delay(800);
			 $("#item_clicado").animate({
			      padding: "0",
			      paddingBottom: "0"
			 },
			 400);
		     });
	       }
	       
	       function marcaComprado(idItem) {
		    var url = "compras/marca-comprado.php?id_item=" + idItem;

		    var comprado;
		    comprado = new montaXMLHTTP();
		    comprado.open("GET", url, true);
		    comprado.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		    comprado.onreadystatechange = function () {
			 if (comprado.readyState === 4) {
			      document.getElementById("item_clicado").innerHTML = comprado.responseText;
//			      $("#item_clicado").fadeIn("fast").delay(800).fadeOut(200, function (){
//				   listaCompras('n_comprados');
//			      });


			      var sexo = new montaXMLHTTP();
			      var url_sexo = "compras/get-sexo.php?id_item=" + idItem;

			      sexo.open("GET", url_sexo, true);
			      sexo.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			      sexo.onreadystatechange = function () {
				   if (sexo.readyState === 4) {
					animacao();
					validaFiltros();
				   }

			      };
			      sexo.send(null);
			 }

		    };
		    comprado.send(null);
	       }
	       
	       function marcaNaoComprado(idItem) {
		    var url = "compras/marca-nao-comprado.php?id_item=" + idItem;
			 
		    var naoComprado;
		    naoComprado = new montaXMLHTTP();
		    naoComprado.open("GET", url, true);
		    naoComprado.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		    naoComprado.onreadystatechange = function () {
			 if (naoComprado.readyState === 4) {
			      document.getElementById("item_clicado").innerHTML = naoComprado.responseText;
//			      $("#item_clicado").fadeIn("fast").delay(800).fadeOut(200, function (){
//				   listaCompras('n_comprados');
//			      });

			      var sexo = new montaXMLHTTP();
			      var url_sexo = "compras/get-sexo.php?id_item=" + idItem;

			      sexo.open("GET", url_sexo, true);
			      sexo.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			      sexo.onreadystatechange = function () {
				   if (sexo.readyState === 4) {
					animacao();
					validaFiltros();
				   }

			      };
			      sexo.send(null);
			 }

		    };
		    naoComprado.send(null);
	       }
	      
	       function marcaBuscaComprado(idItem, busca) {
		    var url = "compras/marca-comprado.php?id_item=" + idItem;
		    
		    var compradoBusca;
		    compradoBusca = new montaXMLHTTP();
		    compradoBusca.open("GET", url, true);
		    compradoBusca.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		    compradoBusca.onreadystatechange = function () {
			 if (compradoBusca.readyState === 4) {
			      document.getElementById("item_clicado").innerHTML = compradoBusca.responseText;

			      var sexo = new montaXMLHTTP();
			      var url_sexo = "compras/get-sexo.php?id_item=" + idItem;

			      sexo.open("GET", url_sexo, true);
			      sexo.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			      sexo.onreadystatechange = function () {
				   if (sexo.readyState === 4) {
					animacao();
					listaComprasBusca(busca);
				   }

			      };
			      sexo.send(null);
			 }

		    };
		    compradoBusca.send(null);		    
	       }
	       
	       function marcaBuscaNaoComprado(idItem, busca) {		    
		    var url = "compras/marca-nao-comprado.php?id_item=" + idItem;

		    var naoCompradoBusca;
		    naoCompradoBusca = new montaXMLHTTP();
		    naoCompradoBusca.open("GET", url, true);
		    naoCompradoBusca.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		    naoCompradoBusca.onreadystatechange = function () {
			 if (naoCompradoBusca.readyState === 4) {
			      document.getElementById("item_clicado").innerHTML = naoCompradoBusca.responseText;
				   
			      var sexo = new montaXMLHTTP();
			      var url_sexo = "compras/get-sexo.php?id_item=" + idItem;

			      sexo.open("GET", url_sexo, true);
			      sexo.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			      sexo.onreadystatechange = function () {
				   if (sexo.readyState === 4) {
					animacao();
					listaComprasBusca(busca);
				   }

			      };
			      sexo.send(null);
			 }
		    };		    
		    naoCompradoBusca.send(null);		    
	       }

	  </script>

	  <script>
	       $(document).ready(function () {
		    $("#txtBusca").keyup(function () {
			 localStorage.clear();
			 
			 var busca = document.getElementById("txtBusca");			 			 			 
			 
			 if(busca.value.toString() !== ""){
			      listaComprasBusca(busca.value.toString());
			 }
			 else {
			      limpaListaENotificacao();
			      $("#filtro_comprados").fadeIn("fast");
			      document.getElementById("notificacao_lista_compras").innerHTML = "Digite ou selecione um filtro, vamos começar :D";
			      $("#notificacao_lista_compras").fadeIn("fast");
			 }
			 
		    });
		    
		    $("#txtBusca").focusin(function () {
			 limpaListaENotificacao();
			 document.getElementById("notificacao_lista_compras").innerHTML = "Digite ou selecione um filtro, vamos começar :D";
			 $("#notificacao_lista_compras").fadeIn("fast");			 
		    });		    		    		  		    
	       });
	  </script>

     </head>
     <body>
	  <header>	       
	       <h1><img onclick="location.href = 'compras.php'" src="../img/Logo.Akimia.transp.750x750.png" alt="Akimia">Compras</h1>
	  </header>

	  <div class="filtros" id="filtro_pesquisa">
	       <form method="post" name="form_busca" id="form_busca" autocomplete="off">
		    <input type="text" placeholder="Buscar" id="txtBusca" name="txtBusca" class="form-control" autofocus="autofocus">
	       </form>
	  </div>

	  <div class="filtros" id="filtro_comprados">
	       <div class="btns_filtros" id="btn_n_comprados"><img src="img/unlike.png" alt="Like">Não comprados</div>
	       <div class="btns_filtros" id="btn_comprados"><img src="img/like.png" alt="Like">Comprados</div>	       
	  </div>

	  <div class="filtros" id="filtro_sexo">
	       <div onclick="" class="btns_filtros" id="btn_masculino"><img src="img/masculino.png" alt="Like"></div>
	       <div onclick="" class="btns_filtros" id="btn_feminino"><img src="img/feminino.png" alt="Like"></div>
	       <div onclick="" class="btns_filtros" id="btn_unissex"><img src="img/indiferente.png" alt="Like"></div>
	  </div>

	  <article id="lista_compras">

	  </article>

	  <div id='notificacao_lista_compras'></div>

	  <div id="item_clicado"></div>	  

	  <script src="../jquery/jquery-2.1.3.min.js" type="text/javascript"></script>
	  <script src="../bootstrap/bootstrap.min.js" type="text/javascript"></script>
	  <!--Biblioteca do scrol suave, juntamente com jQuery-->
	  <script src="../jquery/jquery.nicescroll.min.js"></script> 
	  <script src="js/akimia-adm.js" type="text/javascript"></script>
	  <script type="text/javascript">
		    function updLocalStorage(marca){
			 localStorage.setItem("marca", marca);
			 validaFiltros();
		    }
		    
		    function mataLocalStorage(){
			 localStorage.clear();
			 validaFiltros();
		    }

		    function listaComprasBusca(busca) {
			 if (busca !== "") {
			      limpaListaENotificacao();
			      $("#filtro_comprados").fadeOut("fast");
			      $("#filtro_sexo").fadeOut("fast");
			      
			      var url = "compras/busca.php?filtro_busca=" + busca;
			      
			      var relatorioBusca;
			      relatorioBusca = new montaXMLHTTP();			     
			      relatorioBusca.open("GET", url, true);
			      relatorioBusca.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			      relatorioBusca.onreadystatechange = function () {
				   if (relatorioBusca.readyState === 4) {
					document.getElementById("lista_compras").innerHTML = relatorioBusca.responseText;					
					if(relatorioBusca.responseText === ""){
					     document.getElementById("notificacao_lista_compras").innerHTML = "Sem resultados encontrados para: \"" + busca + '\"';
					     $("#notificacao_lista_compras").fadeIn("fast");
					}
				   }
			      };
			      relatorioBusca.send(null);
			 }			 
		    }
		    
		    function validaFiltros(){	
			 limpaListaENotificacao();
			 			 			 
     			 var n_comprado = $("#btn_n_comprados").hasClass('filtro_selecionado').toString();
			 var comprado = $("#btn_comprados").hasClass('filtro_selecionado').toString();
			 
			 var masculino = $("#btn_masculino").hasClass('filtro_selecionado').toString();
			 var feminino = $("#btn_feminino").hasClass('filtro_selecionado').toString();
			 var unissex = $("#btn_unissex").hasClass('filtro_selecionado').toString();			 
     			 
			 var marca = localStorage.getItem("marca");
			 
			 var url = "compras/gera-relatorio.php?n_comprado=" + n_comprado + "&comprado=" + comprado + "&masculino=" + masculino + "&feminino=" + feminino + "&unissex=" + unissex + "&marca=" + marca;			 			 
				
			 var relatorio;
			 relatorio = new montaXMLHTTP();			 			 
			 relatorio.open("GET", url, true);
			 relatorio.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			 relatorio.onreadystatechange = function () {
			      if (relatorio.readyState === 4) {
				   if(relatorio.responseText === ""){
					document.getElementById("notificacao_lista_compras").innerHTML = "Não encontramos nenhum produto :(";
					$("#notificacao_lista_compras").fadeIn("fast");
					
				   }
				   else if(relatorio.responseText !== ""){
					document.getElementById("lista_compras").innerHTML = relatorio.responseText;
				   }				  
				   
			      }			 
			 };
			 
			 relatorio.send(null);
		    }
		    
	  </script>
     </body>
</html>