<?php

     date_default_timezone_set('America/Sao_Paulo');
     $data_hora = date("d-m-Y_H-i-s");

     require_once '../php_excel/Classes/PHPExcel.php';
     require_once '../../src/classes/conexao.class.php';
     require_once '../../src/dao/comprasDAO.php';
     require_once '../../src/dao/parametrosDAO.php';
     require_once '../../src/classes/parametros_calculo.class.php';


// Instanciamos a classe
     $objPHPExcel = new PHPExcel();

     //Cores
     //Titulos Azuis
     $fundo_azul = array('fill' =>
	 array(
	     'type' => PHPExcel_Style_Fill::FILL_SOLID,
	     'color' => array('rgb' => '5478b2')
	 ),
     );

     $fundo_rosa = array('fill' =>
	 array(
	     'type' => PHPExcel_Style_Fill::FILL_SOLID,
	     'color' => array('rgb' => 'e31b6e')
	 ),
     );

     $fonte_branca = array(
	 'font' => array(
	     'color' => array('rgb' => 'ffffff')
     ));

     //Cores de fundo
     $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($fundo_azul);
     $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($fundo_azul);
     $objPHPExcel->getActiveSheet()->getStyle('A7:Q7')->applyFromArray($fundo_rosa);
     $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($fundo_rosa);
     $objPHPExcel->getActiveSheet()->getStyle('A3')->applyFromArray($fundo_rosa);
     $objPHPExcel->getActiveSheet()->getStyle('A4')->applyFromArray($fundo_rosa);
     $objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray($fundo_rosa);
     $objPHPExcel->getActiveSheet()->getStyle('D2')->applyFromArray($fundo_rosa);
     $objPHPExcel->getActiveSheet()->getStyle('D3')->applyFromArray($fundo_rosa);
     $objPHPExcel->getActiveSheet()->getStyle('D4')->applyFromArray($fundo_rosa);

     //Cores da fonte
     $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($fonte_branca);
     $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($fonte_branca);
     $objPHPExcel->getActiveSheet()->getStyle('A7:Q7')->applyFromArray($fonte_branca);
     $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($fonte_branca);
     $objPHPExcel->getActiveSheet()->getStyle('A3')->applyFromArray($fonte_branca);
     $objPHPExcel->getActiveSheet()->getStyle('A4')->applyFromArray($fonte_branca);
     $objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray($fonte_branca);
     $objPHPExcel->getActiveSheet()->getStyle('D2')->applyFromArray($fonte_branca);
     $objPHPExcel->getActiveSheet()->getStyle('D3')->applyFromArray($fonte_branca);
     $objPHPExcel->getActiveSheet()->getStyle('D4')->applyFromArray($fonte_branca);

// Definimos o estilo da fonte
     $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('A3')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('A4')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('A5')->getFont()->setBold(true);

     $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('D2')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('D3')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('D4')->getFont()->setBold(true);

     $objPHPExcel->getActiveSheet()->getStyle('A7')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('B7')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('C7')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('D7')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('E7')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('F7')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('G7')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('H7')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('I7')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('J7')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('K7')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('L7')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('M7')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('N7')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('O7')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('P7')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('Q7')->getFont()->setBold(true);

// Criamos as colunas
     $objPHPExcel->setActiveSheetIndex(0)
     ->setCellValue('A1', "PARÂMETROS DE CÁLCULO")
     ->setCellValue("A2", "Cotação Dolar")
     ->setCellValue("A3", "Imposto USA")
     ->setCellValue('A4', "IOF")
     ->setCellValue("A5", "Despesa com Importação")
     ->setCellValue('D1', "RESUMO")
     ->setCellValue('D2', "Custo Total")
     ->setCellValue("D3", "Venda Total")
     ->setCellValue("D4", "Lucro Real")
     ->setCellValue('A7', "Marca")
     ->setCellValue("B7", "Sexo")
     ->setCellValue("C7", "Tipo")
     ->setCellValue("D7", "Produto")
     ->setCellValue("E7", "Modelo")
     ->setCellValue("F7", "Cor")
     ->setCellValue("G7", "Tamanho")
     ->setCellValue("H7", "Quantidade")
     ->setCellValue("I7", "Custo (U$)")
     ->setCellValue("J7", "IOF")
     ->setCellValue("K7", "Custo")
     ->setCellValue("L7", "Markup")
     ->setCellValue("M7", "Venda")
     ->setCellValue("N7", "Lucro")
     ->setCellValue("O7", "Custo Total")
     ->setCellValue("P7", "Venda Total")
     ->setCellValue("Q7", "Lucro Total");

     $mesclar_centralizar = array(
	 'alignment' => array(
	     'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	 )
     );

     $objPHPExcel->getActiveSheet()->mergeCells("A1:B1");
     $objPHPExcel->getActiveSheet()->mergeCells("D1:E1");
     $objPHPExcel->getActiveSheet()->getStyle("A1:B1")->applyFromArray($mesclar_centralizar);
     $objPHPExcel->getActiveSheet()->getStyle("D1:E1")->applyFromArray($mesclar_centralizar);

// Podemos configurar diferentes larguras paras as colunas como padrão
     $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
     $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(18);


     $conexao = new Conexao();
     $conn = $conexao->getConexao();

     //Parametros de calculo
     $parametros = $conn->query("SELECT * FROM parametros_calculo");
     $valores = $parametros->fetch(PDO::FETCH_ASSOC);
     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, $valores[cotacaoDolar]);
     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 3, $valores[impostoUSA]);
     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 4, $valores[IOF]);
     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 5, $valores[despesa_viagem]);

     //Resumo
     $parametrosDAO = new parametrosDAO();
     $comprasDAO = new comprasDAO();

     $parametro = $parametrosDAO->getDadosParametro();
     $despesaViagem = $parametro->GetDespesaViagem();
     $lucroVerdadeiro = $comprasDAO->getTotalLucroTotal() - $despesaViagem;

     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 2, "R$ " . number_format($comprasDAO->getTotalCustoTotal(), 2, ',', '.') . "");
     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 3, "R$ " . number_format($comprasDAO->getTotalVendaTotal(), 2, ',', '.') . "");
     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 4, "R$ " . number_format($lucroVerdadeiro, 2, ',', '.') . "");

     $retorno = $conn->query("SELECT * FROM lista_compras ORDER BY marca ASC");
     $contador = 8;
     while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {

	  if ($linhas[iof] == "1") {
	       $iof = "Sim";
	  } else {
	       $iof = "Não";
	  }

	  // Também podemos escolher a posição exata aonde o dado será inserido (coluna, linha, dado);	  
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $contador, $linhas[marca]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $contador, $linhas[sexo]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $contador, $linhas[tipo]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $contador, $linhas[produto]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $contador, $linhas[modelo]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $contador, $linhas[cor]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $contador, $linhas[tamanho]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $contador, $linhas[quantidade_total]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $contador, "$ " . number_format($linhas[valor_dolar], 2, '.', ',') . "");
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $contador, $iof);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $contador, "R$ " . number_format($linhas[valor_rs], 2, ',', '.') . "");
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $contador, number_format($linhas[markup], 2, '.', ',') . " %");
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $contador, "R$ " . number_format($linhas[venda], 2, ',', '.') . "");
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $contador, "R$ " . number_format($linhas[lucro], 2, ',', '.') . "");
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $contador, "R$ " . number_format($linhas[custo_total], 2, ',', '.') . "");
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, $contador, "R$ " . number_format($linhas[venda_total], 2, ',', '.') . "");
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, $contador, "R$ " . number_format($linhas[lucro_total], 2, ',', '.') . "");
	  $contador++;
     }

     unset($conn);

// Podemos renomear o nome das planilha atual, lembrando que um único arquivo pode ter várias planilhas
     $objPHPExcel->getActiveSheet()->setTitle('Lista de Compras - Akimia');

// Cabeçalho do arquivo para ele baixar
     header('Content-Type: application/vnd.ms-excel');
     header('Content-Disposition: attachment;filename="ExportacaoFechamento_Data' . $data_hora . '.xls"');
     header('Cache-Control: max-age=0');
// Se for o IE9, isso talvez seja necessário
     header('Cache-Control: max-age=1');

// Acessamos o 'Writer' para poder salvar o arquivo
     $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

// Salva diretamente no output, poderíamos mudar arqui para um nome de arquivo em um diretório ,caso não quisessemos jogar na tela
     $objWriter->save('php://output');

     exit;


     