<?php
     if (isset($_POST['btnEnviarExcel'])) {
	  $dao = new comprasDAO();
	  $compra = new Compra();
	  
	  $daoPARAMETROS = new parametrosDAO();
	  $parametros = new ParametrosCalculo();
	  	  
	  $data = new Spreadsheet_Excel_Reader();
	  $data->setOutputEncoding('UTF-8');
	  	 
	  //Se o arquivo não for vazio e for um arquivo excel
	  if (!empty($_FILES['arquivo_excel']) && ($_FILES['arquivo_excel']['type'] == "application/vnd.ms-excel" || $_FILES['arquivo_excel']['type'] == "application/octet-stream")) {
	       $data->read($_FILES['arquivo_excel']['tmp_name']);
	       	       
	       //Obtem cotação do dolar, célula A1... [linha][coluna]
	       if (utf8_encode($data->sheets[0]['cells'][2][1]) == "Cotação Dolar"){
		    $cotacaoDolar = (double)$data->sheets[0]['cells'][2][2];
		    $parametros->SetCotacaoDolar($cotacaoDolar);
	       }
	       
	       //Obtem impostoUSA
	       if (utf8_encode($data->sheets[0]['cells'][3][1]) == "Imposto USA"){
		    $impostoUSA = (double)$data->sheets[0]['cells'][3][2];		    
		    $parametros->SetImpostoUSA($impostoUSA);		    
	       }
	       
	       //Obtem IOF
	       if (utf8_encode($data->sheets[0]['cells'][4][1]) == "IOF"){
		    $IOF = (double)$data->sheets[0]['cells'][4][2];		    
		    $parametros->SetIOF($IOF);
	       }
	       
	       //Obtem IOF
	       if (utf8_encode($data->sheets[0]['cells'][5][1]) == "Despesa com Importação"){
		    $despesa_viagem = (double)$data->sheets[0]['cells'][5][2];		    
		    $parametros->SetDespesaViagem($despesa_viagem);
	       }
	       	       	       
	       //Cadastra parametros
	       $daoPARAMETROS->cadastraParametros($parametros);
	       
	       	       
	       $qtdLinhas = $data->sheets[0]['numRows'];	       
	       //Limpa lista
	       $dao->limpaListaCompras();	       
//	       //Percorrendo os valores a partir da 8ª linha
	       for ($i = 8; $i <= $qtdLinhas; $i++) {		    
		    $compra = new Compra();		    		    
		    $compra->setMarca(utf8_encode($data->sheets[0]['cells'][$i][1]));
		    $compra->setSexo(utf8_encode($data->sheets[0]['cells'][$i][2]));
		    $compra->setTipo(utf8_encode($data->sheets[0]['cells'][$i][3]));		    
		    $compra->setProduto(utf8_encode($data->sheets[0]['cells'][$i][4]));
		    $compra->setModelo(utf8_encode($data->sheets[0]['cells'][$i][5]));
		    $compra->setCor(utf8_encode($data->sheets[0]['cells'][$i][6]));
		    $compra->setTamanho($data->sheets[0]['cells'][$i][7]);
		    $compra->setQuantidadeTotal((int)$data->sheets[0]['cells'][$i][8]);
		    		   
		    $dao->cadastraListaCompra($compra);
	       }
	       	
	       unset($_POST['btnEnviarExcel']);
	       ?>
	       <script>
		    $("#sucesso_planilha").fadeIn(function (){
			 document.form_arquivo_excel.reset();			
//			 window.open('lista-compras.php');
//			 location.href="index.php";
		    });
	       </script>
	       <?php
	       
	  }
//	  //Não é .xls
	  else{
	       ?>
	       <script>
		    $("#formato_invalido").fadeIn();
	       </script>
	       <?php
	  }	  	  
     }

