<?php

     require_once '../../src/classes/conexao.class.php';
     require_once '../../src/classes/compra.class.php';
     require_once '../../src/dao/comprasDAO.php';

     require_once '../../src/classes/parametros_calculo.class.php';
     require_once '../../src/dao/parametrosDAO.php';
          
     $dao = new comprasDAO();
     $parametrosDAO = new parametrosDAO();
     
     $id_item = $_POST['id_item'];
     $valor = $_POST['valor'];

     $valor = str_replace("R$ ", "", $valor);
     $valor = str_replace(".", "", $valor);
     $valor = str_replace(",", ".", $valor);

     $dao->upd_valorVendaCompra($id_item, $valor);
     
     $compra = $dao->getDadosCompra($id_item);     
     $parametro = $parametrosDAO->getDadosParametro();
     $compra_calculada = $dao->calculaValoresFechamento($compra, $parametro);     
     $dao->updValoresCompra($compra_calculada, $id_item);

     