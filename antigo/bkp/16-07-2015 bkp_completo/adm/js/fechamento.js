function getTabelaResumo() {
     $.ajax({
	  url: "fechamento/get-tabela-resumo.php",
	  success: function (retorno) {
	       $("#tabela-resumo").html(retorno);
	  }
     });
}

function atualizaTabelaPorIdItem(id_item) {
     idValorRS = id_item + "valorbr";
     $.ajax({
	  url: "getters-fechamento/get-valorRS.php",
	  data: {id_item: id_item},
	  type: 'POST',
	  context: $("#" + idValorRS),
	  success: function (retorno) {
	       $("#" + idValorRS).html(retorno);
	  }
     });

     idMarkup = id_item + "markup";
     $.ajax({
	  url: "getters-fechamento/get-markup.php",
	  data: {id_item: id_item},
	  type: 'POST',
	  context: $("#" + idMarkup),
	  success: function (retorno) {
	       $("#" + idMarkup).html(retorno);
	  }
     });

     idLucro = id_item + "lucro";
     $.ajax({
	  url: "getters-fechamento/get-lucro.php",
	  data: {id_item: id_item},
	  type: 'POST',
	  context: $("#" + idLucro),
	  success: function (retorno) {
	       $("#" + idLucro).html(retorno);
	  }
     });

     idCustoTotal = id_item + "custo_total";
     $.ajax({
	  url: "getters-fechamento/get-custoTotal.php",
	  data: {id_item: id_item},
	  type: 'POST',
	  context: $("#" + idCustoTotal),
	  success: function (retorno) {
	       $("#" + idCustoTotal).html(retorno);
	  }
     });

     idVendaTotal = id_item + "venda_total";
     $.ajax({
	  url: "getters-fechamento/get-vendaTotal.php",
	  data: {id_item: id_item},
	  type: 'POST',
	  context: $("#" + idVendaTotal),
	  success: function (retorno) {
	       $("#" + idVendaTotal).html(retorno);
	  }
     });

     idLucroTotal = id_item + "lucro_total";
     $.ajax({
	  url: "getters-fechamento/get-lucroTotal.php",
	  data: {id_item: id_item},
	  type: 'POST',
	  context: $("#" + idLucroTotal),
	  success: function (retorno) {
	       $("#" + idLucroTotal).html(retorno);
	  }
     });
}

function upd_valorDolarProduto(elemento) {
     var id_item = elemento.id;
     var valor_dolar = elemento.value;

     $.ajax({
	  url: "fechamento/grava-valor-dolar-compra.php",
	  data: {valor: valor_dolar, id_item: id_item},
	  type: 'POST',
	  success: function (retorno) {
	       atualizaTabelaPorIdItem(id_item);
	       
	       if(valor_dolar === "$ 0.00" || valor_dolar === ""){
		    var td = $(elemento);
		    $(td.parent()).addClass("outline_azul");
		    $(td.parent()).removeClass("outline_verde");
	       }
	       else{		    
		    var td = $(elemento);
		    $(td.parent()).addClass("outline_verde");
		    $(td.parent()).removeClass("outline_azul");
	       }
	  }
     });

}

function upd_IOFProduto(elemento) {
     var iof = elemento.value;
     var id_item = elemento.id;

     $.ajax({
	  url: "fechamento/grava-iof-produto.php",
	  data: {iof: iof, id_item: id_item},
	  type: 'POST',
	  success: function (retorno) {	       
	       atualizaTabelaPorIdItem(id_item);
	       
	       var td = $(elemento);
	       $(td.parent()).css({
	       });
	  }
     });
}

function upd_valorVendaProduto(elemento) {
     var id_item = elemento.id;
     var valor_venda = elemento.value;

     $.ajax({
	  url: "fechamento/grava-valor-venda-compra.php",
	  data: {valor: valor_venda, id_item: id_item},
	  type: 'POST',
	  success: function (retorno) {
	       atualizaTabelaPorIdItem(id_item);
	       
	       if(valor_venda === "R$ 0,00" || valor_venda === ""){
		    var td = $(elemento);
		    $(td.parent()).addClass("outline_azul");
		    $(td.parent()).removeClass("outline_verde");
	       }
	       else{		    
		    var td = $(elemento);
		    $(td.parent()).addClass("outline_verde");
		    $(td.parent()).removeClass("outline_azul");
	       }
	  }
     });

}

$(document).ready(function () {
     $(".dolar").maskMoney({showSymbol: true, symbol: "$ ", decimal: ".", thousands: ","});
     $(".real").maskMoney({showSymbol: true, symbol: "R$ ", decimal: ",", thousands: "."});
     $(".porcentagem").maskMoney({showSymbol: true, symbol: "% ", decimal: ",", thousands: "."});

     setInterval(function () {
	  getTabelaResumo();
     }, 500);

});