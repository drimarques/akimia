function marcaItemNaoComprado(item) {
     $(item).toggleClass('item_naocomprado_selecionado');
}

function marcaItemJaComprado(item) {
     $(item).toggleClass('item_comprado_selecionado');
}

function selecionaMarca(item) {
     $(item).toggleClass('marca_selecionada');
     verificaFiltroMarcas();
}

function selecionaTipo(item) {
     $(item).toggleClass('tipo_selecionado');
}




