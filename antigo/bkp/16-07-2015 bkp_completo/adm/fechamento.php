<!--
<?php
     require_once '../src/classes/conexao.class.php';
     require_once '../src/classes/compra.class.php';
     require_once '../src/dao/comprasDAO.php';
     require_once '../src/classes/parametros_calculo.class.php';
     require_once '../src/dao/parametrosDAO.php';

     $dao = new comprasDAO();
     $parametrosDAO = new parametrosDAO();
?>
-->
<!DOCTYPE html>
<html lang="pt-br">
     <head>
	  <title>Fechamento - Akimia</title>
	  <meta charset="UTF-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	  <meta name="author" content="Adriano Marques">
	  <link rel="shortcut icon" href="img/Logo.Akimia.Mini.png">
	  <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	  <link rel="stylesheet" href="../bootstrap/bootstrap.min.css">
	  <link rel="stylesheet" href="../bootstrap/bootstrap-theme.min.css">
	  <link href="css/estilo.css" rel="stylesheet" type="text/css">
	  <script src="../jquery/jquery.min.js"></script>	  
	  <script src="js/fechamento.js"></script>	  

</head>
<body onload="getTabelaResumo();">
     <header class="header">	       
	  <h1><img onclick="location.href = 'fechamento.php'" src="img/Logo.Akimia.Mini.png" alt="Akimia">Fechamento</h1>
     </header>	 

     <div id="atualizando_tabela" ><img src="img/loading.gif" alt="Loading">Atualizando lista...</div>

<?php
     $parametrosDAO->listaParametrosFechamento();
?>
     <table id="tabela-resumo" class="table-responsive">

     </table>
<?php
     $dao->listaRelatorioFechamento();
?>

     <script src="../jquery/jquery.min.js"></script>
     <script src="../jquery/jquery.maskMoney.js"></script>	  
     <script src="../bootstrap/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>



