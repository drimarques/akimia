<!DOCTYPE html>
<html lang="pt-br">
     <head>
	  <title>Menu mobile</title>
	  <meta charset="UTF-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	  <meta name="author" content="Adriano Marques">
	  <link rel="shortcut icon" href="../img/Logo.Akimia.transp.750x750.png">
	  <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	  <link rel="stylesheet" href="../bootstrap/bootstrap.min.css">
	  <link rel="stylesheet" href="../bootstrap/bootstrap-theme.min.css">
	  <link href="estilo.css" rel="stylesheet" type="text/css">
	  <script src="../jquery/jquery-2.1.3.min.js"></script>
	  
	  <script>
	       $(document).ready(function (){		    
		    $("#abrir").click(function (){
			 $("#menu-mobile").animate({
			      width: "85%"
			 }, 
			 500,
			 function (){
			      $("#abrir").css({"display":"none"});
			      $("#fechar").css({"display":"block"});			      
			 });
		    });
		    
		    $("#fechar").click(function (){
			 $("#menu-mobile").animate({
			      width: "0"
			 }, 
			 500,
			 function (){
			      $("#abrir").css({"display":"block"});
			      $("#fechar").css({"display":"none"});			      
			 });
		    });
	       });
	  </script>
	  
     </head>
     <body>	  	  	  
	  <section id="menu-mobile"></section>
	  
	  <p id="abrir">Abrir</p>
	  <p id="fechar">Fechar</p>
	  	  
	  <script src="../jquery/jquery-2.1.3.min.js" type="text/javascript"></script>
	  <script src="../bootstrap/bootstrap.min.js" type="text/javascript"></script>	  
     </body>
</html>