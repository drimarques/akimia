<?php

     class Conexao{
	  
	  #Atributos privados
	  private $Host = "localhost";
	  private $Banco = "akimia";
	  private $User = "root";
	  private $Pass = "";
	  var $Conexao;
	  
	  function getConexao(){
	       return $this->Conexao;
	  }
	  
	  #Construtor
	  function __construct() {
	       $this->Conexao = new PDO("mysql:host={$this->Host}; dbname={$this->Banco}", $this->User, $this->Pass, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	  }
	  
     }

