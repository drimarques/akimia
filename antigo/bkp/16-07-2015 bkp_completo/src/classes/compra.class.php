<?php

     class Compra {

	  var $Tipo;
	  var $Sexo;
	  var $Produto;
	  var $Marca;
	  var $Modelo;
	  var $Cor;
	  var $QuantidadeTotal;
	  var $QuantidadeComprada;
	  var $Tamanho;
	  var $Comprado;
	  var $ValorDolar;
	  var $IOF;
	  var $ValorRS;
	  var $Markup;
	  var $Venda;
	  var $Lucro;
	  var $CustoTotal;
	  var $VendaTotal;
	  var $LucroTotal;

	  function setTipo($tipo) {
	       $this->Tipo = $tipo;
	  }

	  function getTipo() {
	       return $this->Tipo;
	  }

	  function setSexo($sexo) {
	       $this->Sexo = $sexo;
	  }

	  function getSexo() {
	       return $this->Sexo;
	  }

	  function setProduto($produto) {
	       $this->Produto = $produto;
	  }

	  function getProduto() {
	       return $this->Produto;
	  }

	  function setMarca($marca) {
	       $this->Marca = $marca;
	  }

	  function getMarca() {
	       return $this->Marca;
	  }

	  function setModelo($modelo) {
	       $this->Modelo = $modelo;
	  }

	  function getModelo() {
	       return $this->Modelo;
	  }

	  function setCor($cor) {
	       $this->Cor = $cor;
	  }

	  function getCor() {
	       return $this->Cor;
	  }

	  function setQuantidadeTotal($qtd) {
	       $this->QuantidadeTotal = $qtd;
	  }

	  function getQuantidadeTotal() {
	       return $this->QuantidadeTotal;
	  }

	  function setQuantidadeComprada($qtd) {
	       $this->QuantidadeComprada = $qtd;
	  }

	  function getQuantidadeComprada() {
	       return $this->QuantidadeComprada;
	  }

	  function setTamanho($tamanho) {
	       $this->Tamanho = $tamanho;
	  }

	  function getTamanho() {
	       return $this->Tamanho;
	  }

	  function setComprado($comprado) {
	       $this->Comprado = $comprado;
	  }

	  function getComprado() {
	       return $this->Comprado;
	  }

	  function setValorDolar($valor) {
	       $this->ValorDolar = $valor;
	  }

	  function getValorDolar() {
	       return $this->ValorDolar;
	  }

	  function setIOF($iof) {
	       $this->IOF = $iof;
	  }

	  function getIOF() {
	       return $this->IOF;
	  }

	  function setValorRS($valor) {
	       $this->ValorRS = $valor;
	  }

	  function getValorRS() {
	       return $this->ValorRS;
	  }

	  function setMarkup($markup) {
	       $this->Markup = $markup;
	  }

	  function getMarkup() {
	       return $this->Markup;
	  }

	  function setVenda($valor) {
	       $this->Venda = $valor;
	  }

	  function getVenda() {
	       return $this->Venda;
	  }

	  function setLucro($valor) {
	       $this->Lucro = $valor;
	  }

	  function getLucro() {
	       return $this->Lucro;
	  }
	  
	  function setCustoTotal($valor) {
	       $this->CustoTotal = $valor;
	  }

	  function getCustoTotal() {
	       return $this->CustoTotal;
	  }
	  
	  function setVendaTotal($valor) {
	       $this->VendaTotal = $valor;
	  }

	  function getVendaTotal() {
	       return $this->VendaTotal;
	  }
	  
	  function setLucroTotal($valor) {
	       $this->LucroTotal = $valor;
	  }

	  function getLucroTotal() {
	       return $this->LucroTotal;
	  }

     }
     