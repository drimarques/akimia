<?php

     require_once '../../src/classes/conexao.class.php';
     require_once '../../src/classes/compra.class.php';
     require_once '../../src/dao/comprasDAO.php';

     $id_item = $_POST['id_item'];
     
     $dao = new comprasDAO();
     $compra = $dao->getDadosCompra($id_item);

     $quantidade_restante = $compra->QuantidadeTotal - $compra->QuantidadeComprada;
     
     echo "<div id='info_item_produto'>$compra->Marca - $compra->Produto $compra->Modelo - $compra->Cor</div>
	  <div class='info_item_qtds'>Quantidade total <span>$compra->QuantidadeTotal</span></div>
	  <div class='info_item_qtds'>Quantidade comprada <span>$compra->QuantidadeComprada</span></div>
	  <div class='info_item_qtds'>Quantidade restante <span>$quantidade_restante</span></div>";
     