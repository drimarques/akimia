<?php

     require_once '../../src/classes/conexao.class.php';
     require_once '../../src/classes/compra.class.php';
     require_once '../../src/dao/comprasDAO.php';

     require_once '../../src/classes/parametros_calculo.class.php';
     require_once '../../src/dao/parametrosDAO.php';
          
     $dao = new comprasDAO();
     $parametrosDAO = new parametrosDAO();
     
     $ids = $dao->getIdsCompras();
     
     for($i = 0; $i < count($ids); $i++){	  
	  $compra = $dao->getDadosCompra($ids[$i]);     
	  $parametro = $parametrosDAO->getDadosParametro();
	  $compra_calculada = $dao->calculaValoresFechamento($compra, $parametro);     
	  $dao->updValoresCompra($compra_calculada, $ids[$i]);
     }
     
     

     