<?php

     require_once '../../src/classes/conexao.class.php';
     require_once '../../src/classes/compra.class.php';
     require_once '../../src/dao/comprasDAO.php';
     
     require_once '../../src/classes/parametros_calculo.class.php';
     require_once '../../src/dao/parametrosDAO.php';
          
     $dao = new comprasDAO();
     $parametrosDAO = new parametrosDAO();

     $id_item = $_POST['id_item'];
     $iof = $_POST['iof'];

     $dao->upd_IOFCompra($id_item, $iof);
         
     $compra = $dao->getDadosCompra($id_item);     
     $parametro = $parametrosDAO->getDadosParametro();     
     $compra_calculada = $dao->calculaValoresFechamento($compra, $parametro);     
     $dao->updValoresCompra($compra_calculada, $id_item);

     