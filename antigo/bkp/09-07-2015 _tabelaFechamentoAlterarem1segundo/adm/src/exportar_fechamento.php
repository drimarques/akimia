<?php
     date_default_timezone_set('America/Sao_Paulo');
     $data_hora = date("d-m-Y_H-i-s");
     
     require_once '../php_excel/Classes/PHPExcel.php';
     require_once '../../src/classes/conexao.class.php';
     

// Instanciamos a classe
     $objPHPExcel = new PHPExcel();

// Definimos o estilo da fonte
     $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('A3')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('A4')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('A5')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('A6')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('A7')->getFont()->setBold(true);
     
     $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('D2')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('D3')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('D4')->getFont()->setBold(true);
     
     $objPHPExcel->getActiveSheet()->getStyle('A9')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('B9')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('C9')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('D9')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('E9')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('F9')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('G9')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('H9')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('I9')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('J9')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('K9')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('L9')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('M9')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('N9')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('O9')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('P9')->getFont()->setBold(true);
     $objPHPExcel->getActiveSheet()->getStyle('Q9')->getFont()->setBold(true);

// Criamos as colunas
     $objPHPExcel->setActiveSheetIndex(0)
     ->setCellValue('A1', "RESUMO")
     ->setCellValue("A2", "Valor - Dólar (USD)")
     ->setCellValue("A3", "Valor (R$)")
     ->setCellValue('A4', "Venda (R$)")
     ->setCellValue("A5", "Lucro")
     ->setCellValue("A6", "Despesa com Importação")
     ->setCellValue("A7", "Lucro verdadeiro")
     
     ->setCellValue('D1', "PARÂMETROS DE CALCULO")
     ->setCellValue('D2', "Cotação Dolar")
     ->setCellValue("D3", "Imposto USA")
     ->setCellValue("D4", "IOF")
     
     ->setCellValue('A9', "Marca")
     ->setCellValue("B9", "Sexo")
     ->setCellValue("C9", "Tipo")
     ->setCellValue("D9", "Produto")
     ->setCellValue("E9", "Modelo")
     ->setCellValue("F9", "Cor")
     ->setCellValue("G9", "Tamanho")
     ->setCellValue("H9", "Quantidade")
     ->setCellValue("I9", "Custo (U$)")
     ->setCellValue("J9", "IOF")
     ->setCellValue("K9", "Custo (R$)")
     ->setCellValue("L9", "Markup %")
     ->setCellValue("M9", "Venda (R$)")
     ->setCellValue("N9", "Lucro (R$)")
     ->setCellValue("O9", "Custo Total (R$)")
     ->setCellValue("P9", "Venda Total (R$)")
     ->setCellValue("Q9", "Lucro Total (R$)");

// Podemos configurar diferentes larguras paras as colunas como padrão
     $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
     $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
     $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(18);
     $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(18);


     $conexao = new Conexao();
     $conn = $conexao->getConexao();
          
     $parametros = $conn->query("SELECT * FROM parametros_calculo");
     $valores = $parametros->fetch(PDO::FETCH_ASSOC);
     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 2, $valores[cotacaoDolar]);
     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 3, $valores[impostoUSA]);
     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 4, $valores[IOF]);     
          
     $retorno = $conn->query("SELECT * FROM lista_compras ORDER BY marca ASC");
     $contador = 10;
     while ($linhas = $retorno->fetch(PDO::FETCH_ASSOC)) {
	  
	  if($linhas[iof] == "1"){
	       $iof = "Sim";
	  }
	  else{
	       $iof = "Não";
	  }
	  
	  // Também podemos escolher a posição exata aonde o dado será inserido (coluna, linha, dado);	  
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $contador, $linhas[marca]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $contador, $linhas[sexo]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $contador, $linhas[tipo]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $contador, $linhas[produto]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $contador, $linhas[modelo]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $contador, $linhas[cor]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $contador, $linhas[tamanho]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $contador, $linhas[quantidade_total]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $contador, $linhas[valor_dolar]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $contador, $iof);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $contador, $linhas[valor_rs]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $contador, $linhas[markup]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $contador, $linhas[venda]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $contador, $linhas[lucro]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $contador, $linhas[custo_total]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, $contador, $linhas[venda_total]);
	  $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, $contador, $linhas[lucro_total]);
	  $contador++;
     }
     

     unset($conn);                         

// Podemos renomear o nome das planilha atual, lembrando que um único arquivo pode ter várias planilhas
     $objPHPExcel->getActiveSheet()->setTitle('Lista de Compras - Akimia');

// Cabeçalho do arquivo para ele baixar
     header('Content-Type: application/vnd.ms-excel');
     header('Content-Disposition: attachment;filename="ExportacaoFechamento_Data'.$data_hora.'.xls"');
     header('Cache-Control: max-age=0');
// Se for o IE9, isso talvez seja necessário
     header('Cache-Control: max-age=1');

// Acessamos o 'Writer' para poder salvar o arquivo
     $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

// Salva diretamente no output, poderíamos mudar arqui para um nome de arquivo em um diretório ,caso não quisessemos jogar na tela
     $objWriter->save('php://output');

     exit;
     
     
     