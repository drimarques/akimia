<?php
     if (isset($_POST['btnEnviarExcel'])) {
	  $dao = new comprasDAO();
	  $compra = new Compra();
	  	  
	  $data = new Spreadsheet_Excel_Reader();
	  $data->setOutputEncoding('UTF-8');
	  	 
	  //Se o arquivo não for vazio e for um arquivo excel
	  if (!empty($_FILES['arquivo_excel']) && ($_FILES['arquivo_excel']['type'] == "application/vnd.ms-excel" || $_FILES['arquivo_excel']['type'] == "application/octet-stream")) {
	       $data->read($_FILES['arquivo_excel']['tmp_name']);
	       	       
	       $qtdLinhas = $data->sheets[0]['numRows'];
	       
	       //Limpa lista
	       $dao->limpaListaCompras();	       
//	       //Percorrendo os valores a partir da 10ak linha
	       for ($i = 10; $i <= $qtdLinhas; $i++) {		    
		    $compra = new Compra();		    		    
		    $compra->setMarca(utf8_encode($data->sheets[0]['cells'][$i][1]));
		    $compra->setSexo(utf8_encode($data->sheets[0]['cells'][$i][2]));
		    $compra->setTipo(utf8_encode($data->sheets[0]['cells'][$i][3]));		    
		    $compra->setProduto(utf8_encode($data->sheets[0]['cells'][$i][4]));
		    $compra->setModelo(utf8_encode($data->sheets[0]['cells'][$i][5]));
		    $compra->setCor(utf8_encode($data->sheets[0]['cells'][$i][6]));
		    $compra->setTamanho($data->sheets[0]['cells'][$i][7]);
		    $compra->setQuantidadeTotal((int)$data->sheets[0]['cells'][$i][8]);
		    		   
		    $dao->cadastraListaCompra($compra);
	       }
	       	
	       unset($_POST['btnEnviarExcel']);
	       ?>
	       <script>
		    $("#sucesso_planilha").fadeIn(function (){
			 document.form_arquivo_excel.reset();			
//			 window.open('lista-compras.php');
//			 location.href="index.php";
		    });
	       </script>
	       <?php
	       
	  }
//	  //Não é .xls
	  else{
	       ?>
	       <script>
		    $("#formato_invalido").fadeIn();
	       </script>
	       <?php
	  }	  	  
     }

