function getTabelaResumo() {
     $.ajax({
	  url: "fechamento/get-tabela-resumo.php",
	  success: function (retorno) {
	       $("#tabela-resumo").html(retorno);
	  }
     });
}

function atualizaTabelaPorIdItem(id_item) {
     idValorRS = id_item + "valorbr";
     $.ajax({
	  url: "getters-fechamento/get-valorRS.php",
	  data: {id_item: id_item},
	  type: 'POST',
	  context: $("#" + idValorRS),
	  success: function (retorno) {
	       $("#" + idValorRS).html(retorno);
	  }
     });

     idMarkup = id_item + "markup";
     $.ajax({
	  url: "getters-fechamento/get-markup.php",
	  data: {id_item: id_item},
	  type: 'POST',
	  context: $("#" + idMarkup),
	  success: function (retorno) {
	       $("#" + idMarkup).html(retorno);
	  }
     });

     idLucro = id_item + "lucro";
     $.ajax({
	  url: "getters-fechamento/get-lucro.php",
	  data: {id_item: id_item},
	  type: 'POST',
	  context: $("#" + idLucro),
	  success: function (retorno) {
	       $("#" + idLucro).html(retorno);
	  }
     });

     idCustoTotal = id_item + "custo_total";
     $.ajax({
	  url: "getters-fechamento/get-custoTotal.php",
	  data: {id_item: id_item},
	  type: 'POST',
	  context: $("#" + idCustoTotal),
	  success: function (retorno) {
	       $("#" + idCustoTotal).html(retorno);
	  }
     });

     idVendaTotal = id_item + "venda_total";
     $.ajax({
	  url: "getters-fechamento/get-vendaTotal.php",
	  data: {id_item: id_item},
	  type: 'POST',
	  context: $("#" + idVendaTotal),
	  success: function (retorno) {
	       $("#" + idVendaTotal).html(retorno);
	  }
     });

     idLucroTotal = id_item + "lucro_total";
     $.ajax({
	  url: "getters-fechamento/get-lucroTotal.php",
	  data: {id_item: id_item},
	  type: 'POST',
	  context: $("#" + idLucroTotal),
	  success: function (retorno) {
	       $("#" + idLucroTotal).html(retorno);
	  }
     });

     getTabelaResumo();

}

function atualizaTabelaGeral() {
     $("#atualizando_tabela").fadeIn("fast");
     $.ajax({
	  url: "fechamento/upd-tabela-geral.php",
	  type: 'POST',
	  success: function (retorno) {
	       $("#atualizando_tabela").fadeOut("fast").delay(1500, function () {
		    location.href = "fechamento.php";
	       });
	  }
     });
}

function upd_valorDolarProduto(elemento) {
     var id_item = elemento.id;
     var valor_dolar = elemento.value;

     $.ajax({
	  url: "fechamento/grava-valor-dolar-compra.php",
	  data: {valor: valor_dolar, id_item: id_item},
	  type: 'POST',
	  success: function (retorno) {
	       atualizaTabelaPorIdItem(id_item);
//	       elemento.style.border = "#33cc00 solid 1px";
	       var td = $(elemento);
	       $(td.parent()).css({
		    
	       });
	  }
     });

}

function upd_IOFProduto(elemento) {
     var iof = elemento.value;
     var id_item = elemento.id;

     $.ajax({
	  url: "fechamento/grava-iof-produto.php",
	  data: {iof: iof, id_item: id_item},
	  type: 'POST',
	  success: function (retorno) {
	       atualizaTabelaPorIdItem(id_item);
//	       elemento.style.border = "#33cc00 solid 1px";
	       var td = $(elemento);
	       $(td.parent()).css({
		    
	       });
	  }
     });
}

function upd_valorVendaProduto(elemento) {
     var id_item = elemento.id;
     var valor_venda = elemento.value;

     $.ajax({
	  url: "fechamento/grava-valor-venda-compra.php",
	  data: {valor: valor_venda, id_item: id_item},
	  type: 'POST',
	  success: function (retorno) {
	       atualizaTabelaPorIdItem(id_item);
//	       elemento.style.border = "#33cc00 solid 1px";
	       var td = $(elemento);
	       $(td.parent()).css({
		    
	       });
	  }
     });

}

$(document).ready(function () {
     $(".dolar").maskMoney({showSymbol: true, symbol: "$ ", decimal: ".", thousands: ","});
     $(".real").maskMoney({showSymbol: true, symbol: "R$ ", decimal: ",", thousands: "."});
     $(".porcentagem").maskMoney({showSymbol: true, symbol: "% ", decimal: ",", thousands: "."});

     $("#cotacao-dolar").focusout(function () {
	  var cotacao_dolar = $("#cotacao-dolar").val();
	  $.ajax({
	       url: "fechamento/grava-cotacao-dolar.php",
	       data: {valor: cotacao_dolar},
	       type: 'POST',
	       success: function (retorno) {
		    var td = $("#cotacao-dolar");
		    $(td.parent()).css({
			 
		    });

		    atualizaTabelaGeral();
	       }
	  });
     });

     $("#imposto-usa").focusout(function () {
	  var imposto_usa = $("#imposto-usa").val();
	  $.ajax({
	       url: "fechamento/grava-imposto-usa.php",
	       data: {imposto_usa: imposto_usa},
	       type: 'POST',
//	       context: $('#teste'),
	       success: function (retorno) { //success: function () {	
		    var td = $("#imposto-usa");
		    $(td.parent()).css({
			 
		    });

		    atualizaTabelaGeral();
	       }
	  });
     });

     $("#IOF").focusout(function () {
	  var iof = $("#IOF").val();
	  $.ajax({
	       url: "fechamento/grava-iof.php",
	       data: {IOF: iof},
	       type: 'POST',
//	       context: $('#teste'),
	       success: function (retorno) { //success: function () {
		    var td = $("#IOF");
		    $(td.parent()).css({
			 
		    });
//		    $("#IOF").css({"outline": "4px solid #33cc00"});
		    atualizaTabelaGeral();
	       }
	  });
     });

     $("#despesa_imp").focusout(function () {
	  var despesa_importacao = $("#despesa_imp").val();
	  console.log(despesa_importacao);
	  $.ajax({
	       url: "fechamento/grava-despesa-importacao.php",
	       data: {despesa_importacao: despesa_importacao},
	       type: 'POST',
//	       context: $('#teste'),
	       success: function (retorno) {
		    var td = $("#despesa_imp");
		    $(td.parent()).css({
			 "outline": "4px solid #33cc00"
		    });
//		    $("#IOF").css({"outline": "4px solid #33cc00"});
		    atualizaTabelaGeral();
	       }
	  });
     });

});