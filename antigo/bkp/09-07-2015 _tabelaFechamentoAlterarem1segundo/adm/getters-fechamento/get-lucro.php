<?php

     require_once '../../src/classes/conexao.class.php';
     require_once '../../src/classes/compra.class.php';
     require_once '../../src/dao/comprasDAO.php';
     
     $id_item = $_POST['id_item'];
     $dao = new comprasDAO();
     $compra = $dao->getDadosCompra($id_item);
          
     echo "R$ ".number_format($compra->getLucro(), 2, ',', '.');