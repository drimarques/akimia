function carregandoLogin(texto, estado) {
	$("#btnLogin").val(texto);
	$("#btnLogin").attr("disabled", estado);
	if (estado) {
		$("#btnLogin").css({
			"opacity" : "0.7"
		});
	} else {
		$("#btnLogin").css({
			"opacity" : "1"
		});
	}
}

function criaUsuario() {
	var usuario = {
		"email" : $("#emailLogin").val(),
		"senha" : $("#senhaLogin").val(),
		"erroLogin" : false,
		"textoErro" : null,
		tremeFormulario : function(margin) {
			$("section.login").animate({
				"marginLeft" : margin,
			}, 100, function() {
			});
		},
		validaEmail : function() {
			var validador = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2}/;
			if (validador.exec(this.email)) {
				return true;
			} else {
				return false;
			}
		},
		notificacao : function() {
			$("#notificacaoLogin").text(this.textoErro);
			$("#notificacaoLogin").fadeIn();			
		}
	};

	return usuario;

}

$(function() {
	$("#btnLogin").click(function() {
		carregandoLogin("Aguarde...", true);
		var usuario = criaUsuario();

		if (usuario.email === "" || usuario.senha === "") {
			usuario.erroLogin = true;
			usuario.textoErro = "Preencha todos os campos."
		}

		if (usuario.erroLogin === false) {
			if (!usuario.validaEmail()) {
				usuario.erroLogin = true;
				usuario.textoErro = "Formato de e-mail inválido."
			}
		}
		
		if (usuario.erroLogin === true) {			
			usuario.tremeFormulario("-=12");
			usuario.tremeFormulario("+=12");
			usuario.tremeFormulario("-=12");
			usuario.tremeFormulario("+=12");		
			carregandoLogin("Entrar", false);
			usuario.notificacao();
		}
		else{
			$.ajax({
				url : "src/autenticacao/valida-login.php",
				data : {
					email : usuario.email,
					senha : usuario.senha
				},
				type : 'POST',
				context : $(".login"),
				success : function(retorno) {					
					console.log(retorno);					
					if(retorno === "erro"){
						usuario.textoErro = "Usuário ou senha incorretos."
						usuario.tremeFormulario("-=12");
						usuario.tremeFormulario("+=12");
						usuario.tremeFormulario("-=12");
						usuario.tremeFormulario("+=12");		
						carregandoLogin("Entrar", false);
						usuario.notificacao();
					}
					else if(retorno === "logado com sucesso"){
						location.href = "dashboard/index.php";
					}
				}
			});
		}					
	});
	
	
	
	
})