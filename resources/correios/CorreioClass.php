<?php

class Tracking {

    public $track;
    public $table;
    public $erro = false;
    public $erroMsg = false;    

    /**
    * Construtor
    *
    * @param string $track Código da encomenda
    * return void
    */
    public function setTrack($track=false){

        if ( strlen($track) !== 13) {
            $this->erro = true;
            $this->erroMsg = 'Código de encomenda Inválido!';
        }
        $this->track = $track;

    }

    public function trackHTML()
    {
        if( $this->erro === true ):
            return $this->erro;
        else:

            $url = 'http://websro.correios.com.br/sro_bin/txect01$.QueryList?P_LINGUA=001&P_TIPO=001&P_COD_UNI=' . $this->track;
            $html = utf8_encode( file_get_contents( $url ) );

            if (strpos($html, "src=../correios/") !==false) {
                $html  = str_replace('src=../correios/',
                    'src=http://websro.correios.com.br/correios/', $html );
            }

            preg_match( '/<table  border cellpadding=1 hspace=10>.*<\/TABLE>/s', $html, $table );

            // hash para monitoramento de alteração de status
            $this->hash = md5($html);           

            return ( count( $table ) == 1 ) ? $table[0] : "Objeto não encontrado";

        endif;
    }


    public function trackObject(){
        $html = utf8_encode(file_get_contents('http://websro.correios.com.br/sro_bin/txect01$.QueryList?P_LINGUA=001&P_TIPO=001&P_COD_UNI='.$this->track));

        // Verifica se o objeto ainda não foi postado, caso seja o caso, retorna erro e mensagem
        if (strstr($html, '<table') === false){
            $this->erro = true;
            $this->erroMsg = 'Objeto ainda não foi adicionado no sistema';
            return;
        }

        // hash para monitoramento de alteração de status
        $this->hash = md5($html);

        // Limpa o codigo html
        $html = preg_replace("@\r|\t|\n| +@", ' ', $html);
        $html = str_replace('</tr>', "</tr>\n", $html);

        // Pega as linhas com o rastreamento
        if (preg_match_all('@<tr>(.*)</tr>@', $html, $mat,PREG_SET_ORDER)){
            $track = array();
            $mat = array_reverse($mat);
            $temp = null;

            // Formata as linhas e gera um vetor
            foreach($mat as $item){
                if (preg_match("@<td rowspan=[12]>(.*)</td><td>(.*)</td><td><font color=\"[0-9A-F]{6}\">(.*)</font></td>@", $item[0], $d)){
                    // Cria uma linha de track

                    $tmp = array(
                        'date' => $d[1],
                        'DateSql' => preg_replace('@([0-9]{2})/([0-9]{2})/([0-9]{4}) ([0-9]{2}):([0-9]{2})@', '$3-$2-$1 $4:$5:00',$d[1] ),
                        'local' => $d[2],
                        'action' => strtolower($d[3]),
                        'details' => ''
                    );

                    // Se tiver um encaminhamento armazenado
                    if ($temp){
                        $tmp['details'] = $temp;
                        $temp = null;
                    }

                    // Adiciona o item na lista de rastreamento
                    $track[] = (object)$tmp;
                }else if (preg_match("@<td colspan=2>(.*)</td>@", $item[0], $d)){
                    // Se for um encaminhamento, armazena para o proximo item
                    $temp = $d[1];
                }
                $this->status = $tmp['action'];
            }
            $this->track = $track;
            return $this->track;
        }

        // Caso retorne um html desconhecido ou falhe, retorna erro de comunicação
        $this->erro = true;
        $this->erroMsg = 'Falha de Comunicação com os correios';

    }   

}