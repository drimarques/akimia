<?php
header('Content-Type: text/html; charset=utf-8');

require_once '../../src/classes/inclui-classes.php';
require_once '../../src/daos/inclui-daos.php';

$email = $_POST["email"];
$senha = $_POST["senha"];

$usuario = new Usuario();
$usuario->setEmail($email);
$usuario->setSenha($senha);

$dao = new UsuarioDAO();
if($dao->validaLogin($usuario)){
	echo "logado com sucesso";
} else {
	echo "usuário ou senha incorretos";
}