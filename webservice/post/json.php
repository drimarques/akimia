<?php
require_once '../../src/classes/inclui-classes.php';
require_once '../../src/daos/inclui-daos.php';

header('Content-type: application/json; charset=utf-8');

$email = $_POST["email"];
$senha = $_POST["senha"];

$usuario = new Usuario();
$usuario->setEmail($email);
$usuario->setSenha($senha);

$dao = new UsuarioDAO();
if($dao->validaLogin($usuario)){
	echo json_encode("logado com sucesso");
} else {
	echo json_encode("usuário ou senha incorretos");
}
 