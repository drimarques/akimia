<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Login - Akimia</title>
<link rel="shortcut icon" href="img/icone_lowcost.png">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="shortcut icon" href="img/logo-akimia-small.png" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,700,700italic,800italic,800' rel='stylesheet' type='text/css'>
<link href="dashboard/css/padroes.css" rel="stylesheet" type="text/css" />
<link href="css/login.css" rel="stylesheet" type="text/css" />
</head>
<body>

	<section class="login">
		<img alt="Akimia Produtos Importados" src="img/logo-akimia-small.png">
		<form method="post" name="form-login" autocomplete="on">
			<input type="email" name="emailLogin" id="emailLogin" placeholder="E-mail" autofocus="autofocus" class="campo-form">
			<input type="password" name="senhaLogin" id="senhaLogin" placeholder="Senha" class="campo-form">
			<div class="notificacao" id="notificacaoLogin">E-mail ou senha incorretos.</div>
			<input type="button" name="btnLogin" id="btnLogin" value="Entrar" class="botao hover">			
		</form>
	</section>

	<section class="rodape">
		<div id="copy">
			Akimia Produtos Importados &#169;2012 - <?php echo date('Y') ?>
		</div>
		<div id="developer">
			Desenvolvido e mantido por <a target="_blank" href="https://www.linkedin.com/pub/adriano-marques/b6/522/4b">Adriano Marques.</a>
		</div>
	</section>

	<script src="resources/jquery/jquery-2.1.4.min.js"></script>
	<script src="resources/jquery/jquery.bpopup.min.js"></script>	
	<script src="js/login.js" type="text/javascript"></script>
</body>
</html>
